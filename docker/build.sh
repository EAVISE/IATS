#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#docker build -t nvcr.io/eavise/pytorch:cuda9 docker/pytorch/
PROJ_NAME="IATS"

if [[ -d "$PROJ_NAME" ]]; then
  read -p "Project already exists! Do you want to update? (y/N)" -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "Updating..."
    cd IATS
    git pull
    cd ..
  else
    echo "Continuing without updating..."
  fi
else
  echo "Project doesn't exist yet! Cloning..."
  git clone https://gitlab.com/EAVISE/IATS
fi

docker build -f Dockerfile -t kve/iats:root .


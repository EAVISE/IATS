#!/bin/bash

nvidia-docker run --rm -ti \
                    -v $(pwd)/Datasets/oxya/:/root/Datasets/oxya:ro \
                    -v $(pwd)/weights/:/root/weights \
                    -v $(pwd)/output/:/root/output kve/iats:root \
                    /bin/bash

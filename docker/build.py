import os
import subprocess

from iats import settings

if __name__ == '__main__':
    git_root = settings.ROOT_DIR.parent
    os.chdir(git_root)
    docker_path = "docker/Dockerfile"
    print(git_root / docker_path)
    subprocess.run(['docker', 'build', '-f', docker_path, '-t', settings.DOCKER_NAME, '.'])

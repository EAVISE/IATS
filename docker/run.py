import os
import subprocess
import pathlib as pl

from iats import settings
from iats.utils.config import load_config

if __name__ == '__main__':
    git_root = settings.ROOT_DIR.parent
    config = load_config(settings.CONFIG_PATH)
    os.chdir(git_root)
    data_path = pl.Path(config.dataset.path).parent
    data_path_docker = pl.Path('root', data_path.relative_to(data_path.parent.parent))
    command = "/bin/bash"
    subprocess.run(['nvidia-docker', 'run', '--rm', '-ti',
                    '-v', f"{data_path}:/{data_path_docker}:ro",
                    '-v', f"{os.getcwd()}/weights/:/root/weights",
                    '-v', f"{os.getcwd()}/output/:/root/output",
                    settings.DOCKER_NAME,
                    command])

"""
This script attempts to map the information contained in the audit file (i.e., the file containing the list of
audits, to what site each audit pertains and when the audit took place) onto the answered questionnaires.
"""
import math
import os
from datetime import timedelta

from termcolor import cprint

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.tenforce_config import TenForceConfig
import pandas as pd
from collections import Counter, defaultdict


def main(audit_file=None):
    if audit_file is None:
        audit_file = os.path.join(TenForceConfig.DATA_DIR, TenForceConfig.AUDIT_FILE)

    da = TFDataAnalysis()
    df = da.data
    print(f"Number of unique sites in data: {len(df['Sites : Nr'].unique())}")

    audit_df = pd.read_excel(audit_file)
    # Clean-up step 1: only keep rows with no missing values
    # --> This is actually a bad step, as this significantly reduces the number of audits that can be mapped
    # onto the data
    # audit_nonull = audit_df[audit_df.notnull().all(axis=1)]

    # audit_sites = audit_df["Sites : Nr"]
    audit_cnts = Counter()
    audit_scores = defaultdict(list)
    audit_ids = defaultdict(list)

    for i, r in audit_df.iterrows():
        print(f"\rAt line {i}", end='')
        nr = r['Sites : Nr']
        # title = r['Sites : Title']
        da_title = r['Dynamic Audits Title']

        if not df[(df['Sites : Nr'] == nr) & (df['Dynamic Audits : Title'] == da_title)].empty:
            audit_cnts[nr] += 1
            score = r['Dynamic Audits score (%)']
            if math.isnan(score):
                score = -1
            audit_scores[nr].append(int(score))
            audit_ids[nr].append(r['Dynamic Audits Nr'])
    print()

    c_qn = Counter()
    c_tc = Counter()
    data_scores = defaultdict(list)
    b_color = False
    print_for_sites = {3417966}  # A list of sites ids to print debug info for

    # Parse data on a "per site" basis
    # The reason is that if you parse on a "per row" basis, in the assumption that rows pertaining to the
    # same contractor/site follow each other, well... you'd be wrong. Once in a while (rarely though), lines
    # from different sites get mixed up, like e.g. for sites 3082722 and 3301346, throwing sand into the engine.
    line_diff = 25
    for site in df["Sites : Nr"].unique():
        df_site = df[df["Sites : Nr"] == site]
        prev_audit = ""
        prev_tc = -1
        prev_i = -1
        seq = set()
        seq_tc = set()
        penalty = 0
#            nok_sum = sum(df[(df['Sites : Nr'] == k) & (df['Compliancy'] == 'NOK')]['Weight'])
        b_first = True
        for i, r in df_site.iterrows():
            audit = r['Dynamic Audits : Title']
            q_id = r['Question_Id']
            t_id = r['Topic_Id']

            b_new_seq = False
            if audit != prev_audit or (i-prev_i) > line_diff:
                if site in print_for_sites:
                    print(f"New audit/site: new audit? ({audit != prev_audit})\t"
                          f"row skip? {i-prev_i > line_diff} --> {i-prev_i}")
                seq = set()
                seq_tc = set()
                c_qn[site] += 1
                c_tc[site] += 1
                if b_first:
                    b_first = False
                else:
                    data_scores[site].append(100-penalty)
                penalty = 0
                b_color = not b_color
                b_new_seq = True

            if not b_new_seq and q_id in seq:
                c_qn[site] += 1
                seq = set()
            seq.add(q_id)

            if not b_new_seq and prev_tc != t_id and t_id in seq_tc:
                if site in print_for_sites:
                    print(f"New audit: recurrence of previously seen topic")
                c_tc[site] += 1
                data_scores[site].append(100-penalty)
                penalty = 0
                seq_tc = set()
                b_color = not b_color
            seq_tc.add(t_id)
            prev_tc = t_id

            if site in print_for_sites:
                if b_color:
                    print(f"Row: {i}\t{site}: Q_Id: {q_id}\tT_Id: {t_id}\tNb. audits: {c_tc[site]}")
                else:
                    cprint(f"Row: {i}\t{site}: Q_Id: {q_id}\tT_Id: {t_id}\tNb. audits: {c_tc[site]}", color='cyan')

            if r['Compliancy'] == 'NOK' or r['Compliancy'] == 'SOS':
                penalty += r['Weight']
            prev_audit = audit
            prev_i = i
        # Don't forget to append last site's score!
        data_scores[site].append(100 - penalty)

    # The following piece of code can be used to check the amount of audits as determined by parsing the
    # questions, rather than the topics.
    # ############################################################
    # cnt = 0
    # ok, nok = 0, 0
    # for k, v in c_qn.most_common():
    #     print(f"{k}\t{v}")
    #     cnt += v
    #     if audit_cnts[k] == v:
    #         ok += 1
    #         print("Hurray!")
    #     else:
    #         nok += 1
    #         cprint(f"O-oh! Expected {audit_cnts[k]}", color='red')
    #         for i, r in da.data[da.data["Sites : Nr"] == k].iterrows():
    #             cprint(f"{r['Question_Id']}\t{r['Topic_Id']}", color='cyan')
    # print(cnt)
    # print(f"Ok: {ok}\tNok: {nok}")
    # ############################################################

    cnt = 0
    ok, nok = 0, 0
    col_m = audit_df.columns[12]
    col_p = audit_df.columns[15]
    tot_diff = 0
    data_audits = defaultdict(list)
    for k, v in c_tc.most_common():
        print(f"{k}\t{v}")
        cnt += v
        if audit_cnts[k] == v:
            ok += 1
            data_audits[k] = audit_ids[k]
            print("Hurray!")
        else:
            # Check audit doesn't seem to be duplicated in audit excel
            sub_df = audit_df[audit_df["Sites : Nr"] == k]
            # 1. Check audits don't contain rows where closed date differs between row M and P
            subsub_df = sub_df[sub_df.apply(lambda x: x[col_m].date() == x[col_p].date(), axis=1)]
            if subsub_df.shape[0] == v:
                ok += 1
                cprint("Alt Hurray!", color='red')
                data_audits[k] = subsub_df['Dynamic Audits Nr'].to_list()
                continue
            # 2. Check audits' close time doesn't differ by less than 5 minutes
            first_time = sub_df.iloc[0][col_m]
            tdelta = timedelta(hours=1)
            cluster_matches = [sub_df.iloc[0]['Dynamic Audits Nr']]
            for i, r in sub_df.iterrows():
                if r[col_m] - first_time > tdelta:
                    first_time = r[col_m]
                    cluster_matches.append(r['Dynamic Audits Nr'])
            if len(cluster_matches) == v:
                ok += 1
                cprint("Alt Hurray through time clustering!", color='yellow')
                data_audits[k] = cluster_matches
                continue

            cprint(f"O-oh! Expected {audit_cnts[k]}", color='red')
            # See if score arrays can be aligned
            mapping = resolve(audit_scores[k], data_scores[k])
            cprint(f"Can be resolved? {mapping != []}\n\tAudits: {audit_scores[k]}\n\tData  : {data_scores[k]}")
            if mapping:
                ok += 1
                cprint("Alt Hurray!", color='magenta')
                data_audits[k] = [audit_ids[k][j] for j in mapping]
                continue

            nok += 1
            diff = v - audit_cnts[k]
            tot_diff += diff if diff > 0 else -diff

            for i, r in da.data[da.data["Sites : Nr"] == k].iterrows():
                cprint(f"{r['Question_Id']}\t{r['Topic_Id']}\t{r['Compliancy']}", color='cyan')
    print(cnt)
    print(f"Ok: {ok}\tNok: {nok}")
    print(f"Total difference in audits: {tot_diff}")

    # At this point, we have a list of audit ids for all sites. Well ok, for some sites we might
    # not have a match, but ok, we will just ignore these sites.
    # We will now fill in the appropriate audit dates into a separate column in the data DataFrame.
    c_tc = Counter()

    # First, add extra columns to data df to contain audit id and date
    audit_ids_col = df.shape[0]*[-1]
    audit_dates_col = df.shape[0]*[pd.NaT]
    # Then, loop over sites, and fill in audit times
    line_diff = 20
    at_site = 0
    for site in df["Sites : Nr"].unique():
        at_site += 1
        print(f"\rAt site {at_site}", end='', flush=True)

        df_site = df[df["Sites : Nr"] == site]
        prev_audit = ""
        prev_tc = -1
        prev_i = -1

        audit_id = None
        audit_time = None
        seq_tc = set()

        for i, r in df_site.iterrows():
            audit = r['Dynamic Audits : Title']
            t_id = r['Topic_Id']

            b_new_seq = False
            if audit != prev_audit or (i - prev_i) > line_diff:
                c_tc[site] += 1
                seq_tc = set()
                b_new_seq = True

            if not b_new_seq and prev_tc != t_id and t_id in seq_tc:
                c_tc[site] += 1
                seq_tc = set()
                b_new_seq = True

            seq_tc.add(t_id)
            prev_tc = t_id

            if b_new_seq:
                if len(data_audits[site]) >= c_tc[site]:
                    audit_id = data_audits[site][c_tc[site]-1]
                    sub_df = audit_df[audit_df['Dynamic Audits Nr'] == audit_id]
                    if sub_df.shape[0] != 1:
                        cprint(f"\nSomething wrong with audit match for site {site}", color='red')
                        cprint(f'\n\tAudit nr: {audit_id}')
                        audit_time = None
                    else:
                        audit_time = sub_df.iloc[0]['Dynamic Audits closed on'].date()
                else:
                    audit_time = None

            if audit_time is not None:
                audit_ids_col[i] = audit_id
                audit_dates_col[i] = audit_time

            prev_audit = audit
            prev_i = i
    print()
    df['Audit_Id'] = audit_ids_col
    df['Audit_Date'] = audit_dates_col

    # Save the resulting DataFrame to disk.
    df.to_pickle(os.path.join(TenForceConfig.DATA_DIR, "tenforce_data_with_audits.pkl"))


def resolve(audit: list, data: list):
    """
    Check if all values from the shortest list can be mapped in sequence to values in the longer list.

    Args:
        audit:
        data:

    Returns:

    """
    if len(data) > len(audit):
        return []

    len_d = len(data)
    s_idx = 0
    matches = []
    for i, e in enumerate(audit):
        if e == data[s_idx]:
            matches.append(i)
            s_idx += 1
            if s_idx == len_d:
                return matches

    return []


if __name__ == '__main__':
    main()

import os
from collections import Counter

import pickle
import numpy as np
import pandas as pd

from iats.tenforce.data.question_mapper import QuestionMapper
from iats.tenforce.tenforce_config import TenForceConfig


class TFDataAnalysis:
    def __init__(self):
        self.data = pickle.load(open(os.path.join(TenForceConfig.DATA_DIR, "tenforce_data_with_audits.pkl"), 'rb'))
        self.qm = QuestionMapper()

    def analyse_critical_questions(self, df: pd.DataFrame, b_verbose=False):
        """
        Check which questions appear in case the deficiency of a site equals 1 (i.e., all answered
        questions were NOK).

        Args:
            df: the dataframe to analyze
            b_verbose: print information to console

        Returns:
            (cnt1, cnt2), with cnt1 a Counter object with the critical question IDs and their respective counts for
            the analysis performed on site level, and analogous for cnt2 on audit level.

        """
        uq_sites = df['Sites : Nr'].unique()
        nb_audits = len(df['Audit_Id'].unique())
        audit_critical_qstns = Counter()  # Which questions answered how often when deficiency = 1?
        critical_qstns_per_audit = []  # How many questions answered when deficiency = 1?
        site_critical_qstns = Counter()  # Which questions answered how often when deficiency = 1?
        critical_qstns_per_site = []  # How many questions answered when deficiency = 1?

        for site in uq_sites:
            df_s = df[df['Sites : Nr'] == site]
            for audit in df_s["Audit_Id"].unique():
                if audit == -1:
                    continue
                df_s_audit = df_s[df_s["Audit_Id"] == audit]
                audit_defi = self.get_deficiency_for_df(df_s_audit, b_weighted=False)

                if audit_defi == 1:
                    nok, ok, sos, _, _, _ = self.get_stats_for_df(df_s_audit)
                    # Answered questions were all answered NOK, but audit might also have NA questions,
                    # so filter dataframe
                    audit_critical_qstns.update(df_s_audit[df_s_audit["Compliancy"] == "NOK"]["Question_Id"].values)
                    critical_qstns_per_audit.append(nok)

            # planned_time = da_c_s['Sites : Due date'].iloc[0] - da_c_s['Sites : Planned start'].iloc[0]
            # plnd_time_per_site.append(planned_time.days + 1)
            # true_time = da_c_s['Sites : Actual due date'].iloc[0] - da_c_s['Sites : Actual start'].iloc[0]
            # true_time_per_site.append(true_time.days + 1)
            deficiency = self.get_deficiency_for_df(df_s)

            # Is this a "deficiency=1" site? If so, check which question(s) was/were answered.
            if deficiency == 1:
                nok, ok, sos, _, _, _ = self.get_stats_for_df(df_s)
                # Answered questions were all answered NOK, but site might also have NA questions, so filter dataframe
                site_critical_qstns.update(df_s[df_s["Compliancy"] == "NOK"]["Question_Id"].values)
                critical_qstns_per_site.append(nok)

        if b_verbose:
            print("At site level:")
            print("Deficiency == 1 questions:")
            for q, cnts in site_critical_qstns.most_common():
                # If text of question exceeds 50 characters, truncate
                if len(self.qm.get_nl_question(q)) > 50:
                    print("\t{:-3d}: {:3d} :: {}...".format(cnts, q, self.qm.get_nl_question(q)[:50]))
                else:
                    print("\t{:-3d}: {:3d} :: {}".format(cnts, q, self.qm.get_nl_question(q)))
            print("Number of critical sites: {}/{} [{:4.2f}%]".format(len(critical_qstns_per_site), len(uq_sites),
                                                                      100*len(critical_qstns_per_site)/len(uq_sites)
                                                                      if len(uq_sites) > 0 else 0))
            if critical_qstns_per_site:
                print("Average nb. of qstns answered when def == 1: {:3.1f}".format(np.average(critical_qstns_per_site)))
            print("============================================================")
            print("At audit level:")
            print("Deficiency == 1 questions:")
            for q, cnts in audit_critical_qstns.most_common():
                # If text of question exceeds 50 characters, truncate
                if len(self.qm.get_nl_question(q)) > 50:
                    print("\t{:-3d}: {:3d} :: {}...".format(cnts, q, self.qm.get_nl_question(q)[:50]))
                else:
                    print("\t{:-3d}: {:3d} :: {}".format(cnts, q, self.qm.get_nl_question(q)))
            print("Number of critical audits: {}/{} [{:4.2f}%]".format(len(critical_qstns_per_audit), nb_audits,
                                                                       100*len(critical_qstns_per_audit)/nb_audits
                                                                       if nb_audits > 0 else 0))
            if critical_qstns_per_site:
                print("Average nb. of qstns answered when def == 1: {:3.1f}".format(np.average(critical_qstns_per_audit)))

        return site_critical_qstns, audit_critical_qstns

    @classmethod
    def get_stats_for_df(cls, df, b_add_na=False, b_use_days_weight=False):
        """

        Args:
            df: the DataFrame from which to extract statistics
            b_add_na: also add NA-count stats; False by default
            b_use_days_weight:

        Returns:

        """
        # Get NOK stats
        nb_nok = sum(df[(df['Compliancy'] == "NOK")]['Days Weight']) if b_use_days_weight else\
            len(df[(df['Compliancy'] == "NOK")])
        sum_nok = sum(df[(df['Compliancy'] == "NOK")].Weight)
        # Get OK stats
        nb_ok = sum(df[(df['Compliancy'] == "OK")]['Days Weight']) if b_use_days_weight else\
            len(df[(df['Compliancy'] == "OK")])
        sum_ok = sum(df[(df['Compliancy'] == "OK")].Weight)
        # Get SOS stats
        nb_sos = sum(df[(df['Compliancy'] == "SOS")]['Days Weight']) if b_use_days_weight else\
            len(df[(df['Compliancy'] == "SOS")])
        sum_sos = sum(df[(df['Compliancy'] == "SOS")].Weight)
        if b_add_na:
            # Get NA stats
            nb_na = sum(df[(df['Compliancy'].isnull())]['Days Weight']) if b_use_days_weight else\
                len(df[df['Compliancy'].isnull()])
            sum_na = sum(df[df['Compliancy'].isnull()].Weight)
        return (nb_nok, nb_ok, nb_sos, nb_na, sum_nok, sum_ok, sum_sos, sum_na) if b_add_na else\
            (nb_nok, nb_ok, nb_sos, sum_nok, sum_ok, sum_sos)

    @classmethod
    def get_deficiency_for_df(cls, df, b_weighted=True):
        """
        Get deficiency for provided dataframe

        Args:
            df: DataFrame to process
            b_weighted: if True, use question scores to weigh result, else each question has same weight (of 1)

        Returns:

        """
        nb_nok, nb_ok, nb_sos, sum_nok, sum_ok, sum_sos = cls.get_stats_for_df(df)
        if not b_weighted:
            total = nb_nok + nb_ok + nb_sos
            return nb_nok/total if total > 0 else 0
        else:
            total = sum_nok + sum_ok + sum_sos
            return sum_nok/total if total > 0 else 0

    @classmethod
    def sort_chronologically(cls, df: pd.DataFrame):
        """
        Sort DataFrame according to planned start date of works

        Args:
            df: the DataFrame to sort

        Returns: sorted copy of provided DataFrame

        """
        return df.sort_values(by=['Sites : Planned start', 'Sites : Nr'])

"""
Class holding statistics for "Critical Questions".
-What questions are most critical?
-What are the odds that if a critical question is NOK, no further questions are answered?
"""
import os
import pickle
from collections import Counter

import numpy as np

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.tenforce_config import TenForceConfig


class CriticalQuestions:
    def __init__(self, keep_top=3, b_verbose=False):
        """

        Args:
            keep_top: number of most common critical questions to keep
            b_verbose: print some extra information to console
        """
        print("Initializing CriticalQuestions...")
        cqs_file = os.path.join(TenForceConfig.DATA_DIR, 'critical_questions.pkl')

        da = TFDataAnalysis()
        if os.path.isfile(cqs_file):
            all_cqs = pickle.load(open(cqs_file, 'rb'))
        else:
            # Get questions (cqs) over entire data
            all_cqs = da.analyse_critical_questions(da.data)[0]
            pickle.dump(all_cqs, open(cqs_file, 'wb'))

        # For the x most prevalent cqs, check how often they were answered NOK for "non-critical" sites
        self.cqs = Counter()
        self.cqs_counts = Counter()
        for qid, _ in all_cqs.most_common(keep_top):
            # Get all rows for this question where answer was NOK
            da_q = da.data[(da.data['Question_Id'] == qid) & (da.data['Compliancy'] == 'NOK')]
            # Sort according to start date
            da_q = da.sort_chronologically(da_q)
            # Get unique sites this question has been answered NOK for
            uq_sites_for_q = da_q['Sites : Nr'].unique()
            # Go over sites and check deficiency
            defis = []
            crit_sites = 0
            for site in uq_sites_for_q:
                da_s = da.data[da.data['Sites : Nr'] == site]
                defi = da.get_deficiency_for_df(da_s)
                if defi == 1:
                    crit_sites += 1
                defis.append(defi)

            if b_verbose:
                print("Question {}:".format(qid))
                print("\tCritical sites: {} out of {} [{:5.1f}%]".format(crit_sites, len(defis), 100*crit_sites/len(defis)))
                print("\tAverage deficiency over sites: {:5.3f}".format(np.mean(defis)))

            cqs_cnts = len(defis)
            self.cqs[qid] = crit_sites/cqs_cnts
            self.cqs_counts[qid] = cqs_cnts

    def is_critical_question(self, qid: int):
        """
        Check whether question with question id = qid is a critical question

        Args:
            qid: the id of the question to check

        Returns: boolean

        """
        return qid in self.cqs

    def get_critical_ids(self):
        """
        Get set of critical question ids.

        Returns: set

        """
        return set(self.cqs.keys())

    def get_critical_odds(self, qid: int, b_return_counts=False):
        """
        Get the odds that a NOK for this qid will result in a critical site.

        Args:
            qid: the id of the question to look up
            b_return_counts: also return counts for this qid

        Returns: odds, or (odds, counts)

        """
        # self.cqs is a Counter, so returns 0 if element not present
        return self.cqs[qid] if not b_return_counts else (self.cqs[qid], self.cqs_counts[qid])

"""
This class allows to map the elements in the "Question" column in the Tenforce data to unique IDs.
"""
import os
import pickle

from termcolor import cprint

from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class QuestionMapper:
    def __init__(self, qm_file=None):
        if qm_file is None:
            qm_file = os.path.join(TenForceConfig.DATA_DIR, 'question_mapping.pkl')
        self._qm = pickle.load(open(qm_file, 'rb'))
        self.nl_questions, self.nl_idxs = Tools.sort_with_indices(list(self._qm['Title - NL']))
        self.fr_questions, self.fr_idxs = Tools.sort_with_indices(list(self._qm['Title - FR']))

        self.topics = sorted(self._qm.Topic.unique())
        self.topic_per_qn = []
        self.audits = sorted(self._qm.Audit.unique())
        self.audit_per_qn = []
        for qn in self._qm['Title - NL']:
            # Get index for question
            qn_idx = self.get_idx_for(qn)
            # Get topic for this question by querying the Topic column at the right index
            self.topic_per_qn.append(Tools.binary_search(self.topics, self._qm.Topic.iloc[qn_idx]))
            # Get audit for this question by querying the Audit column at the right index
            self.audit_per_qn.append(Tools.binary_search(self.audits, self._qm.Audit.iloc[qn_idx]))

    def get_idx_for(self, qstn: str):
        """
        For a provided question, look up its index. This is a convenience function so as not to always have to unpack
        the result of self.get_values_for() in case you only need the idx.

        Args:
            qstn: the question to look up

        Returns:
            The index of this question, or -1 if the question is not known.

        """
        # Known Dutch question?
        pos = Tools.binary_search(self.nl_questions, qstn, b_return_insert=True)
        if pos >= 0:
            return self.nl_idxs[pos]
        # Before checking for French, check if this is one of those "the question is too long and
        # appears shortened in the data XLSX" cases.
        else:
            pos = -(pos + 1)
            if self.nl_questions[pos].startswith(qstn):
                return self.nl_idxs[pos]
        # Known French question?
        pos = Tools.binary_search(self.fr_questions, qstn, b_return_insert=True)
        if pos >= 0:
            return self.fr_idxs[pos]
        # Before considering this an unknown question, check for same thing as NL-case.
        else:
            pos = -(pos + 1)
            if self.fr_questions[pos].startswith(qstn):
                return self.fr_idxs[pos]
        # Unknown question, return -1
        cprint("Warning: QuestionMapper -> Unknown question:\n{}".format(qstn), color='red')
        return -1

    def get_lang_for(self, qstn: str):
        """
        Check whether this is a NL question or a FR question.

        Args:
            qstn: the question to look up

        Returns: 'NL' is NL, 'FR' if FR and None if unknown.

        """
        # Known Dutch question?
        pos = Tools.binary_search(self.nl_questions, qstn, b_return_insert=True)
        if pos >= 0:
            return 'NL'
        else:
            pos = -(pos + 1)
            if self.nl_questions[pos].startswith(qstn):
                return 'NL'
        # Known French question?
        pos = Tools.binary_search(self.fr_questions, qstn, b_return_insert=True)
        if pos >= 0:
            return 'FR'
        else:
            pos = -(pos + 1)
            if self.fr_questions[pos].startswith(qstn):
                return 'FR'
        # Unknown question, return None
        return None

    def get_values_for(self, qstn: str, b_return_strings=False):
        """
        For a provided question, look up its index, as well as its topic and audit (indices or strings).

        Args:
            qstn: the question to look up
            b_return_strings: whether to return the topic and audit indices (False), or string values (True)

        Returns:
            A tuple (question idx, topic, audit).

        """
        qn_idx = self.get_idx_for(qstn)
        if qn_idx < 0:
            return qn_idx

        topic = self.topic_per_qn[qn_idx]
        audit = self.audit_per_qn[qn_idx]
        if b_return_strings:
            topic, audit = self.topics[topic], self.audits[audit]

        return qn_idx, topic, audit

    def get_nl_question(self, qn_idx: int):
        """
        Get the NL question corresponding to a given index.

        Args:
            qn_idx: the index of the question

        Returns:
            The NL text for this question
        """
        return self._qm['Title - NL'].iloc[qn_idx]

    def get_fr_question(self, qn_idx: int):
        """
        Get the FR question corresponding to a given index.

        Args:
            qn_idx: the index of the question

        Returns:
            The FR text for this question
        """
        return self._qm['Title - FR'].iloc[qn_idx]

    def get_topic_for(self, qn_idx: int):
        return self.topics[self.topic_per_qn[qn_idx]]

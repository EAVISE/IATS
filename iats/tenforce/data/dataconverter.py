"""
The data for this case was delivered as XLSX files. Use this script to convert this to ready-to-read pandas DataFrames.
"""
import os
import pandas as pd

from iats.tenforce.data.question_mapper import QuestionMapper
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tenforce_config import TenForceConfig


class DataConverter:
    def run(self, outdir=TenForceConfig.DATA_DIR, outfile_data='tenforce_data.pkl',
            outfile_qstn='question_mapping.pkl'):
        # 1. Create and save to disk a DataFrame from the file containing the list of questions.
        print("Processing question file...")
        df = pd.read_excel(os.path.join(TenForceConfig.DATA_DIR, TenForceConfig.MSG_FILE))
        df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
        df.to_pickle(os.path.join(outdir, outfile_qstn))
        print("Converted file '{}' to '{}'.".format(os.path.join(TenForceConfig.DATA_DIR, TenForceConfig.MSG_FILE),
                                                    os.path.join(outdir, outfile_qstn)))
        # 2. Load QuestionMapper with this file.
        qm = QuestionMapper(qm_file=os.path.join(outdir, outfile_qstn))

        # 3. Create a DataFrame from the data file.
        print("Processing data file...")
        df = pd.read_excel(os.path.join(TenForceConfig.DATA_DIR, TenForceConfig.DATA_FILE))
        df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

        # 4. Use the QuestionMapper to map the "Question" column in this DataFrame onto unique IDs, and add the
        # result as a new column to this DataFrame.
        # Put the new column in the right place, while we're at it; that is, next to 'Question'.
        df['Question_Id'] = [qm.get_idx_for(x) for x in df.Question]
        cols = list(df.columns)
        offset = cols.index('Question') + 1
        cols = cols[:offset] + [cols[-1]] + cols[offset:-1]
        df = df[cols]

        # 5. Load TopicMapper with this DataFrame and QuestionMapper
        tm = TopicMapper(df=df)

        # 6. Use the TopicMapper to map topics to unique IDs, and add the result as a new column to this DataFrame.
        # Put the new column in the right place, while we're at it; that is, next to 'Dynamic Topics : Title'.
        df['Topic_Id'] = [tm.get_idx_for(x) for x in df['Dynamic Topics : Title']]
        cols = list(df.columns)
        offset = cols.index('Dynamic Topics : Title') + 1
        cols = cols[:offset] + [cols[-1]] + cols[offset:-1]
        df = df[cols]

        # 7. Save the resulting DataFrame to disk.
        df.to_pickle(os.path.join(outdir, outfile_data))
        print("Converted file '{}' to '{}'.".format(os.path.join(TenForceConfig.DATA_DIR, TenForceConfig.DATA_FILE),
                                                    os.path.join(outdir, outfile_data)))


if __name__ == '__main__':
    dc = DataConverter()
    dc.run()

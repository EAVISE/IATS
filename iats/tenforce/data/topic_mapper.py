"""
The file containing the NL en FR versions for each question only contains the NL versions
of the topic. This file will map NL and FR versions onto each other by taking a side road using the questions,
and assign a unique ID to each topic.
"""
import array
import os
import pandas as pd
import pickle

from iats.tenforce.data.question_mapper import QuestionMapper
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class TopicMapper:
    def __init__(self, df: pd.DataFrame=None, tm_file=None):
        """
        Create TopicMapper data, if not existing yet, or load precomputed data.
        In case TopicMapper hasn't been initialized yet, you need to provide the DataFrame to process
        in order to extract the topic information.

        Args:
            df: the DataFrame from which the necessary topic data will be extracted
            tm_file: the name of the output file to generate; this will be a pickled tuple containing
            the Dutch and French names of all found subtopics, as well as the necessary index information.
        """
        if tm_file is None:
            tm_file = os.path.join(TenForceConfig.DATA_DIR, 'topic_mapping.pkl')

        if os.path.isfile(tm_file):
            self.nl_sorted, self.nl_idxs, self.fr_sorted, self.fr_idxs, self.topic_idx_per_qn =\
                pickle.load(open(tm_file, 'rb'))
        else:
            print("Initializing TopicMapper...")
            qm = QuestionMapper()

            if df is None:
                da = TFDataAnalysis()
                df = da.data

            # Unique questions present in DataFrame?
            uq_qstns = sorted(df['Question_Id'].unique())
            uq_topics = set(df['Dynamic Topics : Title'].unique())
            # For each question, look for a NL and FR variation, and extract corresponding topic
            topics = set()
            nl_topics = set()
            topic_per_qn = {}
            for qid in uq_qstns:
                nl_topic = None
                fr_topic = None

                da_qstn = df[df['Question_Id'] == qid]
                for idx, row in da_qstn.iterrows():
                    qstn = row['Question']
                    lang = qm.get_lang_for(qstn)
                    if not nl_topic and lang == 'NL':
                        nl_topic = row['Dynamic Topics : Title']
                        topic_per_qn[qid] = nl_topic
                        if nl_topic in nl_topics:
                            break
                        nl_topics.add(nl_topic)
                    elif not fr_topic and lang == 'FR':
                        fr_topic = row['Dynamic Topics : Title']
                    if nl_topic and fr_topic:
                        break

                if nl_topic and fr_topic:
                    # print("[{}] <---> [{}]".format(nl_topic, fr_topic))
                    topics.add((nl_topic, fr_topic))

            # Sort topics according to NL versions
            topics = sorted(topics, key=lambda x: x[0])
            # Extract NL and FR topics, in matching order, sort and keep reference to original position
            nl_topics, fr_topics = zip(*topics)
            self.nl_sorted, self.nl_idxs = Tools.sort_with_indices(nl_topics)
            self.fr_sorted, self.fr_idxs = Tools.sort_with_indices(fr_topics)

            # Check which topics have not been resolved
            uq_topics = uq_topics.difference(set(nl_topics))
            uq_topics = uq_topics.difference(set(fr_topics))
            # Can unresolved topics be mapped?
            resolved = set()
            for t in uq_topics:
                best_match = None
                min_d = 1000
                for t2 in nl_topics+fr_topics:
                    d = self.levenshtein(t, t2)
                    if d < min_d:
                        best_match = t2
                        min_d = d
                # Edit distance is small enough; accept t as alternative to best_match
                if min_d < 2:
                    # First, NL or FR topic?
                    lang = self.get_lang_for(best_match)
                    lang_topics = self.nl_sorted if lang == 'NL' else self.fr_sorted
                    lang_ids = self.nl_idxs if lang == 'NL' else self.fr_idxs
                    # Next, get position of best_match in lang_topics, as well as insert position for t
                    insert_pos = -(Tools.binary_search(lang_topics, t, b_return_insert=True)+1)
                    topic_pos = Tools.binary_search(lang_topics, best_match)
                    # Insert match in correct position
                    lang_ids = lang_ids[:insert_pos] + (lang_ids[topic_pos],) + lang_ids[insert_pos:]
                    lang_topics = lang_topics[:insert_pos] + (t,) + lang_topics[insert_pos:]
                    if lang == 'NL':
                        self.nl_idxs = lang_ids
                        self.nl_sorted = lang_topics
                    else:
                        self.fr_idxs = lang_ids
                        self.fr_sorted = lang_topics
                    resolved.add(t)
            uq_topics = uq_topics.difference(resolved)

            # Unresolved topics?
            if uq_topics:
                print("Could not resolve following topics:")
                for t in uq_topics:
                    print("\t{}".format(t))

            # Determine topic index for each question
            self.topic_idx_per_qn = {}
            for qid, nl_topic in topic_per_qn.items():
                # print("{}: {} - {}".format(qid, self.get_idx_for(nl_topic), nl_topic))
                self.topic_idx_per_qn[qid] = self.get_idx_for(nl_topic)

            # Dump results to pickle
            pickle.dump((self.nl_sorted, self.nl_idxs, self.fr_sorted, self.fr_idxs, self.topic_idx_per_qn),
                        open(tm_file, 'wb'))

        # Keep map from "original" index to sorted index, so as to be able to
        # query the text for a given topic.
        self.nl_orig_to_sorted_idx = array.array('i', range(len(self.nl_idxs)))
        for i, e in enumerate(self.nl_idxs):
            self.nl_orig_to_sorted_idx[e] = i
        self.fr_orig_to_sorted_idx = array.array('i', range(len(self.fr_idxs)))
        for i, e in enumerate(self.fr_idxs):
            self.fr_orig_to_sorted_idx[e] = i

    # Taken from https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
    def levenshtein(self, s1, s2):
        if len(s1) < len(s2):
            return self.levenshtein(s2, s1)

        # len(s1) >= len(s2)
        if len(s2) == 0:
            return len(s1)

        previous_row = range(len(s2) + 1)
        for i, c1 in enumerate(s1):
            current_row = [i + 1]
            for j, c2 in enumerate(s2):
                # j+1 instead of j since previous_row and current_row are one character longer
                insertions = previous_row[j + 1] + 1
                deletions = current_row[j] + 1  # than s2
                substitutions = previous_row[j] + (c1 != c2)
                current_row.append(min(insertions, deletions, substitutions))
            previous_row = current_row

        return previous_row[-1]

    def get_idx_for(self, topic: str):
        """
        For a provided topic, look up its index.

        Args:
            topic: the topic to look up

        Returns:
            The index of this topic, or -1 if the topic is not known.

        """
        # Known Dutch question?
        pos = Tools.binary_search(self.nl_sorted, topic)
        if pos >= 0:
            return self.nl_idxs[pos]
        # Known French question?
        pos = Tools.binary_search(self.fr_sorted, topic)
        if pos >= 0:
            return self.fr_idxs[pos]
        # Unknown question, return -1
        return -1

    def get_lang_for(self, topic: str):
        """
        Check whether this is a NL topic or a FR topic.

        Args:
            topic: the topic to look up

        Returns: 'NL' is NL, 'FR' if FR and None if unknown.

        """
        # Known Dutch topic?
        pos = Tools.binary_search(self.nl_sorted, topic)
        if pos >= 0:
            return 'NL'
        # Known French topic?
        pos = Tools.binary_search(self.fr_sorted, topic)
        if pos >= 0:
            return 'FR'
        # Unknown topic, return None
        return None

    def get_nl_topic(self, tp_idx: int):
        """
        Get the NL topic corresponding to a given index.

        Args:
            tp_idx: the index of the topic

        Returns:
            The NL text for this topic
        """
        return self.nl_sorted[self.nl_orig_to_sorted_idx[tp_idx]]

    def get_fr_topic(self, tp_idx: int):
        """
        Get the FR topic corresponding to a given index.

        Args:
            tp_idx: the index of the topic

        Returns:
            The FR text for this topic
        """
        return self.fr_sorted[self.fr_orig_to_sorted_idx[tp_idx]]

"""
This class is a quick-and-ugly fix to extract statistics from training data. It will read the training data into memory
as a list of lists, where the outer list contains the rows in the data file, and each inner list represents the
contents of a row.
For a given set of parents and possibly a given target, this class will iterate over all stored rows (i.e., all inner
lists), and check how many of them match the parents/target. In case no target is specified, it will return the number
of times this set of parents has been matched, and the percentage of the data (i.e., total number of samples) this
corresponds to.

The commented out code is a panda-based implementation that is actually a lot slower than the quick-and-ugly fix, so
just keep the quick-and-ugly fix.
"""
import os
from itertools import product

from iats.tenforce.tenforce_config import TenForceConfig
# import pandas as pd


class CountExtractor:
    def __init__(self, datafile):
        # self.samples = pd.read_csv(datafile)
        self.header = None

        b_first = True
        samples = []
        cardinality = None  # Keep track of number of options per variable
        with open(datafile, 'r') as fin:
            for line in fin:
                parts = line.strip().split(',')
                if b_first:
                    self.header = parts
                    cardinality = [set() for _ in range(len(parts))]
                    b_first = False
                    continue
                samples.append(parts)
                for i in range(len(parts)):
                    cardinality[i].add(parts[i])
        self.cardinality = [len(cardinality[i]) for i in range(len(cardinality))]
        self.header_idx = {self.header[i]: i for i in range(len(self.header))}
        self.samples = samples
        self.total_weight = 0
        # Convert weight values from string to float
        if 'Weight' in self.header_idx:
            for sample in samples:
                sample_w = float(sample[self.header_idx['Weight']])
                sample[self.header_idx['Weight']] = sample_w
                self.total_weight += sample_w
        # Convert task values from string to int
        if 'Task' in self.header_idx:
            for sample in samples:
                sample[self.header_idx['Task']] = int(sample[self.header_idx['Task']])

    def get_cnt(self, parents: {}, target: tuple = (), lap_smooth: int = 0, lap_weight = 1, b_weighted=False):
        """

        Args:
            parents: dictionary with k,v pairs: k = parent node name, v = parent node value
            target: tuple, tpl[0] = name of node, tpl[1] = target value of node
            lap_smooth: apply Laplace smoothing with provided (int) value
            lap_weight: weight to be used when applying Laplace smoothing (i.e., the weight of each count
            added due to Laplace smoothing)
            b_weighted: use weights (if present in data file)?

            BEWARE! Some nodes require custom rules for Laplace smoothing, as certain configurations are invalid.
            E.g., the 'CQxx_Crit' nodes, indicating whether a critical question is indeed critical, should always
            have value 1.0 for False, except when the corresponding 'CQxx_A' is 'NOK'.

        Returns:

        """
        # view = self.samples.copy(deep=False)
        # for k, v in parents.items():
        #     view = view[view[k] == v]
        # total = view.shape[0]
        #
        # if target:
        #     view_target = view[view[target[0]] == target[1]]
        #     hit = view_target.shape[0]
        # else:
        #     hit = 0

        if target:
            idx_target = self.header_idx[target[0]]
            # Forcefully disable Laplace smoothing for specific cases
            if lap_smooth:
                if (target[0].endswith('_Crit') and parents[target[0][:-4] + 'A'] != 'NOK') or\
                        (target[0] == 'Sum') or\
                        (target[0].startswith('Topic') and parents['Sum'] == 'T'):
                    lap_smooth = 0
                if target[0].startswith('RQ'):
                    # Get 'Topic' parent node
                    for parent in parents.keys():
                        if parent.startswith('Topic') and parents[parent] == 'F':
                            lap_smooth = 0
                            break

        total = 0
        hit = 0
        for i, sample in enumerate(self.samples):
            # Check parents are matched
            b_ok = True
            for par, v in parents.items():
                if sample[self.header_idx[par]] != v:
                    b_ok = False
                    break
            if not b_ok:
                continue
            # Check target is matched
            total += sample[self.header_idx['Weight']] if b_weighted else 1
            if target and sample[idx_target] == target[1]:
                hit += sample[self.header_idx['Weight']] if b_weighted else 1

        if lap_smooth:
            if target:
                total += self.cardinality[idx_target]*(lap_smooth*lap_weight)
            hit += lap_smooth*lap_weight

        return total, hit, hit/total if (target and total > 0) else\
            (1.0/self.cardinality[idx_target] if target else
             (total/self.total_weight if b_weighted else total/len(self.samples)))


if __name__ == '__main__':
    data_file = os.path.join(TenForceConfig.HOME,
                             "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_alt_data_area.dat")

    uqf = CountExtractor(data_file)
    cqs = ["CQ47_Crit", "CQ58_Crit", "CQ130_Crit"]
    for prod in product(["T", "F"], repeat=len(cqs)):
        parents = {cqs[i]: prod[i] for i in range(len(cqs))}
        pc_true = uqf.get_cnt(parents, ("Sum", "T"))
        print(f"{prod}: {pc_true}")
        pc_true = uqf.get_cnt(parents, ("Sum", "T"), lap_smooth=1)
        print(f"{prod}: {pc_true}")

    print("hihi")

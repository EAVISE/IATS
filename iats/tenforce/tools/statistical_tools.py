import math
import numpy as np


class StatisticalTools:
    # Z values for confidence levels
    z_levels = {
        0.70: 	1.04,
        0.75: 	1.15,
        0.80: 	1.28,
        0.85: 	1.44,
        0.90: 	1.645,
        0.92: 	1.75,
        0.95: 	1.96,
        0.96: 	2.05,
        0.98: 	2.33,
        0.99: 	2.58
    }

    @classmethod
    def _binomial_confidence(cls, z: float, p: float, n: float):
        """

        Args:
            z: posit
            p: prior determined from data
            n: number of measurements

        Returns:

        """
        interval = z * math.sqrt(p*(1-p)/n)
        return p - interval, p + interval

    @classmethod
    def _chose_threshold(cls, z: float, p: float, n: int):
        """

        Args:
            z: posit
            p: prior determined from data
            n: number of measurements

        Returns:

        """
        l_edge, r_edge = cls._binomial_confidence(z, p, n)
        return cls.get_rand(l_edge, r_edge)

    @classmethod
    def random_binomial_test(cls, p: float, n: int, alpha: float = 0.95,
                             b_random_thresh: bool = True, b_return_int: bool = False):
        """

        Args:
            p: prior determined from data
            n: number of measurements
            alpha: desired confidence level
            b_random_thresh: first randomly chose a threshold before randomly choosing a result
            b_return_int: return 1/0 instead of True/False

        Returns:

        """
        # First, randomly select threshold based on binomial distribution
        p_thresh = cls._chose_threshold(cls.z_levels[alpha], p, n) if b_random_thresh else p
        # Then, generate random number and check against generated threshold
        res = np.random.rand() < p_thresh
        if b_return_int:
            res = 1 if res else 0
        return res

    @classmethod
    def sum_log_probs(cls, vals: list, b_one_minus=False):
        res = sum([math.log2(1 + ((1-x) if b_one_minus else x)) for x in vals])
        return res

    @staticmethod
    def get_rand(min: float, max: float):
        """
        Get random number from a beta distribution mapped to provided limits
        Note that a beta(a=5,b=5) distribution is very close to a normal distribution with mean=0.5 and std=0.2.

        Returns:
            random number between min and max
        """
        return min + (np.random.beta(5, 5) * (max - min))

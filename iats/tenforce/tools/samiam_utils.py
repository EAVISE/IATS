"""
This class contains helper methods to create text files that describe a Bayesian network that can be read by SamIam.
"""


class SamIamUtils:
    @staticmethod
    def create_node(name, states: list, pos: tuple, id):
        """
        Create a textual representation of a network node that can be written to a Hugin-style .net file
        and read/interpreted by SamIam.

        Args:
            name:
            states:
            pos:
            id:

        Returns:
            String representing node.
        """
        node = f"node {name}\n"
        node += "{\n\tstates = ("
        for i, state in enumerate(states):
            if i > 0:
                node += ' '
            node += f'"{state}"'
        node += ");\n"
        node += f"\tposition = ({pos[0]} {pos[1]});\n"
        node += '\tdiagnosistype = "AUXILIARY";\n' +\
                '\tDSLxSUBMODEL = "Root Submodel";\n' +\
                '\tismapvariable = "false";\n'
        node += f'\tID = "{id}";\n'
        node += f'\tlabel = "{name}";\n'
        node += f'\texcludepolicy = "include whole CPT";\n'
        node += "}\n"
        return node

    @staticmethod
    def create_potential(var, cond_on: list, cpt: str):
        """
        Create a textual representation of a node's CPT that can be written to a Hugin-style .net file
        and read/interpreted by SamIam.

        Args:
            var:
            cond_on:
            cpt:

        Returns:
            String representing CPT.
        """
        pot = f"potential ( {var} |"
        for var2 in cond_on:
            pot += ' ' + var2
        pot += f" )\n{{\n\tdata = {cpt};\n"
        pot += "}\n"
        return pot

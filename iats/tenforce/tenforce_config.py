"""
Some configuration for the TenForce code. Basically, it is assumed that certain files are present in a
specific directory, namely:
-A file containing the audits proper, i.e., the answered questions per site/audit.
-A file containing the list of questions, both in Dutch and French.
-A file containing the list of audits.
This directory and the names of these files on your system should be specified in this file.

Besides this, also an output directory for created models and evaluation files should be specified.
"""
import os


class TenForceConfig:
    # Home directory of the user
    HOME = os.path.expanduser("~")
    # Home directory for all project related data
    TF_HOME = os.path.join(HOME, "Work", "Projects", "IATS", "Data", "TenForce")
    # Directory contaning the necessary data files
    DATA_DIR = os.path.join(TF_HOME, "TetraDataset")
    # Names of the three required data files
    DATA_FILE = "main_dataset_2018-10-04_16-11 - anon.xlsx"
    MSG_FILE = "audit_ref_data_2018-10-04_20-18.xlsx"
    AUDIT_FILE = "Sites_-_Audits_connection_anon.xlsx"
    # Directory in which output of several scripts will be written
    SAMIAM_DATA_DIR = os.path.join(TF_HOME, "samiam", "with_smoothing")

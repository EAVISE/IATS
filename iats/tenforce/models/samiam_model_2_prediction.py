"""
A script to demonstrate the usage of a Bayesian model to decide which contractor to prioritize using
the 2nd SamIam model(s).

Also includes a custom metric that compares average deficiencies at rank k between the original ranking (i.e.,
the order in which the audits were performed) and our predicted ranking.
"""
import datetime
from itertools import product

import math
import os
from collections import defaultdict
from multiprocessing import Pool, Process, JoinableQueue

from tqdm import tqdm

from iats.tenforce.models.sample_samiam_model_2 import SampleSamIam2
from iats.tenforce.tenforce_config import TenForceConfig
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


class SamplerType:
    AREA = "Area"
    TASK = "Task"


class SamIam2Prediction:
    def __init__(self, bn_file_root: str=None):
        """

        Args:
            bn_file_root: root (=common part) of the filenames of the files containing the
             Area and Task networks to use.
        """
        if bn_file_root is None:
            bn_file_root = os.path.join(TenForceConfig.SAMIAM_DATA_DIR, "bn_model2_alt")
        self.sampler_area = SampleSamIam2(bn_file=bn_file_root + "_area.net")
        self.sampler_task = SampleSamIam2(bn_file=bn_file_root + "_task.net")
        # self.sampler_no_ctor_area = SampleSamIam2(bn_file=bn_file_root + "_no_ctor_area_em.net")
        # self.sampler_no_ctor_task = SampleSamIam2(bn_file=bn_file_root + "_no_ctor_task_em.net")

    def _get_sampler(self, sampler_type):
        if sampler_type == SamplerType.AREA:
            return self.sampler_area
        elif sampler_type == SamplerType.TASK:
            return self.sampler_task
        raise ValueError("Unknown SamplerType.")

    def roll_dice_single(self, configs: [], nb=1000, sampler_type: str = SamplerType.TASK,
                         b_plot=False, b_silent=False):
        """
        Sample a single model, either 'Area' or 'Task'.

        Args:
            configs: list of configs to sample; a config is a dictionary containing the parents to provide
            to the sampling method; typically, these will be the contractor and the area or task
            nb: the number of samples to generate
            sampler_type: either SamplerType.AREA or SamplerType.TASK, depending on which model you wish to sample
            b_plot: generate a plot with the results?
            b_silent: print output or not?

        Returns:
            (results, defis), where 'results' is a list of lists containing one list per config which in turn contains
            all samples as they are returned by the sampling method (i.e., each sample is a tuple of len 3), and 'defis'
            is a list of lists containing one list per config which contains all deficiencies, as extracted from the
            corresponding samples.

        """
        results = []
        defis = []

        sampler = self._get_sampler(sampler_type)

        if b_plot:
            plt.figure()

        for i, config in enumerate(configs):
            if not b_silent:
                print(f"Sampling for choice {i+1} of {len(configs)}...")
            samples = []
            if b_silent:
                for _ in range(nb):
                    samples.append(sampler.sample(parents=config))
            else:
                for _ in tqdm(range(nb)):
                    samples.append(sampler.sample(parents=config))
            results.append(samples)
            defis.append([sample[2] for sample in samples])

            if b_plot:
                ax = plt.subplot(len(configs), 1, i + 1)
                ax.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, align="mid", rwidth=0.5)
                ax2 = ax.twinx()
                ax2.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, cumulative=True,
                        color='r', align="left", rwidth=0.2)
                ax.set_title(f"{config['Contractor']}: {config[sampler_type]}")
                ax.set_xlim(-0.01, 1.01)
                ax.set_xlabel("Deficiency")
                ax.set_ylabel("Bin prob. (%)")
                ax2.set_ylabel("Cumulative p")
        if b_plot:
            plt.tight_layout()
            plt.show()

        return results, defis

    def roll_dice_double(self, configs: [], nb=1000):
        """

        Args:
            configs:
            nb:

        Returns:

        """
        fig = plt.figure()
        for i, config in enumerate(configs):
            print(f"Sampling for choice {i+1} of {len(configs)}...")

            config_area = {'Contractor': config['Contractor'], 'Area': config['Area']}
            config_task = {'Contractor': config['Contractor'], 'Task': config['Task']}
            for j, tpl in enumerate([("Area", config_area), ("Task", config_task)]):
                print(f"{config['Contractor']}: {config[tpl[0]]}")
                defis = {}

                sampler = self._get_sampler(tpl[0])
                samples = []
                for _ in tqdm(range(nb)):
                    samples.append(sampler.sample(parents=tpl[1]))
                defis[i] = [sample[2] for sample in samples]
                for k, pc in enumerate([25, 50, 75]):
                    if k > 0:
                        print("\t", end='')
                    print(f"pc{pc}: {np.percentile(defis[i], pc):.3f}", end='')
                print()

                ax = plt.subplot(len(configs)+1, 2, (2*i + j + 1))
                ax.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, align="mid", rwidth=0.5)
                ax2 = ax.twinx()
                ax2.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, cumulative=True,
                        color='r', align="left", rwidth=0.2)
                ax.set_title(f"{config['Contractor']}: {config[tpl[0]]}")
                ax.set_xlim(-0.01, 1.01)
                ax.set_xlabel("Deficiency")
                ax.set_ylabel("Bin prob. (%)")
                ax2.set_ylabel("Cumulative p")

        plt.tight_layout()
        plt.show()

    def _generate_distributions(self, df_test, pcs, nb, nb_procs=2):
        """
        Method that generates all necessary distributions in order to make predictions for the data contained in df_test
        All arguments mirror those from compare_to_reference().

        Returns:
            res_area, res_task: dictionaries containing the results

        """
        # Only generate modelled distributions once
        print("Generating modelled distributions...")
        configs_area, configs_task, configs_prod = set(), set(), set()
        for i, r in df_test.iterrows():
            ctor = r['Contractor']
            area = r['Area']
            task = str(r['Task'])
            configs_area.add((ctor, area))
            configs_task.add((ctor, task))
            # configs_prod.add((ctor, area, task))

        configs_area = sorted(configs_area)
        configs_task = sorted(configs_task)
        # configs_prod = sorted(configs_prod)
        nb_dists = len(configs_task) + len(configs_area)

        # Convenience method that will generate the distribution for a given configuration.
        # The idea behind putting this into a separate method is to allow the generation of the distributions
        # to happen using multiprocessing, by calling this method.
        def _generate_distri(config, sampler_type, at_dist):
            print(f"\rGenerating distribution {at_dist} of {nb_dists}...", end='')
            res = self.roll_dice_single([{'Contractor': config[0], sampler_type: config[1]}],
                                        nb=nb, sampler_type=sampler_type, b_silent=True)

            # res[1] = list containing one list per config in turn containing the deficiencies for the corresponding
            # samples.
            # res[1][0] = list of deficiencies for the samples for the single config we passed to roll_dice_single()
            res_dic = {f'PC{pc}': np.percentile(res[1][0], pc) for pc in pcs}
            res_dic['Mass'] = self._to_mass(res[1][0])

            q.put((config, res_dic, sampler_type))

        q = JoinableQueue()  # This queue will hold results from subprocesses
        at_dist = 0
        tpls = []  # This list will contain all configurations to be parsed; the list is used for multiprocessing.
        for tpl in [(configs_task, 'Task'), (configs_area, 'Area')]:
            for config in tpl[0]:
                at_dist += 1
                tpls.append((config, tpl[1], at_dist))

        at_tpl = 0
        # Generate the distributions in batches of nb_procs
        while at_tpl < len(tpls):
            print(f"\nTime: {datetime.datetime.now()}")
            buffer = []
            while len(buffer) < nb_procs and at_tpl < len(tpls):
                tpl = tpls[at_tpl]
                p = Process(target=_generate_distri, args=[tpl[0], tpl[1], tpl[2]])
                p.start()
                buffer.append(p)
                at_tpl += 1
            for p in buffer:
                p.join()
        print(f"\rGenerating distribution {nb_dists} of {nb_dists}... done.")
        print(f"Time: {datetime.datetime.now()}")

        # Extract what's been put into queue, and put into relevant dictionaries
        # Each tuple in the queue contains "(config, res_dic, sampler_type)"
        res_area, res_task = {}, {}
        while not q.empty():
            tpl = q.get()
            if tpl[2] == 'Area':
                res_area[tpl[0]] = tpl[1]
            elif tpl[2] == 'Task':
                res_task[tpl[0]] = tpl[1]
            else:
                raise ValueError(f"Unknown sample type: {tpl[2]}")
            q.task_done()

        return res_area, res_task

    @staticmethod
    def _process_date(df, res_area, res_task, date, pcs):
        """

        Args:
            df: DateFrame to process
            res_area:
            res_task:
            date: date for which to process data
            pcs: percentiles

        Returns:

        """
        # Get subset of data relevant for this date
        df_date = df[df['Date'].isin(date)]
        # Check which unique configs are contained within this data
        date_cfgs_area, date_cfgs_task, date_cfgs_prod = set(), set(), set()
        for i, r in df_date.iterrows():
            ctor = r['Contractor']
            area = r['Area']
            task = str(r['Task'])
            date_cfgs_area.add((ctor, area))
            date_cfgs_task.add((ctor, task))
            date_cfgs_prod.add((ctor, area, task))
        date_cfgs_area = sorted(date_cfgs_area)
        date_cfgs_task = sorted(date_cfgs_task)
        date_cfgs_prod = sorted(date_cfgs_prod)
        # Create containers to hold results
        date_res_area, date_res_task = defaultdict(list), defaultdict(list)

        # We prepare data in a format that will be converted into a Pandas DataFrame later on in a
        # different method, namely _process_test_data()
        # We basically list out each variable: Contractor, Area/Task, Percentile values and mass
        for tpl in ((date_res_area, date_cfgs_area, res_area, 'Area'),
                    (date_res_task, date_cfgs_task, res_task, 'Task')):
            # "config in tpl[1]" is an Area/Task config --> (Contractor, Area/Task)
            # For each such config, append the corresponding variables (Contractor, Area/Task) and
            # percentile/mass values to the dictionary containing the results.
            for config in tpl[1]:
                # Append the contractor for this Area config to the
                tpl[0]['Contractor'].append(config[0])
                tpl[0][tpl[3]].append(config[1])
                for pc in pcs:
                    tpl[0][f'PC{pc}'].append(tpl[2][config][f'PC{pc}'])
                tpl[0]['Mass'].append(tpl[2][config]['Mass'])

        return df_date, date_cfgs_area, date_cfgs_task, date_cfgs_prod, date_res_area, date_res_task

    def compare_to_reference(self, test_data: str, pcs: list = None, nb=1000, output_file=None, nb_procs=2):
        """
        This method will generate the necessary distributions to rank the sites contained in the provided
        test_data file, perform a ranking according to those distributions and compare those to the true
        ranking, as stated in the test_data file.

        Args:
            test_data: filename of the file containing the test data
            pcs: percentiles to compute
            nb: number of samples to use to generate distribution
            output_file: name of the output file to generate, "None" to generate no output file
            nb_procs: number of processors to use for parallel computation of distributions

        Returns:

        """
        # Avoid compiler warning about mutable default argument by specifying default to be None
        if pcs is None:
            pcs = [25, 50, 75, 80, 90, 95]
        df_test = pd.read_csv(test_data)

        # df_test = df_test[df_test['Date'] == str(datetime.date(2018, 6, 14))]

        res_area, res_task = self._generate_distributions(df_test, pcs, nb, nb_procs)

        # Sort the dates present in the test data in ascending chronological order
        dates = sorted(df_test['Date'].unique())
        # Also add one entry representing all dates
        dates = [set(dates)] + [{x} for x in dates]

        output = ""
        pred_ros = []  # Ranking Offset of predicted rankings
        ref_ros = []  # Ranking Offset of reference rankings
        for date in dates:
            output += f"Processing date: {sorted(date)}\n"

            df_date, date_cfgs_area, date_cfgs_task, date_cfgs_prod, date_res_area, date_res_task =\
                self._process_date(df_test, res_area, res_task, date, pcs)

            # The next loop will sort the sites according to the generated distributions and the specified sorting
            # rules. It will then check the average ranking offset of our ranking vs. the reference ranking.
            # The rules to sort the results follow the following format:
            # E.g., use ['PC95', 'PC90'] to first sort results according to the 95th percentile, and in case of
            # a tie, use the 90th percentile to sort further.
            for sort_by in [['Mass', 'PC80'], ['PC95', 'PC90', 'PC75'], ['PC90', 'PC80', 'PC50'], ['PC80', 'PC50']]:
                output += f"Sort by: {sort_by}\n"
                date_pp = self._preprocess_test_data(df_date, date_res_area, date_res_task,
                                                     date_cfgs_area, date_cfgs_task, date_cfgs_prod, pcs)

                date_ro = self.compare_rankings(*(date_pp + (sort_by,)))
                pred_ros.append(date_ro[0][0])
                ref_ros.append(date_ro[0][1])
                output += f"\tProd: {date_ro[0][0]:.2f} vs {date_ro[0][1]:.2f}\n"
                output += f"\tArea: {date_ro[1][0]:.2f} vs {date_ro[1][1]:.2f}\n"
                output += f"\tTask: {date_ro[2][0]:.2f} vs {date_ro[2][1]:.2f}\n"

        if output_file is None:
            print(output)
        else:
            with open(output_file, 'w') as fout:
                fout.write(output)
            print(f"Written output to file:\n{output_file}")

        return pred_ros, ref_ros

    def compute_custom_metric(self, test_data: str, pcs: list = None, nb=1000, output_file=None, nb_procs=2):
        # Avoid compiler warning about mutable default argument by specifying default to be None
        if pcs is None:
            pcs = [25, 50, 75, 80, 90, 95]
        df_test = pd.read_csv(test_data)

        res_area, res_task = self._generate_distributions(df_test, pcs, nb, nb_procs)

        # Sort the dates present in the test data in ascending chronological order
        dates_indiv = sorted(df_test['Date'].unique())

        dates = [{x for x in dates_indiv}]
        for i in [0, 5]:
            dates.append({dates_indiv[x] for x in range(i, min(i+5, len(dates_indiv)))})

        output = ""
        for date in dates:
            output += f"Processing date: {sorted(date)}\n"

            df_date, date_cfgs_area, date_cfgs_task, date_cfgs_prod, date_res_area, date_res_task =\
                self._process_date(df_test, res_area, res_task, date, pcs)

            # The next loop will sort the sites according to the generated distributions and the specified sorting
            # rules. It will then check the average ranking offset of our ranking vs. the reference ranking.
            # The rules to sort the results follow the following format:
            # E.g., use ['PC95', 'PC90'] to first sort results according to the 95th percentile, and in case of
            # a tie, use the 90th percentile to sort further.
            for sort_by in [['Mass', 'PC80']]:  # , ['PC95', 'PC90', 'PC75'], ['PC90', 'PC80', 'PC50'], ['PC80', 'PC50']]:
                output += f"Sort by: {sort_by}\n"
                df_res_prod, df_res_area, df_res_task, df_avg, df_area_avg, df_task_avg =\
                    self._preprocess_test_data(df_date, date_res_area, date_res_task,
                                                     date_cfgs_area, date_cfgs_task, date_cfgs_prod, pcs)

                for k in range(1, 11):
                    ref_avg_defi, pred_avg_defi = self._custom_metric(df_avg, df_res_prod, sort_by, at_k=k)
                    output += f"At k={k}:: Ref vs. Avg: {ref_avg_defi:5.3f} - {pred_avg_defi:5.3f}\n"

        if output_file is None:
            print(output)
        else:
            with open(output_file, 'w') as fout:
                fout.write(output)
            print(f"Written output to file:\n{output_file}")

    @staticmethod
    def _preprocess_test_data(df_test: pd.DataFrame, res_area: {}, res_task: {},
                              cfgs_area: list, cfgs_task: list, cfgs_prod: list, pcs: list):
        """
        Given the provided distributions for area and task configs and the provided DataFrame containing the date to
        be used, this method will generate predictions by combining the necessary distributions for each config.
        It will also convert the provided dictionaries to DataFrames

        Args:
            df_test: DataFrame containing the test data
            res_area:
            res_task:
            cfgs_area:
            cfgs_task:
            cfgs_prod:
            pcs:
            sort_by:

        Returns:

        """
        # Convert dictionary to DataFrames
        df_res_area = pd.DataFrame(res_area)
        df_res_task = pd.DataFrame(res_task)

        # Take product of area and task model for relevant configs
        res_prod = defaultdict(list)
        for config in cfgs_prod:
            res_prod['Contractor'].append(config[0])
            res_prod['Area'].append(config[1])
            res_prod['Task'].append(config[2])
            for pc in pcs:
                pc_area = df_res_area[(df_res_area['Contractor'] == config[0]) & (df_res_area['Area'] == config[1])].iloc[0][f'PC{pc}']
                pc_task = df_res_task[(df_res_task['Contractor'] == config[0]) & (df_res_task['Task'] == config[2])].iloc[0][f'PC{pc}']
                res_prod[f'PC{pc}'].append(pc_area * pc_task)
            mass_area = df_res_area[(df_res_area['Contractor'] == config[0]) & (df_res_area['Area'] == config[1])].iloc[0]['Mass']
            mass_task = df_res_task[(df_res_task['Contractor'] == config[0]) & (df_res_task['Task'] == config[2])].iloc[0]['Mass']
            res_prod['Mass'].append(mass_area * mass_task)
            # if sort_by == ['Mass', 'PC80']:
            #     print(f"Config: {config}\tMass: {mass_area * mass_task}")
        df_res_prod = pd.DataFrame(res_prod)

        # Gather the reference (average) deficiencies in a format that can easily be converted to a DataFrame
        test_avg = defaultdict(list)  # Will contain the results for the products area x task
        test_area_avg = defaultdict(list)  # Will contain the results for the areas configs only
        test_task_avg = defaultdict(list)  # Will contain the results for the task configs only
        for config in cfgs_prod:
            test_avg['Contractor'].append(config[0])
            test_avg['Area'].append(config[1])
            test_avg['Task'].append(config[2])
            # In case there are more than 1 sites with the same config, take the average of their deficiencies as
            # the deficiency for this config
            d_avg = np.average(df_test[(df_test['Contractor'] == config[0]) & (df_test['Area'] == config[1]) & (
                        df_test['Task'] == int(config[2]))]['Deficiency'])
            test_avg['Avg'].append(d_avg)
        for config in cfgs_area:
            test_area_avg['Contractor'].append(config[0])
            test_area_avg['Area'].append(config[1])
            d_avg = np.average(df_test[(df_test['Contractor'] == config[0]) & (df_test['Area'] == config[1])]['Deficiency'])
            test_area_avg['Avg'].append(d_avg)
        for config in cfgs_task:
            test_task_avg['Contractor'].append(config[0])
            test_task_avg['Task'].append(int(config[1]))
            d_avg = np.average(df_test[(df_test['Contractor'] == config[0]) & (df_test['Task'] == int(config[1]))]['Deficiency'])
            test_task_avg['Avg'].append(d_avg)

        # Convert data to DataFrames
        df_avg = pd.DataFrame(test_avg)
        df_area_avg = pd.DataFrame(test_area_avg)
        df_task_avg = pd.DataFrame(test_task_avg)

        return df_res_prod, df_res_area, df_res_task, df_avg, df_area_avg, df_task_avg

    def compare_rankings(self, df_res_prod, df_res_area, df_res_task,
                         df_avg, df_area_avg, df_task_avg, sort_by=None):
        """
        This method will compare rankings obtained by using our method to randomly generated rankings for each config


        Args:
            df_res_prod:
            df_res_area:
            df_res_task:
            df_avg:
            df_area_avg:
            df_task_avg:
            sort_by:

        Returns:

        """
        if sort_by is None:
            sort_by = ['PC95', 'PC90', 'PC75']

        prod_avg_ro = self._compare_rankings(df_avg, df_res_prod, sort_by)  # , b_debug=(sort_by == ['Mass', 'PC80']))
        prod_area_ro = self._compare_rankings(df_area_avg, df_res_area, sort_by)
        prod_task_ro = self._compare_rankings(df_task_avg, df_res_task, sort_by)

        return prod_avg_ro, prod_area_ro, prod_task_ro

    def _to_mass(self, defis: list, bin_size=0.01):
        hist = np.histogram(defis, np.arange(0, 1+bin_size, bin_size))
        norm_hist = [x/len(defis) for x in hist[0]]

        mass = 0
        for i, e in enumerate(norm_hist):
            mass += math.pow(bin_size*(i+1)*e, 2)
        mass = math.sqrt(mass)

        return mass

    def _compare_rankings(self, df_ref, df_pred, sort_by: list, b_debug=False):
        """
        Compare predicted ranking to reference ranking.

        Args:
            df_ref: DataFrame containing the reference ranking
            df_pred: DataFrame containing the predicted ranking
            sort_by: the sorting rule to use
            b_debug: if True, extra debug info will be printed, albeit not in this method, but in
            "_avg_ranking_offset", upon which this method depends

        Returns:

        """
        ref_idx = list(df_ref.sort_values(by=['Avg'], ascending=False).index)
        ref_vals = list(df_ref.sort_values(by=['Avg'], ascending=False).Avg)
        pred_idx = list(df_pred.sort_values(by=sort_by, ascending=False).index)

        pred_mass = [df_pred.iloc[pred_idx[i]].Mass for i in pred_idx] if b_debug else []
        pred_ro = self._avg_ranking_offset(ref_idx, pred_idx, ref_vals=ref_vals, pred_mass=pred_mass)

        # Compute average average ranking offset (nope, not a typo) by using randomly shuffled rankings;
        # this is used as a baseline to beat
        rand_rank = list(range(len(ref_idx)))

        rand_ro = []
        for _ in range(25000):
            np.random.shuffle(rand_rank)
            rand_ro.append(self._avg_ranking_offset(ref_idx, rand_rank, ref_vals=ref_vals))
        rand_ro = np.mean(rand_ro)

        return pred_ro, rand_ro

    def _custom_metric(self, df_ref, df_pred, sort_by: list, at_k=1):
        """
        Compute custom metric

        Args:
            df_ref: DataFrame containing the reference ranking
            df_pred: DataFrame containing the predicted ranking
            sort_by: the sorting rule to use
            b_debug: if True, extra debug info will be printed, albeit not in this method, but in
            "_avg_ranking_offset", upon which this method depends

        Returns:

        """
        if at_k > len(df_ref):
            # print("Not enough entries to compute 'At k'.")
            return -1, -1

        # ref_idx = list(df_ref.sort_values(by=['Avg'], ascending=False).index)
        pred_idx = list(df_pred.sort_values(by=sort_by, ascending=False).index)

        ref_avg_defi = np.mean(df_ref.iloc[:at_k]['Avg'])
        pred_avg_defi = np.mean(df_ref.iloc[pred_idx[:at_k]]['Avg'])
        return ref_avg_defi, pred_avg_defi

    def _avg_ranking_offset(self, ref_idx: list, pred_idx: list, ref_vals=None, pred_mass=[]):
        """
        If ref_vals is specified, two switched indices that correspond to identical values will not be
        counted as wrong.

        Args:
            ref_idx:
            pred_idx:
            ref_vals:
            pred_mass:

        Returns:

        """
        # Check lists to be compared ar of equal length
        if len(ref_idx) != len(pred_idx):
            raise ValueError("Provided lists don't have the same length.")
        elif len(ref_idx) == 0:
            return 0

        # Map ref values to positions
        ref_pos = []
        prev_e = 0
        prev_i = 0
        for i, e in enumerate(ref_vals):
            if i > 0 and e == prev_e:
                ref_pos.append(prev_i)
            else:
                ref_pos.append(i)
                prev_e, prev_i = e, i

        if pred_mass:
            print(ref_pos)
            print("Vals\tR.Idx\tP.Idx\tMass\t\tDO")

        sum = 0
        # i: position in the ranking
        # e: index of the configuration at this position in the ranking
        # j: position of the configuration (e) in the predicted ranking
        for i, e in enumerate(ref_idx):
            j = pred_idx.index(e)
            # If the two indices (true and predicted) differ, but represent the same deficiency, distance = 0
            if ref_vals is not None and ref_vals[i] == ref_vals[j]:
                if pred_mass:
                    print(f"{ref_vals[i]:5.3f}\t{ref_idx[i]:3d}\t{j:3d}\t{pred_mass[j]:.8f}\t0")
                continue
            else:
                # sum += abs(i-j)  # Unweighted
                # sum += abs(i-j)/(i+1)  # Weighted by rank
                do = abs(ref_pos[i]-ref_pos[j]) * abs(ref_vals[i] - ref_vals[j])  # Weighted by deficiency of switched sites
                if pred_mass:
                    print(f"{ref_vals[i]:5.3f}\t{ref_idx[i]:3d}\t{j:3d}\t{pred_mass[j]:.8f}\t{do:5.3f}")
                sum += do

        pred_ro = sum/len(ref_idx)
        if pred_mass:
            print(f"Pred RO: {pred_ro}")

        return pred_ro


if __name__ == '__main__':
    # # Possible Area values:
    # # "AREA 1: SUBAREA 1" "AREA 1: SUBAREA 2" "AREA 2: SUBAREA 1" "AREA 2: SUBAREA 2"
    # # "AREA 3: SUBAREA 2" "AREA 4: SUBAREA 1" "AREA 4: SUBAREA 2" "AREA 5: SUBAREA 1" "AREA 5: SUBAREA 2"
    # # Possible Task values:
    # # "31" "34" "35" "77"
    def predict(args, write_to_file=True, overwrite=False):
        """

        Args:
            args: configuration to parse
            write_to_file: self-explanatory
            overwrite: if output file already exists, overwrite? If not, don't run this prediction again

        Returns: results of comparison to reference

        """
        date, nb_samples, lb, weight, lap_smoothing = args
        print(f"Number of samples: {nb_samples}\tLookback: {lb}\tWeight: {weight}\tLaplace smoothing: {lap_smoothing}")
        predictor = SamIam2Prediction(bn_file_root=os.path.join(TenForceConfig.SAMIAM_DATA_DIR,
                                                                f"bn_model2_alt_top5_{date}_{lb}{weight}_lsw{lap_smoothing}"))
        output_file = os.path.join(TenForceConfig.SAMIAM_DATA_DIR,
                                   f'ranking_{date}_top5_{str(nb_samples)}_{lb}{weight}_lsw{lap_smoothing}_wbd.txt')
        if write_to_file and not overwrite and os.path.isfile(output_file):
            return
        return predictor.compare_to_reference(os.path.join(TenForceConfig.SAMIAM_DATA_DIR,
                                                           f'bn_model2_alt_top5_{date}_{lb}{weight}_test.dat'),
                                              nb=nb_samples,
                                              output_file=output_file if write_to_file else None,
                                              nb_procs=10)

    # Do experiment regarding effect of number of samples on results
    if False:
        for nb in [2500, 5000, 7500, 10000, 12500, 15000, 17500, 20000, 25000, 30000, 40000, 50000]:
            print("------------------------------------------------------------")
            print(f"-------------------------  {nb}   -------------------------")
            print("------------------------------------------------------------")
            list_nb_samples = [nb]*10
            list_lb = ["nolb"]  # , "lb10", "lb25", "lb50", "lb100", "lb150", "lb200", "lb250", "lb300"]
            list_weight = [""]  # , "sq", "sqrt"]
            list_lap_smooth = [0]  # , 1, 2, 3, 4, 5]

            pred_res, ref_res = [], []
            at_run = 0
            for combi in product(list_nb_samples, list_lb, list_weight, list_lap_smooth):
                at_run += 1
                print(f"At run {at_run}")
                _nb_samples, _lb, _weight, _lap_smooth = combi
                # Ignore nonsensical combinations
                if _lb == "nolb" and _weight:
                    continue

                combi_res = predict(combi, write_to_file=False)

                pred_res.append(combi_res[0])
                ref_res.append(combi_res[1])
                print("============================================================")

            print("Sort by: [['Mass', 'PC80'], ['PC95', 'PC90', 'PC75'], ['PC90', 'PC80', 'PC50'], ['PC80', 'PC50']]")
            for j, res in enumerate([pred_res, ref_res]):
                if j == 0:
                    print("******************** prediction results ********************")
                else:
                    print("******************** reference results *********************")
                res = np.asarray(res).T
                print(res)
                print()
                print("Average:")
                avgs = np.average(res, axis=1)
                print(avgs)
                print("Std:")
                stds = np.std(res, axis=1)
                print(stds)
                pc = [stds[i]/avgs[i] if avgs[i] > 0 else 0 for i in range(len(avgs))]
                print("std/avg:")
                print(pc)
                print(f"Average ratio: {np.average(pc)}")
                print(f"Std. ratio   : {np.std(pc)}")

    # Generate results
    if False:
        date = '20180820'
        list_nb_samples = [1000]
        list_lb = ["nolb", "lb10", "lb25", "lb50", "lb100", "lb150", "lb200", "lb250", "lb350"]
        list_weight = ["", "sq", "sqrt"]
        list_lap_smooth = [0, 1, 2, 3, 4, 5]

        for combi in product(list_nb_samples, list_lb, list_weight, list_lap_smooth):
            _nb_samples, _lb, _weight, _lap_smooth = combi
            print(f"Config: nb_samples: {_nb_samples} -- lb: {_lb} -- weight: {_weight} -- ls: {_lap_smooth}")
            # Ignore nonsensical combinations
            if _lb == "nolb" and _weight:
                continue

            predict((date,) + combi, write_to_file=False)
            print("============================================================")

    # Compute custom metric
    if True:
        date = '20180603'  # '20180603' '20180617' '20180701' '20180715' '20180729'
        list_nb_samples = [10000]
        list_lb = ["lb25"]
        list_weight = ["sq"]  # ["sq", "sqrt"]
        list_lap_smooth = [1]

        ctors = 'top5'

        for combi in product(list_nb_samples, list_lb, list_weight, list_lap_smooth):
            _nb_samples, _lb, _weight, _lap_smooth = combi
            print(f"Config: nb_samples: {_nb_samples} -- lb: {_lb} -- weight: {_weight} -- ls: {_lap_smooth}")
            # Ignore nonsensical combinations
            if _lb == "nolb" and _weight:
                continue

            predictor = SamIam2Prediction(bn_file_root=os.path.join(TenForceConfig.SAMIAM_DATA_DIR,
                                                                    f"bn_model2_alt_{ctors}_{date}_{_lb}{_weight}_lsw{_lap_smooth}"))
            predictor.compute_custom_metric(os.path.join(TenForceConfig.SAMIAM_DATA_DIR,
                                                               f'bn_model2_alt_{ctors}_{date}_{_lb}{_weight}_test.dat'),
                                            nb=_nb_samples,
                                            output_file=None,
                                            nb_procs=10)

            print("============================================================")

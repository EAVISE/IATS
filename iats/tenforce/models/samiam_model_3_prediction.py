"""
A script to demonstrate the usage of a Bayesian model to decide which contractor to prioritize using
the 3rd SamIam model(s).
"""
import os

from tqdm import tqdm

from iats.tenforce.models.sample_samiam_model_3 import SampleSamIam3
import numpy as np

import matplotlib

from iats.tenforce.tenforce_config import TenForceConfig

matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


class SamIam3Prediction:

    def __init__(self):
        bn_file = os.path.join(TenForceConfig.HOME,
                               "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model3.net")
        self.sampler = SampleSamIam3(bn_file=bn_file)

    def roll_dice(self, configs: [], nb=1000):
        results = {}
        defis = {}

        fig = plt.figure()
        for i, config in enumerate(configs):
            print(f"Sampling for choice {i+1} of {len(configs)}...")
            samples = []
            for _ in tqdm(range(nb)):
                samples.append(self.sampler.sample(parents=config))
            results[i] = samples
            defis[i] = [sample[2] for sample in samples]

            ax = plt.subplot(len(configs), 1, i + 1)
            ax.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, align="mid", rwidth=0.5)
            ax2 = ax.twinx()
            ax2.hist(defis[i], bins=np.arange(0, 1.005, 0.01), density=True, cumulative=True,
                    color='r', align="left", rwidth=0.2)
            ax.set_title(f"{config['Contractor']}: {config['Task']} - {config['Area']}")
            ax.set_xlim(-0.01, 1.01)
            ax.set_xlabel("Deficiency")
            ax.set_ylabel("Bin prob. (%)")
            ax2.set_ylabel("Cumulative p")
        plt.tight_layout()
        plt.show()


if __name__ == '__main__':
    predictor = SamIam3Prediction()
    # Possible Area values:
    # "AREA 1: SUBAREA 1" "AREA 1: SUBAREA 2" "AREA 2: SUBAREA 1" "AREA 2: SUBAREA 2"
    # "AREA 3: SUBAREA 2" "AREA 4: SUBAREA 1" "AREA 4: SUBAREA 2" "AREA 5: SUBAREA 1" "AREA 5: SUBAREA 2")
    # Possible Task values:
    # "31" "34" "35" "77"
    # predictor.roll_dice(configs=[{"Contractor": "A : Sites", "Task": "34"},
    #                              {"Contractor": "G : Sites", "Task": "31"},
    #                              {"Contractor": "C : Sites", "Task": "31"}],
    #                     nb=2500, sampler_type=SamplerType.TASK)
    predictor.roll_dice(configs=[{"Contractor": "A : Sites", "Task": "31", "Area": "AREA 1: SUBAREA 1"},
                                 {"Contractor": "G : Sites", "Task": "31", "Area": "AREA 2: SUBAREA 2"},
                                 {"Contractor": "C : Sites", "Task": "31", "Area": "AREA 5: SUBAREA 1"}],
                        nb=1000)

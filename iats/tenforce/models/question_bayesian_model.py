"""
Simple Bayesian prior model for predicting the probability that a question will be answered "ok".
"""
import os
import pickle

import numpy as np

from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class QuestionBayesianModel:
    def __init__(self):
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Area
        # -Task
        # -Question
        # -Contractor
        data = pickle.load(open(os.path.join(TenForceConfig.DATA_DIR, "tenforce_data.pkl"), 'rb'))

        self.contractors = sorted(list(data["Sites : List"].unique()))
        self.tasks = sorted(list(data["Sites : Task nbr"].unique()))
        self.subareas = sorted(list(data["Sites : Subarea"].unique()))
        self.questions = sorted(list(data.Question_Id.unique()))

        data_file = os.path.join(TenForceConfig.DATA_DIR, "priors.pkl")
        if os.path.isfile(data_file):
            priors = pickle.load(open(data_file, 'rb'))
        else:
            priors = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.subareas),
                               len(self.questions)))

            print("Computing priors...")
            nb_contractors = len(self.contractors)
            nb_tasks = len(self.tasks)
            for ic, c in enumerate(self.contractors):
                da_c = data[data["Sites : List"] == c]
                for it, t in enumerate(self.tasks):
                    print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                          .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                    da_ct = da_c[da_c["Sites : Task nbr"] == t]
                    for isa, sa in enumerate(self.subareas):
                        da_ctsa = da_ct[da_ct["Sites : Subarea"] == sa]
                        for iq, q in enumerate(self.questions):

                            da_ctsaq = da_ctsa[da_ctsa["Question_Id"] == q]

                            nb_nok = len(da_ctsaq[(da_ctsaq['Compliancy'] == "NOK")])
                            nb_ok = len(da_ctsaq[(da_ctsaq['Compliancy'] == "OK")])
                            nb_sos = len(da_ctsaq[(da_ctsaq['Compliancy'] == "SOS")])

                            total = nb_nok + nb_ok + nb_sos

                            priors[ic, it, isa, iq] = nb_ok/total if total > 0 else 0
            print()
            pickle.dump(priors, open(data_file, 'wb'))

        self.priors = priors

    def get_prob_for(self, contractor=None, task=None, subarea=None, question_id: int=None):
        """

        Args:
            contractor:
            task:
            subarea:
            question_id:

        Returns:

        """
        if question_id is None:
            raise ValueError("A question_id is required.")
        elif type(question_id) is not int:
            raise TypeError("Expected an integer for question_id, got {} instead.".format(type(question_id)))

        idx_c = contractor if type(contractor) is int else\
            Tools.binary_search(self.contractors, contractor) if contractor else -1
        idx_t = Tools.binary_search(self.tasks, task) if task > len(self.tasks) else task
        idx_s = subarea if type(subarea) is int else\
            Tools.binary_search(self.subareas, subarea) if subarea else -1

        if idx_c > -1:
            if idx_t > -1:
                # All indices are known, return specific probability
                if idx_s > -1:
                    return self.priors[idx_c, idx_t, idx_s, question_id]
                # Return probabilities over subareas
                else:
                    return self.priors[idx_c, idx_t, :, question_id]
            else:
                # Return probabilities over tasks
                if idx_s > -1:
                    return self.priors[idx_c, :, idx_s, question_id]
                # Etc...
                else:
                    return self.priors[idx_c, :, :, question_id]
        else:
            if idx_t > -1:
                if idx_s > -1:
                    return self.priors[:, idx_t, idx_s, question_id]
                else:
                    return self.priors[:, idx_t, :, question_id]
            else:
                if idx_s > -1:
                    return self.priors[:, :, idx_s, question_id]
                else:
                    return self.priors[:, :, :, question_id]


if __name__ == '__main__':
    bm = QuestionBayesianModel()

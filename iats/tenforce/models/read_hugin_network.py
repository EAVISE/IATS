"""
This class is used to read a network described in a *.net Hugin file into memory. Concretely, this is needed
to load the trained network written to disk by SamIam.
"""
import os
from collections import defaultdict

import pomegranate as pg

from iats.tenforce.models.pomegranate_tools import ExtBayesianNetwork, ExtCPT
from iats.tenforce.tenforce_config import TenForceConfig


class ReadHuginNetwork:
    @staticmethod
    def read_file(fin):
        node_states = {}
        cpt_deps = {}
        cpts = {}

        b_in_node = False
        b_in_cpt = False
        node_name = None
        cpt_node = None  # node: node to which this cpt applies, deps: dependencies
        with open(fin, 'r') as fin:
            for line in fin:
                # End of a block, reset flags
                if line.strip() == '}':
                    b_in_node, b_in_cpt = False, False
                    continue
                # Continuation of node description
                if b_in_node:
                    if line.strip().startswith('states = '):
                        states = line.split('"')[1:-1:2]
                        # Add states to dictionary
                        node_states[node_name] = states
                    else:
                        continue
                # Continuation of potential description
                elif b_in_cpt:
                    cpt = []
                    # It's easier to loop over the lines of the CPT in this subloop
                    # First, get nb. of answers per dependent variable
                    nb_per_dep = []
                    for dep in cpt_deps[cpt_node]:
                        nb_per_dep.append(len(node_states[dep]))
                    # Keep track of "position" in variables
                    cntr_per_dep = [0 for _ in range(0, len(nb_per_dep))]
                    # Skip first line after 'potential', go to next one immediately
                    line = next(fin).strip()
                    while line != '}':
                        vals = line[line.rfind('(')+1:line.find(')')]
                        # Convert to numerical values
                        n_vals = []
                        for val in vals.split('\t'):
                            # Ignore empty entries
                            if val:
                                n_vals.append(float(val))
                        # Add probabilities to cpt
                        entry_trunk = []
                        for i, idx in enumerate(cntr_per_dep):
                            entry_trunk.append(node_states[cpt_deps[cpt_node][i]][idx])
                        for i, val in enumerate(n_vals):
                            entry = entry_trunk.copy()
                            entry.append(node_states[cpt_node][i])
                            entry.append(val)
                            cpt.append(entry)
                        # Update dep counters
                        for idx in range(len(cntr_per_dep)-1, -1, -1):
                            cntr_per_dep[idx] += 1
                            if cntr_per_dep[idx] == nb_per_dep[idx]:
                                cntr_per_dep[idx] = 0
                            # No need to update other indices
                            else:
                                break
                        line = next(fin).strip()
                    cpts[cpt_node] = cpt
                    b_in_cpt = False
                # This line defines the start of a new block
                if not line.startswith('\t'):
                    parts = line.strip().split(' ')
                    block_type = parts[0]
                    if block_type == 'node':
                        b_in_node = True
                        node_name = parts[1]
                    # Example of 'potential' line:
                    # potential ( RQ32_A | Contractor Task Topic_2 )
                    elif block_type == 'potential':
                        b_in_cpt = True
                        # Get part between brackets
                        line = line.split('(')[1].split(')')[0]
                        # Extract cpt_node
                        cpt_node = line.split('|')[0].strip()
                        deps = line.split('|')[1].split(' ')
                        # Strip and remove emtpy entries
                        deps = [deps[i].strip() for i in range(len(deps)) if deps[i].strip()]
                        cpt_deps[cpt_node] = deps
                    else:
                        continue

        # Get children for each node
        children = defaultdict(list)
        for k, v in cpt_deps.items():
            for p in v:
                children[p].append(k)

        # Create root node(s)
        nodes = {}
        created_nodes = set()
        roots = [node for node in cpt_deps.keys() if not cpt_deps[node]]
        for node in roots:
            # Convert array of configurations to dictionary
            cpt = {}
            for v in cpts[node]:
                cpt[v[0]] = v[1]
            # Create root node
            nodes[node] = pg.DiscreteDistribution(cpt)
            created_nodes.add(node)

        # Iteratively continue with children that can be created
        # For this, check that all parent nodes have already been created
        to_parse = [c_node for r_node in roots for c_node in children[r_node]
                    if set(cpt_deps[c_node]).issubset(created_nodes)]
        while to_parse:
            next_parse = []
            for node in to_parse:
                # Create list with required nodes
                dep_nodes = [nodes[d_node] for d_node in cpt_deps[node]]
                # nodes[node] = pg.ConditionalProbabilityTable(cpts[node], dep_nodes)
                nodes[node] = ExtCPT(cpts[node], dep_nodes)
                created_nodes.add(node)
                next_parse += [c_node for c_node in children[node] if set(cpt_deps[c_node]).issubset(created_nodes)]
            to_parse = list(set(next_parse))

        # Create states
        states = {}
        for k, v in nodes.items():
            states[k] = pg.State(nodes[k], k)
        # Create network and add states
        bn = ExtBayesianNetwork("Bayesian Network")
        for k, v in states.items():
            bn.add_state(v)
        # Add edges
        for parent, v in children.items():
            for child in v:
                bn.add_edge(states[parent], states[child])

        bn.bake()

        return bn


if __name__ == '__main__':
    net_file = os.path.join(TenForceConfig.HOME,
                            "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_2in1_rq_em.net")
    # net_file = os.path.join(TenforceConfig.HOME,
    #                         "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_cq.net")

    bn = ReadHuginNetwork.read_file(net_file)

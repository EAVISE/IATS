"""
Class used to hold tables of counts. These tables are similar to CPTs, except each entry holds the
number of times the configuration represented by this entry has been observed.
Besides holding the counts, this class also holds the confidence interval(s) relevant to each node. For
binary nodes this is a binary confidence interval, for multinomial nodes a multinomial confidence interval.

To retrieve the number of occurrences of a particular configuration, use the following syntax (assuming 'ct'
is a CountTable instance):
ct.cnts_for_node['Node'][tpl(...)]
where 'Node' should be replace with the name of the node for which you want to query a configuration, and 'tpl(...)'
represents a tuple of indices that correspond to the configuration. Say, e.g., that you want to know how many counts
there were of 'AREA 1: SUBAREA 2' for Contractor D, the line would become:
ct.cnts_for_node['Area'][(2,1)]
The values in the tuple refer to the indices of the named values in their corresponding arrays in the 'values' object.
Since this sounds like Chinese, here's another example. 'AREA 1: SUBAREA 2' has index 1 in the array contained in
'values['Array']', which lists all values the 'Array' variable can take, ordered alphabetically.

"""
import os
from itertools import product

import numpy as np

from iats.tenforce.tenforce_config import TenForceConfig
from iats.tenforce.tools.multinomial_ci import multinomial_ci


class CountTable:
    def __init__(self, cnt_file):
        print(f"Reading counts from file:\n{cnt_file}")
        values = {}  # Maps possible values for each variable to their index
        cnts_for_node = {}
        vars_for_node = {}
        node = None  # Will contain the name of the node being parsed
        vars = []  # This list will contain the variables (parents + node) whose counts
        # are read out in the current iteration
        b_init_mtx = True  # This boolean will be used to indicate the we are at the first line following
        # the listing of the variables for a particular node, and hence at the start
        # of the listing of the counts proper; at this point, a matrix should be initialized
        # first to contain the counts
        at_line = 0
        with open(cnt_file, "r") as fin:
            for line in fin:
                at_line += 1
                print(f"\rAt line: {at_line}", end="", flush=True)

                parts = line.strip().split("\t")
                # Start of node description
                if parts[0] == "#start":
                    node = parts[1]
                elif parts[0] == "#values":
                    var = parts[1]  # Variable whose values are described in this line
                    vars.append(var)
                    if var in values:
                        continue
                    vals = {parts[i]: i-2 for i in range(2, len(parts))}  # Values for this variable with their index
                    values[var] = vals
                # End of node description, reset stuff
                elif parts[0] == "#end":
                    vars = []
                    b_init_mtx = True
                else:
                    if b_init_mtx:
                        cnts_for_node[node] = np.zeros(tuple(len(values[x]) for x in vars))
                        vars_for_node[node] = vars
                        b_init_mtx = False
                    coor = tuple(values[vars[i]][parts[i]] for i in range(0, len(parts)-1))
                    cnts_for_node[node][coor] = np.double(parts[-1])
        print()
        self.values = values
        self.cnts_for_node = cnts_for_node
        self.vars_for_node = vars_for_node

        # self.cis_for_node = {}  # This dict will contain the confidence intervals
        # alpha = 0.05
        # at_key = 0
        # for k, v in self.cnts_for_node.items():
        #     at_key += 1
        #     if at_key % 10 == 0:
        #         print(f"\rComputing confidence intervals for node {at_key} of {len(self.cnts_for_node)}", end='')
        #     if len(v.shape) == 1:
        #         mtx = multinomial_ci(v, alpha)
        #     else:
        #         mtx = np.zeros(v.shape + (2,))
        #         for p in product(*(list(range(v.shape[i])) for i in range(len(v.shape)-1))):
        #             if sum(v[p]) == 0:
        #                 continue
        #             mtx[p] = multinomial_ci(v[p], alpha)
        #     self.cis_for_node[k] = mtx
        # print(f"\rComputing confidence intervals for node {at_key} of {len(self.cnts_for_node)}")

    # TODO: update docu
    def get_cnt(self, node: str, config: tuple, lp_smooth: int = 0):
        """

        Args:
            node: name of the node for which you want to retrieve counts
            config: tuple representing the configuration for which you want to retrieve counts,
                e.g., a valid config for node 'Area' would be ('A : Sites', 'AREA 1: SUBAREA 1')
            lp_smooth: integer representing the Laplace smoothing to apply; default = 0 = no smoothing

        Returns: how many times this configuration has been observed in the training data

        """
        if lp_smooth < 0:
            raise ValueError("Laplace smoothing parameter should be a positive integer.")
        coor = tuple(self.values[self.vars_for_node[node][i]][config[i]] for i in range(0, len(config)))
        subcoor = coor[:-1]
        total = sum(self.cnts_for_node[node][subcoor + (v,)]
                     for k, v in self.values[self.vars_for_node[node][-1]].items())

        cnt = self.cnts_for_node[node][coor]

        # Apply Lapace smoothing
        if lp_smooth:
            total += len(self.values[self.vars_for_node[node][-1]])*lp_smooth
            cnt += lp_smooth

        return cnt, total, cnt/total if total > 0 else 0


if __name__ == '__main__':
    bn_out_file = os.path.join(TenForceConfig.HOME,
                               "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_alt_area.counts")
    ct = CountTable(bn_out_file)
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 1: SUBAREA 1')))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 1: SUBAREA 2')))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 2: SUBAREA 1')))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 2: SUBAREA 2')))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 3: SUBAREA 2')))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 1: SUBAREA 1'), lp_smooth=1))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 1: SUBAREA 2'), lp_smooth=1))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 2: SUBAREA 1'), lp_smooth=1))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 2: SUBAREA 2'), lp_smooth=1))
    print(ct.get_cnt_for('Area', ('A : Sites', 'AREA 3: SUBAREA 2'), lp_smooth=1))
    print('hihi')

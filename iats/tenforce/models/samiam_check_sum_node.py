"""
Script to check the correctness of the "Sum" node in *.dat files.
"""
import os
from collections import Counter, defaultdict

from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


def check_file(filename):
    b_first = True

    idx_sum = None
    start_rq = None
    defis = defaultdict(list)
    with open(filename, 'r') as fin:
        for line in fin:
            line = line.strip().split(',')
            # Skip header
            if b_first:
                idx_sum = line.index("Sum")
                for start_rq, e in enumerate(line):
                    if e.startswith("RQ"):
                        break
                b_first = False
                continue
            cnts = Counter(line[start_rq:])
            defi = 0
            if cnts["NOK"] or cnts["OK"] or cnts["SOS"]:
                defi = cnts["NOK"]/(cnts["NOK"] + cnts["OK"] + cnts["SOS"])
            if line[idx_sum] == "T" and defi != 1:
                print("WASSUUUUUUUUP?")
                print(line)
                print(cnts)
                print()
            defis[line[0]].append(defi)

    for ctor in sorted(defis.keys()):
        print(f"Nb. critical sites: {sum([1 if d == 1 else 0 for d in defis[ctor]])}")

    nb_ctors = len(defis)
    fig = plt.figure()
    for i, ctor in enumerate(sorted(defis.keys())):
        ax = plt.subplot(nb_ctors, 1, i+1)
        ax.plot(defis[ctor], '.b')
        ax.set_ylim(-0.01, 1.02)
        ax.set_ylabel("Deficiency")
        ax.set_xlabel(ctor)
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    fn = os.path.join(TenForceConfig.HOME,
                      "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_data_area.dat")
    check_file(fn)

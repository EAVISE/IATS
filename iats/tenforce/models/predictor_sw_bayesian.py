"""
Predictor: Sliding Window Bayesian
Experiment where a sliding window over sites is used to compute Bayesian priors, which are then
applied to predict the deficiency of the next site.
Spoiler alert: this doesn't work.
"""

import matplotlib

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.models.topic_bayesian_model import TopicBayesianModel
from iats.utils.tools import Tools

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


def plot_chron_def(df, plt_title="Deficiencies"):
    df = TFDataAnalysis.sort_chronologically(df)
    ctor_sites = df["Sites : Nr"].unique()
    # Deficiencies
    ctor_defs = []
    for site in ctor_sites:
        df_s = df[df["Sites : Nr"] == site]
        deficiency = TFDataAnalysis.get_deficiency_for_df(df_s)
        ctor_defs.append(deficiency)

    plt.figure()
    plt.title(plt_title)
    plt.plot(ctor_defs)
    plt.show()


def main():
    da = TFDataAnalysis()

    contractors = list(da.data["Sites : List"].unique())
    tasks = list(da.data["Sites : Task nbr"].unique())
    subareas = sorted(list(da.data["Sites : Subarea"].unique()))

    # Sites per contractor
    nbsites_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Nr'].unique())
                            for c in sorted(contractors)]
    # Get list with sorted indices of contractors according to descending amount of nb of sites served
    _, most_active_ctors_idxs = Tools.sort_with_indices(nbsites_per_contractor, b_reverse=True)
    most_active_ctors = [sorted(contractors)[i] for i in most_active_ctors_idxs]

    ctor1 = most_active_ctors[0]
    df_c1 = da.data[da.data["Sites : List"] == ctor1]
    df_c1 = da.sort_chronologically(df_c1)
    c1_sites = df_c1["Sites : Nr"].unique()

    # plot_chron_def(df_c1)
    #
    # for area in subareas:
    #     df_a = df_c1[df_c1["Sites : Subarea"] == area]
    #     if df_a.empty:
    #         continue
    #     plot_chron_def(df_a, "Deficiencies for area {}".format(area))

    # The number of sites for the most active contractors is in the order of hundreds.
    # This is insufficient to be able to train a neural network.
    # We will experiment with a Bayesian prior matrix, computed over a sliding window.
    # See what happens when you give more weight to more recent observations.

    mw = 100  # Size moving window
    targets = []
    preds = []
    done = 0
    for i in range(mw, len(c1_sites)-1):
        done += 1
        if done == 101:
            break
        print("At prediction {}...".format(done))
        df_mw = df_c1[df_c1["Sites : Nr"].isin(set(c1_sites[i-mw:mw]))]
        df_target = df_c1[df_c1["Sites : Nr"] == c1_sites[i]]

        defi = da.get_deficiency_for_df(df_target)
        targets.append(defi)

        task = df_target.iloc[0]["Sites : Task nbr"]
        sa = df_target.iloc[0]["Sites : Subarea"]

        if task not in df_mw["Sites : Task nbr"].unique() or sa not in df_mw["Sites : Subarea"].unique():
            preds.append(0)
            continue

        tbm = TopicBayesianModel(df_mw)
        pred = tbm.get_prob_for(ctor1, task, sa)
        preds.append(pred)
        print("{:5.3f} vs {:5.3f}".format(pred, defi))

    plt.figure()

    plt.plot(targets, '.r')
    plt.plot(preds, '.b')
    plt.xlabel('Site')
    plt.ylabel('Deficiency')

    plt.legend(["Target", "Prediction"])

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()

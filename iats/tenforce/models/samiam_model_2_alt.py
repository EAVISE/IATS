"""
An alternative 2nd SamIam model that initializes the CPTs directly from the train data, instead of using EM.

"""
from datetime import date, timedelta
from enum import Enum
from multiprocessing import Pool

import math
import os
import pickle
from collections import defaultdict
from itertools import product

import pandas as pd

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tools.samiam_utils import SamIamUtils
from iats.tenforce.tools.count_extractor import CountExtractor
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class TypeOfWeight(Enum):
    NONE = 'none'
    DEFAULT = 'default'
    SQUARE = 'square'
    SQRT = 'sqrt'


class SamIamModel2Alt:
    def __init__(self, nb_cqs=3):
        # qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        # self.qr, self.qcnts, self.qr_na, self.qcnts_na = pickle.load(open(qr_file, 'rb'))

        # Group questions per topic
        # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
        # the list of questions; hence it only related to questions that truly appear in the data itself.
        tm = TopicMapper()
        qns_per_topic = defaultdict(list)
        for qn_id in sorted(tm.topic_idx_per_qn.keys()):
            topic_idx = tm.topic_idx_per_qn[qn_id]
            qns_per_topic[topic_idx].append(qn_id)
        self.qns_per_topic = qns_per_topic

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.cqs = CriticalQuestions(keep_top=nb_cqs)

        self.cq_bnm, self._cq_root, self._cq_children = None, None, None
        self.rq_bnm, self._rq_root, self._rq_children = None, None, None

        self.uqf_area, self.uqf_task = None, None

    def initialize_vars(self, df: pd.DataFrame, data_file_root, regenerate_train_data=False,
                        lookback_days=math.inf, cutoff_date=None, test_date_range=0,
                        type_of_weight=TypeOfWeight.DEFAULT):
        """
        Extract some variables from a dataframe; these values define what nodes will be created
        in the Bayesian network.

        Also generates the necessary training data that is then written to disk.

        Args:
            df: the DataFrame to use to extract the variables
            data_file_root:
            regenerate_train_data:
            lookback_days:
            cutoff_date:
            test_date_range:
            type_of_weight:

        Returns:

        """
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        if regenerate_train_data or not os.path.isfile(data_file_root + "_area.dat"):
            if cutoff_date is None:
                self.generate_training_data(data_file_root, self.contractors)
            else:
                self.generate_training_data(data_file_root, self.contractors, lookback_days=lookback_days,
                                            cutoff_date=cutoff_date, test_date_range=test_date_range,
                                            type_of_weight=type_of_weight)
        self.uqf_area = CountExtractor(data_file_root + "_area.dat")
        self.uqf_task = CountExtractor(data_file_root + "_task.dat")

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _get_area(self, a: str or int):
        if type(a) is int:
            return a
        return Tools.binary_search(self.subareas, a)

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def compile_bns(self, out_file_root: str, b_weighted=False, lap_smooth: int = 0, lap_weight: float = 1.0):
        """
        Convenience method that will call compile_bn_area() and compile_bn_task().
        The provided file name to this method is assumed to be a "root" that will be completed with
        "_area.net" for the area network, and "_task.net" for the task network.

        Args:
            out_file_root: generated file names will use this as base
            b_weighted: weigh samples?
            lap_smooth: Laplace smoothing to be applied
            lap_weight: Laplace weight to be applied (see CountExtractor.get_cnt())

        Returns:

        """
        self.compile_bn_area(out_file_root + "_area.net", b_weighted, lap_smooth, lap_weight)
        self.compile_bn_task(out_file_root + "_task.net", b_weighted, lap_smooth, lap_weight)

    def compile_bn_area(self, out_file: str, b_weighted=False, lap_smooth: int = 0, lap_weight: float = 1.0):
        """
        Compile Bayesian Network using (sub)Area nodes

        Returns:

        """
        # This string will contain all counts for all variables
        # These counts are the TRUE counts, i.e., no weighting or Laplace smoothing is applied
        # It will be written to a file at the end of this method
        # The format used is the following:
        # The start and end of a node's count description is indicated by #start and #end tags,
        # followed by the name of the node.
        # The #start tag is followed by lines describing the parent and current node variables
        # and the values they can take. This makes it easier when reading the file into memory
        # later to initialize an appropriate matrix
        # After this, the counts proper are listed
        # counts = ""

        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120

        # ##############################
        # Create distributions
        # Root node: contractor
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))

        # counts += f"#start\t{var_ctor}\n"
        # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"

        s_cpt = "("
        for ctor in self.contractors:
            prior = self.uqf_area.get_cnt({'Contractor': ctor}, b_weighted=b_weighted,
                                          lap_smooth=lap_smooth, lap_weight=lap_weight)
            # cnt = prior[0] if not b_weighted else self.uqf_area.get_cnt({'Contractor': ctor}, b_weighted=False)[0]
            s_cpt += f"\t{prior[2]}"
            # counts += f"{ctor}\t{cnt}\n"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))
        # counts += f"#end\t{var_ctor}\n"

        # Contractor -> Area
        var_area = "Area"

        # counts += f"#start\t{var_area}\n"
        # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
        # counts += f"#values\t{var_area}\t" + '\t'.join(self.subareas) + "\n"

        s_cpt = "("
        for i, c in enumerate(self.contractors):
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for a in self.subareas:
                prior = self.uqf_area.get_cnt({'Contractor': c}, ('Area', a),
                                              b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                # cnt = prior[1] if not b_weighted else\
                #     self.uqf_area.get_cnt({'Contractor': c}, ('Area', a), b_weighted=False)[1]
                s_cpt += f"\t{prior[2]}"
                # counts += f"{c}\t{a}\t{cnt}\n"
            s_cpt += "\t)"
        s_cpt += ")"
        nodes.append(SamIamUtils.create_node(name=var_area, states=self.subareas, pos=(pos_x, pos_y-delta_y), id="Area"))
        potentials.append(SamIamUtils.create_potential(var=var_area, cond_on=[var_ctor], cpt=s_cpt))
        # counts += f"#end\t{var_area}\n"

        # Area -> Critical Questions: What answer, if answered at all?
        q_ans = ["NoAns", "OK", "SOS", "NOK", "NA"]
        cq_ids = sorted(self.cqs.get_critical_ids())
        nb_cqs = len(cq_ids)
        var_cqs_a = []

        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_A"

            # counts += f"#start\t{var}\n"
            # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
            # counts += f"#values\t{var_area}\t" + '\t'.join(self.subareas) + "\n"
            # counts += f"#values\t{var}\t" + '\t'.join(q_ans) + "\n"

            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nba, a in enumerate(self.subareas):
                    if nba > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for ans in q_ans:
                        prior = self.uqf_area.get_cnt({'Contractor': c, 'Area': a}, (var, ans), b_weighted=b_weighted,
                                                      lap_smooth=lap_smooth, lap_weight=lap_weight)
                        # cnt = prior[1] if not b_weighted else\
                        #     self.uqf_area.get_cnt({'Contractor': c, 'Area': a}, (var, ans), b_weighted=False)[1]
                        s_cpt += f"\t{prior[2]}"
                        # counts += f"{c}\t{a}\t{ans}\t{cnt}\n"
                    s_cpt += "\t)"
                s_cpt += ")"
            s_cpt += ")"
            # counts += f"#end\t{var}\n"

            var_cqs_a.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                 pos=(pos_x + (-(nb_cqs//2) + i)*delta_x, pos_y - 2*delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_area], cpt=s_cpt))

        # Critical Question: Critical?
        var_cqs_crit = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Crit"

            # counts += f"#start\t{var}\n"
            # counts += f"#values\t{var_cqs_a[i]}\t" + '\t'.join(q_ans) + "\n"
            # counts += f"#values\t{var}\t" + '\t'.join(["T", "F"]) + "\n"

            s_cpt = "("
            for nbans, ans in enumerate(q_ans):
                if nbans > 0:
                    s_cpt += "\n\t\t"
                prior = self.uqf_area.get_cnt({var_cqs_a[i]: ans}, (var, "T"), b_weighted=b_weighted,
                                              lap_smooth=lap_smooth, lap_weight=lap_weight)
                # cnt = prior[1] if not b_weighted else\
                #     self.uqf_area.get_cnt({var_cqs_a[i]: ans}, (var, "T"), b_weighted=False)[1]
                s_cpt += f"(\t{prior[2]}\t{1-prior[2]}\t)"
                # counts += f"{ans}\t{cnt}\n"
            s_cpt += ")"
            # counts += f"#end\t{var}\n"

            var_cqs_crit.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["T", "F"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 3*delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_cqs_a[i]], cpt=s_cpt))

        # Group CQ answers in one node
        var_sum = "Sum"

        # counts += f"#start\t{var_sum}\n"
        # for var in var_cqs_crit:
        #     counts += f"#values\t{var}\t" + '\t'.join(["T", "F"]) + "\n"

        s_cpt = len(var_cqs_crit) * "("
        at_iter = -1
        cntr_per_dep = [0 for _ in range(len(var_cqs_crit))]
        nb_reset = 0  # Nb. of counters reset to 0
        for prod in product(["T", "F"], repeat=len(var_cqs_crit)):
            # cnt = self.uqf_area.get_cnt(parents, ("Sum", "T"), b_weighted=False)[1]
            at_iter += 1
            if at_iter > 0:
                s_cpt += "\n\t\t"
            # Write necessary opening brackets
            s_cpt += nb_reset*"("
            if prod == ('F',)*len(var_cqs_crit):
                s_cpt += "(\t0\t1\t)"
            else:
                s_cpt += "(\t1\t0\t)"
            # counts += '\t'.join(prod) + f"\t{cnt}\n"
            # Update dep counters
            nb_reset = 0
            for idx in range(len(cntr_per_dep) - 1, -1, -1):
                cntr_per_dep[idx] += 1
                if cntr_per_dep[idx] == 2:
                    cntr_per_dep[idx] = 0
                    nb_reset += 1
                    s_cpt += f")"
                # No need to update other indices
                else:
                    break
        # counts += f"#end\t{var_sum}\n"

        nodes.append(SamIamUtils.create_node(name=var_sum, states=["T", "F"],
                                             pos=(pos_x, pos_y - 4*delta_y), id=var_sum))
        potentials.append(SamIamUtils.create_potential(var=var_sum, cond_on=var_cqs_crit, cpt=s_cpt))

        # Continue with topics
        # Per Topic, add hidden node and questions belonging to this topic
        nb_topics = len(self.qns_per_topic)
        nb_qstns = len(self.qstns)
        at_qstn = -1  # Counter that will be used to determine position of node
        for tid in range(nb_topics):
            print(f"\rAt topic {tid}...", end='')
            nb_t_qns = len(self.qns_per_topic[tid])
            var_t = f"Topic_{tid}"

            # counts += f"#start\t{var_t}\n"
            # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
            # counts += f"#values\t{var_area}\t" + '\t'.join(self.subareas) + "\n"
            # counts += f"#values\t{var_sum}\t" + '\t'.join(["T", "F"]) + "\n"
            # counts += f"#values\t{var_t}\t" + '\t'.join(["T", "F"]) + "\n"

            # Create initial CPT for topic; this depends on
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nba, a in enumerate(self.subareas):
                    if nba > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbs, s in enumerate(["T", "F"]):
                        if nbs > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        # cnt_true = self.uqf_area.get_cnt({'Contractor': c, 'Area': a, 'Sum': s}, (var_t, "T"), b_weighted=False)[1]
                        if s == 'T':
                            s_cpt += "\t0\t1\t"
                        else:
                            prior = self.uqf_area.get_cnt({'Contractor': c, 'Area': a, 'Sum': s}, (var_t, "F"),
                                                          b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                            # cnt_false = prior[1] if not b_weighted else\
                            #     self.uqf_area.get_cnt({'Contractor': c, 'Area': a, 'Sum': s}, (var_t, "F"), b_weighted=False)[1]
                            s_cpt += f"\t{1-prior[2]}\t{prior[2]}\t"
                        s_cpt += ")"
                        # counts += f"{c}\t{a}\t{s}\tT\t{cnt_true}\n"
                        # counts += f"{c}\t{a}\t{s}\tF\t{cnt_false}\n"
                    s_cpt += ")"
                s_cpt += ")"
            s_cpt += ")"
            # counts += f"#end\t{var_t}\n"

            # At this point, at_qstn indicates the number of questions that have already been seen
            # This information is used to compute the x-offset of the topic node
            x_offset = -(nb_qstns//2) + at_qstn+1 + nb_t_qns//2
            nodes.append(SamIamUtils.create_node(name=var_t, states=["T", "F"],
                                                 pos=(pos_x + x_offset*delta_x, pos_y - 5*delta_y), id=var_t))
            potentials.append(SamIamUtils.create_potential(var=var_t, cond_on=[var_ctor, var_area, var_sum], cpt=s_cpt))

            var_rq_a = []
            for i, qid in enumerate(self.qns_per_topic[tid]):
                at_qstn += 1
                var = f"RQ{qid}_A"

                if qid not in self.cqs.get_critical_ids():
                    # counts += f"#start\t{var}\n"
                    # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
                    # counts += f"#values\t{var_area}\t" + '\t'.join(self.subareas) + "\n"
                    # counts += f"#values\t{var_t}\t" + '\t'.join(["T", "F"]) + "\n"
                    # counts += f"#values\t{var}\t" + '\t'.join(q_ans) + "\n"
                    s_cpt = "("
                    for nbc, c in enumerate(self.contractors):
                        if nbc > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        for nba, a in enumerate(self.subareas):
                            if nba > 0:
                                s_cpt += "\n\t\t"
                            s_cpt += "("
                            for nbt, t in enumerate(["T", "F"]):
                                if nbt > 0:
                                    s_cpt += "\n\t\t"
                                s_cpt += "("
                                for state in q_ans:
                                    if t == 'F':
                                        s_cpt += "\t1" if state == 'NoAns' else "\t0"
                                    else:
                                        prior = self.uqf_area.get_cnt({'Contractor': c, 'Area': a, var_t: t}, (var, state),
                                                                      b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                                        # cnt = prior[1] if not b_weighted else\
                                        #     self.uqf_area.get_cnt({'Contractor': c, 'Area': a, var_t: t}, (var, state), b_weighted=False)[1]
                                        s_cpt += f"\t{prior[2]}"
                                        # counts += f"{c}\t{a}\t{t}\t{state}\t{cnt}\n"
                                s_cpt += "\t)"
                            s_cpt += ")"
                        s_cpt += ")"
                    s_cpt += ")"
                    # counts += f"#end\t{var}\n"

                    var_rq_a.append(var)
                    nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                         pos=(pos_x + (-(nb_qstns//2) + at_qstn)*delta_x, pos_y - 6*delta_y), id=var))
                    potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_area, var_t], cpt=s_cpt))
                # This is a critical node: separate it from the topics and make it take the same value as its
                # corresponding CQ node.
                else:
                    s_cpt = "("
                    for nbans, ans in enumerate(q_ans):
                        if nbans > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        for state in q_ans:
                            if ans == state:
                                s_cpt += f"\t{1.0}"
                            else:
                                s_cpt += f"\t{0.0}"
                        s_cpt += "\t)"
                    s_cpt += ")"

                    var_rq_a.append(var)
                    nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                         pos=(pos_x + (-(nb_qstns//2) + at_qstn)*delta_x, pos_y - 6*delta_y), id=var))
                    potentials.append(SamIamUtils.create_potential(var=var, cond_on=[f"CQ{qid}_A"], cpt=s_cpt))
        print()

        # String representing network that will be written to *.net file
        s_nw = """net
{
    node_size = (130 55);
}

"""
        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)
        # with open(out_file.replace(".net", ".counts"), "w") as fout:
        #     fout.write(counts)

        print(f"SamIamModel2 BayesianNetwork written to file:\n{out_file}")

    def compile_bn_task(self, out_file: str, b_weighted=False, lap_smooth: int = 0, lap_weight: float = 1.0):
        """
        Compile Bayesian Network using Task nodes

        Returns:

        """
        # This string will contain all counts for all variables
        # These counts are the TRUE counts, i.e., no weighting or Laplace smoothing is applied
        # It will be written to a file at the end of this method
        # The format used is the following:
        # The start and end of a node's count description is indicated by #start and #end tags,
        # followed by the name of the node.
        # The #start tag is followed by lines describing the parent and current node variables
        # and the values they can take. This makes it easier when reading the file into memory
        # later to initialize an appropriate matrix
        # After this, the counts proper are listed
        # counts = ""

        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120

        # ##############################
        # Create distributions
        # Root node: contractor
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))

        # counts += f"#start\t{var_ctor}\n"
        # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"

        s_cpt = "("
        for ctor in self.contractors:
            prior = self.uqf_task.get_cnt({'Contractor': ctor}, b_weighted=b_weighted,
                                          lap_smooth=lap_smooth, lap_weight=lap_weight)
            # cnt = prior[0] if not b_weighted else self.uqf_task.get_cnt({'Contractor': ctor}, b_weighted=False)[0]
            s_cpt += f"\t{prior[2]}"
            # counts += f"{ctor}\t{cnt}\n"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))
        # counts += f"#end\t{var_ctor}\n"

        # Contractor -> Task
        var_task = "Task"

        # counts += f"#start\t{var_task}\n"
        # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
        # counts += f"#values\t{var_task}\t" + '\t'.join(str(self.tasks)) + "\n"

        s_cpt = "("
        for i, c in enumerate(self.contractors):
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for t in self.tasks:
                prior = self.uqf_task.get_cnt({'Contractor': c}, ('Task', t),
                                              b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                # cnt = prior[1] if not b_weighted else \
                #     self.uqf_task.get_cnt({'Contractor': c}, ('Task', t), b_weighted=False)[1]
                s_cpt += f"\t{prior[2]}"
                # counts += f"{c}\t{t}\t{cnt}\n"
            s_cpt += "\t)"
        s_cpt += ")"
        nodes.append(
            SamIamUtils.create_node(name=var_task, states=self.tasks, pos=(pos_x, pos_y - delta_y), id="Task"))
        potentials.append(SamIamUtils.create_potential(var=var_task, cond_on=[var_ctor], cpt=s_cpt))
        # counts += f"#end\t{var_task}\n"

        # Task -> Critical Questions: What answer, if answered at all?
        q_ans = ["NoAns", "OK", "SOS", "NOK", "NA"]
        cq_ids = sorted(self.cqs.get_critical_ids())
        nb_cqs = len(cq_ids)
        var_cqs_a = []

        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_A"

            # counts += f"#start\t{var}\n"
            # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
            # counts += f"#values\t{var_task}\t" + '\t'.join([str(t) for t in self.tasks]) + "\n"
            # counts += f"#values\t{var}\t" + '\t'.join(q_ans) + "\n"

            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for ans in q_ans:
                        prior = self.uqf_task.get_cnt({'Contractor': c, 'Task': t}, (var, ans), b_weighted=b_weighted,
                                                      lap_smooth=lap_smooth, lap_weight=lap_weight)
                        # cnt = prior[1] if not b_weighted else \
                        #     self.uqf_task.get_cnt({'Contractor': c, 'Task': t}, (var, ans), b_weighted=False)[1]
                        s_cpt += f"\t{prior[2]}"
                        # counts += f"{c}\t{t}\t{ans}\t{cnt}\n"
                    s_cpt += "\t)"
                s_cpt += ")"
            s_cpt += ")"
            # counts += f"#end\t{var}\n"

            var_cqs_a.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 2 * delta_y),
                                                 id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task], cpt=s_cpt))

        # Critical Question: Critical?
        var_cqs_crit = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Crit"

            # counts += f"#start\t{var}\n"
            # counts += f"#values\t{var_cqs_a[i]}\t" + '\t'.join(q_ans) + "\n"
            # counts += f"#values\t{var}\t" + '\t'.join(["T", "F"]) + "\n"

            s_cpt = "("
            for nbans, ans in enumerate(q_ans):
                if nbans > 0:
                    s_cpt += "\n\t\t"
                prior = self.uqf_task.get_cnt({var_cqs_a[i]: ans}, (var, "T"), b_weighted=b_weighted,
                                              lap_smooth=lap_smooth, lap_weight=lap_weight)
                # cnt = prior[1] if not b_weighted else \
                #     self.uqf_task.get_cnt({var_cqs_a[i]: ans}, (var, "T"), b_weighted=False)[1]
                s_cpt += f"(\t{prior[2]}\t{1 - prior[2]}\t)"
                # counts += f"{ans}\t{cnt}\n"
            s_cpt += ")"
            # counts += f"#end\t{var}\n"

            var_cqs_crit.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["T", "F"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 3 * delta_y),
                                                 id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_cqs_a[i]], cpt=s_cpt))

        # Group CQ answers in one node
        var_sum = "Sum"

        # counts += f"#start\t{var_sum}\n"
        # for var in var_cqs_crit:
        #     counts += f"#values\t{var}\t" + '\t'.join(["T", "F"]) + "\n"

        s_cpt = len(var_cqs_crit) * "("
        at_iter = -1
        cntr_per_dep = [0 for _ in range(len(var_cqs_crit))]
        nb_reset = 0  # Nb. of counters reset to 0
        for prod in product(["T", "F"], repeat=len(var_cqs_crit)):
            # cnt = self.uqf_area.get_cnt(parents, ("Sum", "T"), b_weighted=False)[1]
            at_iter += 1
            if at_iter > 0:
                s_cpt += "\n\t\t"
            # Write necessary opening brackets
            s_cpt += nb_reset * "("
            if prod == ('F',) * len(var_cqs_crit):
                s_cpt += "(\t0\t1\t)"
            else:
                s_cpt += "(\t1\t0\t)"
            # counts += '\t'.join(prod) + f"\t{cnt}\n"
            # Update dep counters
            nb_reset = 0
            for idx in range(len(cntr_per_dep) - 1, -1, -1):
                cntr_per_dep[idx] += 1
                if cntr_per_dep[idx] == 2:
                    cntr_per_dep[idx] = 0
                    nb_reset += 1
                    s_cpt += f")"
                # No need to update other indices
                else:
                    break
        # counts += f"#end\t{var_sum}\n"

        nodes.append(SamIamUtils.create_node(name=var_sum, states=["T", "F"],
                                             pos=(pos_x, pos_y - 4 * delta_y), id=var_sum))
        potentials.append(SamIamUtils.create_potential(var=var_sum, cond_on=var_cqs_crit, cpt=s_cpt))

        # Continue with topics
        # Per Topic, add hidden node and questions belonging to this topic
        nb_topics = len(self.qns_per_topic)
        nb_qstns = len(self.qstns)
        at_qstn = -1  # Counter that will be used to determine position of node
        for tid in range(nb_topics):
            print(f"\rAt topic {tid}...", end='')
            nb_t_qns = len(self.qns_per_topic[tid])
            var_tc = f"Topic_{tid}"

            # counts += f"#start\t{var_tc}\n"
            # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
            # counts += f"#values\t{var_task}\t" + '\t'.join([str(t) for t in self.tasks]) + "\n"
            # counts += f"#values\t{var_sum}\t" + '\t'.join(["T", "F"]) + "\n"
            # counts += f"#values\t{var_tc}\t" + '\t'.join(["T", "F"]) + "\n"

            # Create initial CPT for topic; this depends on
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbs, s in enumerate(["T", "F"]):
                        if nbs > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        # cnt_true = self.uqf_task.get_cnt({'Contractor': c, 'Task': t, 'Sum': s}, (var_tc, "T"),
                        #                                  b_weighted=False)[1]
                        if s == 'T':
                            s_cpt += "\t0\t1\t"
                        else:
                            prior = self.uqf_task.get_cnt({'Contractor': c, 'Task': t, 'Sum': s}, (var_tc, "F"),
                                                          b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                            # cnt_false = prior[1] if not b_weighted else\
                            #     self.uqf_area.get_cnt({'Contractor': c, 'Area': a, 'Sum': s}, (var_t, "F"), b_weighted=False)[1]
                            s_cpt += f"\t{1-prior[2]}\t{prior[2]}\t"
                        s_cpt += ")"
                        # counts += f"{c}\t{t}\t{s}\tT\t{cnt_true}\n"
                        # counts += f"{c}\t{t}\t{s}\tF\t{cnt_false}\n"
                    s_cpt += ")"
                s_cpt += ")"
            s_cpt += ")"
            # counts += f"#end\t{var_tc}\n"

            # At this point, at_qstn indicates the number of questions that have already been seen
            # This information is used to compute the x-offset of the topic node
            x_offset = -(nb_qstns // 2) + at_qstn + 1 + nb_t_qns // 2
            nodes.append(SamIamUtils.create_node(name=var_tc, states=["T", "F"],
                                                 pos=(pos_x + x_offset * delta_x, pos_y - 5 * delta_y), id=var_tc))
            potentials.append(
                SamIamUtils.create_potential(var=var_tc, cond_on=[var_ctor, var_task, var_sum], cpt=s_cpt))

            var_rq_a = []
            for i, qid in enumerate(self.qns_per_topic[tid]):
                at_qstn += 1
                var = f"RQ{qid}_A"

                if qid not in self.cqs.get_critical_ids():
                    # counts += f"#start\t{var}\n"
                    # counts += f"#values\t{var_ctor}\t" + '\t'.join(self.contractors) + "\n"
                    # counts += f"#values\t{var_task}\t" + '\t'.join([str(t) for t in self.tasks]) + "\n"
                    # counts += f"#values\t{var_tc}\t" + '\t'.join(["T", "F"]) + "\n"
                    # counts += f"#values\t{var}\t" + '\t'.join(q_ans) + "\n"
                    s_cpt = "("
                    for nbc, c in enumerate(self.contractors):
                        if nbc > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        for nbt, t in enumerate(self.tasks):
                            if nbt > 0:
                                s_cpt += "\n\t\t"
                            s_cpt += "("
                            for nbtc, tc in enumerate(["T", "F"]):
                                if nbtc > 0:
                                    s_cpt += "\n\t\t"
                                s_cpt += "("
                                for state in q_ans:
                                    if tc == 'F':
                                        s_cpt += "\t1" if state == 'NoAns' else "\t0"
                                    else:
                                        prior = self.uqf_task.get_cnt({'Contractor': c, 'Task': t, var_tc: tc}, (var, state),
                                                                      b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
                                        # cnt = prior[1] if not b_weighted else \
                                        #     self.uqf_task.get_cnt({'Contractor': c, 'Task': t, var_tc: tc}, (var, state),
                                        #                           b_weighted=False)[1]
                                        s_cpt += f"\t{prior[2]}"
                                        # counts += f"{c}\t{t}\t{tc}\t{state}\t{cnt}\n"
                                s_cpt += "\t)"
                            s_cpt += ")"
                        s_cpt += ")"
                    s_cpt += ")"
                    # counts += f"#end\t{var}\n"

                    var_rq_a.append(var)
                    nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                         pos=(pos_x + (-(nb_qstns // 2) + at_qstn) * delta_x,
                                                              pos_y - 6 * delta_y), id=var))
                    potentials.append(
                        SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task, var_tc], cpt=s_cpt))
                # This is a critical node: separate it from the topics and make it take the same value as its
                # corresponding CQ node.
                else:
                    s_cpt = "("
                    for nbans, ans in enumerate(q_ans):
                        if nbans > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        for state in q_ans:
                            if ans == state:
                                s_cpt += f"\t{1.0}"
                            else:
                                s_cpt += f"\t{0.0}"
                        s_cpt += "\t)"
                    s_cpt += ")"

                    var_rq_a.append(var)
                    nodes.append(SamIamUtils.create_node(name=var, states=q_ans,
                                                         pos=(pos_x + (-(nb_qstns // 2) + at_qstn) * delta_x,
                                                              pos_y - 6 * delta_y), id=var))
                    potentials.append(SamIamUtils.create_potential(var=var, cond_on=[f"CQ{qid}_A"], cpt=s_cpt))
        print()

        # String representing network that will be written to *.net file
        s_nw = """net
{
    node_size = (130 55);
}

"""
        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)
        # with open(out_file.replace(".net", ".counts"), "w") as fout:
        #     fout.write(counts)

        print(f"SamIamModel2 BayesianNetwork written to file:\n{out_file}")

    @staticmethod
    def get_sample_weight(lb: float, days_past: int, type_of_weight: TypeOfWeight = TypeOfWeight.NONE):
        """

        Args:
            lb: (nb of) lookback days
            days_past: days before cutoff date sample audit took place
            type_of_weight: self-explanatory

        Returns:

        """
        weight = 1
        if days_past > lb and type_of_weight != TypeOfWeight.NONE:
            if type_of_weight == TypeOfWeight.DEFAULT:
                weight = lb / days_past
            elif type_of_weight == TypeOfWeight.SQRT:
                weight = math.sqrt(lb / days_past)
            elif type_of_weight == TypeOfWeight.SQUARE:
                weight = math.pow(lb / days_past, 2)
            else:
                raise ValueError(f"Unknown type of weight: {type_of_weight}")
        return weight

    def generate_training_data(self, out_file_root: str, contractors=None,
                               lookback_days=math.inf, cutoff_date=None, test_date_range=0,
                               type_of_weight: TypeOfWeight = TypeOfWeight.NONE):
        """

        Args:
            out_file_root: root of the output files to generate
            contractors: list/set of contractors for whom to extract training date, "None" to use all
            lookback_days
            cutoff_date: only audits up to and including this date will be kept
            test_date_range: (inclusive) number of days after cutoff_date within which audits will be used as test data
                to compare model predictions against, "0" to not generate test data
            type_of_weight:

        Returns:

        """
        print("Generating training data...")
        da = TFDataAnalysis()
        if cutoff_date is None:
            cutoff_date = max(da.data['Audit_Date'].values)

        samples = []
        test_data = []

        uq_audits = sorted(da.data['Audit_Id'].unique())
        nb_uq_audits = len(uq_audits)
        for at_audit, audit_id in enumerate(uq_audits):
            # Skip sites that couldn't be matched to an audit
            if audit_id < 0:
                continue

            print(f"\rAt audit {at_audit+1} of {nb_uq_audits}...", end='')

            df_audit = da.data[da.data['Audit_Id'] == audit_id]
            row_0 = df_audit.iloc[0]
            ctor = row_0['Sites : List']
            if contractors is not None and ctor not in contractors:
                continue
            audit_date = row_0['Audit_Date']
            # Audit too recent to be used as training data?
            if audit_date > cutoff_date:
                # Audit falls within test data range?
                if (audit_date-cutoff_date).days <= test_date_range:
                    test_sample = {
                        'Site': row_0['Sites : Nr'],
                        'Contractor': ctor,
                        'Task': row_0['Sites : Task nbr'],
                        'Area': row_0['Sites : Subarea'],
                        'Date': str(audit_date),
                        'Deficiency': da.get_deficiency_for_df(df_audit)
                    }
                    test_data.append(test_sample)
                else:
                    continue

            days_past = (cutoff_date - audit_date).days
            weight = self.get_sample_weight(lb=lookback_days, days_past=days_past, type_of_weight=type_of_weight)

            # init sequence
            qstn_seq = {
                'Weight': weight,
                'Contractor': ctor,
                'Task': row_0['Sites : Task nbr'],
                'Area': row_0['Sites : Subarea'],
            }

            for i, r in df_audit.iterrows():
                topic_id = r['Topic_Id']
                qstn_id = r['Question_Id']
                # TODO: What's going on here?
                if qstn_id < 0:
                    print("Negative Question ID")
                    continue

                comp = r['Compliancy']
                # NA is marked as float.nan in DataFrame, while others are of type string, so convert to string as well
                if type(comp) == float and math.isnan(comp):
                    comp = "NA"

                qstn_seq[f'RQ{qstn_id}_A'] = comp
                # First observation of a new topic?
                if qstn_id not in self.cqs.get_critical_ids() and f'Topic_{topic_id}' not in qstn_seq:
                    qstn_seq[f'Topic_{topic_id}'] = "T"

            # Don't forget last sequence!
            self._process_sample(qstn_seq)
            samples.append(qstn_seq)
        print("\n")

        cq_ids = self.cqs.get_critical_ids()
        # Create node labels
        if type_of_weight == TypeOfWeight.NONE:
            keys_area, keys_task = [], []
        else:
            keys_area, keys_task = ["Weight"], ["Weight"]
        keys_task += ["Contractor", "Task"]
        keys_area += ["Contractor", "Area"]
        # CQ Question nodes
        for keys in [keys_area, keys_task]:
            for i in sorted(cq_ids):
                keys.append(f"CQ{i}_A")
                keys.append(f"CQ{i}_Crit")
            # Sum
            keys.append(f"Sum")
            # Topic nodes
            for i in range(len(self.qns_per_topic)):
                keys.append(f"Topic_{i}")
            # RQ Question nodes
            for i in self.qstns:
                if i < 0:
                    continue
                keys.append(f"RQ{i}_A")

        # Process samples and write to file
        for tpl in zip([keys_area, keys_task], ["_area.dat", "_task.dat"]):
            keys = tpl[0]
            with open(out_file_root + tpl[1], 'w') as fout:
                # Write header which contains node names
                for j, k in enumerate(keys):
                    fout.write(k)
                    if j < len(keys)-1:
                        fout.write(',')
                    else:
                        fout.write('\n')
                # Write samples
                for sample in samples:
                    for j, k in enumerate(keys):
                        # Next if-clause should be used to generate models with hidden Topic nodes
                        # if k.startswith("Topic"):
                        #     fout.write("N/A")
                        if k in sample:
                            fout.write(str(sample[k]))
                        elif k.startswith("Topic"):
                            fout.write("F")
                        else:
                            fout.write("NoAns")
                        fout.write(',' if j < len(keys)-1 else '\n')

        # Same, but without contractor, to train network that uses all data and can serve
        # as "average" or "baseline"
        # Process samples and write to file
        for tpl in zip([keys_area, keys_task], ["_no_ctor_area.dat", "_no_ctor_task.dat"]):
            keys = [tpl[0][0]] + tpl[0][2:]
            with open(out_file_root + tpl[1], 'w') as fout:
                # Write header which contains node names
                for j, k in enumerate(keys):
                    fout.write(k)
                    if j < len(keys)-1:
                        fout.write(',')
                    else:
                        fout.write('\n')
                # Write samples
                for sample in samples:
                    for j, k in enumerate(keys):
                        if k in sample:
                            fout.write(str(sample[k]))
                        elif k.startswith("Topic"):
                            fout.write("F")
                        else:
                            fout.write("NoAns")
                        fout.write(',' if j < len(keys)-1 else '\n')

        # Write test data if existing
        if test_data:
            with open(out_file_root + '_test.dat', 'w') as fout:
                # Write header
                fout.write("Site,Contractor,Task,Area,Date,Deficiency\n")
                for e in test_data:
                    fout.write(f"{e['Site']},{e['Contractor']},{e['Task']},{e['Area']},{e['Date']},{e['Deficiency']}\n")
        print("Finished generating training data!")

    # # The following code is the "old" code that uses question sequences to determine audits.
    # # It should actually be updated to use topics instead of questions.
    # # Its advantage is that it does not depend on the real audits, and so if the audit (or audit date) for a site
    # # is not known, it can still be used as training data.
    # def generate_training_data(self, contractors, out_file_root: str, cutoff_date: date.today()):
    #     da = TFDataAnalysis()
    #
    #     samples = []
    #
    #     prev_site = -1
    #     qstn_seq = {}
    #     qstns_seen = set()  # We will use this set to check if a sequence starts again
    #     for i, r in da.data.iterrows():
    #         if i % 1000 == 0:
    #             print("\rAt row {}...".format(i), end='')
    #
    #         if r['Sites : List'] not in contractors:
    #             continue
    #
    #         site = r['Sites : Nr']
    #         topic_id = r['Topic_Id']
    #         qstn_id = r['Question_Id']
    #         # TODO: What's going on here?
    #         if qstn_id < 0:
    #             print("Negative Question ID")
    #             continue
    #
    #         comp = r['Compliancy']
    #         # NA is marked as float.nan in DataFrame, while others are of type string, so convert to string as well
    #         if type(comp) == float and math.isnan(comp):
    #             comp = "NA"
    #
    #         # Continuation of run
    #         if prev_site == site and qstn_id not in qstns_seen:
    #             qstns_seen.add(qstn_id)
    #             qstn_seq[f'RQ{qstn_id}_A'] = comp
    #             if qstn_id not in self.cqs.get_critical_ids() and f'Topic_{topic_id}' not in qstn_seq:
    #                 qstn_seq[f'Topic_{topic_id}'] = "T"
    #         # Start of new sequence
    #         else:
    #             if prev_site > -1:
    #                 self._process_sample(qstn_seq)
    #                 samples.append(qstn_seq)
    #             # Reset sequence
    #             qstn_seq = {
    #                 'Contractor': r['Sites : List'],
    #                 'Task': r['Sites : Task nbr'],
    #                 'Area': r['Sites : Subarea'],
    #                 f'RQ{qstn_id}_A': comp
    #             }
    #             if qstn_id not in self.cqs.get_critical_ids():
    #                 qstn_seq[f'Topic_{topic_id}'] = "T"
    #
    #             qstns_seen = set()
    #
    #         prev_site = site
    #
    #     # Don't forget last sequence!
    #     self._process_sample(qstn_seq)
    #     samples.append(qstn_seq)
    #     print("\n")
    #
    #     cq_ids = self.cqs.get_critical_ids()
    #     # Create node labels
    #     keys_task = ["Contractor", "Task"]
    #     keys_area = ["Contractor", "Area"]
    #     # CQ Question nodes
    #     for keys in [keys_area, keys_task]:
    #         for i in sorted(cq_ids):
    #             keys.append(f"CQ{i}_A")
    #             keys.append(f"CQ{i}_Crit")
    #         # Sum
    #         keys.append(f"Sum")
    #         # Topic nodes
    #         for i in range(len(self.qns_per_topic)):
    #             keys.append(f"Topic_{i}")
    #         # RQ Question nodes
    #         for i in self.qstns:
    #             if i < 0:
    #                 continue
    #             keys.append(f"RQ{i}_A")
    #
    #     # Process samples and write to file
    #     for tpl in zip([keys_area, keys_task], ["_area.dat", "_task.dat"]):
    #         keys = tpl[0]
    #         with open(out_file_root + tpl[1], 'w') as fout:
    #             # Write header which contains node names
    #             for j, k in enumerate(keys):
    #                 fout.write(k)
    #                 if j < len(keys)-1:
    #                     fout.write(',')
    #                 else:
    #                     fout.write('\n')
    #             # Write samples
    #             for sample in samples:
    #                 for j, k in enumerate(keys):
    #                     if k in sample:
    #                         fout.write(str(sample[k]))
    #                     elif k.startswith("Topic"):
    #                         fout.write("F")
    #                     else:
    #                         fout.write("NoAns")
    #                     fout.write(',' if j < len(keys)-1 else '\n')
    #
    #     # Same, but without contractor, to train network that uses all data and can serve
    #     # as "average" or "baseline"
    #     # Process samples and write to file
    #     for tpl in zip([keys_area, keys_task], ["_no_ctor_area.dat", "_no_ctor_task.dat"]):
    #         keys = tpl[0][1:]
    #         with open(out_file_root + tpl[1], 'w') as fout:
    #             # Write header which contains node names
    #             for j, k in enumerate(keys):
    #                 fout.write(k)
    #                 if j < len(keys)-1:
    #                     fout.write(',')
    #                 else:
    #                     fout.write('\n')
    #             # Write samples
    #             for sample in samples:
    #                 for j, k in enumerate(keys):
    #                     if k in sample:
    #                         fout.write(str(sample[k]))
    #                     elif k.startswith("Topic"):
    #                         fout.write("F")
    #                     else:
    #                         fout.write("NoAns")
    #                     fout.write(',' if j < len(keys)-1 else '\n')

    def _process_sample(self, sample):
        nb_ok = sum([1 if v == "OK" else 0 for v in sample.values()])
        nb_sos = sum([1 if v == "SOS" else 0 for v in sample.values()])
        sum_ok = nb_ok + nb_sos
        nb_nok = sum([1 if v == "NOK" else 0 for v in sample.values()])
        crit_site = "T" if nb_nok and not sum_ok else "F"
        # Just because a site is critical does not mean it is due to a critical question
        # This next variable encodes whether at least one critical question was, indeed, critical
        crit_qn = "F"
        for i in self.cqs.get_critical_ids():
            if f"RQ{i}_A" in sample:
                sample[f"CQ{i}_A"] = sample[f"RQ{i}_A"]
                sample[f"CQ{i}_Crit"] = crit_site if sample[f"RQ{i}_A"] == "NOK" else "F"
                if sample[f"CQ{i}_Crit"] == "T":
                    crit_qn = "T"
            else:
                sample[f"CQ{i}_A"] = "NoAns"
                sample[f"CQ{i}_Crit"] = "F"

        sample["Sum"] = crit_qn
        # If critical site due to critical question, set all Topics and RQ_A to "F", except the critical questions
        if crit_qn == "T":
            for i in range(len(self.qns_per_topic)):
                sample[f"Topic_{i}"] = "F"
            # RQ Question nodes
            for i in self.qstns:
                if i < 0 or i in self.cqs.get_critical_ids():
                    continue
                sample[f"RQ{i}_A"] = "NoAns"


if __name__ == '__main__':
    bnm = SamIamModel2Alt()
    da = TFDataAnalysis()

    def create_network(args):
        cudate = args[0]
        s_date = str(cudate).replace('-', '')
        lbdays = args[1]
        tow = args[2]
        lap_smooth = args[3]
        lap_weight = args[4]
        proc = args[5]

        print(f"{proc}: ============================================================")
        print(f"{proc}: Cut-off date: {s_date}\tLookback days: {lbdays}\tWeight: {tow.value}" +
              f"\tLaplace Smoothing: {lap_smooth}\tLaplace weight: {lap_weight}")

        b_weighted = True
        if lbdays == math.inf:
            lbtext = 'nolb'
            b_weighted = False
        else:
            lbtext = f'lb{lbdays}'
            if tow == TypeOfWeight.SQRT:
                lbtext += 'sqrt'
            elif tow == TypeOfWeight.SQUARE:
                lbtext += 'sq'

        ctors = 'top5'
        # contractors = set(list(da.data["Sites : List"].unique()))
        contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
        # contractors = {"A : Sites", "C : Sites"}

        data_out_file_root = os.path.join(TenForceConfig.HOME,
                                          "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "with_smoothing",
                                          f"bn_model2_alt_{ctors}_{s_date}_{lbtext}")

        bnm.initialize_vars(da.data[da.data["Sites : List"].isin(contractors)], data_file_root=data_out_file_root,
                            regenerate_train_data=False, lookback_days=lbdays,
                            cutoff_date=cudate, test_date_range=14, type_of_weight=tow)

        # Compile Bayesian networks
        bn_out_file_root = os.path.join(TenForceConfig.HOME,
                                        "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "with_smoothing",
                                        f"bn_model2_alt_{ctors}_{s_date}_{lbtext}_lsw{lap_smooth}")
        # Check if models already exist. If so, skip.
        if os.path.isfile(bn_out_file_root + "_area.net") and os.path.isfile(bn_out_file_root + "_task.net"):
            print(f"{proc}: Models already exist for date {s_date}, lb: {lbtext}, ls: {lap_smooth}, lw: {lap_weight != 1}")
            return
        bnm.compile_bns(out_file_root=bn_out_file_root, b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
        # bnm.compile_bn_area(bn_out_file_root + "_area.net", b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)
        # bnm.compile_bn_task(bn_out_file_root + "_task.net", b_weighted=b_weighted, lap_smooth=lap_smooth, lap_weight=lap_weight)

        # Extract training data: this typically shouldn't be done separately.
        # bnm.generate_training_data(out_file_root=data_out_file_root, contractors=contractors)
        print(f"{proc}: done!")


    # What's the earliest date in the data?
    first_date = sorted(da.data['Audit_Date'])[0]
    last_date = sorted(da.data['Audit_Date'])[-1]
    # Move first_date to first Sunday after first date; sort of makes it cleaner afterwards
    while first_date.weekday() != 6:
        first_date = first_date + timedelta(days=1)

    # time_interval = timedelta(days=14)
    # co_date = first_date
    # list_cudates = []
    # while co_date <= last_date:
    #     co_date = co_date + time_interval
    #     list_cudates.append(co_date)

    list_cudates = [date(2018, 6, 3)]  # 14/6, 17/6, 1/7, 15/7, 29/7, 20/8
    list_lbdays = [10, 25]  # [math.inf, 10, 25, 50, 100, 150, 200, 250, 350]
    list_toweight = [TypeOfWeight.DEFAULT, TypeOfWeight.SQUARE, TypeOfWeight.SQRT]
    list_lap_smooth = [0, 1]  # , 2, 3, 4, 5]
    combis = []
    nb_procs = 8
    at_proc = -1
    for tpl in product(list_cudates, list_lbdays, list_toweight, list_lap_smooth):
        # Ignore nonsensical combinations
        if tpl[1] == math.inf and tpl[2] != TypeOfWeight.DEFAULT:
            continue
        # Compute Laplace weight if necessary, which is when lookback AND Laplace smoothing are both applied
        lap_weight = 1
        if tpl[1] != math.inf and tpl[3]:
            # 'days past' = number of days between oldest audit and cutoff date
            lap_weight = SamIamModel2Alt.get_sample_weight(tpl[1], (tpl[0] - first_date).days, tpl[2])

        at_proc += 1
        combis.append(tpl + (lap_weight,) + (at_proc % nb_procs,))

    with Pool(nb_procs) as p:
        p.map(create_network, combis)

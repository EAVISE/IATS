"""
This class allows to create Bayesian networks that will be written away to HD in text files that can be read
by SamIam (http://reasoning.cs.ucla.edu/samiam/index.php). The filename can be specified by the user, by should
end with the *.net extension.

This class will generate a single network that handles the "Critical" questions and "Remaining" questions at once.
Basically, it is a merger of the two networks created by SamiamModel1. The idea is that this time, since only one
dataset is used to train the network, the results are more consistent. A method to generate the training data
is also included.
"""
import math
import os
import pickle
from collections import defaultdict
from itertools import product

import pandas as pd

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tools.samiam_utils import SamIamUtils
from iats.tenforce.models.samiam_interface import SamIamInterface
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class SamIamModel2:
    def __init__(self, nb_cqs=3):
        qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        self.qr, self.qcnts, self.qr_na, self.qcnts_na = pickle.load(open(qr_file, 'rb'))

        # Group questions per topic
        # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
        # the list of questions; hence it only related to questions that truly appear in the data itself.
        tm = TopicMapper()
        qns_per_topic = defaultdict(list)
        for qn_id in sorted(tm.topic_idx_per_qn.keys()):
            topic_idx = tm.topic_idx_per_qn[qn_id]
            qns_per_topic[topic_idx].append(qn_id)
        self.qns_per_topic = qns_per_topic

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.cqs = CriticalQuestions(keep_top=nb_cqs)

        self.cq_bnm, self._cq_root, self._cq_children = None, None, None
        self.rq_bnm, self._rq_root, self._rq_children = None, None, None

    def initialize_vars(self, df: pd.DataFrame):
        """
        Extract some variables from a dataframe; these values define what nodes will be created
        in the Bayesian network.

        Args:
            df: the DataFrame to use to extract the variables
        """
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _get_area(self, a: str or int):
        if type(a) is int:
            return a
        return Tools.binary_search(self.subareas, a)

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def compile_bns(self, out_file_root: str):
        """
        Convenience method that will call compile_bn_area() and compile_bn_task().
        The provided file name to this method is assumed to be a "root" that will be completed with
        "_area.net" for the area network, and "_task.net" for the task network.

        Args:
            out_file_root:

        Returns:

        """
        self.compile_bn_area(out_file_root + "_area.net")
        self.compile_bn_task(out_file_root + "_task.net")

    def compile_bn_area(self, out_file: str):
        """
        Compile Bayesian Network using (sub)Area nodes

        Returns:

        """
        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))
        s_cpt = "("
        for _ in range(len(self.contractors)):
            s_cpt += f"\t{1/nb_ctors}"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))

        # Contractor -> Area
        nb_areas = len(self.subareas)
        s_cpt = "("
        for i, c in enumerate(self.contractors):
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for _ in self.subareas:
                s_cpt += f"\t{1/nb_areas}"
            s_cpt += "\t)"
        s_cpt += ")"
        var_area = "Area"
        nodes.append(SamIamUtils.create_node(name=var_area, states=self.subareas, pos=(pos_x, pos_y-delta_y), id="Area"))
        potentials.append(SamIamUtils.create_potential(var=var_area, cond_on=[var_ctor], cpt=s_cpt))

        # Area -> Critical Questions: Answered at all?
        cq_ids = sorted(self.cqs.get_critical_ids())
        nb_cqs = len(cq_ids)
        var_cqs_a = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_A"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nba, a in enumerate(self.subareas):
                    if nba > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += f"(\t0.5\t0.5\t)"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_a.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "Ans"],
                                                 pos=(pos_x + (-(nb_cqs//2) + i)*delta_x, pos_y - 2*delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_area], cpt=s_cpt))

        # Critical Question: What answer?
        var_cqs_ans = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Ans"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nba, a in enumerate(self.subareas):
                    if nba > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "((\t0.25\t0.25\t0.25\t0.25\t)\n"
                    s_cpt += "\t\t(\t0.25\t0.25\t0.25\t0.25\t))"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_ans.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NA", "OK", "NOK", "None"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 3 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_area, var_cqs_a[i]], cpt=s_cpt))

        # Critical Question: Critical?
        var_cqs_crit = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Crit"
            s_cpt = "("
            for nbans, ans in enumerate(['NA', 'OK', 'NOK', 'None']):
                if nbans > 0:
                    s_cpt += "\n\t\t"
                if ans == 'NOK':
                    s_cpt += "(\t0.99\t0.01\t)"
                else:
                    s_cpt += "(\t0.01\t0.99\t)"
            s_cpt += ")"

            var_cqs_crit.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["T", "F"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 4 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_cqs_ans[i]], cpt=s_cpt))

        # Group CQ answers in one node
        s_cpt = len(var_cqs_crit) * "("
        at_iter = -1
        cntr_per_dep = [0 for _ in range(len(var_cqs_crit))]
        nb_reset = 0  # Nb. of counters reset to 0
        for prod in product(["T", "F"], repeat=len(var_cqs_crit)):
            at_iter += 1
            prod = list(prod)
            if at_iter > 0:
                s_cpt += "\n\t\t"
            # Write necessary opening brackets
            s_cpt += nb_reset*"("
            if prod == len(var_cqs_crit)*["F"]:
                s_cpt += "(\t0.01\t0.99\t)"
            else:
                s_cpt += "(\t0.99\t0.01\t)"
            # Update dep counters
            nb_reset = 0
            for idx in range(len(cntr_per_dep) - 1, -1, -1):
                cntr_per_dep[idx] += 1
                if cntr_per_dep[idx] == 2:
                    cntr_per_dep[idx] = 0
                    nb_reset += 1
                    s_cpt += f")"
                # No need to update other indices
                else:
                    break

        var_sum = "Sum"
        nodes.append(SamIamUtils.create_node(name=var_sum, states=["T", "F"],
                                             pos=(pos_x, pos_y - 5*delta_y), id=var_sum))
        potentials.append(SamIamUtils.create_potential(var=var_sum, cond_on=var_cqs_crit, cpt=s_cpt))

        # Continue with topics
        # Per Topic, add hidden node and questions belonging to this topic
        nb_topics = len(self.qns_per_topic)
        nb_qstns = len(self.qstns)
        at_qstn = -1  # Counter that will be used to determine position of node
        for tid in range(nb_topics):
            nb_t_qns = len(self.qns_per_topic[tid])
            var_t = f"Topic_{tid}"
            # Create initial CPT for topic; this depends on
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nba, a in enumerate(self.subareas):
                    if nba > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbs, s in enumerate(["T", "F"]):
                        if nbs > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        if s == "T":
                            s_cpt += "\t0.01\t0.99\t"
                        else:
                            s_cpt += "\t0.5\t0.5\t"
                        s_cpt += ")"
                    s_cpt += ")"
                s_cpt += ")"
            s_cpt += ")"

            # At this point, at_qstn indicates the number of questions that have already been seen
            # This information is used to compute the x-offset of the topic node
            x_offset = -(nb_qstns//2) + at_qstn+1 + nb_t_qns//2
            nodes.append(SamIamUtils.create_node(name=var_t, states=["T", "F"],
                                                 pos=(pos_x + x_offset*delta_x, pos_y - 6*delta_y), id=var_t))
            potentials.append(SamIamUtils.create_potential(var=var_t, cond_on=[var_ctor, var_area, var_sum], cpt=s_cpt))

            var_rq_a = []
            for i, qid in enumerate(self.qns_per_topic[tid]):
                at_qstn += 1
                var = f"RQ{qid}_A"
                s_cpt = "("
                for nbc, c in enumerate(self.contractors):
                    if nbc > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nba, a in enumerate(self.subareas):
                        if nba > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        s_cpt += f"(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)\n"
                        s_cpt += f"\t\t(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)"
                        s_cpt += ")"
                    s_cpt += ")"
                s_cpt += ")"

                var_rq_a.append(var)
                nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "OK", "SOS", "NOK", "NA"],
                                                     pos=(pos_x + (-(nb_qstns//2) + at_qstn)*delta_x, pos_y - 7*delta_y), id=var))
                potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_area, var_t], cpt=s_cpt))

        # String representing network that will be written to *.net file
        s_nw = """net
{
    node_size = (130 55);
}

"""
        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)

        print(f"SamIamModel2 BayesianNetwork written to file:\n{out_file}")

    def compile_bn_task(self, out_file: str):
        """
        Compile Bayesian Network using Task nodes

        Returns:

        """
        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))
        s_cpt = "("
        for _ in range(len(self.contractors)):
            s_cpt += f"\t{1/nb_ctors}"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))

        # Contractor -> Task
        nb_tasks = len(self.tasks)
        s_cpt = "("
        for i, c in enumerate(self.contractors):
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for _ in self.tasks:
                s_cpt += f"\t{1/nb_tasks}"
            s_cpt += "\t)"
        s_cpt += ")"
        var_task = "Task"
        nodes.append(SamIamUtils.create_node(name=var_task, states=self.tasks, pos=(pos_x, pos_y-delta_y), id="Task"))
        potentials.append(SamIamUtils.create_potential(var=var_task, cond_on=[var_ctor], cpt=s_cpt))

        # Task -> Critical Questions: Answered at all?
        cq_ids = sorted(self.cqs.get_critical_ids())
        nb_cqs = len(cq_ids)
        var_cqs_a = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_A"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += f"(\t0.5\t0.5\t)"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_a.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "Ans"],
                                                 pos=(pos_x + (-(nb_cqs//2) + i)*delta_x, pos_y - 2*delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task], cpt=s_cpt))

        # Critical Question: What answer?
        var_cqs_ans = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Ans"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "((\t0.25\t0.25\t0.25\t0.25\t)\n"
                    s_cpt += "\t\t(\t0.25\t0.25\t0.25\t0.25\t))"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_ans.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NA", "OK", "NOK", "None"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 3 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task, var_cqs_a[i]], cpt=s_cpt))

        # Critical Question: Critical?
        var_cqs_crit = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Crit"
            s_cpt = "("
            for nbans, ans in enumerate(['NA', 'OK', 'NOK', 'None']):
                if nbans > 0:
                    s_cpt += "\n\t\t"
                if ans == 'NOK':
                    s_cpt += "(\t0.5\t0.5\t)"
                else:
                    s_cpt += "(\t0.5\t0.5\t)"
            s_cpt += ")"

            var_cqs_crit.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["T", "F"],
                                                 pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 4 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_cqs_ans[i]], cpt=s_cpt))

        # Group CQ answers in one node
        s_cpt = len(var_cqs_crit) * "("
        at_iter = -1
        cntr_per_dep = [0 for _ in range(len(var_cqs_crit))]
        nb_reset = 0  # Nb. of counters reset to 0
        for prod in product(["T", "F"], repeat=len(var_cqs_crit)):
            at_iter += 1
            prod = list(prod)
            if at_iter > 0:
                s_cpt += "\n\t\t"
            # Write necessary opening brackets
            s_cpt += nb_reset*"("
            if prod == len(var_cqs_crit)*["F"]:
                s_cpt += "(\t0.5\t0.5\t)"
            else:
                s_cpt += "(\t0.5\t0.5\t)"
            # Update dep counters
            nb_reset = 0
            for idx in range(len(cntr_per_dep) - 1, -1, -1):
                cntr_per_dep[idx] += 1
                if cntr_per_dep[idx] == 2:
                    cntr_per_dep[idx] = 0
                    nb_reset += 1
                    s_cpt += f")"
                # No need to update other indices
                else:
                    break

        var_sum = "Sum"
        nodes.append(SamIamUtils.create_node(name=var_sum, states=["T", "F"],
                                             pos=(pos_x, pos_y - 5*delta_y), id=var_sum))
        potentials.append(SamIamUtils.create_potential(var=var_sum, cond_on=var_cqs_crit, cpt=s_cpt))

        # Continue with topics
        # Per Topic, add hidden node and questions belonging to this topic
        nb_topics = len(self.qns_per_topic)
        nb_qstns = len(self.qstns)
        at_qstn = -1  # Counter that will be used to determine position of node
        for tid in range(nb_topics):
            nb_t_qns = len(self.qns_per_topic[tid])
            var_t = f"Topic_{tid}"
            # Create initial CPT for topic; this depends on
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbs, s in enumerate(["T", "F"]):
                        if nbs > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        for _ in range(2):
                            s_cpt += f"\t0.5"
                        s_cpt += "\t)"
                    s_cpt += ")"
                s_cpt += ")"
            s_cpt += ")"

            # At this point, at_qstn indicates the number of questions that have already been seen
            # This information is used to compute the x-offset of the topic node
            x_offset = -(nb_qstns//2) + at_qstn+1 + nb_t_qns//2
            nodes.append(SamIamUtils.create_node(name=var_t, states=["T", "F"],
                                                 pos=(pos_x + x_offset*delta_x, pos_y - 6*delta_y), id=var_t))
            potentials.append(SamIamUtils.create_potential(var=var_t, cond_on=[var_ctor, var_task, var_sum], cpt=s_cpt))

            var_rq_a = []
            for i, qid in enumerate(self.qns_per_topic[tid]):
                at_qstn += 1
                var = f"RQ{qid}_A"
                s_cpt = "("
                for nbc, c in enumerate(self.contractors):
                    if nbc > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbt, t in enumerate(self.tasks):
                        if nbt > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        s_cpt += f"(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)\n"
                        s_cpt += f"\t\t(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)"
                        s_cpt += ")"
                    s_cpt += ")"
                s_cpt += ")"

                var_rq_a.append(var)
                nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "OK", "SOS", "NOK", "NA"],
                                                     pos=(pos_x + (-(nb_qstns//2) + at_qstn)*delta_x, pos_y - 7*delta_y), id=var))
                potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task, var_t], cpt=s_cpt))

        # String representing network that will be written to *.net file
        s_nw = """net
{
    node_size = (130 55);
}

"""
        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)

        print(f"SamIamModel2 BayesianNetwork written to file:\n{out_file}")

    def generate_training_data(self, contractors, out_file_root: str):
        da = TFDataAnalysis()

        samples = []

        prev_site = -1
        qstn_seq = {}
        qstns_seen = set()  # We will use this set to check if a sequence starts again
        for i, r in da.data.iterrows():
            if i % 1000 == 0:
                print("\rAt row {}...".format(i), end='')

            if r['Sites : List'] not in contractors:
                continue

            site = r['Sites : Nr']
            topic_id = r['Topic_Id']
            qstn_id = r['Question_Id']
            # TODO: What's going on here?
            if qstn_id < 0:
                print("Negative Question ID")
                continue

            comp = r['Compliancy']
            # NA is marked as float.nan in DataFrame, while others are of type string, so convert to string as well
            if type(comp) == float and math.isnan(comp):
                comp = "NA"

            # Continuation of run
            if prev_site == site and qstn_id not in qstns_seen:
                qstns_seen.add(qstn_id)
                qstn_seq[f'RQ{qstn_id}_A'] = comp
                if f'Topic_{topic_id}' not in qstn_seq:
                    qstn_seq[f'Topic_{topic_id}'] = "T"
            # Start of new sequence
            else:
                if prev_site > -1:
                    self._process_sample(qstn_seq)
                    samples.append(qstn_seq)
                # Reset sequence
                qstn_seq = {
                    'Contractor': r['Sites : List'],
                    'Task': r['Sites : Task nbr'],
                    'Area': r['Sites : Subarea'],
                    f'Topic_{topic_id}': "T",
                    f'RQ{qstn_id}_A': comp
                }
                qstns_seen = set()

            prev_site = site

        # Don't forget last sequence!
        self._process_sample(qstn_seq)
        samples.append(qstn_seq)
        print("\n")

        cq_ids = self.cqs.get_critical_ids()
        # Create node labels
        keys_task = ["Contractor", "Task"]
        keys_area = ["Contractor", "Area"]
        # CQ Question nodes
        for keys in [keys_area, keys_task]:
            for i in cq_ids:
                keys.append(f"CQ{i}_A")
                keys.append(f"CQ{i}_Ans")
                keys.append(f"CQ{i}_Crit")
            # Sum
            keys.append(f"Sum")
            # Topic nodes
            for i in range(len(self.qns_per_topic)):
                keys.append(f"Topic_{i}")
            # RQ Question nodes
            for i in self.qstns:
                if i < 0:
                    continue
                keys.append(f"RQ{i}_A")

        # Process samples and write to file
        for tpl in zip([keys_area, keys_task], ["_area.dat", "_task.dat"]):
            keys = tpl[0]
            with open(out_file_root + tpl[1], 'w') as fout:
                # Write header which contains node names
                for j, k in enumerate(keys):
                    fout.write(k)
                    if j < len(keys)-1:
                        fout.write(',')
                    else:
                        fout.write('\n')
                # Write samples
                for sample in samples:
                    for j, k in enumerate(keys):
                        if k.startswith("Topic"):
                            fout.write("N/A")
                        elif k in sample:
                            fout.write(str(sample[k]))
                        # elif k.startswith("Topic"):
                        #     fout.write("F")
                        else:
                            fout.write("NoAns")
                        fout.write(',' if j < len(keys)-1 else '\n')

        # Same, but without contractor, to train network that uses all data and can serve
        # as "average" or "baseline"
        # Process samples and write to file
        for tpl in zip([keys_area, keys_task], ["_no_ctor_area.dat", "_no_ctor_task.dat"]):
            keys = tpl[0][1:]
            with open(out_file_root + tpl[1], 'w') as fout:
                # Write header which contains node names
                for j, k in enumerate(keys):
                    fout.write(k)
                    if j < len(keys)-1:
                        fout.write(',')
                    else:
                        fout.write('\n')
                # Write samples
                for sample in samples:
                    for j, k in enumerate(keys):
                        if k in sample:
                            fout.write(str(sample[k]))
                        elif k.startswith("Topic"):
                            fout.write("F")
                        else:
                            fout.write("NoAns")
                        fout.write(',' if j < len(keys)-1 else '\n')

    def _process_sample(self, sample):
        nb_ok = sum([1 if v == "OK" else 0 for v in sample.values()])
        nb_sos = sum([1 if v == "SOS" else 0 for v in sample.values()])
        sum_ok = nb_ok + nb_sos
        nb_nok = sum([1 if v == "NOK" else 0 for v in sample.values()])
        crit_site = "T" if nb_nok and not sum_ok else "F"
        # Just because a site is critical does not mean it is due to a critical question
        # This next variable encodes whether at least one critical question was, indeed, critical
        crit_qn = "F"
        for i in self.cqs.get_critical_ids():
            if f"RQ{i}_A" in sample:
                sample[f"CQ{i}_A"] = "Ans"
                sample[f"CQ{i}_Ans"] = "OK" if sample[f"RQ{i}_A"] == "SOS" else sample[f"RQ{i}_A"]
                sample[f"CQ{i}_Crit"] = crit_site if sample[f"RQ{i}_A"] == "NOK" else "F"
                if sample[f"CQ{i}_Crit"] == "T":
                    crit_qn = "T"
            else:
                sample[f"CQ{i}_A"] = "NoAns"
                sample[f"CQ{i}_Ans"] = "None"
                sample[f"CQ{i}_Crit"] = "F"
        sample["Sum"] = crit_qn


if __name__ == '__main__':
    bnm = SamIamModel2()
    da = TFDataAnalysis()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    # contractors = {"A : Sites", "C : Sites"}
    bnm.initialize_vars(da.data[da.data["Sites : List"].isin(contractors)])

    # Compile Bayesian networks
    bn_out_file_root = os.path.join(TenForceConfig.HOME,
                                    "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2")
    # bnm.compile_bns(out_file_root=bn_out_file_root)

    # Extract training data
    data_out_file_root = os.path.join(TenForceConfig.HOME,
                                      "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_data_notopics")
    bnm.generate_training_data(contractors, out_file_root=data_out_file_root)

    # Train networks
    for net in ["_area", "_task"]:
        SamIamInterface.train_network(bn_out_file_root + net + ".net", data_out_file_root + net + ".dat")

"""
This class allows to create Bayesian networks that will be written away to HD in text files that can be read
by SamIam (http://reasoning.cs.ucla.edu/samiam/index.php). The filename can be specified by the user, by should
end with the *.net extension.

This class allows to create two seperate networks: one for the Critical Questions (CQs) step, and another for
the Remaining Questions (RQs). The network for the RQs has a root node representing the contractor, followed by
a node representing the task. From there on, one hidden node is added per topic, which is then connected to all
(nodes representing) questions represented by this topic.
Question nodes can have one of five possible states: NoAns, NOK, OK, SOS or NA. In other words, this one node
encodes both the probability of the question not being answered at all (NoAns), as well as the probabilities for
each possible answer.

In addition to this, the class contains a method, "generate_rq_data", to generate training data for the RQ network.
The CQ network doesn't need to be trained; all CPTs can be derived from the data.

This been said, there is a caveat: the data used to train the RQ network doesn't entirely follow the same logic as
the data used to initialize the CQ network. Hence, you could argue both networks aren't really "compatible", or in
other words, that it isn't entirely correct to use them sequentially. 'Cause that is the idea: first sample CQ
network to check if site is critical or not, then if not, sample remaining questions using RQ network.
"""
import math
import os
import pickle
from collections import defaultdict
from itertools import product

import numpy as np
import pandas as pd

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tools.samiam_utils import SamIamUtils
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class Network:
    CQ = "Critical Questions"
    RQ = "Remaining Questions"


class SamIamModel1:
    def __init__(self, nb_cqs=3):
        qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        self.qr, self.qcnts, self.qr_na, self.qcnts_na = pickle.load(open(qr_file, 'rb'))

        # Group questions per topic
        # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
        # the list of questions; hence it only related to questions that truly appear in the data itself.
        tm = TopicMapper()
        qns_per_topic = defaultdict(list)
        for qn_id in sorted(tm.topic_idx_per_qn.keys()):
            topic_idx = tm.topic_idx_per_qn[qn_id]
            qns_per_topic[topic_idx].append(qn_id)
        self.qns_per_topic = qns_per_topic

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.odds_area, self.cnts_area, self.cumul_area, self.cnts_cumul_area = None, None, None, None
        self.odds_task, self.cnts_task, self.cumul_task, self.cnts_cumul_task = None, None, None, None
        self.odds_noans, self.odds_na, self.odds_ok, self.odds_nok = None, None, None, None
        self.cnts_noans, self.cnts_na, self.cnts_ok, self.cnts_nok = None, None, None, None

        self.cqs = CriticalQuestions(keep_top=nb_cqs)

        self.cq_bnm, self._cq_root, self._cq_children = None, None, None
        self.rq_bnm, self._rq_root, self._rq_children = None, None, None

    def initialize_probs(self, df: pd.DataFrame):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors.
        """
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        # Odds per contractor to get assigned a site in a given area
        odds_area = np.zeros((len(self.contractors),
                              len(self.subareas))) + 0.5
        # cnts_area = np.zeros((len(self.contractors),
        #                       len(self.subareas)))
        # Odds per contractor to get assigned a task
        odds_task = np.zeros((len(self.contractors),
                              len(self.tasks))) + 0.5
        # cnts_task = np.zeros((len(self.contractors),
        #                       len(self.tasks)))
        # Odds that a question is not answered
        odds_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns))) + 0.5
        # cnts_noans = np.zeros((len(self.contractors),
        #                        len(self.tasks),
        #                        len(self.qstns)))
        # Odds that a question is NA, given a task
        odds_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns))) + 0.5
        # cnts_na = np.zeros((len(self.contractors),
        #                     len(self.tasks),
        #                     len(self.qstns)))
        # Odds of a question being answered OK/SOS, given a task
        odds_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns))) + 0.5
        # cnts_ok = np.zeros((len(self.contractors),
        #                     len(self.tasks),
        #                     len(self.qstns)))
        # Odds of a question being answered NOK, given a task
        odds_nok = np.zeros((len(self.contractors),
                             len(self.tasks),
                             len(self.qstns))) + 0.5
        # cnts_nok = np.zeros((len(self.contractors),
        #                      len(self.tasks),
        #                      len(self.qstns)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]
            nb_c_sites = len(da_c["Sites : Nr"].unique())
            if not nb_c_sites:
                # cprint("No sites for contractor: [{}]".format(c), color="cyan")
                continue

            # Get (sub)area priors
            for isa, sa in enumerate(self.subareas):
                nb_sa_sites = len(da_c[da_c["Sites : Subarea"] == sa]["Sites : Nr"].unique())
                odds_area[ic, isa] = nb_sa_sites / nb_c_sites

            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_c_t = da_c[da_c["Sites : Task nbr"] == t]

                # Get task priors
                nb_t_sites = len(da_c_t["Sites : Nr"].unique())
                odds_task[ic, it] = nb_t_sites / nb_c_sites
                # cnts_task[ic, it] = nb_c_sites

                # Get odds of questions being answered NA for this task,
                # as well as odds that this question is not answered at all
                for iq, q in enumerate(self.qstns):
                    da_ct_q = da_c_t[da_c_t["Question_Id"] == q]

                    # Odds not answered at all
                    nb_q_sites = len(da_ct_q["Sites : Nr"].unique())
                    if nb_t_sites:
                        odds_noans[ic, it, iq] = 1 - (nb_q_sites/nb_t_sites)
                    # cnts_noans[ic, it, iq] = nb_t_sites if nb_t_sites else 1

                    # Odds if answered
                    nb_q_nok, nb_q_ok, nb_q_sos, nb_q_na, _, _, _, _ = \
                        TFDataAnalysis.get_stats_for_df(da_ct_q, b_add_na=True)

                    q_tot = nb_q_nok + nb_q_ok + nb_q_sos + nb_q_na
                    q_wa_na = q_tot - nb_q_na  # Nb. of times this question was not answered NA

                    if q_tot:
                        odds_na[ic, it, iq] = nb_q_na / q_tot
                        odds_ok[ic, it, iq] = (nb_q_ok + nb_q_sos) / q_tot
                        odds_nok[ic, it, iq] = nb_q_nok / q_tot
        print()

        self.odds_area = odds_area
        # self.cnts_area = cnts_area
        # self.cumul_area = np.zeros((len(self.contractors),
        #                             len(self.subareas)))
        # self.cnts_cumul_area = np.zeros((len(self.contractors),
        #                                  len(self.subareas)))
        # for ic, c in enumerate(self.contractors):
        #     prev_sum = 0
        #     cnts_prev_sum = 0
        #     for isa, sa in enumerate(self.subareas):
        #         self.cumul_area[ic, isa] = self.odds_area[ic, isa] + prev_sum
        #         prev_sum += self.odds_area[ic, isa]
        #         self.cnts_cumul_area[ic, isa] = self.cnts_area[ic, isa] + cnts_prev_sum
        #         cnts_prev_sum += self.cnts_area[ic, isa]

        self.odds_task = odds_task
        # self.cnts_task = cnts_task
        # self.cumul_task = np.zeros((len(self.contractors),
        #                             len(self.tasks)))
        # self.cnts_cumul_task = np.zeros((len(self.contractors),
        #                                  len(self.tasks)))
        # for ic, c in enumerate(self.contractors):
        #     prev_sum = 0
        #     cnts_prev_sum = 0
        #     for it, t in enumerate(self.tasks):
        #         self.cumul_task[ic, it] = self.odds_task[ic, it] + prev_sum
        #         prev_sum += self.odds_task[ic, it]
        #         self.cnts_cumul_task[ic, it] = self.cnts_task[ic, it] + cnts_prev_sum
        #         cnts_prev_sum += self.cnts_task[ic, it]

        self.odds_noans = odds_noans
        # self.cnts_noans = cnts_noans
        self.odds_na = odds_na
        # self.cnts_na = cnts_na
        self.odds_ok = odds_ok
        # self.cnts_ok = cnts_ok
        self.odds_nok = odds_nok
        # self.cnts_nok = cnts_nok

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _get_area(self, a: str or int):
        if type(a) is int:
            return a
        return Tools.binary_search(self.subareas, a)

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def compile_bn_cq(self, out_file: str):
        """
        Compile Bayesian Network that predicts critical questions.

        Returns:

        """
        # String representing network that will be written to *.net file
        s_nw = \
"""net
{
    propagationenginegenerator1791944048146838126L = "edu.ucla.belief.approx.BeliefPropagationSettings@301a6bb8";
    jenginegenerator6060830225489488864L = "edu.ucla.belief.inference.JoinTreeSettings@16d8b9be";
    recoveryenginegenerator6944530267470113528l = "edu.ucla.util.SettingsImpl@65c1b29f";
    node_size = (130 55);
}

"""
        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))
        s_cpt = "("
        for _ in range(len(self.contractors)):
            s_cpt += f"\t{1/nb_ctors}"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))

        # Contractor -> Subarea
        s_cpt = "("
        for i, c in enumerate(self.contractors):
            ic = self._get_contractor(c)
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for t in self.tasks:
                it = self._get_task(t)
                s_cpt += f"\t{self.odds_task[ic, it]}"
            s_cpt += ")"
        s_cpt += ")"
        var_task = "Task"
        nodes.append(SamIamUtils.create_node(name=var_task, states=self.tasks, pos=(pos_x, pos_y-delta_y), id="Task"))
        potentials.append(SamIamUtils.create_potential(var=var_task, cond_on=[var_ctor], cpt=s_cpt))

        # Subarea -> Critical Questions: Answered at all?
        cq_ids = sorted(self.cqs.get_critical_ids())
        nb_cqs = len(cq_ids)
        var_cqs_a = []
        for i, qid in enumerate(cq_ids):
            iq = self._qid_to_iq(qid)
            var = f"CQ{qid}_A"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                ic = self._get_contractor(c)
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    it = self._get_task(t)
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += f"(\t{self.odds_noans[ic, it, iq]}\t{1 - self.odds_noans[ic, it, iq]}\t)"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_a.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "Ans"],
                                          pos=(pos_x + (-(nb_cqs//2) + i)*delta_x, pos_y - 2*delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task], cpt=s_cpt))

        # Critical Question: What answer?
        var_cqs_ans = []
        for i, qid in enumerate(cq_ids):
            iq = self._qid_to_iq(qid)
            var = f"CQ{qid}_Ans"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                ic = self._get_contractor(c)
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    it = self._get_task(t)
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nans, ans in enumerate(["NoAns", "Ans"]):
                        if nans == 0:
                            s_cpt += f"(\t{0}"
                            s_cpt += f"\t{0}"
                            s_cpt += f"\t{0}"
                            s_cpt += f"\t{1}\t)"
                        elif nans == 1:
                            s_cpt += f"\n\t\t(\t{self.odds_na[ic, it, iq]}"
                            s_cpt += f"\t{self.odds_ok[ic, it, iq]}"
                            s_cpt += f"\t{self.odds_nok[ic, it, iq]}"
                            s_cpt += f"\t{0}\t)"
                    s_cpt += ")"
                s_cpt += ")"
            s_cpt += ")"

            var_cqs_ans.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["NA", "OK", "NOK", "None"],
                                          pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 3 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task, var_cqs_a[i]], cpt=s_cpt))

        # Critical Question: Critical?
        var_cqs_crit = []
        for i, qid in enumerate(cq_ids):
            var = f"CQ{qid}_Crit"
            s_cpt = "("
            for nbans, ans in enumerate(['NA', 'OK', 'NOK', 'None']):
                if nbans > 0:
                    s_cpt += "\n\t\t"
                if ans == 'NOK':
                    c_odds = self.cqs.get_critical_odds(qid)
                    s_cpt += f"(\t{c_odds}\t{1 - c_odds}\t)"
                else:
                    s_cpt += f"(\t{0}\t{1}\t)"
            s_cpt += ")"

            var_cqs_crit.append(var)
            nodes.append(SamIamUtils.create_node(name=var, states=["T", "F"],
                                          pos=(pos_x + (-(nb_cqs // 2) + i) * delta_x, pos_y - 4 * delta_y), id=var))
            potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_cqs_ans[i]], cpt=s_cpt))

        # Group CQ answers in one node
        s_cpt = len(var_cqs_crit) * "("
        at_iter = -1
        cntr_per_dep = [0 for _ in range(len(var_cqs_crit))]
        nb_reset = 0  # Nb. of counters reset to 0
        for prod in product(["T", "F"], repeat=len(var_cqs_crit)):
            at_iter += 1
            prod = list(prod)
            if at_iter > 0:
                s_cpt += "\n\t\t"
            # Write necessary opening brackets
            s_cpt += nb_reset*"("
            if prod == len(var_cqs_crit)*["F"]:
                s_cpt += f"(\t{0}\t{1}\t)"
            else:
                s_cpt += f"(\t{1}\t{0}\t)"
            # Update dep counters
            nb_reset = 0
            for idx in range(len(cntr_per_dep) - 1, -1, -1):
                cntr_per_dep[idx] += 1
                if cntr_per_dep[idx] == 2:
                    cntr_per_dep[idx] = 0
                    nb_reset += 1
                    s_cpt += f")"
                # No need to update other indices
                else:
                    break

        var_sum = "Sum"
        nodes.append(SamIamUtils.create_node(name=var_sum, states=["T", "F"],
                                      pos=(pos_x, pos_y - 5 * delta_y), id=var_sum))
        potentials.append(SamIamUtils.create_potential(var=var_sum, cond_on=var_cqs_crit, cpt=s_cpt))

        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)

        print(f"Critical Questions BayesianNetwork written to file:\n{out_file}")

    def compile_bn_rq(self, out_file: str):
        """
        Compile Bayesian Network that predicts remaining questions.

        Returns:

        """
        # String representing network that will be written to *.net file
        s_nw = \
"""net
{
    propagationenginegenerator1791944048146838126L = "edu.ucla.belief.approx.BeliefPropagationSettings@301a6bb8";
    jenginegenerator6060830225489488864L = "edu.ucla.belief.inference.JoinTreeSettings@16d8b9be";
    recoveryenginegenerator6944530267470113528l = "edu.ucla.util.SettingsImpl@65c1b29f";
    node_size = (130 55);
}

"""
        nodes = []
        potentials = []

        pos_x, pos_y = 0, 0
        delta_x, delta_y = 180, 120
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        var_ctor = "Contractor"
        nodes.append(SamIamUtils.create_node(name=var_ctor, states=self.contractors, pos=(pos_x, pos_y), id="Ctor"))
        s_cpt = "("
        for _ in range(len(self.contractors)):
            s_cpt += f"\t{1 / nb_ctors}"
        s_cpt += "\t)"
        potentials.append(SamIamUtils.create_potential(var=var_ctor, cond_on=[], cpt=s_cpt))

        # Contractor -> Subarea
        s_cpt = "("
        for i, c in enumerate(self.contractors):
            ic = self._get_contractor(c)
            if i > 0:
                s_cpt += '\n\t\t'
            s_cpt += "("
            for t in self.tasks:
                it = self._get_task(t)
                s_cpt += f"\t{self.odds_task[ic, it]}"
            s_cpt += ")"
        s_cpt += ")"
        var_task = "Task"
        nodes.append(SamIamUtils.create_node(name=var_task, states=self.tasks, pos=(pos_x, pos_y - delta_y), id="Task"))
        potentials.append(SamIamUtils.create_potential(var=var_task, cond_on=[var_ctor], cpt=s_cpt))

        # Per Topic, add hidden node and questions belonging to this topic
        nb_topics = len(self.qns_per_topic)
        nb_qstns = len(self.qstns)
        at_qstn = -1  # Counter that will be used to determine position of node
        for tid in range(nb_topics):
            nb_t_qns = len(self.qns_per_topic[tid])
            var_t = f"Topic_{tid}"
            s_cpt = "("
            for nbc, c in enumerate(self.contractors):
                if nbc > 0:
                    s_cpt += "\n\t\t"
                s_cpt += "("
                for nbt, t in enumerate(self.tasks):
                    if nbt > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for _ in range(2):
                        s_cpt += f"\t0.5"
                    s_cpt += "\t)"
                s_cpt += ")"
            s_cpt += ")"

            # At this point, at_qstn indicates the number of questions that have already been seen
            x_offset = -(nb_qstns//2) + at_qstn+1 + nb_t_qns//2
            nodes.append(SamIamUtils.create_node(name=var_t, states=["T", "F"],
                                          pos=(pos_x + x_offset*delta_x, pos_y - 2*delta_y), id=var_t))
            potentials.append(SamIamUtils.create_potential(var=var_t, cond_on=[var_ctor, var_task], cpt=s_cpt))

            var_rq_a = []
            for i, qid in enumerate(self.qns_per_topic[tid]):
                at_qstn += 1
                var = f"RQ{qid}_A"
                s_cpt = "("
                for nbc, c in enumerate(self.contractors):
                    if nbc > 0:
                        s_cpt += "\n\t\t"
                    s_cpt += "("
                    for nbt, t in enumerate(self.tasks):
                        if nbt > 0:
                            s_cpt += "\n\t\t"
                        s_cpt += "("
                        s_cpt += f"(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)\n"
                        s_cpt += f"\t\t(\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t{0.2}\t)"
                        s_cpt += ")"
                    s_cpt += ")"
                s_cpt += ")"

                var_rq_a.append(var)
                nodes.append(SamIamUtils.create_node(name=var, states=["NoAns", "OK", "SOS", "NOK", "NA"],
                                              pos=(pos_x + (-(nb_qstns//2) + at_qstn)*delta_x, pos_y - 3*delta_y), id=var))
                potentials.append(SamIamUtils.create_potential(var=var, cond_on=[var_ctor, var_task, var_t], cpt=s_cpt))

        for node in nodes:
            s_nw += node
        for pot in potentials:
            s_nw += pot
        with open(out_file, "w") as fout:
            fout.write(s_nw)

        print(f"Remaining Questions BayesianNetwork written to file:\n{out_file}")

    def generate_rq_data(self, contractors, out_file: str):
        da = TFDataAnalysis()

        samples = []

        prev_site = -1
        qstn_seq = {}
        qstns_seen = set()  # We will use this set to check if a sequence starts again
        for i, r in da.data.iterrows():
            if i % 1000 == 0:
                print("\rAt row {}...".format(i), end='')

            site = r['Sites : Nr']
            topic_id = r['Topic_Id']
            qstn_id = r['Question_Id']
            # TODO: What's going on here?
            if qstn_id < 0:
                print("Negative Question ID")
                continue

            comp = r['Compliancy']
            qstn_na = (type(comp) == float and math.isnan(comp))  # Is this question answered NA?
            if qstn_na:
                comp = "NA"

            # Continuation of run
            if prev_site == site and qstn_id not in qstns_seen:
                qstns_seen.add(qstn_id)
                qstn_seq[f'RQ{qstn_id}_A'] = comp
                if f'Topic_{topic_id}' not in qstn_seq:
                    qstn_seq[f'Topic_{topic_id}'] = "T"
            # Start of new sequence
            else:
                if prev_site > -1:
                    samples.append(qstn_seq)
                # Reset sequence
                qstn_seq = {
                    'Contractor': r['Sites : List'],
                    'Task': r['Sites : Task nbr'],
                    f'Topic_{topic_id}': "T",
                    f'RQ{qstn_id}_A': comp
                }
                qstns_seen = set()

            prev_site = site

        # Don't forget last sequence!
        samples.append(qstn_seq)
        print("\n")

        keys = ["Contractor", "Task"]
        for i in range(len(self.qns_per_topic)):
            keys.append(f"Topic_{i}")
        for i in self.qstns:
            if i < 0:
                continue
            keys.append(f"RQ{i}_A")

        with open(out_file, 'w') as fout:
            for j, k in enumerate(keys):
                fout.write(k)
                if j < len(keys)-1:
                    fout.write(',')
                else:
                    fout.write('\n')
            for sample in samples:
                if sample['Contractor'] not in contractors:
                    continue
                for j, k in enumerate(keys):
                    if k in sample:
                        fout.write(str(sample[k]))
                    elif k.startswith("Topic"):
                        fout.write("F")
                    else:
                        fout.write("NoAns")
                    if j < len(keys) - 1:
                        fout.write(',')
                    else:
                        fout.write('\n')


if __name__ == '__main__':
    cq_out_file = os.path.join(TenForceConfig.HOME,
                                    "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_cq.net")
    rq_out_file = os.path.join(TenForceConfig.HOME,
                                    "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_2in1_rq.net")

    bnm = SamIamModel1()
    da = TFDataAnalysis()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    # contractors = {"A : Sites", "C : Sites"}
    bnm.initialize_probs(da.data[da.data["Sites : List"].isin(contractors)])
    bnm.compile_bn_cq(out_file=cq_out_file)
    # bnm.compile_bn_rq(out_file=rq_out_file)
    # data_out_file = os.path.join(TenforceConfig.HOME,
    #                              "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "rq_2in1_data.dat")
    # bnm.generate_rq_data(contractors, out_file=data_out_file)
    # SamIamInterface.train_network(rq_out_file, data_out_file)

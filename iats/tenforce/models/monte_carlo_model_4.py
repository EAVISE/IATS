"""
This fourth model adds "time dependency" for the probabilities. The idea is that the "further back" in the past
an observation (=site) was, the less it counts toward the priors.

"""
import datetime
import os
import pickle
from collections import defaultdict

import pandas as pd
import numpy as np
from termcolor import cprint

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools
from iats.tenforce.tools.statistical_tools import StatisticalTools as st

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


class MonteCarloModel4:
    def __init__(self, days_cutoff: int = 360, thres_cutuff: float = 0.10):
        """

        Args:
            time_cutoff: number of days after which observations start counting less towards prior determination
            thres_cutuff: ignore sites/questions whose weight falls below this threshold
        """
        if days_cutoff <= 0:
            raise ValueError("Argument 'days_cutoff' should be > 0.")
        self.days_cutoff = days_cutoff
        self.thres_cutoff = thres_cutuff

        qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        self.qr, self.qcnts, self.qr_na, self.qcnts_na = pickle.load(open(qr_file, 'rb'))

        # Group questions per topic
        # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
        # the list of questions; hence it only related to questions that truly appear in the data itself.
        tm = TopicMapper()
        qns_per_topic = defaultdict(list)
        for qn_id in sorted(tm.topic_idx_per_qn.keys()):
            topic_idx = tm.topic_idx_per_qn[qn_id]
            qns_per_topic[topic_idx].append(qn_id)
        self.qns_per_topic = qns_per_topic
        # The following list contains the "bins" that group questions belonging to the same topic
        self.topic_edges = []
        for topic_idx in sorted(self.qns_per_topic.keys()):
            if not self.topic_edges:
                self.topic_edges.append(self.qns_per_topic[topic_idx][0])
            self.topic_edges.append(self.qns_per_topic[topic_idx][-1] + 1)

        self.probs = None

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.odds_area, self.cnts_area, self.cumul_area, self.cnts_cumul_area = None, None, None, None
        self.odds_task, self.cnts_task, self.cumul_task, self.cnts_cumul_task = None, None, None, None
        self.odds_noans, self.odds_na, self.odds_ok = None, None, None
        self.cnts_noans, self.cnts_na, self.cnts_ok = None, None, None

        self.cqs = CriticalQuestions(keep_top=5)

    def initialize_probs(self, df: pd.DataFrame, ref_date: datetime.date=None):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors; expected to be sorted chronologically, ascending
            ref_date: date from which prior observations will count less
        """
        # Take latest date in dataframe as ref_date if not specified; take "actual due date" for this
        if ref_date is None:
            ref_date = df['Sites : Actual due date'].iloc[-1]
        # Add days past reference date and according weight
        df = df.copy()
        df['Days Past'] = [(ref_date - x).days for x in df['Sites : Actual due date']]
        df['Days Weight'] = [1 if x < self.days_cutoff else
                             ((self.days_cutoff / x)**2 if self.days_cutoff / x > self.thres_cutoff else 0)
                             for x in df['Days Past']]
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        # Odds per contractor to get assigned a site in a given area
        odds_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        cnts_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        # Odds per contractor to get assigned a task
        odds_task = np.zeros((len(self.contractors),
                              len(self.tasks)))
        cnts_task = np.zeros((len(self.contractors),
                              len(self.tasks)))

        # Odds that a question is not answered
        odds_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        cnts_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        # Odds that a question is NA, given a task
        odds_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        cnts_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        # Odds of a question being answered OK (if not NA), given a task
        odds_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        cnts_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]

            site_weights = {}
            c_site_weights = []
            for site in da_c['Sites : Nr'].unique():
                site_weight = da_c[da_c['Sites : Nr'] == site].iloc[0]['Days Weight']
                site_weights[site] = site_weight
                c_site_weights.append(site_weight)

            c_site_weights = sum(c_site_weights)
            if not c_site_weights:
                cprint("No sites for contractor: [{}]".format(c), color="cyan")
                continue

            # Get (sub)area priors
            for isa, sa in enumerate(self.subareas):
                df_sa = da_c[da_c["Sites : Subarea"] == sa]
                sa_site_weights = sum([site_weights[x] for x in df_sa['Sites : Nr'].unique()])

                odds_area[ic, isa] = sa_site_weights / c_site_weights

            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_c_t = da_c[da_c["Sites : Task nbr"] == t]

                # Get task priors
                t_site_weights = sum([site_weights[x] for x in da_c_t['Sites : Nr'].unique()])

                odds_task[ic, it] = t_site_weights / c_site_weights
                cnts_task[ic, it] = c_site_weights

                # Get odds of questions being answered NA for this task,
                # as well as odds that this question is not answered at all
                for iq, q in enumerate(self.qstns):
                    da_ct_q = da_c_t[da_c_t["Question_Id"] == q]

                    # Odds not answered at all
                    q_site_weights = sum([site_weights[x] for x in da_ct_q['Sites : Nr'].unique()])
                    odds_noans[ic, it, iq] = 1 - (q_site_weights/t_site_weights) if t_site_weights > 0 else 1
                    cnts_noans[ic, it, iq] = t_site_weights if t_site_weights else 0

                    # Odds if answered
                    nb_q_nok, nb_q_ok, nb_q_sos, nb_q_na, _, _, _, _ = \
                        TFDataAnalysis.get_stats_for_df(da_ct_q, b_add_na=True, b_use_days_weight=True)

                    q_tot = nb_q_nok + nb_q_ok + nb_q_sos + nb_q_na
                    q_wa_na = q_tot - nb_q_na  # Nb. of times this question was not answered NA

                    # Use min(1, ...) to avoid some numerical stuff where you end up with odds that are
                    # ever so slightly bigger than 1, which results in errors later on when computing sqrt(p*(1-p))
                    odds_na[ic, it, iq] = min(1, nb_q_na / q_tot) if q_tot else 1
                    cnts_na[ic, it, iq] = q_tot
                    odds_ok[ic, it, iq] = min(1, nb_q_ok / q_wa_na) if q_wa_na else 0
                    cnts_ok[ic, it, iq] = q_wa_na

        print()

        self.odds_area = odds_area
        self.cnts_area = cnts_area
        self.cumul_area = np.zeros((len(self.contractors),
                                    len(self.subareas)))
        self.cnts_cumul_area = np.zeros((len(self.contractors),
                                         len(self.subareas)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            cnts_prev_sum = 0
            for isa, sa in enumerate(self.subareas):
                self.cumul_area[ic, isa] = self.odds_area[ic, isa] + prev_sum
                prev_sum += self.odds_area[ic, isa]
                self.cnts_cumul_area[ic, isa] = self.cnts_area[ic, isa] + cnts_prev_sum
                cnts_prev_sum += self.cnts_area[ic, isa]

        self.odds_task = odds_task
        self.cnts_task = cnts_task
        self.cumul_task = np.zeros((len(self.contractors),
                                    len(self.tasks)))
        self.cnts_cumul_task = np.zeros((len(self.contractors),
                                         len(self.tasks)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            cnts_prev_sum = 0
            for it, t in enumerate(self.tasks):
                self.cumul_task[ic, it] = self.odds_task[ic, it] + prev_sum
                prev_sum += self.odds_task[ic, it]
                self.cnts_cumul_task[ic, it] = self.cnts_task[ic, it] + cnts_prev_sum
                cnts_prev_sum += self.cnts_task[ic, it]

        self.odds_noans, self.cnts_noans = odds_noans, cnts_noans
        self.odds_na, self.cnts_na = odds_na, cnts_na
        self.odds_ok, self.cnts_ok = odds_ok, cnts_ok

    def _get_past_days(self, df: pd.DataFrame, ref_date: pd.Timestamp):
        site_date = df.iloc[0]['Sites : Actual due date']
        days_past = (ref_date - site_date).days
        date_weight = 1 if days_past < self.days_cutoff else self.days_cutoff / days_past
        return site_date, days_past, date_weight

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def _chose_area(self, c):
        """

        Args:
            c: contractor to chose a random area for; can be a string denominating the contractor, or its index

        Returns: the chosen area (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        area_idx = Tools.binary_search(self.cumul_area[c, :], r, b_return_insert=True)
        if area_idx < 0:
            area_idx = -(area_idx + 1)
        return area_idx

    def _chose_task(self, c):
        """

        Args:
            c: contractor to chose a random task for; can be a string denominating the contractor, or its index

        Returns: the chosen task (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        task_idx = Tools.binary_search(self.cumul_task[c, :], r, b_return_insert=True)
        if task_idx < 0:
            task_idx = -(task_idx + 1)
        return task_idx

    def get_pred_for(self, c, nb_iters=10, b_model_noans=True, b_model_cq=True, b_debug=True):
        """
        Simulate nb_iters

        Args:
            c: contractor (name or index)
            nb_iters: number of iterations
            b_model_noans: model effect of unanswered questions?
            b_model_cq: model effect of critical questions?
            b_debug: print some extra info

        Returns:

        """
        ic = self._get_contractor(c)

        defs = []
        q_ans = []
        q_ans_na = []
        for iter in range(nb_iters):
            # isa = self._chose_area(ic)
            it = self._chose_task(ic)
            # Precompute sum log per topic for odds noans
            sumlog_topic = {}
            for tidx in sorted(self.qns_per_topic.keys()):
                list_iq = []
                for qid in self.qns_per_topic[tidx]:
                    iq = self._qid_to_iq(qid)
                    # iq will be <0 if this question did not appear in the dataset used to compute the priors
                    if iq > -1:
                        list_iq.append(iq)
                sumlog_topic[tidx] = st.sum_log_probs(self.odds_noans[ic, it, list_iq], b_one_minus=True)

            nb_not_na = 0
            nb_ok = 0
            nb_na = 0
            b_critical = False

            # Keep track of what was decided per question
            answer_per_qn = np.zeros((max(self.qstns) + 1))
            # If we take into account critical questions, predict these first
            if b_model_cq:
                for qid in self.cqs.get_critical_ids():
                    iq = self._qid_to_iq(qid)
                    if b_model_noans:
                        # Answered at all?
                        if st.random_binomial_test(self.odds_noans[ic, it, iq], self.cnts_noans[ic, it, iq]):
                            continue
                    answer_per_qn[qid] = 1
                    # Answered NA?
                    if st.random_binomial_test(self.odds_na[ic, it, iq], self.cnts_na[ic, it, iq]):
                        nb_na += 1
                        continue
                    nb_not_na += 1
                    # Answered ok?
                    if st.random_binomial_test(self.odds_ok[ic, it, iq], self.cnts_ok[ic, it, iq]):
                        continue
                    # Woops... NOK. Critical?
                    if st.random_binomial_test(*self.cqs.get_critical_odds(qid, b_return_counts=True)):
                        b_critical = True
                        break

            if b_critical:
                defs.append(1.)
                q_ans.append(nb_na + nb_not_na)
                q_ans_na.append(nb_na)
                if b_debug:
                    print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(
                        iter, 1.0, nb_ok, nb_not_na, nb_na))
                continue

            nb_na, nb_not_na = 0, 0
            #
            # # Roll the dice on which questions are answered
            for iq, qid in enumerate(self.qstns):
                # Critical questions have already been decided, no need to roll again
                if b_model_cq and self.cqs.is_critical_question(qid):
                    continue
                if b_model_noans:
                    # Answered at all?
                    if st.random_binomial_test(self.odds_noans[ic, it, iq], self.cnts_noans[ic, it, iq]):
                        continue
                answer_per_qn[qid] = 1

            # Debug
            # print(answer_per_qn)
            # for i in range(1, len(self.topic_edges)):
            #     print("{}: {}".format(i-1, answer_per_qn[self.topic_edges[i-1]:self.topic_edges[i]]))

            # # Random completion
            # 1. Get indices of answered questions
            idx_ans = np.nonzero(answer_per_qn)[0].tolist()
            # 2. For these questions, generate random true/false for relevant pairs
            throws_per_qn = defaultdict(list)
            for qid in idx_ans:
                throws_per_qn[qid].append(1)  # Append existing result for current question
                idx_pairs = np.nonzero(self.qr[qid, :])[0].tolist()
                for other_id in idx_pairs:  # qr[qid, qid] == 0, so qid not present in idx_pairs
                    throws_per_qn[other_id].append(
                        st.random_binomial_test(self.qr[qid, other_id], self.qcnts[qid], b_return_int=True))
            # 3. Average over throws for each question
            avg_per_qn = []
            for i in range(len(throws_per_qn)):
                throws = throws_per_qn[i]
                avg_per_qn.append(np.mean(throws) if throws else 0)
            avg_per_qn = np.asarray(avg_per_qn)

            # Reset answer per question
            new_answer_per_qn = np.zeros((max(self.qstns) + 1))
            # 4. Per topic, consider all questions belonging to that topic and take:
            #     a. the sum of the logs of the probabilities that these questions were answered (=1-p(noans))
            #     b. the sum of the logs of the averages just computed
            # If b >= a: keep all questions answered in avg_per_qn.
            # If a > b : consider all questions unanswered
            for tidx in sorted(self.qns_per_topic.keys()):
                list_qid = []
                nb_for_topic = 0
                for qid in self.qns_per_topic[tidx]:
                    iq = self._qid_to_iq(qid)
                    if iq > -1:
                        nb_for_topic += answer_per_qn[qid]
                        list_qid.append(qid)
                a = sumlog_topic[tidx]
                # if nb_for_topic < math.sqrt(len(self.qns_per_topic[tidx])):
                #     b = 0
                # else:
                b = st.sum_log_probs(avg_per_qn[list_qid])

                # Keep questions considered as answered according to avg_per_qn
                if b > a:
                    for qid in list_qid:
                        if avg_per_qn[qid] > 0:
                            new_answer_per_qn[qid] = 1

            # 5. For questions that where answered, randomly generate answer
            for qid in np.nonzero(new_answer_per_qn)[0].tolist():
                iq = self._qid_to_iq(qid)
                # Answered NA?
                if st.random_binomial_test(self.odds_na[ic, it, iq], self.cnts_na[ic, it, iq]):
                    nb_na += 1
                    continue
                nb_not_na += 1
                # Answered ok?
                if st.random_binomial_test(self.odds_ok[ic, it, iq], self.cnts_ok[ic, it, iq]):
                    nb_ok += 1

            defi = 1 - nb_ok/nb_not_na if nb_not_na else 1
            if b_debug:
                print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(iter, defi, nb_ok, nb_not_na, nb_na))

            defs.append(defi)
            q_ans.append(sum(new_answer_per_qn))
            q_ans_na.append(nb_na)

        return np.mean(defs), np.std(defs), defs, q_ans, q_ans_na


if __name__ == '__main__':
    # -> kijk naar evolutie van quantiles
    mcm = MonteCarloModel4()
    da = TFDataAnalysis()

    # Most long ago and most recent 'Actual due date'
    date_min = pd.Timestamp(2016, 10, 1)
    date_max = da.data['Sites : Actual due date'].max()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    date_till = date_min
    inc_months = 4
    while (date_max - date_till).days > -inc_months * 31:
        print("Taking into account data till ", end='')
        cprint("{}".format(date_till.date()), color='cyan')
        sub_da = da.data[da.data['Sites : Actual due date'] <= date_till]
        mcm.initialize_probs(sub_da)
        for c in ["G : Sites"]:  # sorted(contractors):
            mean, std, defs, q_ans, q_ans_na =\
                mcm.get_pred_for(c, nb_iters=2500, b_model_noans=True, b_model_cq=True, b_debug=False)
            q_ans_not_na = [q_ans[i] - q_ans_na[i] for i in range(len(defs))]
            print("Contractor: {} -- Mean: {:5.3f} -- Std: {:5.3f}".format(c, mean, std))
            print("Quantiles: 5%: {:3.3f} -- 25%: {:3.3f} -- 50%: {:3.3f} -- 75%: {:3.3f} -- 95%: {:3.3f}".format(
                np.quantile(defs, 0.05), np.quantile(defs, 0.25), np.quantile(defs, 0.5),
                np.quantile(defs, 0.75), np.quantile(defs, 0.95)))

            if True:
                # plt.figure()
                # plt.ylim(0, 1.05)
                #
                # plt.title("Monte Carlo simulation for\ncontractor {}".format(c))
                # plt.plot(range(len(defs)), defs, '.b')
                # plt.xlabel("Random sample")
                # plt.ylabel("Deficiency")
                # plt.show()

                fig = plt.figure()
                fig.suptitle("Contractor: {}".format(c), y=1)

                ax1 = plt.subplot(211)
                ax1.bar(range(len(defs)), q_ans_not_na)
                ax1.bar(range(len(defs)), q_ans_na, bottom=q_ans_not_na)
                ax1.set_ylabel("#Questions")

                ax2 = plt.subplot(212, sharex=ax1)
                ax2.set_ylim(0, 1.05)
                ax2.plot(range(len(defs)), defs, '.b')
                ax2.set_ylabel("Deficiency")
                ax2.set_xlabel("Site")

                plt.tight_layout()
                plt.show()

                plt.figure()
                plt.hist(defs, bins=np.arange(0, 1.01, 0.01))
                plt.show()

        date_till = date_till + pd.DateOffset(months=inc_months)

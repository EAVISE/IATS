import os
from collections import Counter

from tqdm import tqdm

from iats.tenforce.models.pomegranate_tools import PomeGranateTools
from iats.tenforce.models.read_hugin_network import ReadHuginNetwork
from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


def main():
    bn_cq_file = os.path.join(TenForceConfig.HOME,
                            "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_cq.net")
    bn_rq_file = os.path.join(TenForceConfig.HOME,
                            "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_2in1_rq_em.net")

    bn_cq = ReadHuginNetwork.read_file(bn_cq_file)
    bn_rq = ReadHuginNetwork.read_file(bn_rq_file)

    def sample(choice: {} = None):
        # First, sample critical questions
        sample_cq = PomeGranateTools.sample_network(bn=bn_cq, parents=choice)
        choice = {"Contractor": sample_cq["Contractor"], "Task": sample_cq["Task"]}
        if sample_cq["Sum"] == 'T':
            return choice, {}, 1
        # If critical step is survived, sample remaing questions, seeding contractor and site from previous step
        sample_rq = PomeGranateTools.sample_network(bn=bn_rq, parents=choice)
        q_ans = {}
        for i in [5, 6, 7]:
            name = bn_cq.states[i].name
            q_ans[name] = sample_cq[name]
        for i, state in enumerate(bn_rq.states):
            if state.name.endswith('_A'):
                q_ans[state.name] = sample_rq[state.name]

        cnts = Counter(q_ans.values())
        denom = cnts["OK"] + cnts["NOK"]
        if denom == 0:
            defi = 1
        else:
            defi = cnts["NOK"]/denom
        return choice, q_ans, defi

    nb_samples = 1250
    samples = []
    for _ in tqdm(range(nb_samples)):
        samples.append(sample(choice={"Contractor": "A : Sites"}))

    tasks = Counter()
    for sample in samples:
        tasks[sample[0]["Task"]] += 1

    for k, v in tasks.items():
        print(f"{k}: {v/nb_samples}")

    plt.figure()
    plt.plot([sample[2] for sample in samples], '.b')
    plt.show()


if __name__ == '__main__':
    main()

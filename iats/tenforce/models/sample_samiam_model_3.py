"""
Helper class to sample the SamIam Model 3.
"""
import os
from collections import Counter
import numpy as np

from tqdm import tqdm

from iats.tenforce.models.pomegranate_tools import PomeGranateTools
from iats.tenforce.models.read_hugin_network import ReadHuginNetwork
from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


class SampleSamIam3:
    def __init__(self, bn_file=None):
        if bn_file is None:
            bn_file = os.path.join(TenForceConfig.HOME,
                                   "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model3_em.net")
        self.model = ReadHuginNetwork.read_file(bn_file)

    def sample(self, parents: {} = None):
        sample_bn = PomeGranateTools.sample_network(bn=self.model, parents=parents)

        q_ans = {}
        for i, state in enumerate(self.model.states):
            if state.name.startswith('RQ') and state.name.endswith('_A'):
                q_ans[state.name] = sample_bn[state.name]

        cnts = Counter(q_ans.values())
        denom = cnts["OK"] + cnts["NOK"]
        if denom == 0:
            defi = 1
        else:
            defi = cnts["NOK"]/denom
        return sample_bn, q_ans, defi


def main():
    sampler = SampleSamIam3()
    nb_samples = 1000
    samples = []
    for _ in tqdm(range(nb_samples)):
        samples.append(sampler.sample(parents={"Contractor": "G : Sites"}))
    defis = [sample[2] for sample in samples]

    tasks = Counter()
    for sample in samples:
        tasks[sample[0]["Task"]] += 1

    for k, v in tasks.items():
        print(f"{k}: {v/nb_samples}")

    plt.figure()
    ax1 = plt.subplot(211)
    ax1.plot(defis, '.b')
    ax2 = plt.subplot(212)
    ax2.hist(defis, np.arange(0, 1.01, 0.02))
    plt.show()


if __name__ == '__main__':
    main()

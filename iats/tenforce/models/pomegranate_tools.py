"""
This class contains methods that overwrite or expand some of the default behavior of PomeGranate Bayesian networks.
"""
import numpy as np

import pomegranate as pg
from pomegranate.BayesianNetwork import BayesianNetwork


class ExtBayesianNetwork(BayesianNetwork):
    """
    'Extended' Bayesian Network: this expands the 'BayesianNetwork' PomeGranate class with two variables,
    'self.names' and 'self.distris', that contain the names and CPTs of the nodes respectively.

    """
    def __init__(self, name):
        super().__init__(name)
        self.names = None
        self.distris = None

    def bake(self):
        super().bake()
        self.names = [self.states[i].name for i in range(len(self.states))]
        self.distris = {self.names[i]: self.states[i].distribution for i in range(len(self.states))}


class ExtCPT(pg.ConditionalProbabilityTable):
    def __init__(self, table, parents, frozen=False):
        super().__init__(table, parents, frozen)

        self.d_values = {}  # d_values = dictionary values
        for row in table:
            self.d_values[tuple(row[:-1])] = row[-1]

    # Taken from pomegranate/utils.pyx; for some reason this method does not appear in the compiled object
    @staticmethod
    def _check_random_state(seed=None):
        """Turn seed into a np.random.RandomState instance.

        This function will check to see whether the input seed is a valid seed
        for generating random numbers. This is a slightly modified version of
        the code from sklearn.utils.validation.

        Parameters
        ----------
        seed : None | int | instance of RandomState
            If seed is None, return the RandomState singleton used by np.random.
            If seed is an int, return a new RandomState instance seeded with seed.
            If seed is already a RandomState instance, return it.
            Otherwise raise ValueError.
        """

        if seed is None or seed is np.random:
            return np.random.mtrand._rand
        # if isinstance(seed, (np.Integral, np.integer)):
        #     return np.random.RandomState(seed)
        if isinstance(seed, np.random.mtrand.RandomState):
            return seed
        raise ValueError('%r cannot be used to seed a numpy.random.RandomState'
                         ' instance' % seed)

    def alt_sample(self, parent_values=None, n=None, random_state=None):
        """
        Return a random sample from the conditional probability table.

        This is a reworking of the original sample method from PomeGranate's ConditionalProbabilityTable class
        that will first randomly chose a threshold as a function of the amount of times a particular observation
        (i.e., an entry in the CPT table) was made, and then randomly select the outcome of the corresponding
        coin flip.

        Note that the original implementation of the sample method is in Cython and is approx. 2x faster.

        Args:
            parent_values:
            n:
            random_state:

        Returns:

        """
        random_state = self._check_random_state(random_state)

        if parent_values is None:
            parent_values = {}

        for parent in self.parents:
            if parent not in parent_values:
                parent_values[parent] = parent.sample(
                    random_state=random_state)

        sample_cands = []
        sample_vals = []

        # From all possible configurations for this node, pick out the one specified by the used parents
        for key, ind in self.keymap.items():
            # This for-loop will only be 'passed' if the parents of the configuration of interest
            # corresponds to the parents for this particular entry in the keymap, which means that
            # key[:-1] should correspond to parent_values
            for j, parent in enumerate(self.parents):
                if parent_values[parent] != key[j]:
                    break
            else:
                sample_cands.append(key[-1])
                # The next line is the original line of code from the Cython source:
                # sample_vals.append(cexp(self.values[ind]))
                # Since we are working in Python here, and not Cython, we can not use the cexp method used in the
                # original code.
                # But actually, we don't need it. In the original code, self.values is a C-style array containing
                # the logs of the CPs contained in the CPT. However, since this is a C-variable, it is not callable
                # from outside Cython. In other words, we don't have access to this field in this code.
                # For this reason, and to avoid having to use the exponential function, we have created a new
                # self.values variable in the __init__ of this class, which contains the CPs directly, not their
                # log values.
                sample_vals.append(self.d_values[key])

        # This next line is not necessary for our implementation, since values should always sum to 1
        # sample_vals /= np.sum(sample_vals)

        # According to the numpy docs, using 'nonzero' is preferred in this case:
        # https://docs.scipy.org/doc/numpy/reference/generated/numpy.where.html
        # sample_ind = np.where(random_state.multinomial(1, sample_vals))[0][0]
        sample_ind = np.nonzero(random_state.multinomial(1, sample_vals))[0][0]

        if n is None:
            return sample_cands[sample_ind]
        else:
            states = random_state.randint(1000000, size=n)
            return [self.sample(parent_values, n=None, random_state=state)
                    for state in states]


class PomeGranateTools:
    @staticmethod
    def sample_network(bn: ExtBayesianNetwork, parents: dict = None):
        """
        Custom method to sample a PomeGranate Bayesian network. The reason we have to write our own method is that
        the foreseen method in the PomeGranate package is not implemented yet.

        Args:
            bn: the Bayesian network to sample
            parents: dictionary containing the parent nodes and their values, in case you want to fix some nodes

        Returns:
            choice: a dictionary containing as keys the names of the nodes and as values their sampled value

        """
        choice = parents.copy()
        if choice is None:
            choice = {}
            chosen = set()
        else:
            chosen = set([bn.names.index(k) for k in choice.keys()])

        # Get root(s)
        root = []
        for i, tpl in enumerate(bn.structure):
            # Is this a root node?
            if not tpl:
                root.append(i)

        # Make random selection for root(s), unless...
        for r in root:
            # ...the value for the root has been specified by user.
            if r in chosen:
                continue
            choice[bn.names[r]] = bn.distris[bn.names[r]].sample()
            chosen.add(r)
        # Check children recursively
        while len(chosen) < len(bn.states):
            for idx, parents in enumerate(bn.structure):
                # State had already been chosen?
                if idx in chosen:
                    continue
                # All prerequisites are met?
                if set(parents).issubset(chosen):
                    name = bn.names[idx]
                    parent_values = {bn.distris[bn.states[p].name]: choice[bn.states[p].name] for p in parents}
                    choice[name] = bn.distris[name].sample(parent_values=parent_values)
                    chosen.add(idx)

        return choice


if __name__ == '__main__':
    print("Hihi!")
    print(ExtCPT._check_random_state())

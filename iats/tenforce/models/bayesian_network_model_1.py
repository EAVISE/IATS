"""
First attempt at a Bayesian Network implementation.
!!! CAUTION !!! Although the names of the objects containing the priors (e.g., self.odds_na) are the
same as for the Markov models, they are computed differently.

This first model will take into account Critical Questions, but will, if this step is survived,
answer each remaining question independently.
"""
import pickle
from collections import defaultdict, Counter
from itertools import product

import numpy as np
import os
import pandas as pd
import matplotlib

from iats.tenforce.models.pomegranate_tools import PomeGranateTools

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt

import pomegranate as pg

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.tenforce.tenforce_config import TenForceConfig
from iats.utils.tools import Tools


class BayesianNetworkModel1:
    def __init__(self):
        qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        self.qr, self.qcnts, self.qr_na, self.qcnts_na = pickle.load(open(qr_file, 'rb'))

        # Group questions per topic
        # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
        # the list of questions; hence it only related to questions that truly appear in the data itself.
        tm = TopicMapper()
        qns_per_topic = defaultdict(list)
        for qn_id in sorted(tm.topic_idx_per_qn.keys()):
            topic_idx = tm.topic_idx_per_qn[qn_id]
            qns_per_topic[topic_idx].append(qn_id)
        self.qns_per_topic = qns_per_topic
        # The following list contains the "bins" that group questions belonging to the same topic
        self.topic_edges = []
        for topic_idx in sorted(self.qns_per_topic.keys()):
            if not self.topic_edges:
                self.topic_edges.append(self.qns_per_topic[topic_idx][0])
            self.topic_edges.append(self.qns_per_topic[topic_idx][-1] + 1)

        self.probs = None

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.odds_area, self.cnts_area, self.cumul_area, self.cnts_cumul_area = None, None, None, None
        self.odds_task, self.cnts_task, self.cumul_task, self.cnts_cumul_task = None, None, None, None
        self.odds_noans, self.odds_na, self.odds_ok, self.odds_nok = None, None, None, None
        self.cnts_noans, self.cnts_na, self.cnts_ok, self.cnts_nok = None, None, None, None

        self.cqs = CriticalQuestions(keep_top=3)

        self.cq_bnm, self._cq_root, self._cq_children = None, None, None
        self.rq_bnm, self._rq_root, self._rq_children = None, None, None

    def initialize_probs(self, df: pd.DataFrame):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors.
        """
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        # Odds per contractor to get assigned a site in a given area
        odds_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        # cnts_area = np.zeros((len(self.contractors),
        #                       len(self.subareas)))
        # Odds per contractor to get assigned a task
        odds_task = np.zeros((len(self.contractors),
                              len(self.tasks)))
        # cnts_task = np.zeros((len(self.contractors),
        #                       len(self.tasks)))
        # Odds that a question is not answered
        odds_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        # cnts_noans = np.zeros((len(self.contractors),
        #                        len(self.tasks),
        #                        len(self.qstns)))
        # Odds that a question is NA, given a task
        odds_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        # cnts_na = np.zeros((len(self.contractors),
        #                     len(self.tasks),
        #                     len(self.qstns)))
        # Odds of a question being answered OK/SOS, given a task
        odds_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        # cnts_ok = np.zeros((len(self.contractors),
        #                     len(self.tasks),
        #                     len(self.qstns)))
        # Odds of a question being answered NOK, given a task
        odds_nok = np.zeros((len(self.contractors),
                             len(self.tasks),
                             len(self.qstns)))
        # cnts_nok = np.zeros((len(self.contractors),
        #                      len(self.tasks),
        #                      len(self.qstns)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]
            nb_c_sites = len(da_c["Sites : Nr"].unique())
            if not nb_c_sites:
                # cprint("No sites for contractor: [{}]".format(c), color="cyan")
                continue

            # Get (sub)area priors
            for isa, sa in enumerate(self.subareas):
                nb_sa_sites = len(da_c[da_c["Sites : Subarea"] == sa]["Sites : Nr"].unique())
                odds_area[ic, isa] = nb_sa_sites / nb_c_sites

            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_c_t = da_c[da_c["Sites : Task nbr"] == t]

                # Get task priors
                nb_t_sites = len(da_c_t["Sites : Nr"].unique())
                odds_task[ic, it] = nb_t_sites / nb_c_sites
                # cnts_task[ic, it] = nb_c_sites

                # Get odds of questions being answered NA for this task,
                # as well as odds that this question is not answered at all
                for iq, q in enumerate(self.qstns):
                    da_ct_q = da_c_t[da_c_t["Question_Id"] == q]

                    # Odds not answered at all
                    nb_q_sites = len(da_ct_q["Sites : Nr"].unique())
                    odds_noans[ic, it, iq] = 1 - (nb_q_sites/nb_t_sites) if nb_t_sites > 0 else 1
                    # cnts_noans[ic, it, iq] = nb_t_sites if nb_t_sites else 1

                    # Odds if answered
                    nb_q_nok, nb_q_ok, nb_q_sos, nb_q_na, _, _, _, _ = \
                        TFDataAnalysis.get_stats_for_df(da_ct_q, b_add_na=True)

                    q_tot = nb_q_nok + nb_q_ok + nb_q_sos + nb_q_na
                    q_wa_na = q_tot - nb_q_na  # Nb. of times this question was not answered NA

                    odds_na[ic, it, iq] = nb_q_na / q_tot if q_tot else 1
                    # cnts_na[ic, it, iq] = q_tot
                    odds_ok[ic, it, iq] = (nb_q_ok + nb_q_sos) / q_tot if q_tot else 0
                    # cnts_ok[ic, it, iq] = q_wa_na
                    odds_nok[ic, it, iq] = nb_q_nok / q_tot if q_tot else 0
        print()

        self.odds_area = odds_area
        # self.cnts_area = cnts_area
        # self.cumul_area = np.zeros((len(self.contractors),
        #                             len(self.subareas)))
        # self.cnts_cumul_area = np.zeros((len(self.contractors),
        #                                  len(self.subareas)))
        # for ic, c in enumerate(self.contractors):
        #     prev_sum = 0
        #     cnts_prev_sum = 0
        #     for isa, sa in enumerate(self.subareas):
        #         self.cumul_area[ic, isa] = self.odds_area[ic, isa] + prev_sum
        #         prev_sum += self.odds_area[ic, isa]
        #         self.cnts_cumul_area[ic, isa] = self.cnts_area[ic, isa] + cnts_prev_sum
        #         cnts_prev_sum += self.cnts_area[ic, isa]

        self.odds_task = odds_task
        # self.cnts_task = cnts_task
        # self.cumul_task = np.zeros((len(self.contractors),
        #                             len(self.tasks)))
        # self.cnts_cumul_task = np.zeros((len(self.contractors),
        #                                  len(self.tasks)))
        # for ic, c in enumerate(self.contractors):
        #     prev_sum = 0
        #     cnts_prev_sum = 0
        #     for it, t in enumerate(self.tasks):
        #         self.cumul_task[ic, it] = self.odds_task[ic, it] + prev_sum
        #         prev_sum += self.odds_task[ic, it]
        #         self.cnts_cumul_task[ic, it] = self.cnts_task[ic, it] + cnts_prev_sum
        #         cnts_prev_sum += self.cnts_task[ic, it]

        self.odds_noans = odds_noans
        # self.cnts_noans = cnts_noans
        self.odds_na = odds_na
        # self.cnts_na = cnts_na
        self.odds_ok = odds_ok
        # self.cnts_ok = cnts_ok
        self.odds_nok = odds_nok
        # self.cnts_nok = cnts_nok

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _get_area(self, a: str or int):
        if type(a) is int:
            return a
        return Tools.binary_search(self.subareas, a)

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def _chose_area(self, c):
        """

        Args:
            c: contractor to chose a random area for; can be a string denominating the contractor, or its index

        Returns: the chosen area (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        area_idx = Tools.binary_search(self.cumul_area[c, :], r, b_return_insert=True)
        if area_idx < 0:
            area_idx = -(area_idx + 1)
        return area_idx

    def _chose_task(self, c):
        """

        Args:
            c: contractor to chose a random task for; can be a string denominating the contractor, or its index

        Returns: the chosen task (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        task_idx = Tools.binary_search(self.cumul_task[c, :], r, b_return_insert=True)
        if task_idx < 0:
            task_idx = -(task_idx + 1)
        return task_idx

    def compile_bn_cq(self):
        """
        Compile Bayesian Network that predicts critical questions.

        Returns:

        """
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        node_contractor = pg.DiscreteDistribution({f"{ctor}": 1/nb_ctors for ctor in self.contractors})

        # Contractor -> Subarea
        cpt = []
        for c in self.contractors:
            ic = self._get_contractor(c)
            for t in self.tasks:
                it = self._get_task(t)
                cpt.append([f"{c}", f"{t}", self.odds_task[ic, it]])
        node_task = pg.ConditionalProbabilityTable(cpt, [node_contractor])

        # Subarea -> Critical Questions: Answered at all?
        nodes_cq = []
        cq_ids = sorted(self.cqs.get_critical_ids())
        for qid in cq_ids:
            iq = self._qid_to_iq(qid)
            cpt_na = []
            for c in self.contractors:
                ic = self._get_contractor(c)
                for t in self.tasks:
                    it = self._get_task(t)
                    cpt_na.append([f"{c}", f"{t}", 'NoAns', self.odds_noans[ic, it, iq]])
                    cpt_na.append([f"{c}", f"{t}", 'Ans', 1 - self.odds_noans[ic, it, iq]])
            nodes_cq.append(pg.ConditionalProbabilityTable(cpt_na, [node_contractor, node_task]))

        # Critical Question: What answer?
        nodes_cq_ans = []
        for i, qid in enumerate(cq_ids):
            iq = self._qid_to_iq(qid)
            cpt_ans = []
            for c in self.contractors:
                ic = self._get_contractor(c)
                for t in self.tasks:
                    it = self._get_task(t)
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'NA', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'OK', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'NOK', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'None', 1])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'NA', self.odds_na[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'OK', self.odds_ok[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'NOK', self.odds_nok[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'None', 0])
            nodes_cq_ans.append(pg.ConditionalProbabilityTable(cpt_ans, [node_contractor, node_task, nodes_cq[i]]))

        # Critical Question: Critical?
        nodes_cq_critical = []
        for i, qid in enumerate(cq_ids):
            cpt_crit = []
            for ans in ['NA', 'OK', 'NOK', 'None']:
                if ans == 'NOK':
                    c_odds = self.cqs.get_critical_odds(qid)
                    cpt_crit.append([ans, 'T', c_odds])
                    cpt_crit.append([ans, 'F', 1 - c_odds])
                else:
                    cpt_crit.append([ans, 'T', 0])
                    cpt_crit.append([ans, 'F', 1])
            nodes_cq_critical.append(pg.ConditionalProbabilityTable(cpt_crit, [nodes_cq_ans[i]]))

        # Group CQ answers in one node
        cpt_cq_sum = []
        for prod in product(["T", "F"], repeat=len(nodes_cq_critical)):
            prod = list(prod)
            if prod == len(nodes_cq_critical)*["F"]:
                cpt_cq_sum.append(prod + ['T', 0])
                cpt_cq_sum.append(prod + ['F', 1])
            else:
                cpt_cq_sum.append(prod + ['F', 0])
                cpt_cq_sum.append(prod + ['T', 1])
        node_cq_sum = pg.ConditionalProbabilityTable(cpt_cq_sum, nodes_cq_critical)

        # ##############################
        # Create "states", a.k.a., nodes
        s_ctor = pg.State(node_contractor, "Contractor")
        s_task = pg.State(node_task, "Task")
        # Critical Question Answered/ Which answer/ Critical?
        ss_cq = [pg.State(node_cq, "CQ.A {}".format(cq_ids[i])) for i, node_cq in enumerate(nodes_cq)]
        ss_cqa = [pg.State(node_cqa, "CQ.Ans {}".format(cq_ids[i])) for i, node_cqa in enumerate(nodes_cq_ans)]
        ss_cqcrit = [pg.State(node_cqcrit, "CQ.Crit {}".format(cq_ids[i])) for i, node_cqcrit in enumerate(nodes_cq_critical)]
        s_cq_sum = pg.State(node_cq_sum, "CQ.Sum")

        # ##############################
        # Create Bayesian Network
        bnm = pg.BayesianNetwork("Bayesian Network")
        # Add all nodes to the network
        all_nodes = [s_ctor, s_task] + ss_cq + ss_cqa + ss_cqcrit
        all_nodes.append(s_cq_sum)
        for i, state in enumerate(all_nodes):
            bnm.add_state(state)
            # print(f"{i:2d}: [{state.name}]")
        # Contractor --> Task
        bnm.add_edge(s_ctor, s_task)
        # Contractor/Task --> CQ.A
        for state in ss_cq:
            bnm.add_edge(s_ctor, state)
            bnm.add_edge(s_task, state)
        # Contractor/Task/CQ.A --> CQ.Ans
        for i, state in enumerate(ss_cqa):
            bnm.add_edge(s_ctor, state)
            bnm.add_edge(s_task, state)
            bnm.add_edge(ss_cq[i], state)
        # CQ.Ans --> CQ.Crit & CQ.Crit --> CQ.Sum
        for i, state in enumerate(ss_cqcrit):
            bnm.add_edge(ss_cqa[i], state)
            bnm.add_edge(state, s_cq_sum)

        bnm.bake()

        self.cq_bnm = bnm

        # Get children per node
        root = []
        children = defaultdict(list)
        for i, tpl in enumerate(bnm.structure):
            # Is this a root node?
            if not tpl:
                root.append(i)
            for j in tpl:
                children[j].append(i)
        self._cq_root = root
        self._cq_children = children

        print("Critical Questions BayesianNetwork initialized...")

    def compile_bn_rq(self):
        """
        Compile Bayesian Network that predicts remaining questions.

        Returns:

        """
        # ##############################
        # Create distributions
        # Root node: contractor
        nb_ctors = len(self.contractors)
        node_contractor = pg.DiscreteDistribution({f"{ctor}": 1 / nb_ctors for ctor in self.contractors})

        # Contractor -> Subarea
        cpt = []
        for c in self.contractors:
            ic = self._get_contractor(c)
            for t in self.tasks:
                it = self._get_task(t)
                cpt.append([f"{c}", f"{t}", self.odds_task[ic, it]])
        node_task = pg.ConditionalProbabilityTable(cpt, [node_contractor])

        # Remaining Questions: answered?
        nodes_rq = []
        rq_ids = sorted(set(self.qstns).difference(self.cqs.get_critical_ids()))

        for qid in rq_ids:
            iq = self._qid_to_iq(qid)
            cpt_na = []
            for c in self.contractors:
                ic = self._get_contractor(c)
                for t in self.tasks:
                    it = self._get_task(t)
                    na_odds = self.odds_noans[ic, it, iq]
                    cpt_na.append([f"{c}", f"{t}", 'NoAns', na_odds])
                    cpt_na.append([f"{c}", f"{t}", 'Ans', 1 - na_odds])
            nodes_rq.append(pg.ConditionalProbabilityTable(cpt_na, [node_contractor, node_task]))

        # Remaining Questions: what answer?
        nodes_rq_ans = []
        for i, qid in enumerate(rq_ids):
            iq = self._qid_to_iq(qid)
            cpt_ans = []
            for c in self.contractors:
                ic = self._get_contractor(c)
                for t in self.tasks:
                    it = self._get_task(t)
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'NA', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'OK', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'NOK', 0])
                    cpt_ans.append([f"{c}", f"{t}", 'NoAns', 'None', 1])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'NA', self.odds_na[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'OK', self.odds_ok[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'NOK', self.odds_nok[ic, it, iq]])
                    cpt_ans.append([f"{c}", f"{t}", 'Ans', 'None', 0])
            nodes_rq_ans.append(pg.ConditionalProbabilityTable(cpt_ans, [node_contractor, node_task, nodes_rq[i]]))

        # ##############################
        # Create "states", a.k.a., nodes
        s_ctor = pg.State(node_contractor, "Contractor")
        s_task = pg.State(node_task, "Task")
        # Remaning Questions
        ss_rq = [pg.State(node_q, f"RQ.A {rq_ids[i]}") for i, node_q in enumerate(nodes_rq)]
        ss_rqa = [pg.State(node_rqa, "RQ.Ans {}".format(rq_ids[i])) for i, node_rqa in enumerate(nodes_rq_ans)]

        # ##############################
        # Create Bayesian Network
        bnm = pg.BayesianNetwork("Bayesian Network")
        # Add all nodes to the network
        all_nodes = [s_ctor, s_task] + ss_rq + ss_rqa
        for i, state in enumerate(all_nodes):
            bnm.add_state(state)
            # print(f"{i:2d}: [{state.name}]")
        # Contractor --> Task
        bnm.add_edge(s_ctor, s_task)
        # Contractor/Task --> CQ.A
        for state in ss_rq:
            bnm.add_edge(s_ctor, state)
            bnm.add_edge(s_task, state)
        # Contractor/Task --> RQ
        for i, state in enumerate(ss_rq):
            bnm.add_edge(s_ctor, state)
            bnm.add_edge(s_task, state)
        # Contractor/Task/RQ.A --> RQ.Ans
        for i, state in enumerate(ss_rqa):
            bnm.add_edge(s_ctor, state)
            bnm.add_edge(s_task, state)
            bnm.add_edge(ss_rq[i], state)

        bnm.bake()

        self.rq_bnm = bnm

        # Get children per node
        root = []
        children = defaultdict(list)
        for i, tpl in enumerate(bnm.structure):
            # Is this a root node?
            if not tpl:
                root.append(i)
            for j in tpl:
                children[j].append(i)
        self._rq_root = root
        self._rq_children = children

        print("Remaining Questions BayesianNetwork initialized...")

    def sample(self, choice: dict=None):
        # First, sample critical questions
        sample_cq = PomeGranateTools.sample_network(choice=choice, bn=self.cq_bnm)
        if sample_cq[11] == 'T':
            return {}, 1
        # If critical step is survived, sample remaing questions, seeding contractor and site from previous step
        choice = {0: sample_cq[0], 1: sample_cq[1]}
        sample_rq = PomeGranateTools.sample_network(choice=choice, bn=self.rq_bnm)
        q_ans = {}
        for i in [5, 6, 7]:
            q_ans[self.cq_bnm.states[i].name] = sample_cq[i]
        # Number of questions may vary between contractors, so determine limits dynamically
        for i in range(1 + len(sample_rq)//2, len(sample_rq)):
            q_ans[self.rq_bnm.states[i].name] = sample_rq[i]

        cnts = Counter(q_ans.values())
        return q_ans, cnts["NOK"]/(cnts["OK"]+cnts["NOK"])


if __name__ == '__main__':
    bnm = BayesianNetworkModel1()
    da = TFDataAnalysis()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    bnm.initialize_probs(da.data[da.data["Sites : List"].isin(contractors)])
    bnm.compile_bn_cq()
    bnm.cq_bnm.plot("bn_cq_topology.pdf")
    bnm.compile_bn_rq()
    bnm.rq_bnm.plot("bn_rq_topology.pdf")
    # for e in bnm.bnm.marginal():
    #     print(e)
    # crit = []
    # for _ in range(1):
    #     crit.append(bnm.sample(choice={0: "C : Sites"})[11])
    # print(Counter(crit))
    # print(bnm.sample_network(network=Network.CQ))
    # print(bnm.sample_network(network=Network.RQ))
    samples = []
    for _ in range(500):
        samples.append(bnm.sample(choice={0: "G : Sites"})[1])

    plt.figure()
    plt.plot(samples, '.b')
    plt.show()

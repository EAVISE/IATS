"""
This first model "rolls the dice" for each question separately, disregarding distribution standard deviations.
"""
import pandas as pd
import numpy as np
from termcolor import cprint

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.utils.tools import Tools

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


class MonteCarloModel1:
    def __init__(self, deficiency_thres=0.2):
        """

        Args:
            deficiency_thres: the deficiency threshold above which a site is considered "not ok"
        """
        self.def_thres = deficiency_thres
        self.probs = None

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.odds_area, self.cumul_area = None, None
        self.odds_task, self.cumul_task = None, None
        self.odds_noans, self.odds_na, self.odds_ok = None, None, None

        self.cqs = CriticalQuestions()

    def initialize_probs(self, df: pd.DataFrame):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors.
        """
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        # Odds per contractor to get assigned a site in a given area
        odds_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        # Odds per contractor to get assigned a task
        odds_task = np.zeros((len(self.contractors),
                              len(self.tasks)))

        # Odds that a question is not answered
        odds_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        # Odds that a question is NA, given a task
        odds_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        # Odds of a question being answered OK (if not NA), given a task
        odds_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]
            nb_c_sites = len(da_c["Sites : Nr"].unique())
            if not nb_c_sites:
                cprint("No sites for contractor: [{}]".format(c), color="cyan")
                continue

            # Get (sub)area priors
            for isa, sa in enumerate(self.subareas):
                nb_sa_sites = len(da_c[da_c["Sites : Subarea"] == sa]["Sites : Nr"].unique())
                odds_area[ic, isa] = nb_sa_sites / nb_c_sites

            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_c_t = da_c[da_c["Sites : Task nbr"] == t]

                # Get task priors
                nb_t_sites = len(da_c[da_c["Sites : Task nbr"] == t]["Sites : Nr"].unique())
                odds_task[ic, it] = nb_t_sites / nb_c_sites

                # Get odds of questions being answered NA for this task,
                # as well as odds that this question is not answered at all
                for iq, q in enumerate(self.qstns):
                    da_ct_q = da_c_t[da_c_t["Question_Id"] == q]

                    # Odds not answered at all
                    nb_q_sites = len(da_ct_q["Sites : Nr"].unique())
                    odds_noans[ic, it, iq] = 1 - (nb_q_sites/nb_t_sites) if nb_t_sites > 0 else 1

                    # Odds if answered
                    nb_q_nok, nb_q_ok, nb_q_sos, nb_q_na, _, _, _, _ = \
                        TFDataAnalysis.get_stats_for_df(da_ct_q, b_add_na=True)

                    q_tot = nb_q_nok + nb_q_ok + nb_q_sos + nb_q_na
                    q_wa_na = q_tot - nb_q_na  # Nb. of times this question was not answered NA

                    odds_na[ic, it, iq] = nb_q_na / q_tot if q_tot else 0
                    odds_ok[ic, it, iq] = nb_q_ok / q_wa_na if q_wa_na else 0

                # for isa, sa in enumerate(self.subareas):
                #     da_ctsa = da_ct[da_ct["Sites : Subarea"] == sa]
                #
                #     tot_nok, tot_ok, tot_sos, tot_na, _, _, _, _ = TFDataAnalysis.get_stats_for_df(da_ctsa, b_add_na=True)
                #     total = tot_nok + tot_ok + tot_sos + tot_na
                #
                #     for itop, top in enumerate(self.topics):
                #
                #         da_ctsat = da_ctsa[da_ctsa["Topic_Id"] == top]
                #         top_nok, top_ok, top_sos, top_na, _, _, _, _ =\
                #             TFDataAnalysis.get_stats_for_df(da_ctsat, b_add_na=True)
                #
                #         top_total = top_nok + top_ok + top_sos + top_na
                #
                #         priors[ic, it, isa, itop] = top_ok/top_total if top_total > 0 else 0
                #         topic_weights[ic, it, isa, itop] = top_total/total if top_total > 0 else 0
        print()

        self.odds_area = odds_area
        self.cumul_area = np.zeros((len(self.contractors),
                                       len(self.subareas)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            for isa, sa in enumerate(self.subareas):
                self.cumul_area[ic, isa] = self.odds_area[ic, isa] + prev_sum
                prev_sum += self.odds_area[ic, isa]

        self.odds_task = odds_task
        self.cumul_task = np.zeros((len(self.contractors),
                                    len(self.tasks)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            for it, t in enumerate(self.tasks):
                self.cumul_task[ic, it] = self.odds_task[ic, it] + prev_sum
                prev_sum += self.odds_task[ic, it]

        self.odds_noans = odds_noans
        self.odds_na = odds_na
        self.odds_ok = odds_ok

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def _chose_area(self, c):
        """

        Args:
            c: contractor to chose a random area for; can be a string denominating the contractor, or its index

        Returns: the chosen area (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        area_idx = Tools.binary_search(self.cumul_area[c, :], r, b_return_insert=True)
        if area_idx < 0:
            area_idx = -(area_idx + 1)
        return area_idx

    def _chose_task(self, c):
        """

        Args:
            c: contractor to chose a random task for; can be a string denominating the contractor, or its index

        Returns: the chosen task (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        task_idx = Tools.binary_search(self.cumul_task[c, :], r, b_return_insert=True)
        if task_idx < 0:
            task_idx = -(task_idx + 1)
        return task_idx

    # def get_prob_for(self, contractor=None, task=None, subarea=None, topic_id: int=None):
    #     """
    #
    #     Args:
    #         contractor:
    #         task:
    #         subarea:
    #         topic_id:
    #
    #     Returns:
    #
    #     """
    #     if topic_id is not None and type(topic_id) is not int:
    #         raise TypeError("Expected an integer for topic_id, got {} instead.".format(type(topic_id)))
    #
    #     idx_c = contractor if type(contractor) is int else\
    #         Tools.binary_search(self.contractors, contractor) if contractor else -1
    #     idx_t = Tools.binary_search(self.tasks, task) if task > len(self.tasks) else task
    #     idx_s = subarea if type(subarea) is int else\
    #         Tools.binary_search(self.subareas, subarea) if subarea else -1
    #
    #     if (idx_c < 0 or idx_t < 0 or idx_s < 0) and topic_id is None:
    #         raise ValueError("topic_id can only be None if all other indices are specified.")
    #
    #     if idx_c > -1:
    #         if idx_t > -1:
    #             # All indices are known, return specific probability
    #             if idx_s > -1:
    #                 return sum([self.topic_weights[idx_c, idx_t, idx_s, i] * self.priors[idx_c, idx_t, idx_s, i]
    #                             for i in range(len(self.topic_weights))]) if topic_id is None\
    #                     else self.priors[idx_c, idx_t, idx_s, topic_id]
    #             # Return probabilities over subareas
    #             else:
    #                 return self.priors[idx_c, idx_t, :, topic_id]
    #         else:
    #             # Return probabilities over tasks
    #             if idx_s > -1:
    #                 return self.priors[idx_c, :, idx_s, topic_id]
    #             # Etc...
    #             else:
    #                 return self.priors[idx_c, :, :, topic_id]
    #     else:
    #         if idx_t > -1:
    #             if idx_s > -1:
    #                 return self.priors[:, idx_t, idx_s, topic_id]
    #             else:
    #                 return self.priors[:, idx_t, :, topic_id]
    #         else:
    #             if idx_s > -1:
    #                 return self.priors[:, :, idx_s, topic_id]
    #             else:
    #                 return self.priors[:, :, :, topic_id]

    def get_pred_for(self, c, nb_iters=10, b_model_noans=True, b_model_cq=True, b_debug=True):
        """
        Simulate nb_iters

        Args:
            c: contractor (name or index)
            nb_iters: number of iterations
            b_model_noans: model effect of unanswered questions?
            b_model_cq: model effect of critical questions?
            b_debug: print some extra info

        Returns:

        """
        ic = self._get_contractor(c)

        defs = []
        for iter in range(nb_iters):
            isa = self._chose_area(ic)
            it = self._chose_task(ic)
            nb_answered = 0
            nb_ok = 0
            nb_na = 0
            b_critical = False
            # If we take into account critical questions, predict these first
            if b_model_cq:
                for qid in self.cqs.get_critical_ids():
                    iq = self._qid_to_iq(qid)
                    if b_model_noans:
                        # Answered at all?
                        r = np.random.rand()
                        if r < self.odds_noans[ic, it, iq]:
                            continue
                    # Answered NA?
                    r = np.random.rand()
                    if r < self.odds_na[ic, it, iq]:
                        nb_na += 1
                        continue
                    nb_answered += 1
                    # Answered ok?
                    r = np.random.rand()
                    if r < self.odds_ok[ic, it, iq]:
                        nb_ok += 1
                        continue
                    # Woops... NOK. Critical?
                    r = np.random.rand()
                    if r < self.cqs.get_critical_odds(qid):
                        defs.append(1.)
                        b_critical = True
                        break

            if b_critical:
                if b_debug:
                    print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(
                        iter, 1.0, nb_ok, nb_answered, nb_na))
                continue

            for iq, q in enumerate(self.qstns):
                # If we model critical questions, skip critical questions in this step
                if b_model_cq and self.cqs.is_critical_question(self._iq_to_qid(iq)):
                    continue
                if b_model_noans:
                    # Answered at all?
                    r = np.random.rand()
                    if r < self.odds_noans[ic, it, iq]:
                        continue
                # Answered NA?
                r = np.random.rand()
                if r < self.odds_na[ic, it, iq]:
                    nb_na += 1
                    continue
                nb_answered += 1
                # Answered ok?
                r = np.random.rand()
                if r < self.odds_ok[ic, it, iq]:
                    nb_ok += 1

            defi = 1 - nb_ok/nb_answered if nb_answered else 1
            if b_debug:
                print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(iter, defi, nb_ok, nb_answered, nb_na))

            defs.append(defi)

        return np.mean(defs), np.std(defs), defs


if __name__ == '__main__':
    mcm = MonteCarloModel1()
    da = TFDataAnalysis()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    mcm.initialize_probs(da.data[da.data["Sites : List"].isin(contractors)])
    for epoch in range(1, 2):
        cprint("Epoch {}".format(epoch), color="cyan")
        for c in ["G : Sites"]:  # sorted(contractors):
            mean, std, defs = mcm.get_pred_for(c, nb_iters=500, b_model_noans=True, b_model_cq=True, b_debug=False)
            print("Contractor: {} -- Mean: {:5.3f} -- Std: {:5.3f}".format(c, mean, std))

            if True:
                plt.figure()
                plt.ylim(0, 1.05)

                plt.title("Monte Carlo simulation for\ncontractor {}".format(c))
                plt.plot(range(len(defs)), defs, '.b')
                plt.xlabel("Random sample")
                plt.ylabel("Deficiency")
                plt.show()

"""
Helper class to sample the SamIam Model 2.
"""
import os
from collections import Counter
import numpy as np

from tqdm import tqdm

from iats.tenforce.models.pomegranate_tools import PomeGranateTools
from iats.tenforce.models.read_hugin_network import ReadHuginNetwork
from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


class SampleSamIam2:
    def __init__(self, bn_file=None):
        if bn_file is None:
            bn_file = os.path.join(TenForceConfig.HOME,
                                   "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_em.net")
        self.model = ReadHuginNetwork.read_file(bn_file)

    def sample(self, parents: {} = None):
        """
        Generate one sample given the provided parents.

        Args:
            parents: the parent nodes and their values, in case you want to fix some nodes

        Returns:
            (sample_bn, q_ans, defi) where:
                sample_bn: the "filled in" Bayesian network
                q_ans: subset of sample_bn containing only the nodes corresponding to question answers (i.e., no
                contractor/area/task/topic... nodes)
                defi: the deficiency corresponding to this sample

        """
        sample_bn = PomeGranateTools.sample_network(bn=self.model, parents=parents)

        q_ans = {}
        for i, state in enumerate(self.model.states):
            if state.name.startswith('RQ') and state.name.endswith('_A'):
                q_ans[state.name] = sample_bn[state.name]

        cnts = Counter(q_ans.values())
        denom = cnts["OK"] + cnts["NOK"] + cnts["SOS"]
        if denom == 0:
            defi = 1
        else:
            defi = cnts["NOK"]/denom
        return sample_bn, q_ans, defi


def main():
    bn_file_1 = os.path.join(TenForceConfig.HOME,
                             "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "with_smoothing",
                             "bn_model2_alt_top5_20180614_nolb_ls0_area.net")
    bn_file_2 = os.path.join(TenForceConfig.HOME,
                             "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "with_smoothing",
                             "bn_model2_alt_top5_20180614_nolb_ls1_area.net")
    # bn_file_3 = os.path.join(TenForceConfig.HOME,
    #                          "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "with_smoothing",
    #                          "bn_model2_alt_top5_20180614_lb10_lsw1_area.net")

    plt.figure()
    bn_files = [bn_file_1, bn_file_2]
    plot_titles = ["20180614 Area LB=0 LS=0", "20180614 Area LB=0 LS=1"]
    for i, tpl in enumerate(zip(bn_files, plot_titles)):
        bn_file, plot_title = tpl
        sampler = SampleSamIam2(bn_file=bn_file)
        nb_samples = 10000
        samples = []
        for _ in tqdm(range(nb_samples)):
            samples.append(sampler.sample(parents={"Contractor": "A : Sites"}))
        defis = [sample[2] for sample in samples]
        ax = plt.subplot(len(bn_files), 1, i+1)
        ax.hist(defis, np.arange(0, 1.01, 0.01), density=True)
        ax.set_xlabel("Deficiency")
        ax.set_ylabel("#")
        ax.set_title(plot_title + f'\nNb. samples: {nb_samples}')

        print(f"File: {bn_file_1}")
        print("Control - Area priors:")
        tasks = Counter()
        for sample in samples:
            tasks[sample[0]["Area"]] += 1

        for k, v in tasks.items():
            print(f"{k}: {v/nb_samples}")

    plt.tight_layout()
    plt.show()


    # plt.figure()
    # ax1 = plt.subplot(211)
    # ax1.plot(defis, '.b')
    # ax1.set_ylim(-0.01, 1.02)
    # ax2 = plt.subplot(212)
    # ax2.hist(defis, np.arange(0, 1.01, 0.01), density=True)
    # ax3 = ax2.twinx()
    # ax3.hist(defis, np.arange(0, 1.01, 0.01), density=True, cumulative=True, color="red", align="left", rwidth=0.2)
    # plt.show()


if __name__ == '__main__':
    main()

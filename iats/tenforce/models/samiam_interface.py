"""
This class is used to call a Java class that uses SamIam (http://reasoning.cs.ucla.edu/samiam/index.php) to apply
Expectation Maximization to learn the CPTs of a Bayesian Network.
"""
import jpype


class SamIamInterface:
    @staticmethod
    def train_network(file_net, file_dat):
        """

        Args:
            file_net:
            file_dat:

        Returns:

        """

        cp = "-Djava.class.path=/home/lmertens/Work/Projects/IATS/Code/iats/tenforce/jlib/JCode.jar"
        # file_net = "/home/lmertens/Work/Projects/IATS/Data/TenForce/samiam/bn_2in1_rq.net"
        # file_dat = "/home/lmertens/Work/Projects/IATS/Data/TenForce/samiam/rq_2in1_data.dat"

        if not jpype.isJVMStarted():
            jpype.startJVM(jpype.getDefaultJVMPath(), '-ea', cp)
        jcode = jpype.JClass('jcode.Main')
        jcode.main(jpype.JArray(jpype.java.lang.String, 1)([file_net, file_dat]))
        # For some reason, calling shutdownJVM() causes a crash. Just don't call it, works perfectly fine like that.
        # jpype.shutdownJVM()

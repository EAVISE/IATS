"""
Same as monte_carlo_model_2, but with a different way of computing the odds of having a question not be answered at all.

Result: it does not work better.
"""
import pandas as pd
import numpy as np
from termcolor import cprint

from iats.tenforce.data.critical_questions import CriticalQuestions
from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.utils.tools import Tools
from iats.tenforce.tools.statistical_tools import StatisticalTools as st

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


class MonteCarloModel2bis:
    def __init__(self):
        self.probs = None

        self.contractors = None
        self.subareas = None
        self.tasks = None
        self.qstns = None

        self.odds_area, self.cnts_area, self.cumul_area, self.cnts_cumul_area = None, None, None, None
        self.odds_task, self.cnts_task, self.cumul_task, self.cnts_cumul_task = None, None, None, None
        self.odds_noans, self.odds_na, self.odds_ok = None, None, None
        self.cnts_noans, self.cnts_na, self.cnts_ok = None, None, None

        self.cqs = CriticalQuestions()

    def initialize_probs(self, df: pd.DataFrame):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors.
        """
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic
        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.qstns = sorted(list(df.Question_Id.unique()))
        # Odds per contractor to get assigned a site in a given area
        odds_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        cnts_area = np.zeros((len(self.contractors),
                              len(self.subareas)))
        # Odds per contractor to get assigned a task
        odds_task = np.zeros((len(self.contractors),
                              len(self.tasks)))
        cnts_task = np.zeros((len(self.contractors),
                              len(self.tasks)))

        # Odds that a question is not answered
        odds_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        cnts_noans = np.zeros((len(self.contractors),
                               len(self.tasks),
                               len(self.qstns)))
        # Odds that a question is NA, given a task
        odds_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        cnts_na = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        # Odds of a question being answered OK (if not NA), given a task
        odds_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))
        cnts_ok = np.zeros((len(self.contractors),
                            len(self.tasks),
                            len(self.qstns)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]
            nb_c_sites = len(da_c["Sites : Nr"].unique())
            if not nb_c_sites:
                cprint("No sites for contractor: [{}]".format(c), color="cyan")
                continue

            # Get (sub)area priors
            for isa, sa in enumerate(self.subareas):
                nb_sa_sites = len(da_c[da_c["Sites : Subarea"] == sa]["Sites : Nr"].unique())
                odds_area[ic, isa] = nb_sa_sites / nb_c_sites

            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_c_t = da_c[da_c["Sites : Task nbr"] == t]

                # Get task priors
                nb_t_sites = len(da_c_t["Sites : Nr"].unique())
                odds_task[ic, it] = nb_t_sites / nb_c_sites
                cnts_task[ic, it] = nb_c_sites

                # Get odds of questions being answered NA for this task,
                # as well as odds that this question is not answered at all
                for iq, q in enumerate(self.qstns):
                    da_ct_q = da_c_t[da_c_t["Question_Id"] == q]

                    if nb_t_sites:
                        # Odds not answered at all
                        nb_q_sites = len(da_ct_q["Sites : Nr"].unique())
                        q_ans_weights = []
                        for site in da_ct_q["Sites : Nr"].unique():
                            da_s = da_c_t[da_c_t["Sites : Nr"] == site]
                            # print("Len(site): {} :: Avg. ans. per q.: {} :: Ans. for q: {}".format(
                            #     len(da_s), len(da_s)/len(da_s["Question_Id"].unique()), len(da_s[da_s["Question_Id"] == q])
                            # ))
                            avg_ans = len(da_s)/len(da_s["Question_Id"].unique())
                            q_ans_weight = len(da_s[da_s["Question_Id"] == q])/avg_ans
                            q_ans_weights.append(q_ans_weight)

                        ratio = sum(q_ans_weights)/nb_t_sites
                        if ratio > 1:
                            ratio = 1
                        odds_noans[ic, it, iq] = 1 - ratio
                        cnts_noans[ic, it, iq] = nb_t_sites
                    else:
                        odds_noans[ic, it, iq] = 1
                        cnts_noans[ic, it, iq] = 0

                    # Odds if answered
                    nb_q_nok, nb_q_ok, nb_q_sos, nb_q_na, _, _, _, _ = \
                        TFDataAnalysis.get_stats_for_df(da_ct_q, b_add_na=True)

                    q_tot = nb_q_nok + nb_q_ok + nb_q_sos + nb_q_na
                    q_wa_na = q_tot - nb_q_na  # Nb. of times this question was not answered NA

                    odds_na[ic, it, iq] = nb_q_na / q_tot if q_tot else 0
                    cnts_na[ic, it, iq] = q_tot
                    odds_ok[ic, it, iq] = nb_q_ok / q_wa_na if q_wa_na else 0
                    cnts_ok[ic, it, iq] = q_wa_na
        print()

        self.odds_area = odds_area
        self.cnts_area = cnts_area
        self.cumul_area = np.zeros((len(self.contractors),
                                    len(self.subareas)))
        self.cnts_cumul_area = np.zeros((len(self.contractors),
                                         len(self.subareas)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            cnts_prev_sum = 0
            for isa, sa in enumerate(self.subareas):
                self.cumul_area[ic, isa] = self.odds_area[ic, isa] + prev_sum
                prev_sum += self.odds_area[ic, isa]
                self.cnts_cumul_area[ic, isa] = self.cnts_area[ic, isa] + cnts_prev_sum
                cnts_prev_sum += self.cnts_area[ic, isa]

        self.odds_task = odds_task
        self.cnts_task = cnts_task
        self.cumul_task = np.zeros((len(self.contractors),
                                    len(self.tasks)))
        self.cnts_cumul_task = np.zeros((len(self.contractors),
                                         len(self.tasks)))
        for ic, c in enumerate(self.contractors):
            prev_sum = 0
            cnts_prev_sum = 0
            for it, t in enumerate(self.tasks):
                self.cumul_task[ic, it] = self.odds_task[ic, it] + prev_sum
                prev_sum += self.odds_task[ic, it]
                self.cnts_cumul_task[ic, it] = self.cnts_task[ic, it] + cnts_prev_sum
                cnts_prev_sum += self.cnts_task[ic, it]

        self.odds_noans, self.cnts_noans = odds_noans, cnts_noans
        self.odds_na, self.cnts_na = odds_na, cnts_na
        self.odds_ok, self.cnts_ok = odds_ok, cnts_ok

    def _get_contractor(self, c: str or int):
        if type(c) is int:
            return c
        return Tools.binary_search(self.contractors, c)

    def _get_task(self, t: int):
        return Tools.binary_search(self.tasks, t) if t > len(self.tasks) else t

    def _iq_to_qid(self, iq: int):
        """
        Convert index in self.qstns to unique question id

        Args:
            iq: index in self.qstns

        Returns: int

        """
        return self.qstns[iq]

    def _qid_to_iq(self, qid: int):
        """
        Convert unique question id to index in self.qstns

        Args:
            qid: question id

        Returns: int

        """
        return Tools.binary_search(self.qstns, qid)

    def _chose_area(self, c):
        """

        Args:
            c: contractor to chose a random area for; can be a string denominating the contractor, or its index

        Returns: the chosen area (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        area_idx = Tools.binary_search(self.cumul_area[c, :], r, b_return_insert=True)
        if area_idx < 0:
            area_idx = -(area_idx + 1)
        return area_idx

    def _chose_task(self, c):
        """

        Args:
            c: contractor to chose a random task for; can be a string denominating the contractor, or its index

        Returns: the chosen task (index)

        """
        c = self._get_contractor(c)
        r = np.random.rand()
        task_idx = Tools.binary_search(self.cumul_task[c, :], r, b_return_insert=True)
        if task_idx < 0:
            task_idx = -(task_idx + 1)
        return task_idx

    def get_pred_for(self, c, nb_iters=10, b_model_noans=True, b_model_cq=True, b_debug=True):
        """
        Simulate nb_iters

        Args:
            c: contractor (name or index)
            nb_iters: number of iterations
            b_model_noans: model effect of unanswered questions?
            b_model_cq: model effect of critical questions?
            b_debug: print some extra info

        Returns:

        """
        ic = self._get_contractor(c)

        defs = []
        for iter in range(nb_iters):
            isa = self._chose_area(ic)
            it = self._chose_task(ic)
            nb_answered = 0
            nb_ok = 0
            nb_na = 0
            b_critical = False
            # If we take into account critical questions, predict these first
            if b_model_cq:
                for qid in self.cqs.get_critical_ids():
                    iq = self._qid_to_iq(qid)
                    if b_model_noans:
                        # Answered at all?
                        if st.random_binomial_test(self.odds_noans[ic, it, iq], self.cnts_noans[ic, it, iq]):
                            continue
                    # Answered NA?
                    if st.random_binomial_test(self.odds_na[ic, it, iq], self.cnts_na[ic, it, iq]):
                        nb_na += 1
                        continue
                    nb_answered += 1
                    # Answered ok?
                    if st.random_binomial_test(self.odds_ok[ic, it, iq], self.cnts_ok[ic, it, iq]):
                        nb_ok += 1
                        continue
                    # Woops... NOK. Critical?
                    if st.random_binomial_test(*self.cqs.get_critical_odds(qid, b_return_counts=True)):
                        defs.append(1.)
                        b_critical = True
                        break

            if b_critical:
                if b_debug:
                    print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(
                        iter, 1.0, nb_ok, nb_answered, nb_na))
                continue

            for iq, q in enumerate(self.qstns):
                # If we model critical questions, skip critical questions in this step
                if b_model_cq and self.cqs.is_critical_question(self._iq_to_qid(iq)):
                    continue
                if b_model_noans:
                    # Answered at all?
                    if st.random_binomial_test(self.odds_noans[ic, it, iq], self.cnts_noans[ic, it, iq]):
                        continue
                # Answered NA?
                if st.random_binomial_test(self.odds_na[ic, it, iq], self.cnts_na[ic, it, iq]):
                    nb_na += 1
                    continue
                nb_answered += 1
                # Answered ok?
                if st.random_binomial_test(self.odds_ok[ic, it, iq], self.cnts_ok[ic, it, iq]):
                    nb_ok += 1

            defi = 1 - nb_ok/nb_answered if nb_answered else 1
            if b_debug:
                print("Iter {}: deficiency = {} ({} out of {}, {} NAs)".format(iter, defi, nb_ok, nb_answered, nb_na))

            defs.append(defi)

        return np.mean(defs), np.std(defs), defs


if __name__ == '__main__':
    mcm = MonteCarloModel2bis()
    da = TFDataAnalysis()

    contractors = {"A : Sites", "C : Sites", "D : Sites", "G : Sites", "P : Sites"}
    mcm.initialize_probs(da.data[da.data["Sites : List"].isin(contractors)])
    for epoch in range(1, 2):
        cprint("Epoch {}".format(epoch), color="cyan")
        for c in ["G : Sites"]:  # sorted(contractors):
            mean, std, defs = mcm.get_pred_for(c, nb_iters=5000, b_model_noans=True, b_model_cq=True, b_debug=False)
            print("Contractor: {} -- Mean: {:5.3f} -- Std: {:5.3f}".format(c, mean, std))

            if True:
                plt.figure()
                plt.ylim(0, 1.05)

                plt.title("Monte Carlo simulation for\ncontractor {}".format(c))
                plt.plot(range(len(defs)), defs, '.b')
                plt.xlabel("Random sample")
                plt.ylabel("Deficiency")
                plt.show()

                plt.figure()
                plt.hist(defs, bins=np.arange(0, 1.01, 0.01))
                plt.show()

"""
Simple Bayesian prior model for predicting a site's score, using prior information concerning
the area, task and topic.

This model produces unusable results. It is kept here mostly for 'archiving' purposes.
"""
import numpy as np
import pandas as pd

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.utils.tools import Tools


class TopicBayesianModel:
    def __init__(self, df: pd.DataFrame):
        """

        Args:
            df: the DataFrame to use to extract the Bayesian priors.
        """
        # All relevant data should be loaded into a 4D matrix whose dimensions are:
        # -Contractor
        # -Area
        # -Task
        # -Topic

        self.contractors = sorted(list(df["Sites : List"].unique()))
        self.subareas = sorted(list(df["Sites : Subarea"].unique()))
        self.tasks = sorted(list(df["Sites : Task nbr"].unique()))
        self.topics = sorted(list(df.Topic_Id.unique()))

        priors = np.zeros((len(self.contractors),
                           len(self.tasks),
                           len(self.subareas),
                           len(self.topics)))
        topic_weights = np.zeros((len(self.contractors),
                                  len(self.tasks),
                                  len(self.subareas),
                                  len(self.topics)))

        print("Computing priors...")
        nb_contractors = len(self.contractors)
        nb_tasks = len(self.tasks)
        for ic, c in enumerate(self.contractors):
            da_c = df[df["Sites : List"] == c]
            for it, t in enumerate(self.tasks):
                print("\rAt contractor {:-2d}/{}, task {:-2d}/{}"
                      .format(ic + 1, nb_contractors, it + 1, nb_tasks), end='', flush=True)
                da_ct = da_c[da_c["Sites : Task nbr"] == t]
                for isa, sa in enumerate(self.subareas):
                    da_ctsa = da_ct[da_ct["Sites : Subarea"] == sa]

                    tot_nok, tot_ok, tot_sos, _, _, _ = TFDataAnalysis.get_stats_for_df(da_ctsa)
                    total = tot_nok + tot_ok + tot_sos

                    for itop, top in enumerate(self.topics):

                        da_ctsat = da_ctsa[da_ctsa["Topic_Id"] == top]
                        top_nok, top_ok, top_sos, _, _, _ = TFDataAnalysis.get_stats_for_df(da_ctsat)

                        top_total = top_nok + top_ok + top_sos

                        priors[ic, it, isa, itop] = top_ok/top_total if top_total > 0 else 0
                        topic_weights[ic, it, isa, itop] = top_total/total if top_total > 0 else 0
        print()

        self.priors = priors
        self.topic_weights = topic_weights

    def get_prob_for(self, contractor=None, task=None, subarea=None, topic_id: int=None):
        """

        Args:
            contractor:
            task:
            subarea:
            topic_id:

        Returns:

        """
        if topic_id is not None and type(topic_id) is not int:
            raise TypeError("Expected an integer for topic_id, got {} instead.".format(type(topic_id)))

        idx_c = contractor if type(contractor) is int else\
            Tools.binary_search(self.contractors, contractor) if contractor else -1
        idx_t = Tools.binary_search(self.tasks, task) if task > len(self.tasks) else task
        idx_s = subarea if type(subarea) is int else\
            Tools.binary_search(self.subareas, subarea) if subarea else -1

        if (idx_c < 0 or idx_t < 0 or idx_s < 0) and topic_id is None:
            raise ValueError("topic_id can only be None if all other indices are specified.")

        if idx_c > -1:
            if idx_t > -1:
                # All indices are known, return specific probability
                if idx_s > -1:
                    return sum([self.topic_weights[idx_c, idx_t, idx_s, i] * self.priors[idx_c, idx_t, idx_s, i]
                                for i in range(len(self.topic_weights))]) if topic_id is None\
                        else self.priors[idx_c, idx_t, idx_s, topic_id]
                # Return probabilities over subareas
                else:
                    return self.priors[idx_c, idx_t, :, topic_id]
            else:
                # Return probabilities over tasks
                if idx_s > -1:
                    return self.priors[idx_c, :, idx_s, topic_id]
                # Etc...
                else:
                    return self.priors[idx_c, :, :, topic_id]
        else:
            if idx_t > -1:
                if idx_s > -1:
                    return self.priors[:, idx_t, idx_s, topic_id]
                else:
                    return self.priors[:, idx_t, :, topic_id]
            else:
                if idx_s > -1:
                    return self.priors[:, :, idx_s, topic_id]
                else:
                    return self.priors[:, :, :, topic_id]

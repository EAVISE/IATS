"""
Checking for "Critical Questions". More precisely, look at what questions were answered in case
the deficiency for a site =1, as in these cases typically only 1 or 2 questions were answered.
This suggests that, if in these particular cases the same questions always reappear, some
questions are indeed more critical than others.
"""
import numpy as np

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.utils.tools import Tools

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


def main():
    da = TFDataAnalysis()

    contractors = list(da.data["Sites : List"].unique())
    tasks = list(da.data["Sites : Task nbr"].unique())
    subareas = sorted(list(da.data["Sites : Subarea"].unique()))

    uq_topic_ids = sorted(da.data["Topic_Id"].unique())

    # Sites per contractor
    nbsites_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Nr'].unique())
                            for c in sorted(contractors)]
    # Get list with sorted indices of contractors according to descending amount of nb of sites served
    _, most_active_ctors_idxs = Tools.sort_with_indices(nbsites_per_contractor, b_reverse=True)
    most_active_ctors = [sorted(contractors)[i] for i in most_active_ctors_idxs]

    # ############################################################
    # Critical questions (cqs) over entire data
    # ############################################################
    if True:
        print("Critical question analysis over entire data:")
        all_cqs = da.analyse_critical_questions(da.data, b_verbose=True)
        # Compare analysis over sites to analysis over audits
        print("Most common questions (left: site, right: audit, '#'=number of times question was answered when def=1):")
        for i in range(10):
            site_el = all_cqs[0].most_common()[i]
            audit_el = all_cqs[1].most_common()[i]
            print(f"{i}: #{site_el[1]}: {site_el[0]}\t#{audit_el[1]}: {audit_el[0]}")
        # For the x most prevalent cqs from site analysis, check how often they were answered NOK for
        # "non-critical" sites and audits
        total_sites = []
        critical_sites = []
        for qid, c in all_cqs[0].most_common(10):
            # Get all rows for this question where answer was NOK
            da_q = da.data[(da.data['Question_Id'] == qid) & (da.data['Compliancy'] == 'NOK')]
            # Sort according to start date
            da_q = da.sort_chronologically(da_q)
            # Get unique sites this question has been answered NOK for
            uq_sites_for_q = da_q['Sites : Nr'].unique()
            uq_audits_for_q = set(da_q['Audit_Id'].unique())
            if qid == 58:
                print(f"Unique sites  for 58: {len(uq_sites_for_q)}")
                print(f"Unique audits for 58: {len(uq_audits_for_q)}")
            # Go over sites and check deficiency
            audit_defis = []
            site_defis = []

            for site in uq_sites_for_q:
                da_s = da.data[da.data['Sites : Nr'] == site]
                for audit in da_s["Audit_Id"].unique():
                    # Only consider audits that contain the particular question we are investigating
                    if audit == -1 or audit not in uq_audits_for_q:
                        continue

                    da_s_audit = da_s[da_s["Audit_Id"] == audit]
                    audit_defi = da.get_deficiency_for_df(da_s_audit, b_weighted=False)
                    audit_defis.append(audit_defi)
                defi = da.get_deficiency_for_df(da_s)
                site_defis.append(defi)
            crit_sites = sum([1 if x == 1 else 0 for x in site_defis])
            crit_audits = sum([1 if x == 1 else 0 for x in audit_defis])

            total_sites.append(len(site_defis))
            critical_sites.append(crit_sites)
            print("Question {}:".format(qid))
            print("\tCritical sites : {} out of {} [{:5.1f}%]".format(crit_sites, len(site_defis), 100*crit_sites/len(site_defis)))
            print("\tAverage deficiency over sites : {:5.3f}".format(np.mean(site_defis)))
            print("\tCritical audits: {} out of {} [{:5.1f}%]".format(crit_audits, len(audit_defis), 100*crit_audits/len(audit_defis)))
            print("\tAverage deficiency over audits: {:5.3f}".format(np.mean(audit_defis)))

    # ############################################################
    # Plot results: critical vs non-critical sites when question was answered NOK
    # ############################################################
    if False:
        pc_critical = [critical_sites[i]/total_sites[i] for i in range(len(total_sites))]
        plt.figure()
        plt.title("Ratio critical/non-critical sites for top 25 critical questions")
        plt.bar(range(len(total_sites)), pc_critical)
        plt.bar(range(len(total_sites)), [1 - pc_critical[i] for i in range(len(total_sites))], bottom=pc_critical)
        plt.xticks(range(len(total_sites)), [qid for qid, c in all_cqs.most_common(25)])
        plt.xlabel("Question ID")
        plt.ylabel("Ratio critical/non-critical")
        plt.legend(["Critical", "Non-Critical"])

        ax = plt.gca()
        ax.set_ylim(0, 1.15)
        for i, v in enumerate(total_sites):
            ax.text(i, 1.03, "{}".format(v), color='black', fontsize=12,
                    fontdict={'horizontalalignment': 'center'})
            ax.text(i, 1.012, "{:4.3f}".format(avg_def[i]), color='blue', fontsize=10,
                    fontdict={'horizontalalignment': 'center'})
        ax.text(1.5, 1.11, 'Total sites for which question was answered "NOK"', color='black', fontsize=12)
        ax.text(1.5, 1.092, 'Avg. deficiency over sites', color='blue', fontsize=10)

        plt.tight_layout()
        plt.show()

    # ############################################################
    # Check critical questions for Top5 contractors
    # ############################################################
    if True:
        for c in most_active_ctors[:5]:
            print("\nAnalysis for contractor: {}".format(c))
            da_c = da.data[da.data["Sites : List"] == c]
            # Critical questions for this contractor
            da.analyse_critical_questions(da_c, b_verbose=True)

    # ############################################################
    # Check critical sites and questions as a function of topic
    # ############################################################
    if False:
        tm = TopicMapper()
        uq_topic_ids = sorted(da.data['Topic_Id'].unique())
        for t in uq_topic_ids:
            print("\nAnalysis for topic: {} - {}".format(t, tm.get_nl_topic(t)))
            da_t = da.data[da.data["Topic_Id"] == t]
            # Critical questions for this topic
            da.analyse_critical_questions(da_t)


if __name__ == '__main__':
    main()

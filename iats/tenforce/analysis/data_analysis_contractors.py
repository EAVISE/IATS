"""
Perform some in-depth data analysis on the TenForce data per contractor.
Questions to be answered are:
-is contractor performance dependent on external factors such as area and task, or purely
a function of the contractor itself?
-does the performance per contractor show any seasonal dependency?
-...
"""
import matplotlib

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.utils.tools import Tools

matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd


def CDependsOn(contractor: str, df: pd.DataFrame, dependent: list, col_name, plt_name, da: TFDataAnalysis):
    """
    Check dependency of contractor on the provided dependent.

    Args:
        contractor: the contractor to use
        df: DataFrame containing all data for a specific contractor
        dependent: list of values (areas, tasks,...) to check against
        col_name: name of the relevant column in the DataFrame
        plt_name: name of the dependent in the generated plots
        da: instance of TFDataAnalysis class
    Returns:

    """
    # Sort according to start date
    df = df.sort_values(by=['Sites : Planned start', 'Sites : Nr'])

    def_per_def = []
    sites_per_def = []
    for d in dependent:
        df_d = df[df[col_name] == d]
        def_per_def.append(da.get_deficiency_for_df(df_d))
        sites_per_def.append(len(df_d["Sites : Nr"].unique()))
        uq_sites = df_d['Sites : Nr'].unique()
        def_per_audit = []
        def_per_site = []
        qstns_per_site = []
        plnd_time_per_site = []
        true_time_per_site = []
        for site in uq_sites:
            df_d_s = df_d[df_d['Sites : Nr'] == site]
            for audit in df_d_s["Audit_Id"].unique():
                df_d_s_audit = df_d_s[df_d_s["Audit_Id"] == audit]
                audit_defi = da.get_deficiency_for_df(df_d_s_audit, b_weighted=False)
                def_per_audit.append(audit_defi)

            planned_time = df_d_s['Sites : Due date'].iloc[0] - df_d_s['Sites : Planned start'].iloc[0]
            plnd_time_per_site.append(planned_time.days + 1)
            true_time = df_d_s['Sites : Actual due date'].iloc[0] - df_d_s['Sites : Actual start'].iloc[0]
            true_time_per_site.append(true_time.days + 1)
            def_per_site.append(TFDataAnalysis.get_deficiency_for_df(df_d_s))
            nok, ok, sos, _, _, _ = TFDataAnalysis.get_stats_for_df(df_d_s)
            qstns_per_site.append(nok+ok+sos)
        # If no sites for dependent, continue
        if not def_per_site:
            continue
        fig = plt.figure()
        fig.suptitle("Contractor: {} - {}: {}".format(contractor, plt_name, d), y=1)

        ax1 = plt.subplot(411)
        ax1.set_title('Planned/true time per site')
        ax1.bar(np.arange(-0.125, len(def_per_site)-0.125, 1), plnd_time_per_site, 0.25)
        ax1.bar(np.arange(0.125, len(def_per_site)+0.125, 1), true_time_per_site, 0.25)
        ax1.legend(["Plannend", "True"])

        ax2 = plt.subplot(412, sharex=ax1)
        ax2.set_title('Questions answered per site')
        ax2.bar(range(len(def_per_site)), qstns_per_site)

        ax3 = plt.subplot(413, sharex=ax1)
        ax3.set_title('Chronological deficiency over sites')
        ax3.plot(def_per_site, marker='.', linestyle='none')

        ax4 = plt.subplot(414)
        ax4.set_title('Chronological deficiency over audits')
        ax4.plot(def_per_audit, marker='.', linestyle='none')

        plt.tight_layout()
        plt.show()

    fig = plt.figure()
    fig.suptitle("Contractor {} vs. {}".format(contractor[0], plt_name), y=1)

    b1 = plt.bar(np.arange(len(dependent))-0.125, sites_per_def, width=0.25)

    ax2 = plt.twinx()
    b2 = ax2.bar(np.arange(len(dependent))+0.125, def_per_def, width=0.25, color='red')

    plt.legend([b1, b2], ["#Sites", "Deficiency"])
    plt.xticks(range(len(dependent)), [f'Value {i}' for i in range(1, len(dependent)+1)])

    plt.tight_layout()
    plt.show()


def main():
    da = TFDataAnalysis()

    contractors = list(da.data["Sites : List"].unique())
    sites = list(da.data["Sites : Nr"].unique())
    tasks = list(da.data["Sites : Task nbr"].unique())
    subareas = sorted(list(da.data["Sites : Subarea"].unique()))
    areas = sorted(list({x.split(':')[0] for x in subareas}))

    # Sites per contractor
    nbsites_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Nr'].unique())
                            for c in sorted(contractors)]
    # Get list with sorted indices of contractors according to descending amount of nb of sites served
    _, most_active_ctors_idxs = Tools.sort_with_indices(nbsites_per_contractor, b_reverse=True)
    most_active_ctors = [sorted(contractors)[i] for i in most_active_ctors_idxs]

    for c in ["C : Sites"]:  # most_active_ctors[:5]:
        print("Analysis for contractor: {}".format(c))
        da_c = da.data[da.data["Sites : List"] == c]
        # Plot chronological evolution of deficiency for contractor
        # Sort according to start date
        da_c = da_c.sort_values(by=['Sites : Planned start', 'Sites : Nr'])
        uq_sites = da_c['Sites : Nr'].unique()
        def_per_audit = []
        def_per_site = []
        qstns_per_site = []
        qstns_na_per_site = []

        for site in uq_sites:
            da_c_s = da_c[da_c['Sites : Nr'] == site]
            for audit in da_c_s["Audit_Id"].unique():
                da_c_s_audit = da_c_s[da_c_s["Audit_Id"] == audit]
                audit_defi = da.get_deficiency_for_df(da_c_s_audit, b_weighted=False)
                def_per_audit.append(audit_defi)

            # planned_time = da_c_s['Sites : Due date'].iloc[0] - da_c_s['Sites : Planned start'].iloc[0]
            # plnd_time_per_site.append(planned_time.days + 1)
            # true_time = da_c_s['Sites : Actual due date'].iloc[0] - da_c_s['Sites : Actual start'].iloc[0]
            # true_time_per_site.append(true_time.days + 1)
            def_per_site.append(TFDataAnalysis.get_deficiency_for_df(da_c_s, b_weighted=False))
            nok, ok, sos, na, _, _, _, _ = TFDataAnalysis.get_stats_for_df(da_c_s, b_add_na=True)
            qstns_per_site.append(nok + ok + sos)
            qstns_na_per_site.append(na)

        fig = plt.figure()
        fig.suptitle("Contractor: {}".format(c[0]), y=1)

        ax1 = plt.subplot(311)
        ax1.bar(range(len(qstns_per_site)), qstns_per_site)
        ax1.bar(range(len(qstns_na_per_site)), qstns_na_per_site, bottom=qstns_per_site)
        ax1.set_ylabel("#Questions")
        ax1.set_xlabel("Site")
        ax1.legend(["Answered", "NA"])

        ax2 = plt.subplot(312, sharex=ax1)
        ax2.plot(range(len(def_per_site)), def_per_site, '.b')
        ax2.set_ylabel("Deficiency")
        ax2.set_xlabel("Site")

        ax3 = plt.subplot(313)
        ax3.plot(range(len(def_per_audit)), def_per_audit, '.b')
        ax3.set_ylabel("Deficiency")
        ax3.set_xlabel("Audit")

        plt.tight_layout()
        plt.show()

        # Visually inspect if contractor performance depends on subarea
        CDependsOn(c, da_c, subareas, "Sites : Subarea", "Subareas", da)
        # Visually inspect if contractor performance depends on task
        CDependsOn(c, da_c, tasks, "Sites : Task nbr", "Task", da)


if __name__ == '__main__':
    main()

import os
from collections import Counter

from iats.tenforce.tenforce_config import TenForceConfig
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


class AnalyzeSamIamSamples:
    def parse_file(self, file: str):
        """

        Args:
            file: path to the file to be parsed

        Returns:

        """

        b_first = True
        columns = None
        samples = []
        with open(file, 'r') as fin:
            for line in fin:
                line = line.strip()
                if b_first:
                    columns = line.split(',')
                    b_first = False
                    continue
                samples.append(line.split(','))
        start = None
        for i, col in enumerate(columns):
            if col.startswith("RQ") and col.endswith("_A"):
                start = i
                break
        end = len(columns)

        deficiencies = []
        for sample in samples:
            nb_ans = sum([1 if sample[i] != "NoAns" else 0 for i in range(start, end)])
            nb_nok = sum([1 if sample[i] == "NOK" else 0 for i in range(start, end)])
            defi = nb_nok/nb_ans if nb_ans else 0
            print(f"Deficiency: {defi:.3f}")
            deficiencies.append(defi)

        plt.figure()
        plt.plot(deficiencies, '.b')
        plt.show()


if __name__ == '__main__':
    asis = AnalyzeSamIamSamples()
    in_file = os.path.join(TenForceConfig.HOME,
                           "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "Sample2in1_CtorG.dat")
    asis.parse_file(in_file)

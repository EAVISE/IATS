from collections import defaultdict
from itertools import product

import matplotlib
import numpy as np

from iats.tenforce.data.topic_mapper import TopicMapper

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt
import seaborn as sns


def main():
    # Group questions per topic
    # Don't forget that the TopicMapper is derived from the data itself, not from the csv containing
    # the list of questions; hence it only related to questions that truly appear in the data itself.
    tm = TopicMapper()
    qns_per_topic = defaultdict(list)
    for qn_id in sorted(tm.topic_idx_per_qn.keys()):
        topic_idx = tm.topic_idx_per_qn[qn_id]
        qns_per_topic[topic_idx].append(qn_id)
    max_id = max(tm.topic_idx_per_qn.keys())
    mtx = np.zeros((max_id+1, max_id+1))
    for k, v in qns_per_topic.items():
        for e1, e2 in product(v, v):
            if e1 == e2:
                continue
            mtx[e1, e2] = 1

    # Print heatmap
    plt.figure()
    plt.title("Questions sharing same topic", y=1.05)

    ax = sns.heatmap(mtx, cbar=None)
    ax.xaxis.tick_top()
    plt.xlabel("Question Id")
    plt.ylabel("Question Id")

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()

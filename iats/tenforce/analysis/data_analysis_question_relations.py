"""
This class checks whether questions are answered "in bulk".

Not all questions are answered for every site. Questions are grouped into topics. The question this
script tries to answer is whether when questions are answered for a given site, they are answered per topic,
or per any other form of "grouping".
"""
import math
import os
import pickle
from collections import Counter
from itertools import product

import matplotlib
import numpy as np

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.tenforce_config import TenForceConfig

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt
import seaborn as sns  # Import after matplotlib!


def cnts_to_mtx(cnts: Counter, max_qid: int):
    """

    Args:
        cnts: a Counter with as keys sequences of questions, and as values the amount of times
        those sequences were seen in the data
        max_qid: the highest question id present

    Returns:

    """
    # Keep track of how often a question is answered in combination with another question

    # This list will be filled with a counter per question that will keep track
    # of how often the other questions were answered in combination with this question
    qstn_cntrs = []
    # This counter keeps track of the total amount of times each question was answered
    qstn_cnts = Counter()
    for _ in range(max_qid+1):
        qstn_cntrs.append(Counter())
    for seq, cnt in cnts.most_common():
        # # Use the following in case you only want to consider sequences with unique questions
        # if len(set(seq)) != len(seq):
        #     continue
        # # Use the following to transform the sequence into a set, so as to be sure
        # # the sequence only contains each question at most ONCE
        seq = sorted(list(set(seq)))
        print("{:4d}: {}".format(cnt, seq))
        len_tpl = len(seq)
        for i in range(len_tpl):
            qstn_cnts[seq[i]] += cnt
            for j in range(i + 1, len_tpl):
                qstn_cntrs[seq[i]][seq[j]] += cnt
                qstn_cntrs[seq[j]][seq[i]] += cnt
    return qstn_cnts, qstn_cntrs


def main(b_write_to_disk=False):
    da = TFDataAnalysis()
    qstn_seqs = Counter()  # Question sequences
    qstn_seqs_na = Counter()  # Questions answered NA per sequence

    prev_topic_id = da.data.iloc[0]['Topic_Id']
    qstn_seq = []
    qstn_seq_na = []
    max_qid = -1  # Keep track of highest qid seen for later

    # If the following boolean is set to 'True', only sequences of questions belonging to the same topic
    # will be considered.
    # If it is set to 'False', sequences will keep running until sites are switched or a question that was
    # already answered is seen again, in which case a new sequence starts.
    b_per_topic = False

    if b_per_topic:
        for i, r in da.data.iterrows():
            print("\rAt row {}...".format(i), end='')

            qstn_id = r['Question_Id']
            comp = r['Compliancy']
            qstn_na = (type(comp) == float and math.isnan(comp))  # Is this question answered NA?

            if qstn_id > max_qid:
                max_qid = qstn_id

            topic_id = r['Topic_Id']

            if topic_id == prev_topic_id:
                qstn_seq.append(qstn_id)
                if qstn_na:
                    qstn_seq_na.append(qstn_id)
            else:
                qstn_seqs[tuple(qstn_seq)] += 1
                qstn_seqs_na[tuple(qstn_seq_na)] += 1

                qstn_seq = [qstn_id]
                qstn_seq_na = [qstn_id] if qstn_na else []

            prev_topic_id = topic_id
        # Don't forget last sequence!
        qstn_seqs[tuple(qstn_seq)] += 1
        qstn_seqs_na[tuple(qstn_seq_na)] += 1
        print("\n")
    else:
        seen = set()
        prev_site = da.data.iloc[0]["Sites : Nr"]
        for i, r in da.data.iterrows():
            print("\rAt row {}...".format(i), end='')

            site = r["Sites : Nr"]
            qstn_id = r['Question_Id']
            comp = r['Compliancy']
            qstn_na = (type(comp) == float and math.isnan(comp))  # Is this question answered NA?

            if qstn_id > max_qid:
                max_qid = qstn_id

            if qstn_id in seen or site != prev_site:
                qstn_seqs[tuple(qstn_seq)] += 1
                qstn_seqs_na[tuple(qstn_seq_na)] += 1

                qstn_seq = [qstn_id]
                qstn_seq_na = [qstn_id] if qstn_na else []
                seen = set()
            else:
                qstn_seq.append(qstn_id)
                if qstn_na:
                    qstn_seq_na.append(qstn_id)
                seen.add(qstn_id)
            prev_site = site

        # Don't forget last sequence!
        qstn_seqs[tuple(qstn_seq)] += 1
        qstn_seqs_na[tuple(qstn_seq_na)] += 1
        print("\n")

    # Convert to counts per pair
    qstn_cnts, qstn_cntrs = cnts_to_mtx(qstn_seqs, max_qid)
    qstn_cnts_na, qstn_cntrs_na = cnts_to_mtx(qstn_seqs_na, max_qid)

    # Extract probabilities of having questions x and y answered together
    # Note that this is a sparse matrix, but that the dimensions are small enough to not have
    # to bother to use sparse formats
    probs = np.zeros((max_qid+1, max_qid+1))
    probs_na = np.zeros((max_qid+1, max_qid+1))
    for i, j in product(range(max_qid+1), range(max_qid+1)):
        # print("{} : {}:: {}, {}".format(i, j, qstn_cnts[i], qstn_cntrs[i][j]))
        probs[i, j] = qstn_cntrs[i][j]/qstn_cnts[i] if qstn_cnts[i] > 0 else 0
        probs_na[i, j] = qstn_cntrs_na[i][j]/qstn_cnts_na[i] if qstn_cnts_na[i] > 0 else 0

    # Print heatmap of co-occurence
    plt.figure()
    plt.title("Co-occurence", y=1.05)

    ax = sns.heatmap(probs.T)
    ax.xaxis.tick_top()
    plt.xlabel("Question Id 'To'")
    plt.ylabel("Question Id 'From'")

    plt.tight_layout()
    plt.show()

    # Print heatmap of being answered NA together
    plt.figure()
    plt.title("Pairwise NA", y=1.05)

    ax = sns.heatmap(probs_na.T)
    ax.xaxis.tick_top()
    plt.xlabel("Question Id 'To'")
    plt.ylabel("Question Id 'From'")

    plt.tight_layout()
    plt.show()

    if b_write_to_disk:
        # Write probability matrix and question pair counts to disc
        qr_file = os.path.join(TenForceConfig.DATA_DIR, 'question_relations.pkl')
        pickle.dump((probs, qstn_cnts, probs_na, qstn_cnts_na), open(qr_file, 'wb'))
        print("Results written to file {}.".format(qr_file))


if __name__ == '__main__':
    main(b_write_to_disk=False)

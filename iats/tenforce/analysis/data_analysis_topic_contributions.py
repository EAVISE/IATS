"""
Check contribution of each topic to site weight for top contractors.
The site weight = sum of weight of all answered questions.
NA-answered questions are ignored.
"""
import numpy as np

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.tenforce.data.topic_mapper import TopicMapper
from iats.utils.tools import Tools

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


def main():
    da = TFDataAnalysis()

    contractors = list(da.data["Sites : List"].unique())
    tasks = list(da.data["Sites : Task nbr"].unique())
    subareas = sorted(list(da.data["Sites : Subarea"].unique()))

    uq_topic_ids = sorted(da.data["Topic_Id"].unique())

    # Sites per contractor
    nbsites_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Nr'].unique())
                            for c in sorted(contractors)]
    # Get list with sorted indices of contractors according to descending amount of nb of sites served
    _, most_active_ctors_idxs = Tools.sort_with_indices(nbsites_per_contractor, b_reverse=True)
    most_active_ctors = [sorted(contractors)[i] for i in most_active_ctors_idxs]

    for c in most_active_ctors[:5]:
        print("\nAnalysing topic contribution for contractor: {}".format(c))
        da_c = da.data[da.data["Sites : List"] == c]
        da_c = da.sort_chronologically(da_c)
        uq_sites = da_c["Sites : Nr"].unique()
        topic_contributions = np.zeros((len(uq_sites), len(uq_topic_ids)))
        top_topics = np.zeros((len(uq_sites), 5))  # 9, 12, 17, 19
        list_top_topic_ids = [9, 12, 15, 17, 19]
        top_topic_ids = set(list_top_topic_ids)
        for i, s in enumerate(uq_sites):
            da_c_s = da_c[da_c["Sites : Nr"] == s]
            total_weight = sum(da_c_s[da_c_s["Compliancy"].notnull()]["Weight"])
            if total_weight == 0:
                continue
            # site_stats = da.get_stats_for_df(da_c_s)
            temp_top_topics = []
            per_topic = []
            for t in uq_topic_ids:
                da_c_s_t = da_c_s[da_c_s["Topic_Id"] == t]
                topic_weight = sum(da_c_s_t[da_c_s_t["Compliancy"].notnull()]["Weight"])
                per_topic.append(topic_weight/total_weight)
                # topic_stats = da.get_stats_for_df(da_c_s_t)
                # per_topic[t] = (topic_weight, topic_stats)
                if t in top_topic_ids:
                    temp_top_topics.append(topic_weight/total_weight)
            topic_contributions[i, :] = np.array(per_topic)
            top_topics[i, :] = np.array(temp_top_topics)

        # Plot relative contribution of top 5 most important topics to site weight as stacked bar plot
        if False:
            plt.figure()
            plt.bar(range(len(uq_sites)), top_topics[:, 0])
            # plt.bar(range(len(uq_sites)), top_topics[:, 1], bottom=top_topics[:, 0])
            for k in range(1, 5):
                plt.bar(range(len(uq_sites)), top_topics[:, k], bottom=np.sum(top_topics[:, 0:k], axis=1))
            plt.legend([f"Topic {i}" for i in list_top_topic_ids])
            plt.tight_layout()
            plt.show()

        # Plot relative contribution of top 5 most important topics to site weight as - unstacked - line plot
        if False:
            plt.figure()
            plt.plot(top_topics)
            plt.legend([f"Topic {i}" for i in list_top_topic_ids])
            plt.tight_layout()
            plt.show()

        # Plot boxplot showing distribution of topic contribution to site weight for all topics
        if True:
            plt.figure()
            plt.title("Topic contributions to site weight\nfor contractor {} ({} sites)".format(c, len(uq_sites)))
            plt.boxplot(topic_contributions)
            plt.ylabel("Share of total")
            plt.xlabel("Topic ID")
            plt.tight_layout()
            plt.show()


if __name__ == '__main__':
    main()

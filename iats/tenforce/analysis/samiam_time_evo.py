"""
This script will use a series of different models, that were trained with data that were consecutively generated using
data spanning a larger timeframe, to generate random data in an attempt to show the evolution of the models.
"""
import os

from tqdm import tqdm

from iats.tenforce.models.sample_samiam_model_2 import SampleSamIam2
from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


def sample_model(model_filename, ctor, nb_samples: int):
    sampler = SampleSamIam2(bn_file=model_filename)
    defis = []
    for _ in tqdm(range(nb_samples)):
        defis.append(sampler.sample(parents={"Contractor": ctor})[2])

    return defis


def main():
    # Contractor for which to preform the analysis
    contractor = "C : Sites"

    # Directory containing the models to use
    bn_file_dir = os.path.join(TenForceConfig.HOME,
                               "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "time_evo")
    # Base name of the models to use; it is assumed this root is followed by "_yyyymmdd", where "yy..." represents
    # the cut off date of the data used to train the model
    bn_file_root = "bn_model2_alt_top5"
    # Full filename should be equal to bn_file_root + '_yyyymmdd' + bn_file_end
    bn_file_end = "lb10_ls0_area.net"

    # For this experiment, we are interested in models generated using no lookback and no Laplace smoothing
    all_files = os.listdir(bn_file_dir)
    # Get all unique dates from the filenames
    all_dates = set()
    for file in all_files:
        if file.startswith(bn_file_root) and file.endswith(bn_file_end):
            all_dates.add(file[len(bn_file_root) + 1: len(bn_file_root) + 9])

    all_dates = sorted(all_dates)

    # Generate 100 data points per model
    defis = []
    for i, date in enumerate(all_dates):
        print(f"At date {i+1} of {len(all_dates)}")
        defis += sample_model(os.path.join(bn_file_dir, bn_file_root + f'_{date}_' + bn_file_end),
                              ctor=contractor, nb_samples=100)

    plt.figure()
    plt.plot(defis, '.b')
    plt.show()


if __name__ == '__main__':
    main()

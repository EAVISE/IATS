"""
Perform some "general" data analysis on the TenForce data.
By "general" we mean: global statistics on how many contractors, tasks, area etc.,
as well as deficiency rates per contractor, task, area...
The more in-depth analysis per contractor is contained in a separate script.
"""
import math
import matplotlib

from iats.tenforce.data.tf_data_analysis import TFDataAnalysis
from iats.utils.tools import Tools

import numpy as np
from sklearn.linear_model import LinearRegression

matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt


def main():
    da = TFDataAnalysis()
    # What columns are present in the DataFrame (df)?
    print(da.data.columns)
    # How many items does the df contain?
    print("Total number of rows  : {}".format(len(da.data)))
    # How many questions were answered in what way?
    qstn_answers = da.data.Compliancy.value_counts(dropna=False)
    print(qstn_answers)
    plt.figure()
    plt.title("Distribution of possible answers over all answered questions")
    plt.pie(qstn_answers.values, labels=[x if type(x) == str else 'NA' for x in qstn_answers.index], autopct="%4.1f%%")
    plt.show()

    # Get unique values for certain variables of interest
    contractors = list(da.data["Sites : List"].unique())
    sites = list(da.data["Sites : Nr"].unique())
    tasks = list(da.data["Sites : Task nbr"].unique())
    subareas = sorted(list(da.data["Sites : Subarea"].unique()))
    areas = sorted(list({x.split(':')[0] for x in subareas}))  # Main areas

    # Some statistics relating to unique tasks/contracots/sites
    print("Nb. unique contractors: {}".format(len(contractors)))
    print("Nb. unique sites      : {}".format(len(sites)))
    print("Nb. unique tasks      : {}".format(len(tasks)))

    # How many areas do contractors serve? How many contractors serve a particular area?
    areas_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Subarea'].unique())
                            for c in sorted(contractors)]
    tasks_per_contractor = [len(da.data[da.data["Sites : List"] == c]['Sites : Task nbr'].unique())
                            for c in sorted(contractors)]
    contractors_per_area = [len(da.data[da.data["Sites : Subarea"] == a]['Sites : List'].unique())
                            for a in sorted(subareas)]
    # ############################################################
    # Generate plot showing areas per contractor, and vice versa
    # ############################################################
    if True:
        plt.figure()
        # Areas per contractor
        ax1 = plt.subplot(211)
        ax1.title.set_text("Number of areas per contractor")
        plt.bar(range(len(contractors)), areas_per_contractor)
        plt.xticks(range(len(contractors)), [x[0] for x in sorted(contractors)])
        plt.xlabel("Contractor")
        plt.ylabel("#Sites")
        # Contractors per area
        ax2 = plt.subplot(212)
        ax2.title.set_text("Number of contractors per area")
        plt.bar(range(len(subareas)), contractors_per_area)
        plt.xticks(range(len(subareas)), [s[:6] + ': ' + s[-1] for s in subareas])
        plt.xlabel("Area")
        plt.ylabel("#Contractors")
        plt.tight_layout()
        plt.show()

    # ############################################################
    # Stats per contractor
    # ############################################################
    if False:
        # Check nb. of sites and deficiency rate per contractor
        print()
        sites_per_contractor = []
        for c in sorted(contractors):
            da_c = da.data[da.data["Sites : List"] == c]
            nb_qstns = len(da_c)
            sites_per_contractor.append(len(da_c["Sites : Nr"].unique()))
            nb_ok = len(da.data[(da.data["Sites : List"] == c) & (da.data["Compliancy"] == 'OK')])
            nb_sos = len(da.data[(da.data["Sites : List"] == c) & (da.data["Compliancy"] == 'SOS')])
            nb_nok = len(da.data[(da.data["Sites : List"] == c) & (da.data["Compliancy"] == 'NOK')])
            print("Contractor {}: Questions - {:-5d}, OK/SOS: {:-5d}, NOK: {:-5d}, neg. pc: {}"
                  .format(c, nb_qstns, (nb_ok + nb_sos), nb_nok, nb_nok / (nb_sos + nb_ok + nb_nok)))

        # Evolution of deficiency rate per contractor
        print()
        all_def_rates = []
        sorted_cs = sorted(contractors)
        total_answ_qstns = []
        for c in sorted_cs:
            # Get rows for contractor
            da_c = da.data[da.data["Sites : List"] == c]
            # Get unique sites in this subset
            uq_sites = list(da_c["Sites : Nr"].unique())
            # For each site, get "deficiency" rate
            def_rates = []
            total = 0
            for site in uq_sites:
                da_c_s = da_c[da_c["Sites : Nr"] == site]
                for audit in da_c_s["Audit_Id"].unique():
                    da_c_s_audit = da_c_s[da_c_s["Audit_Id"] == audit]
                    defi = da.get_deficiency_for_df(da_c_s_audit, b_weighted=False)
                    def_rates.append(defi)

                # nb_works = da_c_s.shape[0]
                nb_ok = len(da_c_s[da_c_s["Compliancy"] == 'OK'])
                nb_nok = len(da_c_s[da_c_s["Compliancy"] == 'NOK'])
                nb_sos = len(da_c_s[da_c_s["Compliancy"] == 'SOS'])
                sub_total = nb_ok + nb_nok + nb_sos
                total += sub_total
            total_answ_qstns.append(total)
            all_def_rates.append(def_rates)
        deficiencies = [np.average(all_def_rates[i]) for i in range(len(all_def_rates))]

        # Perform a linear regression through datapoints per contractor
        ricos = []
        for i, l in enumerate(all_def_rates):
            lr = LinearRegression()
            x = np.asarray(list(range(1, len(l)+1))).reshape(-1, 1)
            y = np.asarray(l).reshape(-1, 1)
            lr.fit(x, y)
            ricos.append(lr.coef_[0, 0])

            if False:
                lr_fit = [lr.intercept_ + lr.coef_[0, 0] * j[0] for j in x]
                plt.figure()
                plt.title("Contractor: {}".format(sorted_cs[i][0]))
                plt.plot(x, l, linestyle='', marker='.')
                plt.plot(x, lr_fit, color='red')
                plt.xlabel("@Site (ordered chron.)")
                plt.ylabel("NOK/(NOK+OK+SOS)")
                plt.tight_layout()
                plt.show()
        print("Average dico         : {}".format(np.mean(ricos)))
        print("Average weighted dico: {}".format(np.average(ricos, weights=total_answ_qstns)))

        plt.figure()
        plt.title("LinReg coefficients")
        plt.plot(range(len(sorted_cs)), ricos, '.b')
        plt.xlabel("Contractor")
        plt.ylabel("LinReg coef.")
        plt.xticks(range(len(sorted_cs)), [x[0] for x in sorted(contractors)])
        plt.tight_layout()
        plt.show()

        # Plot some stuff
        plt.figure()
        # Sites per contractor
        x_ticks = list(range(1, len(contractors)+1))
        ax1 = plt.subplot(511)
        ax1.title.set_text("Number of sites per contractor")
        plt.bar(x_ticks, sites_per_contractor)
        plt.xlabel("Contractor")
        plt.ylabel("#Sites")
        # Areas per contractor
        ax2 = plt.subplot(512, sharex=ax1)
        ax2.title.set_text("Number of areas per contractor")
        plt.bar(x_ticks, areas_per_contractor)
        plt.xlabel("Contractor")
        plt.ylabel("#Areas")
        # Areas per contractor
        ax3 = plt.subplot(513, sharex=ax1)
        ax3.title.set_text("Number of tasks per contractor")
        plt.bar(x_ticks, tasks_per_contractor)
        plt.xlabel("Contractor")
        plt.ylabel("#Tasks")
        # Deficiency over all "questions"
        ax4 = plt.subplot(514, sharex=ax1)
        ax4.title.set_text("Average deficiency over audits per contractor")
        plt.plot(x_ticks, deficiencies, '.b')
        # Spread deficiency per contractor
        ax5 = plt.subplot(515, sharex=ax1)
        ax5.title.set_text("Deficiency spread per contractor")
        plt.boxplot(all_def_rates)

        plt.xticks(x_ticks, [x[0] for x in sorted(contractors)])
        plt.tight_layout()
        plt.show()

    # ############################################################
    # Average deficiency rate per main task and subarea;
    # These are computed over all questions for a given task
    # or subarea, NOT using the audits
    # ############################################################
    if False:
        sites_per_task = []
        per_task_nb, per_task_sum = [], []
        for task in sorted(tasks):
            print()
            da_t = da.data[da.data['Sites : Task nbr'] == task]
            sites_per_task.append(len(da_t['Sites : Nr'].unique()))
            nb_nok, nb_ok, nb_sos, sum_nok, sum_ok, sum_sos = da.get_stats_for_df(da_t)
            per_task_nb.append(nb_nok/(nb_nok+nb_ok+nb_sos))
            per_task_sum.append(sum_nok/(sum_nok+sum_ok+sum_sos))

            print("Task {}: Nb. of questions: {}".format(task, len(da_t)))
            print("Number: NOK: {:-7d} - OK: {:-7d} - SOS: {:-7d} -- %NOK: {:5.3f}"
                  .format(nb_nok, nb_ok, nb_sos, nb_nok/(nb_nok+nb_ok+nb_sos)))
            print("Sum   : NOK: {:-7d} - OK: {:-7d} - SOS: {:-7d} -- %NOK: {:5.3f}"
                  .format(sum_nok, sum_ok, sum_sos, sum_nok/(sum_nok+sum_ok+sum_sos)))

        sites_per_subarea = []
        per_sa_nb, per_sa_sum = [], []
        for subarea in subareas:
            print()
            da_a = da.data[da.data['Sites : Subarea'] == subarea]
            sites_per_subarea.append(len(da_a['Sites : Nr'].unique()))
            nb_nok, nb_ok, nb_sos, sum_nok, sum_ok, sum_sos = da.get_stats_for_df(da_a)
            per_sa_nb.append(nb_nok/(nb_nok+nb_ok+nb_sos))
            per_sa_sum.append(sum_nok/(sum_nok+sum_ok+sum_sos))

            print("Subarea {}: Nb. of questions: {}".format(subarea, len(da_a)))
            print("Number: NOK: {:-7d} - OK: {:-7d} - SOS: {:-7d} -- %NOK: {:5.3f}"
                  .format(nb_nok, nb_ok, nb_sos, nb_nok/(nb_nok+nb_ok+nb_sos)))
            print("Sum   : NOK: {:-7d} - OK: {:-7d} - SOS: {:-7d} -- %NOK: {:5.3f}"
                  .format(sum_nok, sum_ok, sum_sos, sum_nok/(sum_nok+sum_ok+sum_sos)))

        plt.figure()
        # Task
        ind_task_nb = np.arange(0.25, 4.25, 1)
        ind_task_sum = np.arange(0.5, 4.5, 1)
        ax1 = plt.subplot(411)
        ax1.set_title("Sites per task")
        plt.bar(range(4), sites_per_task)
        # plt.xticks(range(4), [str(t) for t in sorted(tasks)])  # Non-anonymous
        plt.xticks(range(4), ['Task {}'.format(i) for i in range(1, len(tasks)+1)])  # Anonymous)
        plt.xlabel("Task")
        plt.ylabel("#Sites")

        ax2 = plt.subplot(412)
        ax2.set_title("Deficiency per task")
        plt.bar(ind_task_nb, per_task_nb, 0.25)
        plt.bar(ind_task_sum, per_task_sum, 0.25)
        # plt.xticks(np.arange(0.375, 4.375, 1), [str(t) for t in sorted(tasks)])  # Non-anonymous
        plt.xticks(np.arange(0.375, 4.375, 1), ['Task {}'.format(i) for i in range(1, len(tasks)+1)])  # Anonymous
        plt.xlabel("Task")
        plt.ylabel("Deficiency")
        plt.legend(["Non-weighted", "Weighted"])
        # Area
        ind_sa_nb = np.arange(0.25, 9.25, 1)
        ind_sa_sum = np.arange(0.5, 9.5, 1)
        ax3 = plt.subplot(413)
        ax3.set_title("Sites per area")
        plt.bar(range(9), sites_per_subarea)
        # plt.xticks(range(9), [s[:6] + ': ' + s[-1] for s in subareas])  # Non-anonymous
        plt.xticks(range(9), ['Area {}'.format(i) for i in range(1, len(subareas)+1)])  # Anonymous
        plt.xlabel("Area")
        plt.ylabel("#Sites")

        ax4 = plt.subplot(414)
        ax4.set_title("Deficiency per subarea")
        plt.bar(ind_sa_nb, per_sa_nb, 0.25)
        plt.bar(ind_sa_sum, per_sa_sum, 0.25)
        # plt.xticks(np.arange(0.375, 9.375, 1), [s[:6] + ': ' + s[-1] for s in subareas])  # Non-anonymous
        plt.xticks(np.arange(0.375, 9.375, 1), ['Area {}'.format(i) for i in range(1, len(subareas)+1)])  # Anonymous
        plt.xlabel("Subarea")
        plt.ylabel("Deficiency")
        plt.legend(["Non-weighted", "Weighted"])

        plt.tight_layout()
        plt.show()

    # ############################################################
    # Average performance per question/topic
    # ############################################################
    if True:
        occ_per_qn = []
        def_per_qn = []
        na_per_qn, nok_per_qn, ok_per_qn, sos_per_qn = [], [], [], []
        for qn_idx in sorted(list(da.data['Question_Id'].unique())):
            da_qn = da.data[da.data['Question_Id'] == qn_idx]
            # Since we are looking at a specific question, the "deficiency" will be the same whether
            # computed based on the numbers, or the sums.
            nb_nok, nb_ok, nb_sos, _, _, _ = da.get_stats_for_df(da_qn)

            # print("Question {}: Nb. of questions: {}".format(qn_idx, len(da_qn)))
            # print("Number: NOK: {:-7d} - OK: {:-7d} - SOS: {:-7d} -- %NOK: {:5.3f}"
            #       .format(nb_nok, nb_ok, nb_sos, nb_nok/(nb_nok+nb_ok+nb_sos)))

            nb_na = len(da_qn[da_qn['Compliancy'].isnull()])
            na_per_qn.append(nb_na)
            occ_per_qn.append(len(da_qn))
            nb_ans = nb_nok + nb_ok + nb_sos
            if nb_ans:
                def_per_qn.append(nb_nok/nb_ans)
            else:
                def_per_qn.append(0)
            nok_per_qn.append(nb_nok)
            ok_per_qn.append(nb_ok)
            sos_per_qn.append(nb_sos)

        # Stacked bar plot showing nok, ok and sos numbers per question
        plt.figure()
        plt.title("NOK, OK, SOS and NA ratios per question")
        # Remove idx 0, which corresponds to idx '-1', i.e., questions that could not be mapped
        ind = list(range(1, len(nok_per_qn)))
        # na_per_qn = [occ_per_qn[i] - nok_per_qn[i] - ok_per_qn[i] - sos_per_qn[i] for i in range(len(occ_per_qn))]

        norm_na, norm_nok, norm_ok, norm_sos = [], [], [], []
        for i in ind:
            N = occ_per_qn[i]
            norm_na.append(na_per_qn[i]/N)
            norm_nok.append(nok_per_qn[i]/N)
            norm_ok.append(ok_per_qn[i]/N)
            norm_sos.append(sos_per_qn[i]/N)

        colors = ('r', 'y', 'cyan', 'black')
        data = (norm_nok, norm_sos, norm_ok, norm_na)
        bottom = np.zeros(len(ind))
        for elem, color in zip(data, colors):
            plt.bar(ind, elem, 0.4, bottom=bottom, color=color)
            bottom += elem

        plt.xlabel("Question Idx")
        plt.ylabel("Ratios")
        plt.legend(["NOK", "SOS", "OK", "NA"])

        plt.tight_layout()
        plt.show()

        # Occurrences and deficiencies per question, plotted together
        plt.figure()
        ax1 = plt.subplot(211)
        ax1.set_title("Occurrences per question")
        occ_wa_na = [occ_per_qn[i] - na_per_qn[i] for i in range(len(occ_per_qn))]  # Occurrences without NA
        plt.bar(range(len(occ_per_qn)), occ_wa_na)
        plt.bar(range(len(occ_per_qn)), na_per_qn, bottom=occ_wa_na, color='red')
        plt.legend(["NOK+OK+SOS", "NA"])
        plt.ylabel("#Occurrences")

        ax2 = plt.subplot(212, sharex=ax1)
        ax2.set_title("Deficiency per question")
        plt.plot(ind, def_per_qn[1:], '.b')
        plt.xlabel("Question Idx")
        plt.ylabel("Deficiency")

        plt.tight_layout()
        plt.show()

        # Questions with the most NAs?
        sorted_nas, sorted_nas_idxs = Tools.sort_with_indices(norm_na, b_reverse=True)
        print("\nQuestions with highest #NAs?")
        for i in range(5):
            print("{:-2d}: {:5.3f} - '{}'".format(i, sorted_nas[i], da.qm.nl_questions[sorted_nas_idxs[i]]))
        print("\nQuestions with lowest #NAs?")
        for i in range(1, 6):
            print("{:-2d}: {:5.3f} - '{}'".format(i, sorted_nas[-i], da.qm.nl_questions[sorted_nas_idxs[-i]]))

        # Questions with the highest deficiencies?
        sorted_defs, sorted_defs_idxs = Tools.sort_with_indices(def_per_qn, b_reverse=True)
        print("\nQuestions with highest deficiencies?")
        for i in range(5):
            print("{:-2d}: {:5.3f} - '{}'".format(i, sorted_defs[i], da.qm.nl_questions[sorted_defs_idxs[i]]))
        print("\nQuestions with lowest deficiencies?")
        for i in range(1, 6):
            print("{:-2d}: {:5.3f} - '{}'".format(i, sorted_defs[-i], da.qm.nl_questions[sorted_defs_idxs[-i]]))


if __name__ == '__main__':
    main()

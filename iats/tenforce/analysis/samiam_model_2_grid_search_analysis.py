"""
Script to generate heatmaps from results obtained from a grid-search over Lookback and Laplace smoothing parameters.
"""
import os

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns

from iats.tenforce.analysis.analyze_rankings import AnalyzeRankings
from iats.tenforce.tenforce_config import TenForceConfig


def main():
    # Directory containing the results
    res_dir = TenForceConfig.SAMIAM_DATA_DIR
    # Lookback values
    lbs = [['nolb']] + [[f'lb{x}sq', f'lb{x}', f'lb{x}sqrt'] for x in [10, 25, 50, 100, 150, 200, 250, 350]]
    lbs = [item for items in lbs for item in items]
    # Laplace smoothing values
    lss = [f'lsw{x}' for x in range(0, 6)]

    # Get data and store in numpy array
    data = np.zeros((len(lbs), len(lss)))
    for i, lb in enumerate(lbs):
        for j, ls in enumerate(lss):
            # if lb == 'nolb' and ls.startswith('lsw'):
            #     ls = 'ls' + ls[3:]
            filename = f'ranking_20180820_top5_10000_{lb}_{ls}_wbd.txt'
            file_data = AnalyzeRankings.read_ranking_file(os.path.join(res_dir, filename))
            # Get longest key as date
            date = ''
            for k in list(file_data.keys()):
                if len(k) > len(date):
                    date = k
            # Get sorted by mass
            sort_by = ''
            for k in list(file_data[date].keys()):
                if k.startswith("'Mass'"):
                    sort_by = k
                    break
            # Get value and put into data array
            data[i][j] = file_data[date][sort_by]['Prod'][0]

    # Plot heatmap
    plt.figure()
    plt.title("Influence LB and LS on Ranking Offset", y=1.05)

    ax = sns.heatmap(data, annot=data)
    ax.xaxis.tick_top()
    plt.xlabel("Weighted Laplace smoothing (n)")
    plt.ylabel("Lookback (days)")
    plt.yticks(range(len(lbs)), lbs)

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()

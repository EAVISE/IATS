"""
Analyse the results of an experiment to study the influence of the number of samples used to draw distributions
from the Samiam models.
The main question to answer is: do the results converge?
"""
import ast
import numpy as np
import os

from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


def parse_file(file):
    sort_by = None
    results = []

    b_first = True
    b_in_res = False

    buffer_res = ''
    with open(file, 'r') as fin:
        for line in fin:
            # The first line is of the form:
            # Sort by: [['Mass', 'PC80'], ['PC95', 'PC90', 'PC75'], ['PC90', 'PC80', 'PC50'], ['PC80', 'PC50']]
            # In other words, it contains those columns the results were sorted by
            if b_first:
                line = line[9:-1]
                sort_by = ast.literal_eval(line)
                b_first = False
            elif line.startswith('********************'):
                b_in_res = True
            elif b_in_res:
                buffer_res += line
                if not line.strip():
                    b_in_res = False
                    results.append(np.asarray(ast.literal_eval(','.join(buffer_res.split()))))
                    buffer_res = ''
    return sort_by, results


def main():
    exp_dir = os.path.join(TenForceConfig.TF_HOME, "samiam", "optimal_nb_samples", "run_2")
    files = os.listdir(exp_dir)
    # Filenames are listed in arbitrary order, so let's sort them.
    # The problem is the filenames follow the syntax "results_xxxx.txt", with 'xxxx' a number, but
    # according to this scheme, "ranking_10000.txt" comes before "ranking_2500.txt".
    nb_samples = sorted([int(file[8:-4]) for file in files])

    plot_avgs = []
    plot_stds = []
    for nb in nb_samples:
        file = f"results_{nb}.txt"
        print(f"Number of samples: {nb}")
        sort_by, results = parse_file(os.path.join(exp_dir, file))
        nb_sortbys = len(sort_by)
        sub_results = []
        sub_avgs = []
        sub_std = []
        sub_ratio = []
        for i in range(nb_sortbys):
            plot_avgs.append([])
            plot_stds.append([])

            sorted_by = sort_by[i]
            sub_result = results[0][i::nb_sortbys]
            sub_results.append(sub_result)
            sub_avgs.append(np.average(sub_result, axis=1))
            sub_std.append(np.std(sub_result, axis=1))
            sub_ratio.append([sub_std[j]/sub_avgs[j] for j in range(len(sub_avgs))])
            ratio_avg = np.average(sub_ratio[i])
            ratio_std = np.std(sub_ratio[i])
            plot_avgs[i].append(ratio_avg)
            plot_stds[i].append(plot_avgs[i][-1]+ratio_std)

            print(sorted_by)
            print(f"Avg/Std. ratio: {np.average(sub_ratio[i]):.3f} -- {np.std(sub_ratio[i]):.3f}")
        print("============================================================")

    fig = plt.figure()
    plt.title("Influence of number of samples to generate\ndistribution on ranking")

    h1, = plt.plot(nb_samples, plot_avgs[0], 'r')
    h2, = plt.plot(nb_samples, plot_avgs[1], 'b')
    h3, = plt.plot(nb_samples, plot_avgs[2], 'g')
    h4, = plt.plot(nb_samples, plot_avgs[3], 'c')

    plt.plot(nb_samples, plot_avgs[0], '.r')
    plt.plot(nb_samples, plot_avgs[1], '.b')
    plt.plot(nb_samples, plot_avgs[2], '.g')
    plt.plot(nb_samples, plot_avgs[3], '.c')

    plt.plot(nb_samples, plot_stds[0], 'xr')
    plt.plot(nb_samples, plot_stds[1], 'xb')
    plt.plot(nb_samples, plot_stds[2], 'xg')
    plt.plot(nb_samples, plot_stds[3], 'xc')
    plt.fill_between(nb_samples, plot_avgs[0], plot_stds[0], color='r', alpha=0.2)
    plt.fill_between(nb_samples, plot_avgs[1], plot_stds[1], color='b', alpha=0.2)
    plt.fill_between(nb_samples, plot_avgs[2], plot_stds[2], color='g', alpha=0.2)
    plt.fill_between(nb_samples, plot_avgs[3], plot_stds[3], color='c', alpha=0.2)
    # plt.plot(nb_samples, plot_stds[0], '.b')

    h5, = plt.plot([], [], '.k')
    h6, = plt.plot([], [], 'xk')

    plt.legend([h1, h2, h3, h4, h5, h6], ['Mass', 'PC80', 'PC90', 'PC95', 'Average', 'Avg.+Std.'])
    # plt.xticks(range(len(plot_avgs)), nb_samples)

    plt.xlabel('Nb. of samples')
    plt.ylabel('Avg. performance')

    plt.show()


if __name__ == '__main__':
    main()

"""
A script to analyze rankings as output by samiam_model_2_prediction.
Basically, it expects that results, as output to console, are saved in a txt file, and will generate a plot from there.

"""
import os
from collections import defaultdict

import numpy as np
import matplotlib

from iats.tenforce.tenforce_config import TenForceConfig

matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


class AnalyzeRankings:
    def process_files(self, files: [], titles: [] = None):
        if titles is None:
            titles = [os.path.splitext(os.path.basename(file))[0] for file in files]
        datas = []
        for file in files:
            data = self.read_ranking_file(file)
            datas.append(data)

        # Get average scores; we assume they are equal for all, although in reality small variations occur.
        # These are however small enough to be safely ignored.
        ref = {}
        for date, v in datas[0].items():
            v_0 = list(v.keys())[0]
            ref[date] = {}
            for k, tpl in v[v_0].items():
                ref[date][k] = tpl[1]

        ref_plt_x, ref_plt_y = self._ref_to_bar(ref)

        plot_cfgs = ['Prod']  # ['Prod', 'Area', 'Task']
        dates = sorted(datas[0].keys())
        sortbys = sorted(datas[0][dates[0]].keys())
        sortbys = [sortbys[0]]
        nb_bars = len(plot_cfgs)*(1 + len(files)*len(sortbys))

        width = 0.9/nb_bars
        l_edge = -0.45 + width/2

        fig = plt.figure()
        plt.title("Training data: up until 2018-06-14")

        legend = []
        at_bar = -1
        for cfg in plot_cfgs:
            v = ref_plt_y[cfg]
            at_bar += 1
            xs = np.arange(l_edge+at_bar*width, len(v)+l_edge + at_bar*width, 1)
            plt.bar(xs, v, width=width, align='center')
            legend.append('Ref.' + cfg)
        for i, data in enumerate(datas):
            data_plot_x, data_plot_y = self._data_to_bar(data)
            for sortby in sortbys:
                for cfg in plot_cfgs:
                    try:
                        v = data_plot_y[sortby][cfg]
                    except:
                        v = data_plot_y["'Mass', " + sortby][cfg]
                    at_bar += 1
                    xs = np.arange(l_edge+at_bar*width, len(v)+l_edge + at_bar*width -.01, 1)  #-01 to avoid unwanted behaviour
                    plt.bar(xs, v, width=width, align='center')
                    legend.append(titles[i] + '-' + sortby[1:5] + '-' + cfg)
        plt.xticks(range(0, len(dates)), ['All'] + [x[1:-1] for x in ref_plt_x[1:]])
        plt.ylabel("Ranking distance")

        plt.legend(legend)

        plt.show()

    @staticmethod
    def read_ranking_file(file):
        data = {}
        dates = None
        sortby = None
        with open(file, 'r') as fin:
            for line in fin:
                if line.startswith('Processing date:'):
                    dates = line[18:-2]  # Cut off '}\n' from string, hence ':-2]'
                    data[dates] = {}
                elif line.startswith('Sort by:'):
                    sortby = line[10:-2]  # Cut off '}\n' from string, hence ':-2]'
                    data[dates][sortby] = {}
                elif line.startswith('\t'):
                    line = line.strip()
                    parts = line.split(':')
                    key = parts[0].strip()
                    val1 = parts[1].split('vs')[0].strip()
                    val2 = parts[1].split('vs')[1].strip()
                    data[dates][sortby][key] = (float(val1), float(val2))
                else:
                    continue

        return data

    def _ref_to_bar(self, ref_data: {}):
        xs = sorted(ref_data.keys())
        longest = 0
        max_l = 0
        for i, x in enumerate(xs):
            if len(x) > max_l:
                max_l = len(x)
                longest = i
        xs = [xs[longest]] + xs[:longest] + xs[longest+1:]

        ys = defaultdict(list)

        for date in xs:
            for cfg, v in ref_data[date].items():
                ys[cfg].append(v)

        return xs, ys

    def _data_to_bar(self, data: {}):
        # data -> date -> sortby -> key
        xs = sorted(data.keys())
        longest = 0
        max_l = 0
        for i, x in enumerate(xs):
            if len(x) > max_l:
                max_l = len(x)
                longest = i
        xs = [xs[longest]] + xs[:longest] + xs[longest+1:]
        ys = {}

        for date in xs:
            for sortby, v in data[date].items():
                if sortby not in ys:
                    ys[sortby] = defaultdict(list)
                for cfg, tpl in data[date][sortby].items():
                    ys[sortby][cfg].append(tpl[0])

        return xs, ys


if __name__ == '__main__':
    ar = AnalyzeRankings()
    ar.process_files(files=[
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls2_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls3_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls4_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_nolb_ls5_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10_lsw1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10sqrt_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb10sq_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sq_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sqrt_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sq_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sqrt_ls1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25_lsw1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sq_lsw1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb25sqrt_lsw1_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb50_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb100_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb150_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb200_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb250_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_top5_10000_lb350_ls0_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_full_2500_lb10_ls2_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_full_2500_lb10_ls3_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_full_2500_lb10_ls4_wbd.txt'),
        # os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180614_full_2500_lb10_ls5_wbd.txt')
        os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180820_top5_10000_nolb_lsw0_wbd.txt'),
        os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180820_top5_10000_lb10_lsw0_wbd.txt'),
        os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180820_top5_10000_lb25_lsw0_wbd.txt'),
        os.path.join(TenForceConfig.SAMIAM_DATA_DIR, 'ranking_20180820_top5_10000_lb25sq_lsw0_wbd.txt')
        ]
    # titles = ['2500 NoLB WBD', '2500 LB100² WBD', '2500 LB100 WBD', '2500 LB100√ WBD']
    )
    # titles=['1000 NoLB', '1000 LB100²', '1000 LB100', '1000 LB100√'])
    #                  titles=['1000 LB100√', '2500 LB100√'])

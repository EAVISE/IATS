"""
This script compares results from the SamIam models to the data that was used as input
to determine the CPTs of the model.
"""
import os
from collections import Counter

from toolz.tests.test_dicttoolz import defaultdict
from tqdm import tqdm

from iats.tenforce.models.sample_samiam_model_2 import SampleSamIam2
from iats.tenforce.tenforce_config import TenForceConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


def read_train_data(filename):
    b_first = True
    header_idx = None

    start_rq = None
    defis = defaultdict(list)
    with open(filename, 'r') as fin:
        for line in fin:
            line = line.strip().split(',')
            # Skip header
            if b_first:
                for start_rq, e in enumerate(line):
                    if e.startswith("RQ"):
                        break
                header_idx = {line[i]: i for i in range(len(line))}
                b_first = False
                continue
            cnts = Counter(line[start_rq:])
            defi = 0
            if cnts["NOK"] or cnts["OK"] or cnts["SOS"]:
                defi = cnts["NOK"]/(cnts["NOK"] + cnts["OK"] + cnts["SOS"])
            defis[line[header_idx['Contractor']]].append(defi)

    return defis


def sample_model(model_filename, ctors, nb_samples):
    sampler = SampleSamIam2(bn_file=model_filename)
    defis = defaultdict(list)
    for ctor in ctors:
        samples = []
        for _ in tqdm(range(nb_samples[ctor])):
            samples.append(sampler.sample(parents={"Contractor": ctor}))
        defis[ctor] = [sample[2] for sample in samples]

    return defis


def main(data_file, model_file, contractors):
    defis_train = read_train_data(data_file)
    nb_samples = {ctor: len(defis_train[ctor]) for ctor in defis_train.keys()}
    defis_model = sample_model(model_file, contractors, nb_samples)

    common_keys = set(defis_train.keys()).intersection(set(defis_model.keys()))

    fig = plt.figure()
    nb_plots = len(common_keys)*2
    for i, key in enumerate(sorted(common_keys)):
        ax = plt.subplot(nb_plots, 1, 2*i + 1)
        ax.plot(defis_train[key], '.b')
        ax.set_ylim(-0.01, 1.02)
        ax.set_ylabel("Deficiency")
        ax.set_xlabel(f"{key}: Data")
        ax = plt.subplot(nb_plots, 1, 2*i + 2)
        ax.plot(defis_model[key], '.r')
        ax.set_ylim(-0.01, 1.02)
        ax.set_ylabel("Deficiency")
        ax.set_xlabel(f"{key}: Model")

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    data_file = os.path.join(TenForceConfig.HOME,
                             "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_alt_data_task.dat")
    bn_file = os.path.join(TenForceConfig.HOME,
                           "Work", "Projects", "IATS", "Data", "TenForce", "samiam", "bn_model2_alt_lb100_task.net")
    contractors = [f"{x} : Sites" for x in ['A', 'G', 'C']]

    main(data_file, bn_file, contractors)

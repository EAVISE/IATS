import functools
import glob
import multiprocessing
import os
import pathlib as pl
import re
import typing as tp

import numpy as np
import pandas as pd

import iats.utils.load as load


class OxyaReader(object):
    """A data reading engine to parse text-based SAP logs to Pandas DataFrames.

    Args:
        path: Directory or file to be opened.
        force_rebuild: If set to true, the Reader will not search for a pre-existing CSV file.
        write: If set to true, the reader will write a csv file to disk.
        begin: Begin date (optional, use only with ElasticSearch-CSVs).
        end: End date (optional, use only with ElasticSearch-CSVs).

    """

    def __init__(self, path: tp.Union[str, pl.Path], force_rebuild: bool = False, write: bool = True,
                 begin: str = None, end: str = None) -> None:
        self.path = pl.Path(path)
        self.lines = None
        self.header = None
        self.header_splits = None
        self.date_generated_line = None
        self.df = self.open(force_rebuild=force_rebuild, begin_date=pd.Timestamp(begin), end_date=pd.Timestamp(end))
        write_permission = False
        if isinstance(self.dataframe_location, list):
            if write and (force_rebuild or not any(os.path.isfile(x) for x in list(self.dataframe_location))):
                write_permission = True
        else:
            if write and (force_rebuild or not os.path.isfile(self.dataframe_location)):
                write_permission = True
        if write_permission:
            self.to_csv()

    def open(self, force_rebuild: bool = False, begin_date: pd.Timestamp = None,
             end_date: pd.Timestamp = None) -> pd.DataFrame:
        """Open the selected file or directory.

        If the object's path is a directory, it will first look for a file named 'dataframe.csv' to be opened. If this
        file is not found, it will attempt to open every text file in the directory, concatenate them and sort by index.

        Args:
            force_rebuild: If set to true, the Reader will not search for a pre-existing CSV file.
            begin_date: Begin date (optional, use only with ElasticSearch-CSVs).
            end_date: End date (optional, use only with ElasticSearch-CSVs).

        Returns:
            Dataframe containing the dataset's unfiltered information.

        """
        if os.path.isfile(self.path):
            print("Reading file:", self.path)
            return self.open_file()
        elif os.path.isdir(self.path):
            df = None
            print("Reading from folder:", self.path)
            if not isinstance(self.dataframe_location, list):
                if os.path.isfile(self.dataframe_location) and not force_rebuild:
                    print("Preprocessed dataframe found!")
                    df = pd.read_csv(self.dataframe_location, header=0, index_col=0, parse_dates=True)
            elif all((os.path.isfile(x) for x in self.dataframe_location)) \
                    and all(("output" in str(x) for x in self.dataframe_location)):
                print("Found preprocessed dataframes from ElasticSearch query.")
                files = {self.path / f for f in load.convert_daterange_to_csvs(
                    "output", begin_date, end_date)} \
                    if begin_date and end_date else set(self.dataframe_location)
                files = files.intersection(set(self.dataframe_location))
                df = pd.concat((pd.read_csv(f) for f in sorted(list(files))))
            elif isinstance(self.dataframe_location, list):
                dir_dfs = self.open_dir(self.path)
                ordered_cols = []
                for df in dir_dfs:
                    ordered_cols = ordered_cols + [c for c in df.columns if c not in ordered_cols]
                df = pd.concat(dir_dfs, sort=True).reindex(columns=ordered_cols).sort_index()
            else:
                raise AttributeError("Unclear folder structure, check the contained files for inconsistencies.")
            df = self.convert_categoricals(df)
            return df
        else:
            raise load.DataLoaderError(f"Could not find the right files at {self.path}")

    def open_file(self, path: str = None, clean: bool = True, convert_categoricals: bool = True) -> pd.DataFrame:
        """Open a text file written in the SAP log format and parse it.

        Args:
            path: Path to file.
            clean: Clean up the values in the dataframe to reduce the amount of 'object' dtypes.
            convert_categoricals: Convert 'object' series to categorical.

        Returns:
            Dataframe containing the file's unfiltered information.

        """
        if path is None:
            path = self.path
        print("Opening file:", path)
        try:
            with open(path) as f:
                self.lines = f.readlines()
        except FileNotFoundError as fnf_exc:
            message = f"File at location {path} could not be opened!"
            raise load.DataLoaderError(message) from fnf_exc

        try:
            self.date_generated_line = self.lines[1]
            self.header = self.lines[4], self.lines[5]
        except IndexError as i_exc:
            message = f"Required fields not found!"
            raise load.DataLoaderError(message) from i_exc

        self.header_splits = self.get_header_splits()
        columns = self.parse_header()
        frame_generator = (self.parse_line(line) for line in self.lines[9:-1])

        df = pd.DataFrame(frame_generator, columns=columns)
        if clean:
            df = self.clean(df)
        if convert_categoricals:
            df = self.convert_categoricals(df)
        return df

    def open_dir(self, path: str, clean: bool = True, convert_categoricals: bool = False, pool_size: int = 8) -> \
            tp.List[pd.DataFrame]:
        """Open all text files in the directory which are compatible with the SAP log format.

        Args:
            path: Path to directory.
            clean: If true, cleans the data instead of passing it through as is.
            convert_categoricals: If true, convert 'object' columns to categoricals.
            pool_size: Amount of workers to process in parallel.

        Returns:
            List of opened dataframes.

        """
        files = glob.glob(os.path.join(path, '*.txt'))
        print("Found the following files:", *list(map(lambda x: x.split('/')[-1], files)))
        with multiprocessing.Pool(pool_size) as pool:
            opener = functools.partial(self.open_file, clean=clean, convert_categoricals=convert_categoricals)
            results = pool.map(opener, files)
        return results

    def to_csv(self, *args, path: str = None, **kwargs) -> None:
        """Write the content of the reader's dataframe to a CSV file.

        Args:
            path: Storage location.

        """
        path = path or self.dataframe_location
        print("Writing dataframe to:", path)
        self.df.to_csv(path, *args, **kwargs)

    @property
    def dataframe_location(self) -> tp.Union[pl.Path, tp.List[pl.Path], None]:
        """Storage location of processed CSV file(s).

        Returns:
            Path to the correct stored CSV(s).

        """
        if self.path / 'dataframe.csv' in self.path.glob("*.csv"):
            return self.path / 'dataframe.csv'
        elif len(list(self.path.glob("output-*.csv"))) > 0:
            return list(self.path.glob("output-*.csv"))
        else:
            return None

    def get_header_splits(self) -> np.ndarray:
        """Get the positions of the field start positions in the left part of the SAP log file
        (not delimited by '|').

        Returns:
            Array containing the positions of the field starts.

        """
        header_fields = self.header[0].split('|')
        header_start_chars = np.array(list(header_fields[1]))
        start_indices = np.where(np.core.defchararray.isupper(header_start_chars))
        return start_indices[0]

    def parse_left(self, line: str) -> tp.List[str]:
        """Parse the left side of a line in the SAP log file.

        Args:
            line: Line to be parsed.

        Returns:
            Parsed line.

        """
        splitted_line = np.split(np.array(list(line.split('|')[1])), self.header_splits)
        return ["".join(x.tolist()).strip() for x in splitted_line[1:]]

    def parse_right(self, line: str) -> tp.List[str]:
        """Parse the right side of a line in the SAP log file.

        Args:
            line: Line to be parsed.

        Returns:
            Parsed line.

        """
        return [x.strip() for x in line.split('|')[2:-1]]

    def parse_header(self) -> tp.List[str]:
        """Parse the header lines of the SAP log file to get the column names.

        Returns:
            List of column names.

        """
        left = [' '.join(x).rstrip() for x in zip(self.parse_left(self.header[0]), self.parse_left(self.header[1]))]
        right = [' '.join(x).rstrip() for x in zip(self.parse_right(self.header[0]), self.parse_right(self.header[1]))]
        return left + right

    def parse_line(self, line: str) -> tp.List[str]:
        return self.parse_left(line) + self.parse_right(line)

    @staticmethod
    def find_date(line: str) -> pd.Timestamp:
        """Retrieve a valid date format in a SAP log file's line and parse to a Pandas Timestamp object.

        Args:
            line: Line to be scanned.

        Returns:
            Pandas Timestamp object.

        """
        print("searching in", line)
        pattern = re.compile(r'(\d\d\.\d\d\.\d\d\d\d) / (\d\d:\d\d:\d\d)')
        m = re.search(pattern, line)
        return pd.to_datetime(' '.join(m.groups()), format='%d.%m.%Y %H:%M:%S')

    def clean(self, df: pd.DataFrame = None) -> pd.DataFrame:
        """Clean up the dataframe to reduce the amount of 'object' dtypes.

        Dots and commas are swapped in every non-string record (Dutch to English format conversion). Object columns are
        casted to numeric types wherever possible.
        Dates are added to the timestamp column for enabling correct temporal
        analysis.

        Args:
            df: Dataframe to be cleaned. If not given, selects the Reader's dataframe.

        Returns:
            Cleaned dataframe.

        """
        if df is None:
            df = self.df

        replace_cols = df.drop(['Server', 'Transaction', 'Program Function', 'Tcode', 'CUA internal command'], axis=1,
                               errors='ignore')
        replace_cols = replace_cols.apply(lambda x: x.str.replace('.', '')).apply(lambda x: x.str.replace(',', '.'))
        df[replace_cols.columns] = replace_cols
        df = df.apply(pd.to_numeric, errors='ignore')

        current_date = self.find_date(self.date_generated_line)
        df = self.reparse_dates(df, current_date)

        return df

    def convert_categoricals(self, df: pd.DataFrame) -> pd.DataFrame:
        """Convert the 'object' columns in the given dataframe to 'category'.

        Args:
            df: Pandas dataframe.

        Returns:
            Converted dataframe.

        """
        stats = load.df_types(df)
        for col in stats[stats == 'object'].iteritems():
            if 'time' in col[0]:
                df[col[0]] = pd.to_datetime(df[col[0]], errors='ignore')
            else:
                df[col[0]] = pd.Categorical(df[col[0]])
        return df

    def reparse_dates(self, df: pd.DataFrame, current_date: pd.Timestamp) -> pd.DataFrame:
        """Apply the correct dates to the timestamp column in the given dataframe.

        First, the 'Started' column in the given dataframe will be parsed from objects to timestamps.
        Second, starting from the date given in the SAP log file's header, this method will continuously assign this
        date to the timestamp column until it encounters a special "date limit" field,
        advancing the currently used date.
        The process gets repeated for the rest of the dataframe, incrementing the date whenever necessary.

        Args:
            df: Given dataframe.
            current_date: Date to start from (found in the file's header).

        Returns:
            Updated dataframe.

        """
        df['Started'] = pd.to_datetime(df['Started'], errors='coerce')
        df['Started'] = df['Started'].apply(
            lambda x: x.replace(day=current_date.day, month=current_date.month, year=current_date.year))
        d_indices = df[df['Started'].isnull()].index
        for i in range(len(d_indices)):
            try:
                new_date = pd.to_datetime(df.loc[d_indices[i], 'Server'], format='%d.%m.%Y')
            except ValueError:
                continue
            try:
                sl = slice(d_indices[i] + 1, d_indices[i + 1])
            except IndexError:
                sl = slice(d_indices[i] + 1, None)
            df.iloc[sl, 0] = df.iloc[sl, 0].apply(
                lambda x: x.replace(day=new_date.day, month=new_date.month, year=new_date.year))
        df = df.drop(d_indices)
        df = df.reset_index(drop=True).set_index('Started')
        return df


@load.timing
def main():
    # reader = OxyaReader("/home/kve/Datasets/oxya/bekdonderdag avond na 20uur.txt")
    reader = OxyaReader("/home/kve/Datasets/oxya/")
    # df = reader.df
    reader.to_csv()
    print("Finished")


if __name__ == '__main__':
    main()

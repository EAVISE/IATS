import argparse
import collections
import inspect
import itertools
import os
import pathlib as pl
import tempfile
import time
import typing as tp
import webbrowser

import joblib
import lightgbm as lgb
import matplotlib.dates as mpd
import matplotlib.pyplot as plt
import mpld3
import numpy as np
import pandas as pd
import pandas_ml as pdml
import plotly.offline as po
import plotly.tools as tls
import seaborn as sns
import sklearn.linear_model as lm
import sklearn_pandas as skpd
import statsmodels.api as sm
import xgboost as xgb
from sklearn.base import RegressorMixin
from tqdm import tqdm

import iats.oxya.preprocess as preprocess
import iats.utils
import iats.utils.load as load
from iats.settings import ROOT_DIR
from iats.utils.config import Nestedspace, load_config
from iats.utils.plot import plot_test_vs_predicted


def profile(df: pd.DataFrame) -> None:
    """
    Generates pandas profiling report. Stored as "profile.html".

    Args:
        df: Input DataFrame.
    """
    import pandas_profiling

    prof = pandas_profiling.ProfileReport(df)
    prof.to_file(outputfile="profile.html")


def correlation_plot(df: pd.DataFrame, method: str = 'spearman') -> plt.Axes:
    """
    Plots the correlations between all the columns of a dataframe as a heatmap. Empty columns are filtered out.

    Args:
        df: Input DataFrame.
        method: Correlation method: "spearman" or "pearson".

    Returns:
        Axes reference.
    """
    cross_correlation = df.corr(method=method)
    no_na_rows = pd.notnull(cross_correlation).any()
    cross_no_nas = cross_correlation.drop(no_na_rows[~no_na_rows].index).drop(no_na_rows[~no_na_rows].index, axis=1)
    cross_no_nas_no_infs = cross_no_nas.replace([np.inf, -np.inf], np.nan)
    # fig, ax = plt.subplots(figsize=(20, 20))
    # sns.clustermap(np.abs(cross_no_nas_no_infs), vmin=0)
    # sns.heatmap(np.abs(cross_correlation), vmin=min(np.nanmin(np.abs(cross_correlation).values), 0))
    return sns.heatmap(
        np.abs(cross_correlation.drop(no_na_rows[~no_na_rows].index).drop(no_na_rows[~no_na_rows].index, axis=1)),
        vmin=min(np.nanmin(np.abs(cross_correlation).values), 0))


def line_plot(df: pd.DataFrame, col_name: str = "Response time (ms)",
              start_date: str = "2017-10-12 12:00:00", limit: int = 2000000) -> plt.Axes:
    """
    Draws a simple line graph of a DataFrame column from a given starting point. Very high values are filtered out.

    Args:
        df: DataFrane,
        col_name: Column name.
        start_date: Starting point.
        limit: Values higher than this number are not drawn in the plot.

    Returns:
        Axes reference.
    """
    new_idx = pd.date_range(start_date, df.index[-1], freq='S')
    print("New index range:", new_idx.min(), new_idx.max())
    ax = df[df[col_name] < limit].loc[new_idx, col_name].plot(legend=True, subplots=True)
    return ax


def lm_sklearn(df: pdml.ModelFrame, target: str = 'Response time (ms)') -> np.ndarray:
    """
    Trains a linear regression model using the Scikit-Learn API.
    TODO: make behavior more equal to :func:`linear_model`.

    Args:
        df: Input DataFrame.
        target: Target column.

    Returns:
        Predicted values.
    """
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import mean_squared_error as mse

    y = df[target]
    df.drop(columns=[target], inplace=True)

    X_train, X_test, y_train, y_test = train_test_split(df.values, y, test_size=0.5, random_state=0)

    regr = lm.Ridge()
    regr.fit(X_train, y_train)
    y_pred = regr.predict(X=X_test)

    print("Coefficients:", regr.coef_)
    print("MSE:", mse(y_test, y_pred))

    fig, ax = plt.subplots()
    plot_test_vs_predicted(ax, y_real=y_test, y_pred=y_pred)
    return y_pred


def linear_model(df: pdml.ModelFrame, estimator: RegressorMixin = None,
                 random_state: tp.Union[int, np.random.RandomState] = None, save: pl.Path = None) -> pdml.ModelFrame:
    """
    Trains a linear regression model using the Pandas-ML API.

    Args:
        df: Input ModelFrame.
        estimator: Model to be fit.
        random_state: Randomness initializer.
        save: Path to save sklearn model to.

    Returns:
        ModelFrame containing the predicted results.
    """
    df_train, df_test = df.model_selection.train_test_split(test_size=0.5, random_state=random_state)

    regr = estimator or lm.Ridge()
    df_train.fit(regr)
    df_test.predict(regr)

    if save:
        joblib.dump(regr, save)

    return df_test


@iats.utils.deprecated
def plot_regression(test: pdml.ModelSeries, pred: pdml.ModelSeries, plot_length: int = 1000,
                    axis: plt.Axes = None, title: str = None) -> plt.Axes:
    """
    Plots real and predicted values of a regression model.

    Args:
        test: Real values.
        pred: Predicted values.
        plot_length: Amount of data points to be plotted.
        axis: Axes to be plotted on.
        title: Title of the plot.

    Returns:
        Axes reference.
    """
    assert len(test) == len(pred)
    plot_length = min(len(test), plot_length)
    if axis:
        ax = axis
        if title:
            ax.set_title(title)
    else:
        fig, ax = plt.subplots()
        if title:
            fig.suptitle(title)
    ax.scatter(np.arange(plot_length), test[:plot_length], label='real', s=5)  # color='#81b1d2'
    ax.plot(np.arange(plot_length), pred[:plot_length], label='predicted')  # color='#feffb3'
    return ax


def feat_column_names(base_name: str, amt: int) -> tp.List[str]:
    """
    Generates a list of feature names based on a string.

    Args:
        base_name: Prefix string.
        amt: Amount of feature names to be generated.

    Returns:
        Feature names.
    """
    return [base_name + str(x + 1) for x in range(amt)]


def render_dataframe(df: pd.DataFrame, figure: plt.Figure, backend: 'str' = 'mpld3') -> None:
    """
    Creates a temporary html page containing a figure and a DataFrame. The page is displayed locally in the browser
    and promptly deleted.

    Args:
        df: DataFrame to be rendered.
        figure: Figure to be displayed.
        backend: Backend to be used: "plotly" or "mpld3".
    """
    with tempfile.NamedTemporaryFile('w+b', suffix='.html', delete=False) as fp, open("table_style.css") as css:
        style = "<style scoped>" + '\n'.join(css.readlines()) + "</style>"
        fp.write(style.encode())
        if backend == 'plotly':
            fp.write('<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>'.encode())
            plotly_figure = tls.mpl_to_plotly(figure)
            plotly_div = po.plot(plotly_figure, output_type='div', include_plotlyjs=False)
            fp.write(plotly_div.encode())
        elif backend == 'mpld3':
            d3_fig = mpld3.fig_to_html(figure, figid='fig_comparison', no_extras=True)
            fp.write(d3_fig.encode())
        fp.write(df.set_index('Algorithm').T.to_html(float_format=lambda x: f'{x:.2f}').encode())
    browser = webbrowser.get('chromium-browser')
    browser.open_new_tab(f'file://{fp.name}')
    time.sleep(1)
    os.remove(fp.name)


def evaluate_models(df: pd.DataFrame, target: str = 'Response time (ms)',
                    random_state: tp.Union[int, np.random.RandomState] = None) -> tp.Tuple[pd.DataFrame, plt.Figure]:
    """
    Loads a bunch of linear and tree-based regressors to compare their performance.

    Args:
        df: Input DataFrame.
        target: Target column.
        random_state: Randomness initializer.

    Returns:
        - DataFrame with summary statistics.
        - Plot with true vs predicted values.
    """
    df = preprocess.pdml_preparation(df, target, None)
    # settings adapted from:
    # https://github.com/Azure/fast_retraining/blob/master/experiments/01_airline.ipynb
    linear_estimators = [lm.LinearRegression(),
                         lm.Ridge(),
                         lm.Lasso(),
                         lm.ElasticNet()]
    tree_estimators = [xgb.XGBRegressor(base_score=0.5, colsample_bylevel=1, colsample_bytree=1, gamma=0,
                                        learning_rate=0.1, max_delta_step=0, max_depth=3,
                                        min_child_weight=1, missing=None, n_estimators=100, nthread=-1,
                                        objective='reg:linear', reg_alpha=0, reg_lambda=1,
                                        scale_pos_weight=1, seed=random_state, silent=True, subsample=1),
                       xgb.XGBRegressor(base_score=0.5, colsample_bylevel=1, colsample_bytree=1, gamma=0,
                                        learning_rate=0.1, max_delta_step=0, max_depth=8,
                                        min_child_weight=1, missing=None, n_estimators=30, nthread=-1,
                                        objective='reg:linear', reg_alpha=0, reg_lambda=1,
                                        scale_pos_weight=2, seed=random_state, silent=True, subsample=1),
                       lgb.LGBMRegressor(),
                       lgb.LGBMRegressor(n_estimators=30, min_child_weight=1, learning_rate=0.1,
                                         scale_pos_weight=1, min_split_gain=0.1, reg_lambda=1, subsample=1, n_jobs=-1,
                                         random_state=random_state)]
    estimators = linear_estimators + tree_estimators
    # estimators = tree_estimators
    col_select_range = 10
    col_amt = 2
    row_amt = len(estimators) // col_amt + len(estimators) % col_amt
    fig, ax = plt.subplots(row_amt, col_amt, figsize=(12.80, 9.60), sharex=True)
    summary_list = []
    ax_x, ax_y = 0, 0
    for i, estimator in enumerate(estimators):
        print("Processing:", str(estimator))
        new_row = collections.OrderedDict()
        result = linear_model(df, estimator, random_state)
        new_row['Algorithm'] = estimator.__class__.__name__
        new_row['MSE'] = result.metrics.mean_squared_error()
        new_row['R2'] = result.metrics.r2_score()
        new_row['EV'] = result.metrics.explained_variance_score()

        col_names = feat_column_names('feat', col_select_range)
        try:
            selector = estimator.coef_
        except AttributeError:
            try:
                selector = estimator.feature_importances_
            except AttributeError:
                selector = None

        if selector is not None:
            most_significant_columns = result.data.columns[np.argsort(selector)[::-1]][:col_select_range]
        else:
            most_significant_columns = [None] * col_select_range

        new_row.update(collections.OrderedDict(zip(col_names, most_significant_columns)))
        summary_list.append(new_row)

        ax_x, ax_y = i // col_amt, i % col_amt

        imps = preprocess.feature_importances(estimator)
        print(imps)

        plot_regression(result.target, result.predicted, title=new_row['Algorithm'], axis=ax[ax_x, ax_y])

    handles, labels = ax[ax_x, ax_y].get_legend_handles_labels()
    fig.legend(handles, labels)
    return pd.DataFrame(summary_list), fig


def create_anomaly_df(df: pd.DataFrame, df_test: pd.DataFrame = None, test_cutoff: float = 0.7,
                      estimator: RegressorMixin = None, ignore_cols: tp.List[str] = None,
                      random_state: tp.Union[int, np.random.RandomState] = None,
                      target: str = 'Response time (ms)', save: pl.Path = None) -> tp.Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Create a DataFrame containing target values, prediction values and residuals from a regression.

    Args:
        df: Input DataFrame.
        df_test: Test DataFrame (optional).
        test_cutoff: Relative cutoff for train-test-split (a value of 1 means using the train set as test set).
        estimator: Model to be fit.
        ignore_cols: Features to ignore for training the model.
        random_state: Randomness initializer.
        target: Target column.
        save: Path to save sklearn model to.

    Returns:
        DataFrame with regression results.
    """
    df = preprocess.pdml_preparation(df, target, ignore_cols)
    regr = estimator or lm.ElasticNetCV(l1_ratio=[.1, .5, .7, .9, .95, .99, 1], cv=5, random_state=random_state)
    if df_test is None:
        cutoff = int(test_cutoff * len(df))
        df_train, df_test = df.iloc[:cutoff], df.iloc[cutoff:]
        # df_train, df_test = df.model_selection.train_test_split(test_size=0.5, random_state=random_state)
    else:
        df_train = df
    df_train.fit(regr)
    predictions = regr.predict(df.data)
    df_result = pd.DataFrame(collections.OrderedDict(
        [('target', df.target), ('predicted', predictions), ('difference', predictions - df.target)]))
    df_result = df_result.rename_axis('timestamp').sort_index()
    df_result_transformed = df_result.reset_index().sort_values('difference').reset_index(drop=True)
    return df_result, df_result_transformed


def find_abnormalities(df: pd.DataFrame, target: str = 'Response time (ms)', alpha: int = 10, beta: int = 5,
                       theta: int = 1, ignore_cols: tp.List[str] = None,
                       random_state: tp.Union[int, np.random.RandomState] = None,
                       estimator: RegressorMixin = None, save: pl.Path = None) -> tp.Union[pd.DataFrame, plt.Figure]:
    """
    Apply complex smoothing algorithm to residuals to find abnormal situations. Draws residuals, rolling mean,
    trend and derivative trend.

    Args:
        df: Input DataFrame.
        target: Target column.
        alpha: First rolling parameter.
        beta: Second rolling parameter.
        theta: Rolling threshold.
        ignore_cols: Features to ignore for training the model.
        random_state: Randomness initializer.
        estimator: Sklearn model to load.
        save: Path to save sklearn model to.

    Returns:
        Anomaly DataFrame (see :func:`create_anomaly_df`).
    """
    df_result, df_result_transformed = create_anomaly_df(df=df, estimator=estimator, random_state=random_state,
                                                         ignore_cols=ignore_cols, target=target, save=save)
    # fig1, ax1 = plt.subplots()  # type: plt.Figure, plt.Axes
    # df_result_transformed.reset_index(drop=True).plot.line(y='timestamp')
    # x1.plot(df_result_transformed['timestamp'])
    # time~residuals
    residuals_only = True
    if not residuals_only:
        fig2, ax2 = plt.subplots(2, 1)  # type: plt.Figure, tp.List[plt.Axes]
        rescaled_ax, trend_ax = ax2
        rescaled_ax.plot(df_result['target'], label='rescaled response time (ms)')
        rescaled_ax.legend()
    else:
        fig2, trend_ax = plt.subplots()
    time_axis = mpd.date2num(pd.Series(df_result.index).dt.to_pydatetime())
    subject = df_result['difference']
    trend_ax.plot(time_axis, subject, label='residuals')
    trend_ax.plot(time_axis, subject.rolling(alpha).mean(), label='rolling mean')
    trend = subject[subject - subject.rolling(beta).mean() > theta].rolling(beta).mean().dropna().resample(
        'T').mean().interpolate('linear')
    # from scipy.interpolate import interp1d
    # trend_smooth = interp1d(trend.index, trend, kind='cubic')
    trend_ax.plot(trend, label='trend')
    # ax2.scatter(time_axis, subject, label='residuals2', facecolors='none', edgecolors='orange')
    derivative_trend = 40 * trend.diff() / (trend.index.to_series().diff().dt.total_seconds() / 60)
    derivative_trend = derivative_trend / derivative_trend.max() * subject.abs().max()
    trend_ax.plot(derivative_trend.abs(), label='derivative')
    trend_ax.xaxis_date()
    trend_ax.legend()
    fig2.autofmt_xdate()
    fig2.suptitle(f"alpha={alpha}, beta={beta}, theta={theta}")
    return df_result


def find_lowest_residuals(df: pd.DataFrame, decompose_func: tp.Callable,
                          search_range: tp.Tuple[int, int] = (1000, 2000)) -> tp.Tuple[int, float]:
    """
    Applies a set of frequency decompositions on a time series within a certain range, and returns the period of the
    decomposition for which the MSE is minimal.

    Args:
        df: Input DataFrame containing a 'predicted' series.
        decompose_func: Frequency decomposition function. Currently best supported function:
            :func:`statsmodels.tsa.seasonal.seasonal_decompose`
        search_range: Boundaries of the test range.

    Returns:
        - optimal decomposition period
        - corresponding MSE score
    """
    min_score = 10 ** 9
    period = 0
    pb = tqdm(range(*search_range))
    freq_arg = 'period' if 'period' in inspect.signature(decompose_func).parameters else 'freq'
    for i in pb:
        stl = _apply_decomposition(decompose_func, df['predicted'], freq_arg, i)
        score = np.sqrt((stl.resid ** 2).sum())
        pb.set_description(f"Current score: {score} \t Min score: {min_score}")
        if score < min_score:
            min_score = score
            period = i
    time.sleep(0.2)
    return period, min_score


def _apply_decomposition(decompose_func: tp.Callable, data: pd.Series, func_arg: str, period: int) -> pd.Series:
    """
    Helper function to dynamically call a decomposition function with the right argument names.

    Args:
        decompose_func: Decomposition function to be applied.
        data: Input DataFrame containing a time series.
        func_arg: Argument name for the period of the decomposition function.
        period: Decomposition period.

    Returns:
        Decomposed time series.
    """
    return locals()['decompose_func'](data, **{func_arg: period})


def split_timeseries(df: pd.DataFrame, target: str = 'Response time (ms)', period: int = 1985,
                     random_state: tp.Union[int, np.random.RandomState] = None) -> tp.Tuple[plt.Figure, plt.Axes]:
    """
    Plots the trending and seasonal components as well as the residuals of a time series.

    Args:
        df: Input DataFrame.
        target: Target column.
        period: Decomposition period.
        random_state: Randomness initializer.

    Returns:
        Decomposition plots.
    """
    df_result, df_result_transformed = create_anomaly_df(df, None, random_state, target)
    from stldecompose import decompose
    loess_stl = decompose(df_result['predicted'], period=period)
    loess_stl.plot()
    sm_stl = sm.tsa.seasonal_decompose(df_result['predicted'], freq=period)
    fig, axes = plt.subplots(5, 1, sharex="all")
    if hasattr(sm_stl.observed, 'plot'):  # got pandas use it
        sm_stl.observed.plot(ax=axes[0], legend=False)
        axes[0].set_ylabel('Observed')
        sm_stl.trend.plot(ax=axes[1], legend=False)
        # sm_stl.trend.rolling(10).mean().plot(ax=axes[1], legend=False)
        trend = sm_stl.trend[sm_stl.trend - sm_stl.trend.rolling(5).mean().abs() > 0.25].rolling(10).mean().fillna(0)
        trend.interpolate('linear').plot(ax=axes[1], legend=False)
        axes[1].set_ylabel('Trend')
        sm_stl.seasonal.plot(ax=axes[2], legend=False)
        axes[2].set_ylabel('Seasonal')
        sm_stl.resid.plot(ax=axes[3], legend=False)
        axes[3].set_ylabel('Residual')
        df_result['difference'].plot(ax=axes[4])
        axes[4].set_ylabel('Difference')
    return fig, axes


def find_optimal_seasonality(df: pd.DataFrame, decompose_func: tp.Callable, target: str = 'Response time (ms)',
                             random_state: tp.Union[int, np.random.RandomState] = None) -> tp.Tuple[int, float]:
    """
    Find the optimal seasonality to decompose a time series in a given DataFrame.

    Args:
        df: Input DataFrame.
        decompose_func: Decomposition function.
        target: Target column.
        random_state: Randomness initializer.

    Returns:
        - optimal decomposition period
        - corresponding MSE score
    """
    df_result, df_result_transformed = create_anomaly_df(df, None, random_state, target)
    return find_lowest_residuals(df_result, decompose_func)


class OxyaAnalyser(object):
    """
    Class to load oXya data, parse it and perform experiments.

    Args:
        args: Command-line arguments.

    .. note::
       Use `python analysis .py --help` for more information.
    """

    @load.timing
    def __init__(self, args: argparse.Namespace) -> None:
        # plt.style.use('dark_background')
        parser = preprocess.OxyaParser(args, data=None)
        self.run_args = parser.run_args
        self.target = parser.target
        self.random_state = parser.random_state
        self.sample_fraction = parser.sample_fraction
        self.path = parser.path
        self.reader = parser.reader
        self.dialog_transactions = parser.dialog_transactions
        self.df = parser.df
        self.mapper = parser.mapper
        self.transformed = parser.transformed
        self._print_stats()

    @load.timing
    def run(self, *run_args: tp.Any, **run_kwargs: tp.Any) -> tp.Optional[tp.Any]:
        """
        Run an experiment on a loaded oXya dataset. Current choices are:

            - explore: Plot the data as a linechart.
            - compare: Compare the performance several linear and tree-based models.
            - residuals: Find abnormalities in given dataset.
            - split: Split the data into underlying time series components.
            - period: Find periodicities in the data.
            - sarimax: Use ARIMA with exogeneous variables to examine the data.

        Multiple experiments may be run sequentially.

        Args:
            *run_args: Running arguments.
            **run_kwargs: Running keyword arguments.

        Returns:
            Optional output depending on chosen experiment(s).

        .. note::
           Use `python analysis .py --help` for more information.
        """
        if run_args:
            self.run_args = run_args
        if 'explore' in self.run_args:
            # profile(reader)
            # correlation_plot(self.reader.df)
            line_plot(self.reader.df.loc[self.reader.df['T'] == 'D'], col_name=self.target, **run_kwargs)
        if 'compare' in self.run_args:
            summary, figure = evaluate_models(self.transformed, random_state=self.random_state)
            render_dataframe(summary, figure)
            return summary
        if 'residuals' in self.run_args:
            alphas = (1, 10)  # 2, 5, 10)
            betas = (3, 5)
            thetas = (1, 10)
            save_ = self.path / "saved_model.pkl" if 'save' in self.run_args else None
            load_ = self.path / "saved_model.pkl" if 'load' in self.run_args else None
            results = []
            import tqdm
            for alpha, beta, theta in tqdm.tqdm(itertools.product(alphas, betas, thetas)):
                results.append(find_abnormalities(self.transformed, target=self.target, random_state=self.random_state,
                                                  alpha=alpha, beta=beta, theta=theta, estimator=load_, save=save_))
            return results
        if 'split' in self.run_args:
            split_timeseries(self.transformed, random_state=self.random_state)
        if 'period' in self.run_args:
            # from stldecompose import decompose
            decompose = sm.tsa.seasonal_decompose
            print(find_optimal_seasonality(self.transformed, decompose, random_state=self.random_state))
        if 'sarimax' in self.run_args:
            self._sarimax(self.transformed, random_state=self.random_state)

    def _sarimax(self, df, target='Response time (ms)', random_state=0) -> None:
        import cufflinks as cf
        cf.go_offline()
        # window = line_plot(self.reader)
        # window = self.dialog_transactions.loc[:, "Response time (ms)"]
        # output = window.iplot(kind='scatter', filename='sarimax', asPlot=True)
        # print(output)
        # window.plot()
        df_result, df_result_transformed = create_anomaly_df(df, None, random_state, target)
        fig, ax = plt.subplots(2, 2, figsize=(20, 15))  # type: plt.Figure, np.ndarray[plt.Axes]
        subject = df_result['predicted']
        model = sm.tsa.SARIMAX(subject, order=(1, 0, 0))
        sarimax_result = pd.Series(model.fit().fittedvalues)
        ax[0, 0].plot(subject)
        ax[0, 0].set_title(subject.name)
        fig = sm.graphics.tsa.plot_acf(subject, lags=80, ax=ax[0, 1])
        fig = sm.graphics.tsa.plot_pacf(subject, lags=80, ax=ax[1, 0])
        ax[1, 1].plot(subject, color='blue', label='original')
        ax[1, 1].plot(sarimax_result, color='green', label='SARIMAX')
        ax[1, 1].plot(subject - sarimax_result, color='red', label='difference')
        ax[1, 1].legend()

    def _print_stats(self) -> None:
        stats = load.df_types(self.df)
        # print(self.df.columns)
        print(self.df.columns.min(), self.df.columns.max())
        print(len(stats[stats == 'category']), "categoricals found.")

    @staticmethod
    def show() -> None:
        """
        Display the plots generated by :func:`run`.
        """
        plt.show()

    def _map_features(self, df: pd.DataFrame) -> pd.DataFrame:
        return self._feature_extraction(df).fit_transform(df)

    @staticmethod
    def _feature_extraction(df: pd.DataFrame) -> skpd.DataFrameMapper:
        return preprocess.feature_extraction(df)


def main(args: argparse.Namespace) -> None:
    """
    Main function.

    Args:
        args: Running arguments.

    .. note::
       Use `python analysis .py --help` for more information.
    """
    print(args)
    analyser = OxyaAnalyser(args)
    # test_gen = preprocess.generate_extra_features(analyser.dialog_transactions, args.resample.resample,
    #                                               ['_source.respti'], ['count', 'max', 'median'])
    # print(test_gen['Response time (ms)'].head())
    analyser.run()
    analyser.show()


def arguments() -> argparse.Namespace:
    """
    Argument parser.

    Returns: Parsed arguments

    .. note::
       Use `python analysis .py --help` for more information.
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ns = Nestedspace()
    parser.add_argument('run.run', nargs='*', choices=['explore', 'compare', 'residuals', 'period', 'split', 'sarimax'],
                        default='residuals', help='Specify the analysis to run.')
    parser.add_argument('--path', default=str(pl.Path.home() / 'Datasets' / 'oxya' / 'UG1'),
                        help='Path to dataset folder or file.', dest='dataset.path')
    parser.add_argument('--rebuild', action='store_true', help='Include to rebuild the parsed csv file.',
                        dest='dataset.rebuild')
    parser.add_argument('--rng', default=0, help='Initialize the random number sequence generator.', dest='run.rng')
    parser.add_argument('--sample_factor', default=50, help='Inverse of the sampling fraction.',
                        dest='resample.sample_factor')
    parser.add_argument('--resample', default='S', help='Resampling rate.', dest='resample.resample')
    parser.add_argument('--resample_cats', action='store_true', help='Resample categorical variables.',
                        dest='resample.resample_cats')
    parser.add_argument('--dask', action='store_false', help='Use Dask for resampling (parallel).',
                        dest='resample.dask')
    parser.add_argument('--expand', action='store_true', help='Expand event sequences with additional stats.',
                        dest='run.expand')
    parser.add_argument('--config', default=str(ROOT_DIR / 'config2.ini'),
                        help='Specify the location of a configuration file.')
    parsed_args = parser.parse_args(namespace=ns)
    if 'config' in parsed_args:
        print("Loading configuration from config.ini...")
        args = load_config(parsed_args.config)
    else:
        args = parsed_args
    return args


if __name__ == '__main__':
    main(arguments())

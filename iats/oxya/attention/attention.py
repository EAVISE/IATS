# coding: utf-8

# ## A Dual-Stage Attention-Based Recurrent Neural Network for Time Series Prediction
# source: https://arxiv.org/pdf/1704.02971.pdf
# pytorch example: http://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html
import datetime
import os
import pathlib as pl
import shutil

import matplotlib.pyplot as plt
import pandas as pd
from tensorboardX import SummaryWriter

import iats.utils.utility
from iats.oxya.attention.model import DEVICE, DARNNTrainer, USE_CUDA
from iats.oxya.preprocess import OxyaParser
from iats.utils.config import default_oxya_args
from iats.utils.plot import plot_epoch_loss, plot_iteration_loss, plot_test_vs_predicted

# util.setup_path()

logger = iats.utils.utility.setup_log()
writer = SummaryWriter()
logger.info(f"Is CUDA available? {USE_CUDA}.")
logger.info(f"Using device: {str(DEVICE)}")


def load_nasdaq_data():
    io_dir = '~/Datasets/nasdaq100'
    data = pd.read_csv('{}/small/nasdaq100_padding.csv'.format(io_dir))
    return data


def prune_dir(base: pl.Path) -> None:
    """
    First, removes all empty directories in base folder. Next, iterates over the non-empty directories and removes
    them if they are less than one hour old.

    Args:
        base: Base directory.
    """
    subdirs = filter(lambda x: x.is_dir() and not str(x).startswith('.'), base.iterdir())
    for subdir in subdirs:
        try:
            subdir.rmdir()
        except OSError:  # non-empty directory
            folder_date = datetime.datetime.strptime(str(subdir).split('_')[1], '%Y%m%d-%H%M')
            if datetime.datetime.now() - folder_date < datetime.timedelta(hours=1):
                shutil.rmtree(subdir)


def make_dir(base: pl.Path, prefix: str = 'weights') -> pl.Path:
    """
    Generates a directory name based on current time.

    Args:
        base: Base directory.
        prefix: First part of the new directory's name.

    Returns:
        Path to the new directory.
    """
    return base / '{}_{:%Y%m%d-%H%M}'.format(prefix, datetime.datetime.now())


def main():
    basedir = pl.Path.cwd() / 'weights'
    prune_dir(basedir)
    saved_model_folder = make_dir(basedir)
    if not saved_model_folder.exists():
        os.makedirs(str(saved_model_folder))
    #data = OxyaParser(default_oxya_args())
    #data = data.transformed.reset_index(drop=True)
    data = None
    model = DARNNTrainer(data, target='Response time (ms)', logger=logger, tensorboard_writer=writer,
                         save_path=saved_model_folder, parallel=False, learning_rate=.001, batch_size=3072, continue_training=True)
    model.train(n_epochs=1000, save=True)
    y_real = model.dataset.y[model.dataset.train_size:]
    y_pred = model.predict()

    # losses per iteration
    fig, ax = plt.subplots()
    plot_iteration_loss(ax, model)
    plt.savefig(saved_model_folder / "total_iter_loss.png")

    # losses per epoch
    fig, ax = plt.subplots()
    plot_epoch_loss(ax, model)
    plt.savefig(saved_model_folder / "total_epoch_loss.png")

    # accuracy
    fig, ax = plt.subplots()
    plot_test_vs_predicted(ax, y_pred, y_real)
    plt.savefig(saved_model_folder / "total_accuracy.png")


if __name__ == '__main__':
    main()

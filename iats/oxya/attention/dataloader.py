import logging
import math
import pathlib as pl
import typing as tp

import numpy as np
import pandas as pd
import pandas_ml as pdml
import torch
import torch.utils.data as torchdata

import iats.oxya.preprocess as preprocess
from iats.utils.config import default_oxya_args
from iats.utils.load import convert_daterange_to_csvs

logger = logging.getLogger("DA-RNN")


class Dataset(torchdata.Dataset):
    def __init__(self, data: pd.DataFrame, target: str, debug: bool = False, drop_cols: tp.List[str] = None,
                 transform: tp.Callable = None, window: int = 20, skip: int = 1, train_size: float = 0.7) -> None:
        """
        Class to wrap the STAD data into for fast processing in PyTorch.

        Args:
            data: DataFrame containing the data.
            target: Values to be predicted.
            debug: When True, uses only a small part of the DataFrame for testing purposes.
            drop_cols: Features to be removed from the dataset.
            transform: Transformation to be applied to all features in the dataset.
            window: Size of the look-back window.
            skip: Overlap factor.
            train_size: Fraction of the data to use for training the model.
        """
        self.data = self.prepare_data(data, target, debug, drop_cols)
        self.X, self.y = self.data.data.values, self.data.target.values
        self.train_size = int(self.X.shape[0] * train_size)
        self.y = self.y - self.y[:self.train_size].mean()  # data normalization for Adam optimizers
        self.transform = transform
        self.window = window
        self.overlap = self.window - skip

    @classmethod
    def from_giga_csv(cls, filepath: pl.Path, begin_date: pd.Timestamp, end_date: pd.Timestamp, target: str,
                      debug: bool = False, drop_cols: tp.List[str] = None, transform: tp.Callable = None,
                      window: int = 20, skip: int = 1, train_size: float = 0.7) -> "Dataset":
        """
        Load data from a collection of CSVs scraped from ElasticSearch via the "communicate" module.
        When using the `begin_date` and `end_date` parameters, make sure they have already been grabbed from the ES
        database.

        Args:
            filepath: Location where the CSV files are stored.
            begin_date: Begin date of data to be analyzed.
            end_date: End date of data to be analyzed.
            target: Values to be predicted.
            debug: When True, uses only a small part of the DataFrame for testing purposes.
            drop_cols: Features to be removed from the dataset.
            transform: Transformation to be applied to all features in the dataset.
            window: Size of the look-back window.
            skip: Overlap factor.
            train_size: Fraction of the data to use for training the model.

        Returns: Dataset instance.

        """
        files = [filepath / f for f in convert_daterange_to_csvs("output", begin_date, end_date)]
        frame = pd.concat((pd.read_csv(f) for f in files))
        return cls(frame, target, debug, drop_cols, transform, window, skip, train_size)

    def __len__(self):
        return math.floor((len(self.data) - self.window) / (self.window - self.overlap)) + 1

    def __getitem__(self, index: int) -> np.ndarray:
        begin_idx = max(self.window * index - self.overlap * index, 0)
        item = self.X[begin_idx: begin_idx + self.window], self.y[begin_idx: begin_idx + self.window]
        if self.transform:
            item = self.transform(item)
        return item

    @staticmethod
    def prepare_data(data: pd.DataFrame, target: str, debug: bool = False,
                     drop_cols: tp.List[str] = None) -> pdml.ModelFrame:
        """
        Do some data preprocessing before passing it to the neural network.

        Args:
            data: DataFrame containing the data.
            target: Values to be predicted.
            debug: When True, uses only a small part of the DataFrame for testing purposes.
            drop_cols: Features to be removed from the dataset.

        Returns: Processed DataFrame as ModelFrame.
        """
        if debug:
            data = data.head(100)
        logger.info("Shape of data: %s.\nMissing in data: %s.", data.shape, data.isnull().sum().sum())
        for col in list(data):
            if 'timestamp' in col:
                try:
                    data[col] = pd.to_datetime(data[col])
                except ValueError:
                    pass
        data = data.select_dtypes(include=[np.number, np.datetime64])
        data = pdml.ModelFrame(data, target=target)
        if drop_cols:
            data.drop(columns=drop_cols, errors='ignore', inplace=True)
        return data


class ValueTransform(object):
    def __init__(self):
        pass

    def __call__(self, sample: tp.Union[torch.tensor, pd.DataFrame, np.ndarray]) -> np.ndarray:
        return sample.values if not isinstance(sample, np.ndarray) else sample


class ToTensor(object):
    def __init(self):
        pass

    def __call__(self, sample: torch.tensor) -> torch.tensor:
        return torch.tensor(sample)


class Sampler(torchdata.Sampler):  # Requires new version of torch!
    # https://stackoverflow.com/a/47434173
    def __init__(self, mask: torch.tensor):
        self.mask = mask

    def __len__(self):
        return (self.indices[i] for i in torch.nonzero(self.mask))

    def __iter__(self):
        return len(self.mask)


def load_oxya_data() -> preprocess.OxyaParser:
    """
    Creates an OxyaAnalyser instance with default arguments.

    Returns:
        OxyaAnalyser object.
    """
    args = default_oxya_args()
    return preprocess.OxyaParser(args=args)


def main():
    myset = Dataset.from_giga_csv(pl.Path("..").resolve(), pd.Timestamp("2019-04-24"), pd.Timestamp("2019-04-30"),
                                  target='_source.respti')
    from pprint import pprint
    for i, x in enumerate(myset):
        pprint(x)
        if i > 100:
            break


if __name__ == '__main__':
    main()

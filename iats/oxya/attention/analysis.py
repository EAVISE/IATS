import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import typing as tp

import torch
import torch.onnx

from iats.oxya.attention.model import DARNNTrainer
from iats.enervalis.dataloader import find_files, read_files, preprocess
from iats.oxya.attention.load import load_trainer
from iats.oxya.preprocess import OxyaParser
from iats.utils.plot import plot_test_vs_predicted, plot_epoch_loss, plot_iteration_loss, plot_training_vs_validation
from iats.utils.config import default_oxya_args


def export_model(model: torch.nn.Module):
    encoder_input = torch.randn(1, 19, 56).cuda()
    torch_out = torch.onnx.export(model.encoder, encoder_input, "encoder.onnx", export_params=True)
    return torch_out


def experiment_oxya(weight_folder: str) -> DARNNTrainer:
    # paths
    basedir = Path.cwd() / 'weights' / weight_folder
    saved_model_folder = basedir / 'weights_{:%Y%m%d-%H%M}'.format(datetime.datetime.now())
    state = basedir / 'checkpoint_990.pth.tar'
    # load data
    data = OxyaParser(default_oxya_args()).transformed.reset_index(drop=True)
    # load model
    model = load_trainer(saved_model_folder, state, data, 'Response time (ms)')
    return model


def experiment_enervalis() -> DARNNTrainer:
    # paths
    basedir = Path.home() / 'Datasets' / 'Elia Imbalance' / '2016'
    saved_model_folder = basedir / 'weights_{:%Y%m%d-%H%M}'.format(datetime.datetime.now())
    state = basedir / 'weights' / 'checkpoint_990.pth.tar'
    # load data
    files = find_files(basedir, year=2016)
    df = preprocess(read_files(files))
    data = df.drop(columns=['PPOS', 'PNEG']).reset_index(drop=True)
    # load model
    model = load_trainer(saved_model_folder, state, data, 'MIP')
    return model


def run(experiment: tp.Callable, *args: tp.Any) -> None:
    model = experiment(*args)  # type: DARNNTrainer
    y_real = model.dataset.y
    y_train = model.predict(on_train=True)
    y_val = model.predict(on_train=False)

    # losses per iteration
    fig, ax = plt.subplots()
    plot_iteration_loss(ax, model)

    # losses per epoch
    fig, ax = plt.subplots()
    plot_epoch_loss(ax, model)

    # accuracy
    fig, ax = plt.subplots()
    plot_test_vs_predicted(ax, y_val, y_real[model.dataset.train_size:])

    # eval
    fig, ax = plt.subplots()
    plot_training_vs_validation(ax, y_train, y_val, y_real, model.T)

    plt.show()


def main():
    oxya_weights_outdated = {'old': 'weights_20180501-1802', 'new': 'weights_20180601-1716'}
    oxya_weights = {'old': 'weights_20181122-1747'}
    run(experiment_oxya, oxya_weights['old'])
    # run(experiment_enervalis)


if __name__ == '__main__':
    main()


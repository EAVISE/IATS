import pathlib

import pandas as pd
import tensorboardX
import torch

from iats.oxya.attention import model as model
from iats.utils.utility import setup_log


def load_trainer(save_path: pathlib.Path, saved_state: pathlib.Path, data: pd.DataFrame,
                 target: str, module: model.DualAttentionRNN = None,
                 continue_training: bool = False) -> model.DARNNTrainer:
    """Load a DARNNTrainer instance from a saved state and prepare it for evaluation or continued training.

    Args:
        save_path: Path where the model's states are saved during training.
        saved_state: Saved model state to load.
        data: Data to evaluate or continue training the model with.
        target: Target variable of the data.
        module: Optional instance of DA-RNN network to load directly from memory.
        continue_training: Specifies whether the model is loaded to continue training or to evaluate.

    Returns:
        Loaded DARNNTrainer instance.

    """
    state = torch.load(saved_state)
    model_ = model.DARNNTrainer(data, target=target, logger=setup_log(), model=module,
                                tensorboard_writer=tensorboardX.SummaryWriter(), save_path=save_path,
                                parallel=False, learning_rate=.001, drop_cols=True)
    # model_.encoder.load_state_dict(state['encoder'])
    # model_.decoder.load_state_dict(state['decoder'])
    model_.darnn.load_state_dict(state['darnn'])
    model_.epoch_losses = state['epoch_losses']
    model_.iter_losses = state['iter_losses']
    if not continue_training:
        model_.darnn.eval()
    return model_

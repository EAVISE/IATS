import glob
import logging
import math
import os
import pathlib as pl
import typing as tp

import numpy as np
import pandas as pd
import tensorboardX
import torch
import torch.utils.data as torchdata
from matplotlib import pyplot as plt
from torch import nn, optim
from torch.nn import functional

from iats.oxya.attention.dataloader import Dataset


class Encoder(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, T: int) -> None:
        # input size: number of underlying factors (81)
        # T: number of time steps (10)
        # hidden_size: dimension of the hidden state
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.T = T

        self.lstm_layer = nn.LSTM(input_size=input_size, hidden_size=hidden_size, num_layers=1).to(DEVICE)
        self.lstm_layer.flatten_parameters()
        self.attn_linear = nn.Linear(in_features=2 * hidden_size + T - 1, out_features=1).to(DEVICE)

    def forward(self, input_data: torch.tensor) -> tp.Tuple[torch.tensor, torch.tensor]:
        # input_data: batch_size * T - 1 * input_size
        input_weighted = torch.zeros([input_data.size(0), self.T - 1, self.input_size], requires_grad=True).to(DEVICE)
        input_encoded = torch.zeros([input_data.size(0), self.T - 1, self.hidden_size], requires_grad=True).to(DEVICE)

        # hidden, cell: initial states with dimension hidden_size
        hidden = self.init_hidden(input_data)  # 1 * batch_size * hidden_size
        cell = self.init_hidden(input_data)
        # hidden.requires_grad = False
        # cell.requires_grad = False
        for t in range(self.T - 1):
            # Eqn. 8: concatenate the hidden states with each predictor
            x = torch.cat((hidden.repeat(self.input_size, 1, 1).permute(1, 0, 2),
                           cell.repeat(self.input_size, 1, 1).permute(1, 0, 2),
                           input_data.permute(0, 2, 1)), dim=2)  # batch_size * input_size * (2*hidden_size + T - 1)
            # Eqn. 9: Get attention weights
            x = self.attn_linear(x.view(-1, self.hidden_size * 2 + self.T - 1))  # (batch_size * input_size) * 1
            attn_weights = functional.softmax(
                x.view(-1, self.input_size), dim=-1)  # batch_size * input_size, attn weights with values sum up to 1.
            # Eqn. 10: LSTM
            weighted_input = torch.mul(attn_weights, input_data[:, t, :])  # batch_size * input_size
            # Fix the warning about non-contiguous memory
            # see https://discuss.pytorch.org/t/dataparallel-issue-with-flatten-parameter/8282

            _, lstm_states = self.lstm_layer(weighted_input.unsqueeze(0), (hidden, cell))
            hidden = lstm_states[0]
            cell = lstm_states[1]
            # Save output
            input_weighted[:, t, :] = weighted_input
            input_encoded[:, t, :] = hidden
        return input_weighted, input_encoded

    def init_hidden(self, x: torch.tensor) -> torch.tensor:
        # No matter whether CUDA is used, the returned variable will have the same type as x.
        # dimension 0 is the batch dimension
        return torch.zeros([1, x.size(0), self.hidden_size], requires_grad=True).to(DEVICE)


class Decoder(nn.Module):
    def __init__(self, encoder_hidden_size: int, decoder_hidden_size: int, T: int) -> None:
        super().__init__()

        self.T = T
        self.encoder_hidden_size = encoder_hidden_size
        self.decoder_hidden_size = decoder_hidden_size

        self.attn_layer = nn.Sequential(nn.Linear(2 * decoder_hidden_size + encoder_hidden_size, encoder_hidden_size),
                                        nn.Tanh(), nn.Linear(encoder_hidden_size, 1))
        self.lstm_layer = nn.LSTM(input_size=1, hidden_size=decoder_hidden_size)
        self.lstm_layer.flatten_parameters()
        self.fc = nn.Linear(encoder_hidden_size + 1, 1)
        self.fc_final = nn.Linear(decoder_hidden_size + encoder_hidden_size, 1)

        torch.nn.init.xavier_normal_(self.fc.weight)

    def forward(self, input_encoded: torch.tensor, y_history: torch.tensor) -> torch.tensor:
        # input_encoded: batch_size * T - 1 * encoder_hidden_size
        # y_history: batch_size * (T-1)
        # Initialize hidden and cell, 1 * batch_size * decoder_hidden_size
        hidden = self.init_hidden(input_encoded)
        cell = self.init_hidden(input_encoded)
        # hidden.requires_grad = False
        # cell.requires_grad = False
        for t in range(self.T - 1):
            # Eqn. 12-13: compute attention weights
            # batch_size * T * (2*decoder_hidden_size + encoder_hidden_size)
            x = torch.cat((hidden.repeat(self.T - 1, 1, 1).permute(1, 0, 2),
                           cell.repeat(self.T - 1, 1, 1).permute(1, 0, 2), input_encoded), dim=2)
            x = functional.softmax(self.attn_layer(x.view(-1, 2 * self.decoder_hidden_size + self.encoder_hidden_size))
                                   .view(-1, self.T - 1), dim=-1)  # batch_size * T - 1, row sum up to 1
            # Eqn. 14: compute context vector
            context = torch.bmm(x.unsqueeze(1), input_encoded)[:, 0, :]  # batch_size * encoder_hidden_size
            if t < self.T - 1:
                # Eqn. 15
                y_tilde = self.fc(torch.cat((context, y_history[:, t].unsqueeze(1)), dim=1))  # batch_size * 1
                # Eqn. 16: LSTM

                _, lstm_output = self.lstm_layer(y_tilde.unsqueeze(0), (hidden, cell))
                hidden = lstm_output[0]  # 1 * batch_size * decoder_hidden_size
                cell = lstm_output[1]  # 1 * batch_size * decoder_hidden_size
        # Eqn. 22: final output
        y_pred = self.fc_final(torch.cat((hidden[0], context), dim=1))
        # self.logger.info("hidden %s context %s y_pred: %s", hidden[0][0][:10], context[0][:10], y_pred[:10])
        return y_pred

    def init_hidden(self, x: torch.tensor) -> torch.tensor:
        return torch.zeros([1, x.size(0), self.decoder_hidden_size], requires_grad=True).to(DEVICE)


class DualAttentionRNN(nn.Module):
    def __init__(self, encoder: Encoder, decoder: Decoder) -> None:
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, x: torch.tensor, y_history: torch.tensor) -> torch.tensor:
        input_weighted, input_decoded = self.encoder(x)
        output = self.decoder(input_decoded, y_history)
        return output


class DARNNTrainer:
    def __init__(self, file_data: pd.DataFrame, target: str, logger: logging.Logger, model: DualAttentionRNN = None,
                 tensorboard_writer: tensorboardX.writer = None, save_path: tp.Union[pl.Path, str] = None,
                 name: str = None, encoder_hidden_size: int = 128, decoder_hidden_size: int = 128, T: int = 20,
                 learning_rate: float = 0.01, batch_size: int = 128, lr_shrink_rate: float = 0.9, parallel: bool = True,
                 debug: bool = False, continue_training: bool = False, drop_cols: bool = True) -> None:

        # loggers and output
        self.logger = logger
        self.writer = tensorboard_writer
        self.save_path = save_path.absolute()
        self.name = name
        if self.name is not None:
            self.save_path = self.save_path.parent / name
        if not self.save_path.exists():
            os.makedirs(self.save_path)

        # hyperparameters
        self.T = T
        self.learning_rate = learning_rate
        self.lr_shrink_rate = lr_shrink_rate
        self.batch_size = batch_size

        # training parameters
        self.parallel = parallel
        self.debug = debug
        self.resume = continue_training

        # data
        self.dataset = Dataset.from_giga_csv(pl.Path("..").resolve(), pd.Timestamp("2019-04-24"),
                                             pd.Timestamp("2019-04-30"), target='_source.respti', debug=False,
                                             drop_cols=drop_cols, transform=None, window=20,
                                             train_size=0.7)
        from iats.oxya.preprocess import OxyaParser
        from iats.utils.config import production_oxya_args
        temp_parser = OxyaParser(args=production_oxya_args(), data=self.dataset.data)
        self.dataset.data = temp_parser.data
        self.dataloader = torchdata.DataLoader(self.dataset, batch_size=self.batch_size, drop_last=True, shuffle=True)

        self.logger.info(f"Training size: {self.dataset.train_size:d}.", )

        # losses
        self.iter_losses = np.array([])  # init
        self.epoch_losses = np.array([])  # init
        self.loss_func = nn.MSELoss()  # loss function

        # modules
        if not model:
            self.encoder = Encoder(input_size=self.dataset.X.shape[1], hidden_size=encoder_hidden_size,
                                   T=self.T).to(DEVICE)
            self.decoder = Decoder(encoder_hidden_size=encoder_hidden_size, decoder_hidden_size=decoder_hidden_size,
                                   T=self.T).to(DEVICE)
            self.darnn = DualAttentionRNN(self.encoder, self.decoder)
        else:
            self.darnn = model

        # starting point
        self.start_epoch = 0

        # parallel training?
        if parallel:
            self.darnn = nn.DataParallel(self.darnn)

        # optimizer
        self.darnn_optimizer = optim.Adam(
            params=filter(lambda p: p.requires_grad, self.darnn.parameters()),
            lr=self.learning_rate)

        # load previous state
        if continue_training:
            self.load_training_state()

    def load_training_state(self) -> None:
        try:
            self.save_path = sorted([x for x in pl.Path(self.save_path).resolve().parent.iterdir() if x.is_dir()])[-2]
            latest_model = sorted(list(self.save_path.glob("*.tar")))[-1]
            model_params = torch.load(latest_model)
            # weight
            self.darnn.load_state_dict(model_params['darnn'])
            # optimizer
            self.darnn_optimizer.load_state_dict(model_params['darnn_optimizer'])
            # starting point
            self.start_epoch = model_params['epoch']
        except FileNotFoundError:
            raise RuntimeWarning("No saved weights were found!")

    def partial_reset(self, enable: bool = False) -> None:
        for param in self.encoder.parameters():
            param.requires_grad = enable
        self.darnn_optimizer = optim.Adam(filter(lambda p: p.requires_grad(), self.darnn.parameters()),
                                          lr=self.learning_rate)

    def train(self, n_epochs: int = 10, save: bool = True) -> None:
        batch_count = self.dataset.train_size / self.batch_size
        iter_per_epoch = int(np.ceil(batch_count))
        self.logger.info(f"Iterations per epoch: {batch_count:3.3f} ~ {iter_per_epoch:d}.")
        self.iter_losses = np.zeros(n_epochs * iter_per_epoch + 1)
        self.epoch_losses = np.zeros(n_epochs)

        n_iter = 0

        for i in range(self.start_epoch, n_epochs):
            j = 0
            for data in self.dataloader:
                X = data[0][:, :-1, :].to(DEVICE)
                y_history = data[1][:, :-1].to(DEVICE)
                y_target = data[1][:, -1].to(DEVICE)

                with torch.enable_grad():
                    loss = self.train_iteration(X, y_history, y_target)
                try:
                    self.iter_losses[i * iter_per_epoch + j // self.batch_size] = loss
                except IndexError:
                    print(f"i: {i}, iter_per_epoch: {iter_per_epoch}, j: {j}, batch size: {self.batch_size}")
                    raise
                j += self.batch_size
                n_iter += 1

                # constant: 10,000 / (40,302 / 128) = 31.75
                if n_iter % int(31.75 * batch_count) == 0 and n_iter > 0:
                    for param_group in self.darnn_optimizer.param_groups:
                        param_group['lr'] = param_group['lr'] * self.lr_shrink_rate

            self.epoch_losses[i] = np.mean(self.iter_losses[range(i * iter_per_epoch, (i + 1) * iter_per_epoch)])
            self.writer.add_scalar("epoch_loss", self.epoch_losses[i])
            if i % 10 == 0:
                self.logger.info(f"Epoch {i}, loss: {self.epoch_losses[i]:3.3f}")
            else:
                self.logger.info(f"Epoch {i}")

            if not save:
                continue

            if i % 10 == 0:
                y_train_pred = self.predict(on_train=True)
                y_test_pred = self.predict(on_train=False)
                y_pred = np.concatenate((y_train_pred, y_test_pred))
                self.plot_epoch_results(y_test_pred, y_train_pred, i)
                self.save_state(self.save_path, n_epochs, i)

    def save_state(self, save_path: tp.Union[pl.Path, str], n_epochs: int, current_epoch: int) -> None:
        state = {
            'epoch': current_epoch,
            'darnn': self.darnn.state_dict(),
            'darnn_optimizer': self.darnn_optimizer.state_dict(),
            'epoch_losses': self.epoch_losses,
            'iter_losses': self.iter_losses
        }

        path = save_path / f'checkpoint_{str(current_epoch).zfill(int(math.ceil(np.log10(n_epochs))))}.pth.tar'
        if path.is_file():
            os.remove(path)

        torch.save(state, path)

    def plot_epoch_results(self, y_test_pred: np.ndarray, y_train_pred: np.ndarray, iteration: int) -> None:
        plt.figure()
        plt.plot(range(1, 1 + len(self.dataset.y)), self.dataset.y, label="True")
        plt.plot(range(self.T, len(y_train_pred) + self.T), y_train_pred, label='Predicted - Train')
        plt.plot(range(self.T + len(y_train_pred), len(self.dataset.y) + 1), y_test_pred, label='Predicted - Test')
        plt.legend(loc='upper left')
        plt.savefig(self.save_path / ("epoch" + str(iteration) + ".png"))

    def train_iteration(self, X: torch.tensor, y_history: torch.tensor, y_target: torch.tensor) -> float:
        self.darnn_optimizer.zero_grad()

        X = X.float().requires_grad_().to(DEVICE)
        y_history = y_history.float().requires_grad_().to(DEVICE)
        y_target = y_target.float().requires_grad_().to(DEVICE)

        y_pred = self.darnn(X, y_history)

        loss = self.loss_func(y_pred, y_target.unsqueeze(1))
        loss.backward()

        self.darnn_optimizer.step()

        # if loss.data[0] < 10:
        #     self.logger.info("MSE: %s, loss: %s.", loss.data, (y_pred[:, 0] - y_true).pow(2).mean())

        return loss.item()

    def predict(self, on_train: bool = False) -> np.ndarray:
        # TODO: use dataloader api
        if on_train:
            y_pred = np.zeros(self.dataset.train_size - self.T + 1)
        else:
            y_pred = np.zeros(self.dataset.X.shape[0] - self.dataset.train_size)

        i = 0
        batch_indices = np.arange(len(y_pred))
        while i < len(y_pred):
            batch_idx = batch_indices[i: (i + self.batch_size)]
            X = np.zeros((len(batch_idx), self.T - 1, self.dataset.X.shape[1]))
            y_history = np.zeros((len(batch_idx), self.T - 1))
            for j in range(len(batch_idx)):
                if on_train:
                    X[j, :, :] = self.dataset.X[range(batch_idx[j], batch_idx[j] + self.T - 1), :]
                    y_history[j, :] = self.dataset.y[range(batch_idx[j], batch_idx[j] + self.T - 1)]
                else:
                    X[j, :, :] = self.dataset.X[
                                 range(batch_idx[j] + self.dataset.train_size - self.T,
                                       batch_idx[j] + self.dataset.train_size - 1), :]
                    y_history[j, :] = self.dataset.y[range(batch_idx[j] + self.dataset.train_size - self.T,
                                                           batch_idx[j] + self.dataset.train_size - 1)]

            y_history = torch.from_numpy(y_history).type(torch.FloatTensor).cuda()
            _, input_encoded = self.encoder(torch.from_numpy(X).type(torch.FloatTensor).cuda())
            y_pred[i:(i + self.batch_size)] = self.decoder(input_encoded, y_history).cpu().data.numpy()[:, 0]
            i += self.batch_size
        return y_pred


USE_CUDA = torch.cuda.is_available()
DEVICE = torch.device("cuda" if USE_CUDA else "cpu")

import pytest
import torch
from torch.utils import data as torchdata
from torchvision import transforms

from iats.oxya.attention.dataloader import Dataset, ToTensor, ValueTransform, load_oxya_data


@pytest.fixture
def transformed_dataset():
    data = load_oxya_data().transformed.reset_index(drop=True)
    trf = transforms.Compose([ValueTransform(), ToTensor()])
    return Dataset(data, "Response time (ms)", debug=True, drop_cols=None, transform=trf)


@pytest.mark.skip(reason='hangs')
def test_dataset():
    my_data = Dataset(transformed_dataset.data, "Response time (ms)", debug=True, drop_cols=None)
    for i, block in enumerate(my_data):
        print(i, block)


def test_dataloader(transformed_dataset):
    my_data = transformed_dataset
    dataloader = torchdata.DataLoader(my_data, batch_size=10, drop_last=True, shuffle=True)
    for sample in dataloader:
        print(sample.shape)


def test_transform(transformed_dataset):
    my_data = transformed_dataset
    for i in range(len(my_data)):
        print(my_data[i], len(my_data[i]), i)


@pytest.mark.skip(reason="doesn't work")
def test_random_split(transformed_dataset):
    my_data = transformed_dataset
    d1, d2 = torchdata.random_split(my_data, [int(len(my_data) / 2)] * 2)
    # assert list(my_data[:int(len(my_data)/2)]) == list(d1[:])
    first_half = torch.cat(list(my_data[:int(len(my_data) / 2)]))
    assert (first_half == torch.cat(list(d1[:]))).all()


if __name__ == '__main__':
    test_dataloader(transformed_dataset())

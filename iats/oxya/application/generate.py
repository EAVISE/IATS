import collections
import pathlib as pl
import typing as tp

import argparse
import joblib
import matplotlib.pyplot as plt
import pandas as pd
import pandas_ml as pdml

from iats.oxya.attention.load import load_trainer
from iats.oxya.preprocess import OxyaParser
from iats.settings import CONFIG_PATH
from iats.utils.config import load_config


def load_model(path: pl.Path, data: pd.DataFrame, target: str, kind: str = 'torch'):
    """ Load either a PyTorch or Sklearn model.

    Args:
        path: Model storage location.
        data: Data to be loaded (can be `None` for SKlearn models to specify later).
        target: Target variable (can be `None` for SKlearn models to specify later).
        kind: 'torch' or 'sklearn'.

    Returns: Loaded model.

    """
    if kind == 'torch':
        model = load_trainer(path.parent, path, data, target)
    elif kind == 'sklearn':
        model = joblib.load(path)
    return model


def save_model(model: tp.Any, path: pl.Path, kind: str = 'torch', **kwargs):
    """ Save a trained model to a specified location.

    Args:
        model: Model to be saved.
        path: Model storage location.
        kind: 'torch' or 'sklearn'.
        **kwargs: Extra keyword arguments.
    """
    if kind == 'torch':
        epoch = kwargs.get('epoch', 0)
        total_epochs = kwargs.get('total_epochs', 0)
        model.save_state(path, total_epochs, epoch)
    elif kind == 'sklearn':
        joblib.dump(model, path)


def apply_model(df: pdml.ModelFrame, model: tp.Any, kind: str = 'torch', *args, **kwargs) -> pd.DataFrame:
    """ Apply a trained model to data in a ModelFrame.

    Args:
        df: Data (including target) stored in a ModelFrame.
        model: Model to be used.
        kind: 'torch' or 'sklearn'.
        *args: Extra arguments.
        **kwargs: Extra keyword arguments.

    Returns: Dataframe containing target, predictions of the model, and residuals.

    """
    if kind == 'torch':
        predictions = model.predict(on_train=False)
        target = df.target[-len(predictions):]
        predictions = pd.Series(predictions).shift(-2).values
    elif kind == 'sklearn':
        predictions = model.predict(df.data)
        target = df.target
    df_result = pd.DataFrame(collections.OrderedDict(
        [('target', target), ('predicted', predictions), ('residuals', predictions - target)]))
    df_result = df_result.rename_axis('timestamp').sort_index()
    return df_result


def generate_pipeline(data_path: pl.Path, model_path: pl.Path, config: argparse.Namespace):
    """ Generates a pipeline from CSV file to residuals.

    Args:
        data_path: Data storage location.
        model_path: Model storage location.
        config: Configuration Namespace.

    Returns: Dataframe containing target, predictions of the model, and residuals.

    """
    orig_df = pd.read_csv(data_path, header=0, index_col=0, parse_dates=True, low_memory=False)
    parser = OxyaParser.from_pandas(config, orig_df)
    df = parser.transformed
    data = pdml.ModelFrame(df, target=config.run.target)
    model = load_model(model_path, data, config.run.target)
    df_result = apply_model(data, model)
    return df_result


def main():
    config = load_config(CONFIG_PATH)
    result = generate_pipeline(pl.Path("/home/kve/Datasets/oxya/UG1/dataframe.csv"), pl.Path(
        "/home/kve/Documents/iats/iats/oxya/attention/weights/weights_20181122-1747/checkpoint_990.pth.tar"),
                               config)
    result.plot()
    plt.show()


if __name__ == '__main__':
    main()

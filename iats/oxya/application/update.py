import argparse
import pathlib as pl

import joblib
import pandas as pd
import tensorboardX

from iats.oxya.attention import attention, model
from iats.oxya.attention.load import load_trainer
from iats.oxya.attention.model import DARNNTrainer, DualAttentionRNN
from iats.oxya.preprocess import OxyaParser
from iats.settings import CONFIG_PATH, ROOT_DIR
from iats.utils.config import load_config
from iats.utils.utility import setup_log


def retrain_model():
    pass


def load_instance(path):
    try:
        instance = joblib.load(path)
    except FileNotFoundError as e:
        raise FileNotFoundError(f"Couldn't find file at {path}") from e
    if not isinstance(instance, OxyaParser):
        raise TypeError("Unpickled object should be instance of OxyaParser!")
    return instance


def save_instance(instance, path):
    joblib.dump(instance, path)


def update_sklearn_model(args: argparse.Namespace, parser: OxyaParser, new_data: pd.DataFrame):
    new_parser = OxyaParser.from_pandas(args, new_data)
    updated_model = parser.merge(new_parser)
    return updated_model


def update_darnn_model(args: argparse.Namespace, model: DualAttentionRNN, data: pd.DataFrame) -> None:
    model_save_path = ROOT_DIR / "oxya" / "application" / "weights"
    trainer = load_trainer(save_path=args.dataset.path, saved_state=model_save_path, data=data, module=model,
                           target=args.run.target)
    trainer.partial_reset()
    trainer.train(n_epochs=100, save=True)


def train_new_darnn(args: argparse.Namespace, data: pd.DataFrame) -> DARNNTrainer:
    base_path = ROOT_DIR / "oxya" / "application" / "weights"
    model_save_path = attention.make_dir(base_path, "weights")
    trainer = model.DARNNTrainer(data, target=args.run.target, logger=setup_log(), model=None, name="docker_test",
                                 tensorboard_writer=tensorboardX.SummaryWriter(), save_path=model_save_path,
                                 parallel=False, learning_rate=.001, drop_cols=True)
    trainer.train(n_epochs=1000, save=True)


def main():
    config = load_config(CONFIG_PATH)
    data_path = pl.Path("/home/kve/Datasets/oxya/UG1/dataframe.csv")
    orig_df = pd.read_csv(data_path, header=0, index_col=0, parse_dates=True, low_memory=False)
    parser = OxyaParser.from_pandas(config, orig_df)
    df = parser.transformed
    train_new_darnn(config, df)


if __name__ == '__main__':
    main()

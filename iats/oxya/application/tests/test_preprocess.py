import pathlib as pl

import pandas as pd
import pytest

import iats.oxya.application.preprocess as preprocess


@pytest.fixture
def data():
    path = pl.Path.home() / 'Datasets' / 'AirQualityUCI' / 'AirQualityUCI.csv'
    df = pd.read_csv(path, delimiter=';', decimal=',', parse_dates=[[0, 1]])
    df['Date_Time'] = pd.to_datetime(df['Date_Time'], errors='coerce', format='%d/%m/%Y %H.%M.%S')
    df = df.dropna(how='all')
    df.set_index('Date_Time', inplace=True)
    return df


def test_data(data):
    assert not data.empty


def test_expand(data):
    columns = ['CO(GT)', 'C6H6(GT)', 'T']
    aggregates = ['max', 'sum', 'mean']
    new_df = preprocess.expand(data, '3H', columns, aggregates=aggregates)
    assert len(new_df) == len(data)
    assert len(new_df.columns) == len(data.columns) + len(columns) * len(aggregates)


def main():
    test_expand(data())


if __name__ == '__main__':
    main()

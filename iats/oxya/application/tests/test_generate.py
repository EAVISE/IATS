import pathlib as pl

import iats.oxya.application.generate as generate
from iats.settings import CONFIG_PATH
from iats.utils.config import load_config


def test_generate_pipeline():
    path_data = pl.Path("/home/kve/Datasets/oxya/UG1/dataframe.csv")
    path_model = pl.Path(
        "/home/kve/Documents/iats/iats/oxya/attention/weights/weights_20181122-1747/checkpoint_990.pth.tar")
    config = load_config(CONFIG_PATH)
    df_res = generate.generate_pipeline(path_data, path_model, config)
    assert len(df_res) > 0

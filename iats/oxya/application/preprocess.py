import pathlib as pl
import typing as tp

import pandas as pd
import pandas_ml as pdml

import iats.oxya.preprocess as preprocess
from iats.oxya.dataloader import OxyaReader
from iats.settings import CONFIG_PATH
from iats.utils.config import load_config


def load_df(path: pl.Path) -> pd.DataFrame:
    """ Create a processed DataFrame from a CSV file stored at the given location.

    Args:
        path: Data storage location.

    Returns: Processed DataFrame (via OxyaParser).

    """
    reader = OxyaReader(path, force_rebuild=False, write=False)
    df = reader.df
    return df


def prepare_ml(df: pd.DataFrame, target: str, drop: tp.List[str] = None) -> pdml.ModelFrame:
    """ Create a ModelFrame from a DataFrame without certain columns.

    Args:
        df: Input dataFrame.
        target: Target variable.
        drop: Columns to drop.

    Returns: Pruned ModelFrame.

    """
    if drop is None:
        drop = []
    return pdml.ModelFrame(df.drop(columns=drop), target=target)


def expand(df: pd.DataFrame, resample_rate: str, feat_columns: tp.List[str],
           aggregates: tp.List[str]) -> pd.DataFrame:
    """ Expand a DataFrame with extra features in the given columns.

    Args:
        df: Input DataFrame.
        resample_rate: Feature resolution.
        feat_columns: Columns to calculate new features on.
        aggregates: Aggregates to be used in new feature calculation.

    Returns: DataFrame with expanded features as additional columns.

    """
    if not feat_columns:
        return df
    df = preprocess.generate_extra_features(df, resample_rate, feat_columns, aggregates)
    df.fillna(method='ffill', inplace=True)
    return df


def map_features(df: pd.DataFrame) -> pd.DataFrame:
    extracted_features = preprocess.feature_extraction(df)
    return extracted_features.fit_transform(df)


def preprocess_pipeline() -> tp.Any:
    config = load_config(CONFIG_PATH)
    parser = preprocess.OxyaParser(config)
    return parser.transformed


def main():
    print(preprocess_pipeline())


if __name__ == '__main__':
    main()

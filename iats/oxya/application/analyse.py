import pathlib as pl
import typing as tp

import matplotlib.dates as mpd
import matplotlib.pyplot as plt
import pandas as pd

from iats.oxya.application import generate
from iats.settings import CONFIG_PATH
from iats.utils.config import load_config


def generate_trend(sr: pd.Series, alpha: float, beta: float, theta: float, resample_rate: str) -> pd.Series:
    """ Calculate trend line with given parameters. See `residuals` in analysis.py for how it's used.

    Args:
        sr: Time series containing residuals.
        alpha: Parameter 1.
        beta: Parameter 2.
        theta: Parameter 3.
        resample_rate: Alarm line resolution.

    Returns: Fitted trend signal.
    """
    return sr[sr - sr.rolling(alpha).mean() > theta].rolling(beta).mean().dropna().resample(
        resample_rate).mean().interpolate('linear')


def derive_trend(sr: pd.Series, scale: int) -> pd.Series:
    """ Calculate derivative of trend line to make jumps in trend more clear.

    Args:
        sr: Time series containing trend signal.
        scale: Scale factor.

    Returns: Derived trend signal that can serve as an alarm.

    """
    return scale * sr.diff() / (sr.index.to_series().diff().dt.total_seconds() / 60)


def threshold(sr: pd.Series, thres: tp.Union[int, float]) -> pd.Series:
    """ Returns values of trend that are above a certain threshold.

    Args:
        sr: Derived trendline.
        thres: Threshold value.

    Returns: Boolean series containing alarm signals that suprass the threshold.

    """
    return sr > thres


def prepare_output(df: pd.DataFrame, *args, **kwargs):
    # variables
    alpha = kwargs.get("alpha", 10)
    beta = kwargs.get("beta", 5)
    theta = kwargs.get("theta", 1)
    resample_rate = kwargs.get("resample_rate", "T")
    scale = kwargs.get("scale", 40)

    # df['predicted'] = df['predicted'].shift(-2)
    # df['residuals'] = df['predicted'] - df['target']

    fig, ax = plt.subplots()  # type: plt.Figure, plt.Axes
    time_axis = mpd.date2num(pd.Series(df.index).dt.to_pydatetime())
    ax.plot(time_axis, df['predicted'], label='predicted')
    ax.plot(time_axis, df['target'], label='target')
    ax.plot(time_axis, df['residuals'], label='residuals')
    ax.plot(time_axis, df['residuals'].rolling(alpha).mean(), label='rolling mean')

    trend = generate_trend(df['residuals'], alpha=alpha, beta=beta, theta=theta, resample_rate=resample_rate)
    ax.plot(trend, label='trend')

    derivative_trend_pos = derive_trend(trend, scale=scale).abs()
    ax.plot(derivative_trend_pos, label='derivative')

    ax.xaxis_date()
    ax.legend()
    fig.autofmt_xdate()

    return df


def main():
    config = load_config(CONFIG_PATH)
    result = generate.generate_pipeline(pl.Path("/home/kve/Datasets/oxya/UG1/dataframe.csv"), pl.Path(
        "/home/kve/Documents/iats/iats/oxya/attention/weights/weights_20181122-1747/checkpoint_990.pth.tar"),
                                        config)
    prepare_output(result)
    plt.show()


if __name__ == '__main__':
    main()

import csv
import json
import pathlib as pl
import elasticsearch as es
import elasticsearch.helpers
import iats.utils.load as load


def setup() -> es.Elasticsearch:
    connection = es.Elasticsearch([{'host': 'localhost', 'port': 9200}])
    print(connection)
    return connection


def convert_to_json(data):
    try:
        return data.to_json()
    except AttributeError:
        try:
            import json
            return json.load(data)
        except AttributeError:
            print("Couldn't parse to json.")
            raise


@load.timing
def csv_reader(file_obj, delimiter=','):
    connection = setup()
    reader = csv.DictReader(file_obj)
    i = 1
    results = []
    for row in reader:
        print(row)
        connection.index(index='transaction', doc_type='type', id=i,
                         body=json.dumps(row))
        i = i + 1

        results.append(row)
        print(row)


@load.timing
def bulk_reader(file_obj):
    connection = setup()
    reader = csv.DictReader(file_obj)
    es.helpers.bulk(connection, reader, index="transaction", doc_type="type")


def main():
    path = pl.Path("~", "Datasets", "oxya", "UG1")
    with open((path / "dataframe.csv").expanduser(), "r") as f:
        bulk_reader(f)


if __name__ == '__main__':
    main()

import argparse
import copy
import pathlib as pl
import typing as tp
from collections import OrderedDict
from pprint import pprint

import numpy as np
import pandas as pd
import pandas_ml as pdml
import sklearn.impute as ski
import sklearn.preprocessing as skp
import sklearn_pandas as skpd
from sklearn import base as skb

from iats.oxya.dataloader import OxyaReader


class OxyaParser:
    def __init__(self, args: argparse.Namespace, data: pd.DataFrame = None) -> None:
        self.args = args
        self.run_args = args.run.run
        self.target = args.run.target
        self.random_state = int(args.run.rng)
        self.sample_fraction = 1 / args.resample.sample_factor
        self.path = args.dataset.path
        self.reader = None
        if data is None:
            self.reader = OxyaReader(self.path, force_rebuild=args.dataset.rebuild, begin=args.dataset.begin,
                                     end=args.dataset.end)
            self.data = self.reader.df
        else:
            self.data = data
        self.dialog_transactions = None
        self.df = None
        self.resampled = None
        self.mapper = None
        self.transformed = None
        self.setup()

    @classmethod
    def from_pandas(cls, args: argparse.Namespace, data: pd.DataFrame) -> "OxyaParser":
        print("Reading dataframe...")
        print(f"Length of data: {len(data)} rows.")
        pprint(f"Columns: {list(data.columns)}")
        return cls(args, data)

    @classmethod
    def from_csv(cls, args: argparse.Namespace, path: tp.Union[str, pl.Path]) -> "OxyaParser":
        print(f"Reading CSV file at {path}...")
        data = pd.read_csv(path, index_col='_source.@timestamp', parse_dates=True)
        print(data.index.dtype)
        print(f"Length of data: {len(data)} rows.")
        pprint(f"Columns: {list(data.columns)}")
        return cls(args, data)

    def setup(self, resample_cols=None):
        self.dialog_transactions = self.data
        if self.args.features.type_col and self.args.features.type:
            self.data = self.data.loc[self.data[self.args.features.type_col] == self.args.features.type]
        self.df = self.dialog_transactions.sample(frac=self.sample_fraction,
                                                  random_state=self.random_state).sort_index()
        # range_idx = np.random.choice(len(self.data.index), int(self.sample_fraction * len(self.data.index)))
        # self.df = self.data.iloc[range_idx].sort_index()
        cols = list(self.df)
        cols.insert(0, cols.pop(cols.index(self.target)))
        self.df = self.df.loc[:, cols]
        if self.args.run.expand:
            extras = generate_extra_features(self.df, self.args.resample.resample, self.args.features.feature_columns,
                                             self.args.features.aggregates)
            self.df = extras
        if self.args.resample.resample:
            # columns
            cat_cols = self.df.select_dtypes(include=['category', 'object', np.datetime64]).columns
            numerical_cols = self.df.drop(columns=cat_cols).columns
            # dictionaries
            numerical_dict = OrderedDict(zip(self.df.drop(columns=cat_cols), ['median'] * len(self.df)))
            if self.args.resample.resample_cats:
                categorical_dict = OrderedDict(zip(self.df[cat_cols], [_count_categoricals] * len(self.df)))
            else:
                categorical_dict = {}
            # resampling
            if not self.args.resample.dask:
                try:
                    self.resampled = self.df.set_index(self.args.features.time)
                except KeyError:
                    self.resampled = self.df
                self.resampled = self.resampled.resample(self.args.resample.resample) \
                    .agg(OrderedDict(**numerical_dict, **categorical_dict))
                pass
            else:
                import dask.dataframe as dd
                import multiprocessing as mp
                from dask.multiprocessing import get
                partitions = mp.cpu_count() * 2
                ddata = dd.from_pandas(self.df, npartitions=partitions)
                self.resampled = ddata.resample(self.args.resample.resample).agg(
                    OrderedDict(**numerical_dict, **categorical_dict)).compute(get=get, num_workers=partitions)
            # interpolation
            for col in list(self.resampled[numerical_cols]):
                try:
                    self.resampled[col] = self.resampled[col]. \
                        transform(lambda x: x.interpolate('linear'))
                except ValueError:
                    pass
            self.df = self.resampled
            self.mapper = self._feature_extraction(self.df)
            self.transformed = self._map_features(self.df)
            if self.args.run.dropna:
                try:
                    self.transformed = self.transformed[self.transformed[f"('{self.target}', 'nan')"] != 0]
                except KeyError:
                    self.transformed = self.transformed[self.transformed[self.target] != 0]

    def merge(self, parser: "OxyaParser") -> "OxyaParser":
        new_parser = copy.copy(self)
        new_parser.dialogue_transactions = new_parser.dialogue_transactions.append(parser.dialog_transactions,
                                                                                   sort=True).iloc[
                                           len(parser.dialog_transactions), :]
        new_parser.df = new_parser.df.append(parser.df, sort=True).iloc[len(parser.df), :]
        new_parser.transformed = new_parser.transformed.append(parser.transformed, sort=True).iloc[len(parser.df), :]
        return new_parser

    def _map_features(self, df: pd.DataFrame) -> pd.DataFrame:
        return self._feature_extraction(df).fit_transform(df)

    @staticmethod
    def _feature_extraction(df: pd.DataFrame) -> skpd.DataFrameMapper:
        return feature_extraction(df)


def feature_extraction(df: pd.DataFrame, extract_cats: bool = False) -> skpd.DataFrameMapper:
    """Adds several imputers, scalers and binarizers to a DataFrame preprocessing pipeline.

    Args:
        df: Input DataFrame.
        extract_cats: If true, imputes and binarizes categorical variables. Strongly recommended to not use this
        if one or more categorical columns contain many distinct values.

    Returns:
        DataFrameMapper instance.
    """
    feature_transforms = []
    for num in list(df.select_dtypes(include=[np.float64, np.int64])):
        feature_transforms.append(([num], [ski.SimpleImputer(), skp.RobustScaler()]))
    if extract_cats:
        for cat in df.select_dtypes(include=[pd.Categorical]):
            feature_transforms.append((cat, [skpd.CategoricalImputer(), skp.LabelBinarizer()]))
    mapper = skpd.DataFrameMapper(feature_transforms, df_out=True)
    return mapper


def _interpolate_numericals(x: pd.Series, kind: str = 'linear'):
    return x.interpolate(kind)


def _count_categoricals(x: pd.Series) -> int:
    """
    Count the occurrences for each categorical value in a series.

    Args:
        x: Input Series.

    Returns:
        Counted values.
    """
    return x.value_counts().sample(frac=1, random_state=0).idxmax()


def create_new_resampled_cols(data: pd.DataFrame, resample_rate: str, columns: tp.List[str],
                              aggregators: tp.List[str]) -> pd.DataFrame:
    """
    Resample certain columns of a DataFrame at a given rate, and apply some aggregators.

    Args:
        data: Input DataFrame.
        resample_rate: Resample rate.
        columns: Columns to apply resampling and aggregation on.
        aggregators: List of strings denoting the aggregators to apply.

    Returns:
        Output DataFrame.
    """
    return data[columns].resample(resample_rate).agg(aggregators)


def generate_extra_features(data: pd.DataFrame, resample_rate: str, columns: tp.List[str],
                            aggregators: tp.List[str]) -> pd.DataFrame:
    """
    Creates extra aggregated features for certain columns in the given dataframe and constructs a new DataFrame
    from them.

    .. warning::
       This function consumes a lot of memory!

    Args:
        data: Input DataFrame.
        resample_rate: Resampling rate.
        columns: Columns to apply resampling and aggregation on.
        aggregators: List of strings denoting the aggregators to apply.

    Returns:
        Output DataFrame.
    """
    extra_cols = create_new_resampled_cols(data, resample_rate, columns, aggregators)
    joined = data.merge(extra_cols, how='outer', left_index=True, right_index=True)
    joined.columns = pd.MultiIndex.from_tuples(
        [(str(x), "nan") if not isinstance(x, tuple) else tuple(map(str, x)) for x in joined.columns.values])
    # joined.loc[:, columns] = joined[columns].resample(resample_rate).mean().dropna()
    joined_cols = joined.loc[:, columns].drop('nan', axis=1, level=1)
    # TODO: doublecheck this
    joined.loc[:, joined_cols.columns.tolist()] = joined_cols.resample(
        resample_rate).mean().reindex(joined.index.drop_duplicates()).dropna()
    joined.loc[:, joined_cols.columns.tolist()] = joined_cols.fillna(method='bfill')
    return joined


def pdml_preparation(df: pd.DataFrame, target: str, cols_to_drop: tp.Optional[tp.List[str]]) -> pdml.ModelFrame:
    """
    Filters out some columns from a ModelFrame and creates a featuremap as side-effect. If no column names are given,
    falls back to a preset drop tactic (only valid for Oxya data).

    Args:
        df: Input ModelFrame.
        target: Target column.
        cols_to_drop: Columns to drop from df.

    Returns:
        Pruned ModelFrame.
    """
    if target not in df.columns:
        target = str((target, 'nan'))
    df = pdml.ModelFrame(df, target=target)
    if cols_to_drop is None:
        cols_to_drop = ['Time in WPs (ms)', 'Roll out time (ms)', 'Generating time  (ms)', 'CPU time (ms)',
                        'Roll in time (ms)', 'Load time (ms)', 'Wait time (ms)', 'Processing time (ms)', 'Roll outs',
                        'Wp', 'Load + Gen time (ms)', 'Phys. DB changes', 'Roll (i+w) time (ms)', 'DB req. time (ms)']
    df.drop(columns=cols_to_drop, errors='ignore', inplace=True)
    # feat_names = np.delete(df.columns.values, np.argwhere(df.columns.values == target))
    feat_names = df.data.columns.values
    create_feature_map(feat_names)
    return df


def feature_importances(instance: skb.BaseEstimator) -> tp.Optional[np.ndarray]:
    """
    Calculates importance scores for all features in an estimator.

    Args:
        instance: Estimator.

    Returns:
        Array with feature importance scores, stored in the same order as the features in the given estimator.
    """
    try:
        b = instance._Booster
        fs = b.get_fscore()
    except AttributeError:
        return None
    all_features = [fs.get(f, 0.) for f in b.feature_names]
    all_features = np.array(all_features, dtype=np.float32)
    return all_features / all_features.sum()


def create_feature_map(features: np.ndarray) -> None:
    """
    Dumps feature info into a "fmap.csv" file. Useful for some Scikit-Learn functionalities.

    Args:
        features: List of features to be written.
    """
    with open('fmap.csv', 'w') as f:
        for i, feat in enumerate(features):
            f.write(f'{i}\t{feat}\tq\n')


def main():
    from iats.utils.config import production_oxya_args
    from iats.oxya.attention.dataloader import Dataset
    dataset = Dataset.from_giga_csv(pl.Path("../oxya").resolve(), pd.Timestamp("2019-04-24"),
                                    pd.Timestamp("2019-04-30"),
                                    target='_source.respti', debug=False,
                                    drop_cols=None, transform=None, window=20,
                                    train_size=0.7)
    parser = OxyaParser(production_oxya_args(), dataset.data)


if __name__ == '__main__':
    main()

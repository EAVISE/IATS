import argparse
import functools as ft
import pathlib as pl
import typing as tp

import dask.dataframe as dd
import dask.multiprocessing as dmp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sklearn
import sklearn.ensemble
import sklearn.metrics
import sklearn.model_selection
import sklearn.utils
from scipy.optimize import curve_fit
from sklearn.base import BaseEstimator
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from tqdm import tqdm

import iats.utils.load as load
from iats.enervalis.dataloader import init_prophet_df
from iats.oxya.analysis import OxyaAnalyser


def build_oxya_args() -> argparse.Namespace:
    """
    Sets the arguments for building an analyser object.

    Returns:
        Collection of run arguments.
    """
    args = argparse.Namespace()
    args.path = pl.Path.home() / 'Datasets' / 'oxya'
    args.rebuild = False
    args.rng = 0
    args.sample_factor = 50
    args.run = 'explore'
    args.resample = 'S'
    args.resample_cats = False
    args.dask = True
    args.expand = False
    return args


def static_polynomial(x: np.ndarray, a: float, b: float, c: float, d: float) -> np.ndarray:
    """
    Generates a 3rd-degree polynomial.

    Args:
        x: Array representing the domain.
        a: Coefficient of the 3rd degree term.
        b: Coefficient of the 2nd degree term.
        c: Coefficient of the 1st degree term.
        d: Constant.

    Returns:
        x transformed by the 3rd degree polynomial function.
    """
    return a * x * x * x + b * x * x + c * x + d


def generate_polynomial(degree: int) -> tp.Callable:
    """
    Generates a polynomial function of the n-th degree.

    Args:
        degree: n; degree of the polynomial.

    Returns:
        Polynomial function.
    """

    def polynomial(x, *params):
        terms = 0
        for i in range(degree, -1, -1):
            terms += params[degree - i] * np.power(x, i)
        return terms

    return polynomial


def test_curve_fit(fitter: tp.Callable, window: int, data: pd.DataFrame) -> None:
    """
    Fits a polynomial onto an array using SciPy's "curve_fit" function.

    Args:
        fitter: Polynomial function to be used.
        window: Length of the data to be fit.
        data: Data to be fit.
    """
    for ind in range(0, len(data), window):
        exam = data.loc[ind:ind + window, 'y']
        popt, pcov = curve_fit(fitter, np.arange(window // 2), exam.iloc[:window // 2], maxfev=2 * 10 ** 5)
        print(ind // window, popt)


def get_model(regressor: BaseEstimator, data: tp.Union[pd.DataFrame, pd.Series], degree: int = 3,
              start: int = 3000, train_window: int = 15) -> Pipeline:
    """
    Creates a pipeline consisting of a polynomial feature generator and a regressor to achieve polynomial fitting.

    Args:
        regressor: Instance of regressor algorithm.
        data: Data to be fit.
        degree: Max. degree of the polynomial features.
        start: Starting index.
        train_window: Length of the data to be fit.

    Returns:
        Fitted pipeline.
    """
    if type(data) is pd.DataFrame:
        data = data['y']
    model = Pipeline([('poly', PolynomialFeatures(degree)), ('regr', regressor)])
    model.fit(np.arange(train_window).reshape(-1, 1), data.loc[start:start + train_window - 1])
    return model


def fitted_model(*args: tp.Any, fit_window: int = 10, **kwargs: tp.Any) -> np.ndarray:
    """
    Wrapper function to fit either an existing or a new pipeline.

    Args:
        *args: Positional arguments to be passed to the model constructor.
        fit_window: Length of the data to be fitted.
        **kwargs: Keyword arguments to be passed to the model constructor.

    Returns:
        Fitted pipeline.
    """
    try:
        sklearn.utils.validation.check_is_fitted(args[0].named_steps['regr'], 'coef_')
        return args[0].predict(np.arange(fit_window).reshape(-1, 1))
    except AttributeError:
        return get_model(*args, **kwargs).predict(np.arange(fit_window).reshape(-1, 1))


def rolling_dataframe(data: pd.Series, window: int) -> pd.DataFrame:
    """
    Rolls out a pandas Series using NumPy kernel tricks.
    See: https://stackoverflow.com/a/47483671

    Args:
        data: Series data.
        window: Rolling window size.

    Returns:
        Unrolled series.
    """
    v = np.lib.stride_tricks.as_strided(data.values, (len(data) - (window - 1), window), (data.values.strides * 2))
    return pd.DataFrame(v, index=data.index[:-(window - 1)])


def poly_fit(values: np.ndarray, model: tp.Union[BaseEstimator, Pipeline]) -> np.ndarray:
    """
    Apply-function to fit a model on rows of a DataFrame.

    Args:
        values: Values of the DataFrame-row.
        model: Model to be fitted on the points in the row.

    Returns:
        Coefficients of the fitted model.
    """
    model.fit(np.arange(len(values)).reshape(-1, 1), values)
    return model.named_steps['regr'].coef_


def generate_coefficients(model: tp.Union[BaseEstimator, Pipeline], rolling_df: pd.DataFrame) -> pd.DataFrame:
    """
    Fits polynomial models on an entire DataFrame at once using Dask multiprocessing.

    Args:
        model: Pipeline to be fitted.
        rolling_df: Data.

    Returns:
        Dataframe containing the coefficients of all the fitted models.
    """
    apply_func = ft.partial(poly_fit, model=model)

    def apply_func_to_df(data): return data.apply(apply_func, axis=1)

    roll_ddf = dd.from_pandas(rolling_df, npartitions=30)
    dask_result = roll_ddf.map_partitions(apply_func_to_df).compute(get=dmp.get)
    cols = list(range(model.named_steps['poly'].get_params()['degree'] + 1))

    return pd.DataFrame(dask_result.values.tolist(), columns=cols, index=rolling_df.index)


def roll_coefficients(coefficients: pd.DataFrame, window: int, min_periods: int = 1) -> pd.DataFrame:
    """
    Calculates the rolling mean of entries in a DataFrame while reducing amount of NaN results (wrapper function).

    Args:
        coefficients: Data.
        window: Rolling window size.
        min_periods: Minimum amount of data points in a window (default 1 to avoid NaNs).

    Returns:
        Rolled DataFrame.
    """
    return coefficients.rolling(window, min_periods=min_periods).mean()


def evaluate_datapoint(data: pd.Series, idx: int, train_window: int, test_window: int, coefficients: pd.DataFrame,
                       regressor: BaseEstimator = Ridge(), plot: bool = True) -> float:
    """
    Apply curve fitting on a data point in a time series.

    Args:
        data: Time series data.
        idx: Index of the data point.
        train_window: Length of the fitted interval.
        test_window: Length of the evaluated interval.
        coefficients: DataFrame with fitted coefficients
            (must be calculated with the same window size as ``train_window``).
        regressor: Instance of the algorithm used to generate coefficients.
        plot: Plot the results in a graph. (WARNING: disable when using this function in a loop!)

    Returns:
        Mean Squared Error of the fitted function on the test interval.
    """
    coefs = coefficients.loc[idx]
    values = data.loc[idx:idx + train_window + test_window - 1]  # real
    model = get_model(regressor, data)
    # hack the parameters
    model.named_steps['regr'].coefs_ = coefs.values
    model.named_steps['regr'].intercept_ = coefs[0]
    # prediction step
    predicted = model.predict(np.arange(len(values)).reshape(-1, 1))
    difference = predicted - values
    if plot:
        fig, ax = plt.subplots(2)
        ax[0].plot(np.arange(len(values)), predicted, label='predicted')  # predicted
        ax[0].plot(np.arange(len(values)), values, label='real')  # real
        ax[1].scatter(np.arange(len(values)), difference, label='residuals')  # residuals
        for figax in fig.get_axes():
            figax.legend(loc='lower left')
    mse = sklearn.metrics.mean_squared_error(values[train_window:], predicted[train_window:])
    return mse


@load.timing
def evaluate_timeseries(data: pd.Series, **params: tp.Any) -> pd.Series:
    """
    Runs curve fitting experiment on an entire time series.

    Args:
        data: Data to be analysed.
        **params:
            - degree
            - train_widow
            - test_window
            - regr

    Returns:
        Series containing the results of the evaluation on every index of the dataset.
    """
    test_roll_df = rolling_dataframe(data, params['train_window'])
    test_model = Pipeline([('poly', PolynomialFeatures(params['degree'])), ('regr', params['regr'])])
    test_coefficients = generate_coefficients(test_model, test_roll_df)
    test_roll_coefficients = test_coefficients.rolling(params['train_window'], min_periods=1).mean()

    res_series = pd.Series(index=test_roll_coefficients.index, name='mse')
    for index in tqdm(test_roll_coefficients.index, desc='Iterating over datapoints'):
        try:
            res = evaluate_datapoint(data, index, params['train_window'], params['test_window'],
                                     coefficients=test_roll_coefficients, plot=False)
            res_series[index] = res
        except ValueError as exc:
            print(index, exc)
    return res_series


def normalize(x: pd.Series) -> pd.Series:
    """
    Normalize a Series using min-max scaling.

    Args:
        x: Data.

    Returns:
        Normalized data.
    """
    return (x - x.mean()) / (x.max() - x.min())


def main():
    arguments = build_oxya_args()
    obj = OxyaAnalyser(arguments)
    df = obj.run('residuals')
    pdf = init_prophet_df(df, 'difference')
    plt.figure()
    parameters = {'train_window': 60, 'test_window': 20, 'degree': 9, 'regr': Ridge()}
    result = evaluate_timeseries(pdf['y'], **parameters)
    res_series = pd.Series(result.values, index=pdf['ds'].iloc[:-(parameters['train_window'] - 1)].values)
    res_series.iloc[:-1000].plot(label='MSE', legend=True)
    trend = result[result - result.rolling(5).mean() > 5].rolling(10).mean().dropna()
    derivative_trend = trend.diff() / (trend.index.to_series().diff())
    plt.figure()
    plt.plot(trend, label='trend')
    plt.plot(derivative_trend.abs(), label='derivative_trend')
    res_series.reset_index(drop=True).plot(label='MSE')
    plt.legend()
    plt.figure()
    pdf.set_index('ds', drop=True)['y'].abs().plot(label='abs', legend=True)
    plt.show()


if __name__ == '__main__':
    main()

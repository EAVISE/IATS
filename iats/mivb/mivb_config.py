import os


class MIVBConfig:
    HOME = os.path.expanduser('~')
    MIVB = os.path.join(HOME, "Work", "Projects", "IATS", "Data", "MIVB")
    ESCALATORS = os.path.join(MIVB, "Escalators")
    GATES = os.path.join(MIVB, "Gates")

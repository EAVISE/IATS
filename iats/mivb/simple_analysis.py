import datetime
from collections import Counter, defaultdict

import dill
import numpy as np
import os
import pandas as pd
from matplotlib.lines import Line2D

from iats.mivb.mivb_config import MIVBConfig

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt

escalators = pd.read_csv(os.path.join(MIVBConfig.ESCALATORS, "escalateurs_downtime_2019-03_clean.csv"))
escalators = escalators[escalators["ZSESTAT"] == 503]
escalators['START_DTM'] = escalators.apply(lambda x: datetime.datetime(
    year=int(x['ZASESDST'][-4:]), month=int(x['ZASESDST'][3:5]), day=int(x['ZASESDST'][0:2]),
    hour=int(x['ZASESTM'][0:2]), minute=int(x['ZASESTM'][3:5]), second=int(x['ZASESTM'][6:8])), axis=1)
srtd_esc = escalators.sort_values(by=['START_DTM'])

gates_file = os.path.join(MIVBConfig.GATES, 'gates_selection.pkl')
if not os.path.isfile(gates_file):
    gates = pd.read_excel(os.path.join(MIVBConfig.GATES, "2019-03.XLSX"), dtype={'Sesame: start of 5min period': 'str'})
    # Some of the values contained in the dataset ar
    gates.sort_values(by=['Sesame: start of 5min period', 'Sesame MD - NRel', 'Entries', 'ZSEOEX'],
                      ascending=[True, True, False, False], inplace=True)
    gates.reset_index(drop=True, inplace=True)

    bools = []
    prev = -1
    for idx, row in gates.iterrows():
        nrel = row['Sesame MD - NRel']
        bools.append(not(nrel == prev))
        prev = nrel

    gates = gates[bools]
    gates['Datetime'] = gates.apply(lambda x: datetime.datetime(
        year=int(x['Sesame: start of 5min period'][0:4]), month=int(x['Sesame: start of 5min period'][4:6]),
        day=int(x['Sesame: start of 5min period'][6:8]), hour=int(x['Sesame: start of 5min period'][8:10]),
        minute=int(x['Sesame: start of 5min period'][10:12]), second=int(x['Sesame: start of 5min period'][12:14])), axis=1)

    dill.dump(gates, open(gates_file, 'wb'))
else:
    gates = dill.load(open(gates_file, 'rb'))

# Compute sums of entries and exits over all gates per timestamp
dtimes = []
sum_entries = []
sum_exits = []
sum_tailgate = []
sum_illegal_entries = []

prev_nb = 8
temp_entries, temp_exits, temp_illegal_entries = 0, 0, 0
for idx, row in gates.iterrows():
    print(f"\rAt row {idx}", end='', flush=True)
    nb = row['Sesame MD - NRel']
    # Check data consistency
    if not (prev_nb == 8 and nb == 1) and not (prev_nb == 6 and nb == 8) and not (nb == prev_nb + 1):
        raise ValueError(f'Data appears to be inconsistent at index {idx}...')

    # Upate sums
    temp_entries += row['Entries Without Valid.'] + row['Entries']
    temp_exits += row['Exit Wthout Valid.'] + row['Mobib Exit'] + row['ProData Exit']
    temp_illegal_entries += row['Opposite Entries'] + row['Tailgate Entries']

    # Update containers and reset sums
    if nb == 8:
        # dtimes.append(row[])
        dtimes.append(row['Datetime'])
        sum_entries.append(temp_entries)
        sum_exits.append(temp_exits)
        sum_illegal_entries.append(temp_illegal_entries)
        temp_entries, temp_exits, temp_illegal_entries = 0, 0, 0

    # Update counter
    prev_nb = nb
print()

# Get unique types of defects
err_types = srtd_esc['ZASESST1'].unique()
# In this case, there are 3 possible error types
err_colors = ['cyan', 'magenta', 'red']

# Get some statistics regarding distribution of failures per day and hour of day
per_hour = defaultdict(Counter)
per_day = defaultdict(Counter)
for idx, row in srtd_esc.iterrows():
    # print(row)
    # print("hihi")
    err_type = row['ZASESST1']
    per_hour[err_type][row['START_DTM'].hour] += 1
    per_day[err_type][row['START_DTM'].weekday()] += 1


fig = plt.figure()
nb_plots = len(per_day)
at_plot = 0
for k, v in per_day.items():
    at_plot += 1
    # print(f"Error type: {k}")
    # print(v)
    # print("============================================================")
    xs = sorted(v.keys())
    xs = [x+1 for x in xs]
    ys = [v[x] for x in xs]
    ax = plt.subplot(nb_plots, 1, at_plot)
    ax.set_title(k)
    ax.set_xlim([0, 8])
    plt.bar(xs, ys)
plt.tight_layout()
plt.show()

fig = plt.figure()
nb_plots = len(per_hour)
at_plot = 0
for k, v in per_hour.items():
    at_plot += 1
    # print(f"Error type: {k}")
    # print(v)
    # print("============================================================")
    xs = sorted(v.keys())
    xs = [x+1 for x in xs]
    ys = [v[x] for x in xs]
    ax = plt.subplot(nb_plots, 1, at_plot)
    ax.set_title(k)
    ax.set_xlim([0, 25])
    plt.bar(xs, ys)
plt.tight_layout()
plt.show()

# print(per_hour)
# print(per_day)

# See: https://stackoverflow.com/questions/42734109/two-or-more-graphs-in-one-plot-with-different-x-axis-and-y-axis-scales-in-pyth
fig = plt.figure()
ax1 = fig.add_subplot(111, label="1")
ax2 = fig.add_subplot(111, label="2", frame_on=False, sharex=ax1, sharey=ax1)
# ax3 = fig.add_subplot(111, label="3", frame_on=False)

ax1.plot_date(dtimes, sum_entries, '.b')
ax1.set_ylim([0, max([max(sum_entries), max(sum_exits)]) + 1])

ax2.plot_date(dtimes, sum_exits, '.g')

# ax3.plot(sum_illegal_entries, '.r')

lbl_start = 4
legend_lines = []
legend_names = []
for i, err in enumerate(err_types):
    ax = fig.add_subplot(111, label=f"{lbl_start + i}", frame_on=False, sharex=ax1)
    ax.plot_date(srtd_esc[srtd_esc['ZASESST1'] == err]['START_DTM'],
                 (0.2*i) + np.ones(srtd_esc[srtd_esc['ZASESST1'] == err].shape[0]), marker='x', color=err_colors[i], xdate=True)
    ax.xaxis.tick_top()
    ax.tick_params(axis='x', colors='r')
    ax.yaxis.tick_right()
    ax.tick_params(axis='y', colors="r")
    ax.set_yticks([1 + (0.2*i)])
    ax.set_ylim([0, 2])

    legend_lines.append(Line2D(range(1), range(1), color="white", marker='o', markerfacecolor=err_colors[i]))
    legend_names.append(err)

line1 = Line2D(range(1), range(1), color="white", marker='o', markerfacecolor="blue")
line2 = Line2D(range(1), range(1), color="white", marker='o', markerfacecolor="green")
# line3 = Line2D(range(1), range(1), color="white", marker='o',markersize=5, markerfacecolor="slategray")
# line4 = Line2D(range(1), range(1), color="white", marker='o', markerfacecolor="red")
plt.legend([line1, line2] + legend_lines, ['Entries', 'Exits'] + legend_names, numpoints=1, loc=1)

plt.show()

print("hihi!")

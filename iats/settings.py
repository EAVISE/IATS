import os
import pathlib as pl

ROOT_DIR = pl.Path(os.path.dirname(os.path.abspath(__file__)))
CONFIG_PATH = ROOT_DIR / 'config.ini'
DOCKER_NAME = "kve/iats:root"

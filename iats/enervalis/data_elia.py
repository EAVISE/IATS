import datetime
import os

import pandas as pd


# Practical page to remember how to index columns/rows/cells in pandas:
# https://pythonhow.com/accessing-dataframe-columns-rows-and-cells/
class DataElia:
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Projects", "IATS", "Data", "Elia Imbalance")

    # We are gonna load the CSV, and split the timestamps into several columns for year, month, day and time
    @staticmethod
    def load_data(base_dir=BASE_DIR, b_silent=False):
        # If converted file does not exist yet, the create it...
        if not os.path.exists(os.path.join(base_dir, "dataframe_reworked.pkl")):
            if not b_silent:
                print("Converting CSV file: reworking timestamps...")
            # Time format: 2015-03-01 03:45:00+01:00
            # We remove the "+01:00" part
            data = pd.read_csv(os.path.join(base_dir, "dataframe.csv"), parse_dates=[0],
                               date_parser=lambda x: pd.datetime.strptime(x[:-6], '%Y-%m-%d %H:%M:%S'))
            # Add +1h to all timestamps; we are at a +1 timezone, and we have removed this +1, so all
            # datetimes are offset by on 1 hour "into the past"
            data.iloc[:, 0] = data.iloc[:, 0] + datetime.timedelta(hours=1)
            # Add columns for year, month, day and time
            # Taken from https://stackoverflow.com/questions/30026815/add-multiple-columns-to-pandas-dataframe-from-function
            lambdafunc = lambda x: pd.Series([x[0].year,
                                              x[0].month,
                                              x[0].day,
                                              x[0].time()])
            data[["Year", "Month", "Day", "Time"]] = data.apply(lambdafunc, axis=1)
            # Drop original column
            data.drop(columns=["Unnamed: 0"], inplace=True)
            # Rearrange columns to put the added columns to the front
            cols = data.columns.tolist()
            cols = cols[-4:] + cols[:-4]
            data = data[cols]
            data.to_pickle(os.path.join(base_dir, "dataframe_reworked.pkl"))
        # ...else, load it.
        else:
            if not b_silent:
                print("Loading pickle...")
            data = pd.read_pickle(os.path.join(base_dir, "dataframe_reworked.pkl"))

        return data

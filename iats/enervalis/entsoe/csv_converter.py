# This class reads all CSVs collected from ENTSO-E (https://transparency.entsoe.eu/),
# and converts them to Pandas DataFrames, which are then saved to disk using dill.
import multiprocessing as mp

import dill
import pandas as pd
import os

from iats.enervalis.entsoe.entsoe_config import ENTSOEConfig


class CSVConverter(ENTSOEConfig):
    @classmethod
    def process_dir(cls, base_dir=ENTSOEConfig.BASE_DIR, out_dir=ENTSOEConfig.OUT_DIR):
        files = sorted([f for f in os.listdir(base_dir) if f.endswith(".csv")])
        # The files end with "_201501010000-201601010000.csv", or analogous with different years. Use this to
        # extract all "types" of data, defined by what comes before this ending part in the filename(s).
        uq_names = {}
        for f in files:
            k = f[:-30].replace(' _ ', '_').replace(' ', '_')
            if k not in uq_names:
                uq_names[k] = []
            uq_names[k].append(os.path.join(base_dir, f))

        for k, v in uq_names.items():
            print("Processing files for '{}'...".format(k))
            with mp.Pool(min(len(v), mp.cpu_count())) as pool:
                # Needs to be done using this cls._read_csv function, otherwise will raise pickling errors.
                results = pool.map(cls._read_csv, v)
            df = pd.concat(results).reset_index(drop=True)
            # Depending on the type of data, the timestamp column has a different name.
            col_time = ""
            idx_time = -1
            for s in ["Time (CET)", "MTU (CET)", "MTU"]:
                if s in df.columns:
                    col_time = s
                    idx_time = list(df.columns).index(col_time)
                    break
            if not col_time:
                raise ValueError("No valid/known timestamp column found.")

            # Example timestamp: "01.01.2015 00:00 - 01.01.2015 01:00"
            # First, convert strings to datetime objects
            df[col_time] = df[col_time].apply(lambda x: pd.to_datetime(x[:16], format='%d.%m.%Y %H:%M'))
            # Second, split datetime objects into year/month/day/time

            def lambdafunc(x): pd.Series([x[idx_time].year,
                                          x[idx_time].month,
                                          x[idx_time].day,
                                          x[idx_time].time()])
            df[["Year", "Month", "Day", "Time"]] = df.apply(lambdafunc, axis=1)
            # Drop original column
            df.drop(columns=[col_time], inplace=True)
            # Rearrange columns to put the added columns to the front
            cols = df.columns.tolist()
            cols = cols[-4:] + cols[:-4]
            df = df[cols]
            # Save dataframe
            dill.dump(df, open(os.path.join(out_dir, '{}.dill'.format(k)), 'wb'))

    @classmethod
    def _read_csv(cls, f):
        return pd.read_csv(f)


if __name__ == '__main__':
    CSVConverter.process_dir()

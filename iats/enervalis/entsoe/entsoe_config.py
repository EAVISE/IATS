import os


class ENTSOEConfig:
    HOME = os.path.expanduser("~")
    # The directory containing the CSVs. It is assumed that all CSV files are located directly in this folder,
    # not within subfolders of this folder.
    BASE_DIR = os.path.join(HOME, "Work", "Data", "ENTSO-E")
    # Where to write the pickled Pandas DataFrames to.
    OUT_DIR = BASE_DIR

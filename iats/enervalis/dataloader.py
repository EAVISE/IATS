import datetime
import os
import pathlib as pl
import typing as tp

import fbprophet
import matplotlib.pyplot as plt
import pandas as pd
import pathos.multiprocessing as mp
import seaborn as sns
from tensorboardX import SummaryWriter

import iats.oxya.attention.attention as attn
import iats.oxya.attention.load
import iats.oxya.attention.model as attn_model
import iats.utils.load as load
from iats.utils.plot import generate_training_figures


def find_files(folder: pl.Path, prefix: str = "Imbalance", year: int = 2016) -> tp.List[pl.Path]:
    """
    Finds csv-files containing data to be read.

    Args:
        folder: Base directory.
        prefix: First part of the filenames to be found.
        year: Year in which the data was captured.

    Returns:
        List of file paths.
    """
    files = list(folder.glob(prefix + "-" + str(year) + "*.csv"))
    files.sort()
    return files


def read_files(files: tp.List[pl.Path]) -> pd.DataFrame:
    """
    Reads all the given files and concatenates them into a DataFrame.

    Args:
        files: List of files to be read.

    Returns:
        Concatenated DataFrame.
    """
    with mp.Pool(min(len(files), mp.cpu_count())) as pool:
        results = pool.map(lambda x: pd.read_csv(x), files)
    return pd.concat(results).reset_index(drop=True)


def preprocess(df: pd.DataFrame) -> pd.DataFrame:
    """
    Applies some preprocessing steps to the DataFrame.

    1. Replace comma's with dots.
    2. Set timezone to CET.
    3. Drop superfluous columns.

    Args:
        df: Input DataFrame.

    Returns:
        Output dataFrame.
    """
    df = df.drop(columns=['SR_PRICE']).applymap(lambda x: str(x.replace(',', '.'))).apply(pd.to_numeric,
                                                                                          errors='ignore')
    new_index = df[['EXECDATE', 'QUARTERHOUR']].apply(
        lambda x: pd.to_datetime(x[0] + ' ' + x[1][:5], format='%d/%m/%Y %H:%M'), axis=1)
    localized_index = new_index.dt.tz_localize('CET', ambiguous='infer')
    return df.set_index(localized_index).drop(columns=['Title', 'Validity', 'EXECDATE', 'QUARTERHOUR'])


def plot_stats(df: pd.DataFrame) -> plt.Axes:
    """
    Plots the correlation matrix of a DataFrame's columns.

    Args:
        df: Input DataFrame.

    Returns:
        Axes containing the matrix drawing.
    """
    df.plot(subplots=True, figsize=(10, 18))
    plt.figure()
    ax = sns.heatmap(df.corr().abs(), annot=True)
    plt.show(block=False)
    return ax


def init_prophet_df(df: pd.DataFrame, column: str) -> pd.DataFrame:
    """
    Creates a DataFrame compatible with facebook Prophet from a DataFrame column.

    Args:
        df: Input DataFrame.
        column: Selected column.

    Returns:
        Prophet-compatible DataFrame (columns: 'index', 'ds', 'y').
    """
    df_prophet = pd.DataFrame(df[column].tz_localize(None)).rename(columns={column: 'y'})
    df_prophet.index.rename('ds', inplace=True)
    df_prophet.reset_index(inplace=True)
    return df_prophet


def setup_prophet(df: pd.DataFrame, **kwargs: int) -> fbprophet.Prophet:
    """
    Creates a Prophet forecaster object and fits data to it.

    Args:
        df: Input DataFrame.
        **kwargs: Keyword arguments.

    Returns:
        Fitted Prophet forecaster object.
    """
    m = fbprophet.Prophet(**kwargs)
    m.fit(df)
    return m


def load_all_files(path: pl.Path) -> pd.DataFrame:
    """
    Looks for a "dataframe.csv" file in the specified folder. If it doesn't exist, loads and preprocesses all other
    csv files in the specified path, reads and preprocesses them and saves the result as "dataframe.csv" for faster
    loading in the future.

    Args:
        path: Directory with csv-files.

    Returns:
        Parsed DataFrame.
    """
    if not path.is_dir():
        raise ValueError()
    if (path / "dataframe.csv").is_file():
        df = pd.read_csv(path / "dataframe.csv", index_col=0, parse_dates=True)
    else:
        files = []
        for directory in sorted(path.iterdir()):
            if directory.parts[-1] == "dataframe.csv":
                continue
            files += find_files(directory, year=int(directory.parts[-1]))
        df = read_files(files)
        df = preprocess(df)
        df.to_csv(path / "dataframe.csv")
    return df


def experiment_all_years() -> None:
    path = pl.PosixPath('~', 'Datasets', 'Elia Imbalance').expanduser()
    df = load_all_files(path)
    plot_stats(df)
    df_prophet = init_prophet_df(df, column='SI')
    m = setup_prophet(df_prophet, mcmc_samples=300)
    future = m.make_future_dataframe(periods=365)
    forecast = m.predict(future)
    m.plot(forecast)
    plt.legend(loc='best')
    m.plot_components(forecast)
    plt.show()


def evaluate_darnn():
    # TODO
    pass


def experiment_prophet(column: str = 'MIP', days: int = 30) -> None:
    path = pl.PosixPath('~', 'Datasets', 'Elia Imbalance').expanduser()
    files = []
    files += find_files(path / '2016', year=2016)
    files += find_files(path / '2017', year=2017)
    df = preprocess(read_files(files))
    df_transformed = init_prophet_df(df, column)
    # filter
    df_transformed.loc[df_transformed['y'] == 0, 'y'] = None
    # df_transformed.loc[df_transformed['y'] > 300, 'y'] = None
    df_transformed['y'] = df_transformed['y'].rolling(10).mean().interpolate('linear')
    df_transformed.plot(x='ds', y='y', figsize=(20, 10))
    m = fbprophet.Prophet()
    m.fit(df_transformed)
    future = m.make_future_dataframe(periods=days * 24 * 4, freq='15min')
    forecast = m.predict(future)
    m.plot(forecast)
    plt.show()


def experiment_darnn(train: bool = False) -> None:
    path = pl.PosixPath('~', 'Datasets', 'Elia Imbalance').expanduser()
    writer = SummaryWriter()
    files = []
    files += find_files(path / '2016', year=2016)
    df = preprocess(read_files(files))
    print(df.head())
    data = df.drop(columns=['PPOS', 'PNEG']).reset_index(drop=True)
    save_path = path / '2016' / 'weights'
    if not save_path.exists():
        os.makedirs(save_path)
    if train:
        model = attn_model.DARNNTrainer(data, 'MIP', logger=attn.logger, tensorboard_writer=writer,
                                        save_path=save_path, parallel=False, learning_rate=0.001)
        model.train(n_epochs=1000)
    else:
        basedir = pl.Path.home() / 'Datasets' / 'Elia Imbalance' / '2016'
        saved_model_folder = basedir / 'weights_{:%Y%m%d-%H%M}'.format(datetime.datetime.now())
        state = basedir / 'weights' / 'checkpoint_990.pth.tar'
        model = iats.oxya.attention.load.load_trainer(saved_model_folder, state, data, 'MIP')

    generate_training_figures(model)


if __name__ == '__main__':
    # experiment_all_years()
    # experiment_prophet(column='MIP')
    experiment_darnn()

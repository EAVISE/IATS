import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch import Tensor

from iats.enervalis.data_elia import DataElia
from iats.enervalis.models.convgru import ConvGRU
from iats.utils.tools import Tools

matplotlib.use('Qt5Agg')
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


def window_stack(a, stepsize=1, width=3):
    return np.hstack(a[i:1 + i - width or None:stepsize] for i in range(0, width))


if __name__ == '__main__':
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Code", "IATS-master")

    print("Using cuda? {}".format(use_cuda))
    # set random seed to 0
    np.random.seed(0)
    torch.manual_seed(0)
    # load data and make training set
    # data = torch.load('traindata.pt')
    elia = DataElia.load_data()

    # Train data
    df_2015_train = elia.loc[(elia['Year'] == 2015) & (elia['Month'].isin([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]))]

    si_data_train = df_2015_train['SI'].values
    si_data_train, si_m, si_std = Tools.normalize_data(si_data_train, b_return_params=True)
    si_data_train_alt = si_data_train.reshape(len(si_data_train), 1)
    alpha_data_train = df_2015_train['Alpha'].values
    alpha_data_train, alpha_m, alpha_std = Tools.normalize_data(alpha_data_train, b_return_params=True)
    alpha_data_train_alt = alpha_data_train.reshape(len(alpha_data_train), 1)
    nrv_data_train = df_2015_train['NRV'].values
    nrv_data_train, nrv_m, nrv_std = Tools.normalize_data(nrv_data_train, b_return_params=True)
    nrv_data_train_alt = nrv_data_train.reshape(len(nrv_data_train), 1)
    ppos_data_train = df_2015_train['PPOS'].values
    ppos_data_train, ppos_m, ppos_std = Tools.normalize_data(ppos_data_train, b_return_params=True)
    ppos_data_train_alt = ppos_data_train.reshape(len(ppos_data_train), 1)

    # Test data
    df_2015_test = elia.loc[(elia['Year'] == 2015) & (elia['Month'] == 12)]

    si_data_test = df_2015_test['SI'].values
    si_data_test = Tools.normalize_data(si_data_test, mean=si_m, std=si_std)
    si_data_test_alt = si_data_test.reshape(len(si_data_test), 1)
    alpha_data_test = df_2015_test['Alpha'].values
    alpha_data_test = Tools.normalize_data(alpha_data_test, mean=alpha_m, std=alpha_std)
    alpha_data_test_alt = alpha_data_test.reshape(len(alpha_data_test), 1)
    nrv_data_test = df_2015_test['NRV'].values
    nrv_data_test = Tools.normalize_data(nrv_data_test, mean=nrv_m, std=nrv_std)
    nrv_data_test_alt = nrv_data_test.reshape(len(nrv_data_test), 1)
    ppos_data_test = df_2015_test['PPOS'].values
    ppos_data_test = Tools.normalize_data(ppos_data_test, mean=ppos_m, std=ppos_std)
    ppos_data_test_alt = ppos_data_test.reshape(len(ppos_data_test), 1)

    feat_len = 1
    ws_si = torch.from_numpy(window_stack(si_data_train_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_alpha = torch.from_numpy(window_stack(alpha_data_train_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_nrv = torch.from_numpy(window_stack(nrv_data_train_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_ppos = torch.from_numpy(window_stack(ppos_data_train_alt[:-1], stepsize=1, width=feat_len)).to(device)
    to_cat = (
        # ws_si,
        # ws_alpha,
        # ws_nrv,
        ws_ppos,
    )

    input = torch.cat(to_cat, dim=1).float()
    ws_si, ws_alpha, ws_nrv, ws_ppos = None, None, None, None
    # target = torch.from_numpy(ppos_data_train_alt[feat_len:]).float().to(device)
    target = torch.empty(len(ppos_data_train) - feat_len, 1, dtype=torch.float).to(device)
    for i in range(target.shape[0]):
        target[i][0] = 1 if ppos_data_train[feat_len + i] > ppos_data_train[feat_len + i - 1] else 0

    ws_si = torch.from_numpy(window_stack(si_data_test_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_alpha = torch.from_numpy(window_stack(alpha_data_test_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_nrv = torch.from_numpy(window_stack(nrv_data_test_alt[:-1], stepsize=1, width=feat_len)).to(device)
    ws_ppos = torch.from_numpy(window_stack(ppos_data_test_alt[:-1], stepsize=1, width=feat_len)).to(device)
    to_cat = (
        # ws_si,
        # ws_alpha,
        # ws_nrv,
        ws_ppos,
    )

    test_input = torch.cat(to_cat, dim=1).float()
    # test_target = torch.from_numpy(ppos_data_test_alt[feat_len:]).float().to(device)
    test_target = torch.empty(len(ppos_data_test) - feat_len, 1, dtype=torch.float).to(device)
    for i in range(test_target.shape[0]):
        test_target[i][0] = 1 if ppos_data_test[feat_len + i] > ppos_data_test[feat_len + i - 1] else 0

    # build the model
    # Layer conf: [(blocks, el. per block, hidden size for block),...]
    seq = ConvGRU(dim_input=1, layer_conf=[(4, 2, 128), (2, 2, 64)]).float()
    nb_pos = sum(target).item()
    # criterion = nn.MSELoss()
    criterion = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([(target.shape[0] - nb_pos) / target.shape[0]]).to(device))

    # use LBFGS as optimizer since we can load the whole data to train
    # optimizer = optim.LBFGS(seq.parameters(), lr=0.8)
    optimizer = optim.Adam(seq.parameters())
    nb_epochs = 9999

    best_loss = np.PINF
    since_best = 0
    test_best_loss = np.PINF
    test_since_best = 0
    diff_loss = np.PINF
    # begin to train
    for i in range(nb_epochs):
        print('STEP: ', i)


        def closure():
            optimizer.zero_grad()
            out = seq(input)
            loss = criterion(out, target)
            print('loss:', loss.item())
            loss.backward()
            return loss


        ep_loss = optimizer.step(closure).item()
        if ep_loss < best_loss:
            diff_loss = best_loss - ep_loss
            best_loss = ep_loss
            since_best = 0
        else:
            since_best += 1
        # begin to predict, no need to track gradient here
        with torch.no_grad():
            future = 0
            pred = seq(test_input, future=future)
            if future:
                loss = criterion(pred[:, :-future], test_target)
            else:
                loss = criterion(pred, test_target)
            print('test loss:', loss.item())
            if loss.item() < test_best_loss:
                test_best_loss = loss.item()
                test_since_best = 0
            else:
                test_since_best += 1
            y = Tensor.cpu(pred).detach().numpy()

        plt_range = len(y)
        ref_data = ppos_data_test[-plt_range:]
        pred_data = y[-plt_range:, 0]
        updn_pred = [1 if pred_data[i] > ref_data[i - 1] else 0 for i in range(1, len(pred_data))]
        correct = [1 if ref_data[i] > ref_data[i - 1] else 0 for i in range(1, len(ref_data))]

        nb_corr_pred = sum([1 if updn_pred[i] == correct[i] else 0 for i in range(len(correct))])
        print("Accuracy: {}/{} [{:5.3f}%]".format(nb_corr_pred, len(correct), nb_corr_pred / len(correct)))
        print("Baseline: {}/{} [{:5.3f}%]".format(sum(correct), len(correct), sum(correct) / len(correct)))
        print("Since best/ Test since best: {} - {}".format(since_best, test_since_best))

        if since_best > 20:
            print("No improvement on training since {} epochs; early stopping.".format(since_best))
            break
        # if test_since_best > 5:
        #     print("No improvement on testing since {} epochs; early stopping.".format(test_since_best))
        #     break
        elif diff_loss < 0.000001:
            print("Loss improvement too small; early stopping.\n\tLoss: {}\n\tDiff: {}"
                  .format(best_loss, diff_loss))
            break
        if i < nb_epochs - 10:
            continue
        # draw the result
        plt.figure(figsize=(30, 10))
        plt.title('Predict future values for time sequences\n(Dashlines are predicted values)', fontsize=30)
        plt.xlabel('x', fontsize=20)
        plt.ylabel('y', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        nb_corr_pred = sum([1 if updn_pred[i] == correct[i] else 0 for i in range(len(correct))])
        print("Accuracy: {}/{} [{:5.3f}%]".format(nb_corr_pred, len(correct), nb_corr_pred / len(correct)))
        print("Baseline: {}/{} [{:5.3f}%]".format(sum(correct), len(correct), sum(correct) / len(correct)))

        plt.plot(np.arange(plt_range), pred_data, '.r', linewidth=4.0)
        plt.plot(np.arange(plt_range), ref_data, 'b', linewidth=2.0)
        plt.savefig(os.path.join(BASE_DIR, 'Plots', 'predict%d.pdf' % i))
        if i == nb_epochs - 1:
            plt.show()
        plt.close()

# Load KMI data
import os

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

matplotlib.use('Qt5Agg')


class DataKMI:
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Data", "KMI")
    BASE_FILE = "aws_10min.csv"
    REWORKED_FILE = "aws_reworked.pkl"

    # We are gonna load the CSV, and split the timestamps into several columns for year, month, day and time
    @staticmethod
    def load_data(base_dir=BASE_DIR, base_file=BASE_FILE, conv_file=REWORKED_FILE):
        # If converted file does not exist yet, the create it...
        data_conv = os.path.join(base_dir, conv_file)
        if not os.path.exists(data_conv):
            print("Converting CSV file: reworking timestamps...")
            # Column names:
            # FID,code,timestamp,air_pressure,air_temperature,relative_humidity,precipitation,wind_speed,wind_direction,
            # qc_flags,the_geom
            # The two codes are: 6418 (=Zeebrugge), 6472 (=Humain)
            # return pd.read_csv(data_file)
            data = pd.read_csv(os.path.join(base_dir, base_file), usecols=[1, 2, 3, 4, 5, 6, 7, 8],
                               parse_dates=["timestamp"])
            # Add columns for year, month, day and time
            # Taken from https://stackoverflow.com/questions/30026815/add-multiple-columns-to-pandas-dataframe-from-function
            lambdafunc = lambda x: pd.Series([x[1].year,
                                              x[1].month,
                                              x[1].day,
                                              x[1].time()])
            data[["Year", "Month", "Day", "Time"]] = data.apply(lambdafunc, axis=1)
            # Drop original column
            data.drop(columns=["timestamp"], inplace=True)
            # Rearrange columns to put the added columns to the front
            cols = data.columns.tolist()
            cols = cols[-4:] + cols[:-4]
            data = data[cols]
            data.to_pickle(os.path.join(base_dir, conv_file))
        # ...else, load it.
        else:
            print("Loading pickle...")
            data = pd.read_pickle(data_conv)

        return data


if __name__ == '__main__':
    data = DataKMI.load_data()
    temp1 = data.loc[data['code'] == 6418, 'air_temperature']
    temp2 = data.loc[data['code'] == 6472, 'air_temperature']

    plt.plot(temp1, '.')
    plt.plot(temp2, '.')
    plt.show()

    print("Ello")

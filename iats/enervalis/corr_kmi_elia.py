"""
A script to show that there is basically no exploitable correlation between the KMI data and Elia data.
"""

from iats.enervalis.data_elia import DataElia
from iats.enervalis.data_kmi import DataKMI
from iats.utils.tools import Tools


def main():
    # (A tiny bit) more info at: http://www.elia.be/en/grid-data/balancing/imbalance-prices
    # Available years: 2015, 2016, 2017
    elia = DataElia.load_data()
    kmi = DataKMI.load_data()
    for v in ['precipitation', 'air_pressure', 'air_temperature', 'relative_humidity', 'wind_speed', 'wind_direction']:
        d_kmi = kmi.loc[(kmi['code'] == 6418) & (kmi['Year'] == 2018) & (kmi['Month'].isin([1, 2])), v].fillna(
            method='ffill').values
        d_elia = elia.loc[(elia['Year'] == 2018) & (elia['Month'].isin([1, 2])), 'SI'].values

        d_kmi = Tools.normalize_data(d_kmi)
        d_elia = Tools.normalize_data(d_elia)

        # Interpolate KMI data to obtain same amount of datapoints as Eli data
        d_kmi_interpol = []
        for i, e in enumerate(d_kmi):
            if i % 3 == 0:
                continue
            if i % 3 == 2:
                d_kmi_interpol.append((e + d_kmi[i - 1]) / 2)
            if i % 3 == 1:
                d_kmi_interpol.append(e)

        s = "Normalized correlation btwn. {} and SI".format(v)
        print("{:53s}: {}".format(s, Tools.corr_norm(d_kmi_interpol, d_elia[:-10])))


if __name__ == '__main__':
    main()

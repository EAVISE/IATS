import datetime
import os

import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import torch

from iats.enervalis.data_elia import DataElia

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


if __name__ == '__main__':
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Code", "IATS-master")
    print("Using cuda? {}".format(use_cuda))

    # load data
    elia = DataElia.load_data()
    ppos = elia['PPOS'].values
    ppos_mean = np.mean(ppos)
    ppos_std = np.std(ppos)

    std_thres = 3.0  # Number of standard deviations to take as threshold before considering a value as "peak".
    thres_lo = ppos_mean - std_thres*ppos_std
    thres_hi = ppos_mean + std_thres*ppos_std
    # Extract peak values
    peak_x, peak_y = [], []
    for i, e in enumerate(ppos):
        if e > thres_hi or e < thres_lo:
            peak_x.append(i)
            peak_y.append(e)
    # Extract "target" samples
    set_peaks = set(peak_x)
    target_x, target_y = [], []
    for i, e in enumerate(peak_x):
        for j in range(e-4, e):
            if j in set_peaks:
                continue
            target_x.append(j)
            target_y.append(ppos[j])

    # print some statistics
    print("Number of peak values: {} out of {} [{:5.2f}%]".format(len(peak_y), len(ppos), 100*len(peak_y)/len(ppos)))
    elia_peak = elia.iloc[peak_x]
    elia_peak_full_years = elia_peak[elia_peak['Year'].isin(list(range(2015, 2018)))]
    # Add column containing "date" objects

    elia_peak['Date'] = elia_peak.apply(lambda x: datetime.date(year=x['Year'], month=x['Month'], day=x['Day']), axis=1)

    nb_per_year = []
    print("Number of peak_prediction per year:")
    for i in range(2015, 2019):
        temp_val = len(elia_peak[elia_peak['Year'] == i])
        nb_per_year.append((i, temp_val))
        print("\t{}: {}".format(i, temp_val))

    nb_per_month = []
    print("Number of peak_prediction per month (2015-2017):")
    for i in range(1, 13):
        temp_val = len(elia_peak_full_years[elia_peak['Month'] == i])
        nb_per_month.append((i, temp_val))
        print("\t{}: {}".format(i, temp_val))

    nb_per_day = []
    print("Number of peak_prediction per day of the week:")
    for i in range(1, 8):
        temp_val = len(elia_peak[elia_peak['Date'].apply(lambda x: x.isoweekday() == i)])
        nb_per_day.append((i, temp_val))
        print("\t{}: {}".format(i, temp_val))

    nb_per_hour = []
    print("Number of peak_prediction per hour:")
    for i in range(0, 24):
        temp_val = len(elia_peak[elia_peak['Time'].apply(lambda x: x.hour == i)])
        nb_per_hour.append((i, temp_val))
        print("\t{}: {}".format(i, temp_val))

    # create date ticks for x-axis
    # np.where((elia['Year'] == 2016) & (elia['Month'] == 1) & (elia['Day'] == 1))[0][0]
    date_x_ticks = [np.where((elia['Year'] == y) & (elia['Month'] == m) & (elia['Day'] == 1))[0][0]
                    for m in [1, 7] for y in range(2015, 2019)]
    date_x_label = ["{}/{}".format(elia.iloc[i]['Month'], elia.iloc[i]['Year']) for i in date_x_ticks]

    # draw the results
    # Plot 1: PPOS with marked peak values
    plt.figure(figsize=(30, 10))
    plt.title('PPOS', fontsize=30)
    plt.xlabel('Datetime', fontsize=20)
    plt.ylabel('€/MWh', fontsize=20)
    plt.xticks(date_x_ticks, date_x_label, fontsize=12, rotation='vertical')
    plt.yticks(fontsize=12)

    plt.plot(ppos, '.b', linewidth=0.0)  # All points
    plt.plot(peak_x, peak_y, '.r', linewidth=0.0)  # Overlay peak_prediction
    plt.plot(target_x, target_y, marker='.', color='#00f356', linestyle="None")  # Overlay targets
    plt.legend(["|x| < (mean - {}*std)".format(std_thres),
                "|x| >= (mean - {}*std)".format(std_thres),
                "Target samples"])
    plt.tight_layout()
    plt.show()
    plt.close()

    # Plot 2: Histograms
    plt.figure()

    plt.subplot(2, 2, 1)
    plt.title('Peaks per Year', fontsize=16)
    plt.xlabel('Year', fontsize=12)
    plt.ylabel('#', fontsize=12)
    plt.xticks(*zip(*[(i, str(i)) for i in range(2015, 2019)]), fontsize=8)
    plt.yticks(fontsize=8)
    plt.bar(*zip(*nb_per_year))

    plt.subplot(2, 2, 2)
    plt.title('Peaks per Month (2015-2017)', fontsize=16)
    plt.xlabel('Month', fontsize=12)
    plt.ylabel('#', fontsize=12)
    plt.xticks(*zip(*[(i, str(i)) for i in range(1, 13)]), fontsize=8)
    plt.yticks(fontsize=8)
    plt.bar(*zip(*nb_per_month))

    plt.subplot(2, 2, 3)
    plt.title('Peaks per Day', fontsize=16)
    plt.xlabel('Day', fontsize=12)
    plt.ylabel('#', fontsize=12)
    plt.xticks(*zip(*[(i, str(i)) for i in range(1, 8)]), fontsize=8)
    plt.yticks(fontsize=8)
    plt.bar(*zip(*nb_per_day))

    plt.subplot(2, 2, 4)
    plt.title('Peaks per Hour', fontsize=16)
    plt.xlabel('Hour', fontsize=12)
    plt.ylabel('#', fontsize=12)
    plt.xticks(*zip(*[(i, str(i)) for i in range(0, 24)]), fontsize=8)
    plt.yticks(fontsize=8)
    plt.bar(*zip(*nb_per_hour))

    plt.tight_layout(pad=0.1, w_pad=0.2, h_pad=0.2)
    plt.show()
    plt.close()

# A script that will perform a grid search over several parameter combinations of the ensemble classifier,
# and plot the resulting model performances.
# For each parameter combination, 10 ensembles will be trained, so as to keep track of variations in iterations.
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from iats.enervalis.peak_prediction.peak_ensemble_classifier import PeakEnsembleClassifier

matplotlib.use('Qt5Agg')


def make_plots(acc_data, f1_data, t_data):
    # Make heatmap of accuracy
    plot_data = np.zeros((acc_data.shape[0], acc_data.shape[1]))
    for i in range(acc_data.shape[0]):
        for j in range(acc_data.shape[1]):
            plot_data[i, j] = np.mean(acc_data[i, j, :])
    # Seaborn plots DataFrames as you "see" them, i.e., rows will make up the y-axis, columns the x-axis, which to me
    # personally is counter intuitive. Hence, we transpose the created DataFrame.
    plot_data = pd.DataFrame(data=plot_data, columns=[str(x) for x in nb_nn_options],
                             index=[str(x) for x in fft_sizes]).T
    # We inverse the order of the rows, to put the lowest value on the y-axis at the bottom.
    plot_data = plot_data[::-1]
    data_text = {fft_sizes[i]: ['{:5.3f}±{:5.3f}'.format(np.mean(acc_data[i, j]), np.std(acc_data[i, j]))
                                for j in range(acc_data.shape[1])]
                 for i in range(len(fft_sizes))}
    plot_data_text = pd.DataFrame(data=data_text, index=[str(x) for x in nb_nn_options]).iloc[::-1]

    plt.figure()
    ax = sns.heatmap(plot_data, linewidth=0.5, annot=plot_data_text, fmt='')
    plt.title("Peak detection accuracy on PPOS data", fontsize=16)
    plt.xlabel("FFT size", fontsize=12)
    plt.ylabel("#NNs in ensemble", fontsize=12)
    ax.title.set_position([.5, 1.05])
    plt.show()

    # Make heatmap of max_f1
    plot_data = np.zeros((acc_data.shape[0], acc_data.shape[1]))
    for i in range(acc_data.shape[0]):
        for j in range(acc_data.shape[1]):
            plot_data[i, j] = np.mean(f1_data[i, j, :])
    # Seaborn plots DataFrames as you "see" them, i.e., rows will make up the y-axis, columns the x-axis, which to me
    # personally is counter intuitive. Hence, we transpose the created DataFrame.
    plot_data = pd.DataFrame(data=plot_data, columns=[str(x) for x in nb_nn_options],
                             index=[str(x) for x in fft_sizes]).T
    # We inverse the order of the rows, to put the lowest value on the y-axis at the bottom.
    plot_data = plot_data[::-1]
    data_text = {fft_sizes[i]: ['{:5.3f}@{:4.2f}'.format(np.mean(f1_data[i, j]), np.mean(t_data[i, j]))
                                for j in range(acc_data.shape[1])]
                 for i in range(len(fft_sizes))}
    plot_data_text = pd.DataFrame(data=data_text, index=[str(x) for x in nb_nn_options]).iloc[::-1]

    plt.figure()
    ax = sns.heatmap(plot_data, linewidth=0.5, annot=plot_data_text, fmt='')
    plt.title("Peak detection max f1 on PPOS data", fontsize=16)
    plt.xlabel("FFT size", fontsize=12)
    plt.ylabel("#NNs in ensemble", fontsize=12)
    ax.title.set_position([.5, 1.05])
    plt.show()


def main():
    global fft_sizes, nb_nn_options
    pec = PeakEnsembleClassifier()
    fft_sizes = [64, 128, 256, 512, 1024, 2048]
    nb_nn_options = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    # ##############
    # MLP classifier
    # ##############
    b_preloaded = False
    b_preload = True  # First see if results of previous run have been dumped to HD
    if b_preload:
        pre_loaded = []
        if os.path.isfile("mlp_res_acc.pkl"):
            mlp_res_acc = np.load("mlp_res_acc.pkl")
            pre_loaded.append(True)
        if os.path.isfile("mlp_res_f1.pkl"):
            mlp_res_f1 = np.load("mlp_res_f1.pkl")
            pre_loaded.append(True)
        if os.path.isfile("mlp_res_acc.pkl"):
            mlp_res_t = np.load("mlp_res_t.pkl")
            pre_loaded.append(True)
        if sum(pre_loaded) == 3:
            b_preloaded = True
    if not b_preloaded:
        mlp_res_acc = np.zeros((len(fft_sizes), len(nb_nn_options), 10))
        mlp_res_f1 = np.zeros((len(fft_sizes), len(nb_nn_options), 10))
        mlp_res_t = np.zeros((len(fft_sizes), len(nb_nn_options), 10))

        for i, fft_size in enumerate(fft_sizes):
            for j, nb_mlps in enumerate(nb_nn_options):
                accs, f1s, ts = [], [], []
                for _ in tqdm(range(10)):
                    acc, max_f1, max_t = pec.run(fft_size=fft_size, nb_mlps=nb_mlps, b_use_conv=False, b_silent=True)
                    accs.append(acc)
                    f1s.append(max_f1)
                    ts.append(max_t)
                mlp_res_acc[i, j, :] = accs
                mlp_res_f1[i, j, :] = f1s
                mlp_res_t[i, j, :] = ts
                print("\nFFT Size: {:4d}, NNs: {:2d}: acc={:5.3f}±{:5.3f}, max_f1={:5.3f}±{:5.3f} @t={:4.2f}±{:4.2f}"
                      .format(fft_size, nb_mlps, np.mean(accs), np.std(accs),
                              np.mean(f1s), np.std(f1s), np.mean(ts), np.std(ts)))
        mlp_res_acc.dump("mlp_res_acc.pkl")
        mlp_res_f1.dump("mlp_res_f1.pkl")
        mlp_res_t.dump("mlp_res_t.pkl")
    make_plots(mlp_res_acc, mlp_res_f1, mlp_res_t)
    # #################
    # Conv1D classifier
    # #################
    b_preloaded = False
    b_preload = True  # First see if results of previous run have been dumped to HD
    if b_preload:
        pre_loaded = []
        if os.path.isfile("conv_res_acc.pkl"):
            conv_res_acc = np.load("conv_res_acc.pkl")
            pre_loaded.append(True)
        if os.path.isfile("conv_res_f1.pkl"):
            conv_res_f1 = np.load("conv_res_f1.pkl")
            pre_loaded.append(True)
        if os.path.isfile("conv_res_acc.pkl"):
            conv_res_t = np.load("conv_res_t.pkl")
            pre_loaded.append(True)
        if sum(pre_loaded) == 3:
            b_preloaded = True
    if not b_preloaded:
        conv_res_acc = np.zeros((len(fft_sizes), len(nb_nn_options), 10))
        conv_res_f1 = np.zeros((len(fft_sizes), len(nb_nn_options), 10))
        conv_res_t = np.zeros((len(fft_sizes), len(nb_nn_options), 10))

        for i, fft_size in enumerate(fft_sizes):
            for j, nb_mlps in enumerate(nb_nn_options):
                accs, f1s, ts = [], [], []
                for _ in tqdm(range(10)):
                    acc, max_f1, max_t = pec.run(fft_size=fft_size, nb_mlps=nb_mlps, b_use_conv=False, b_silent=True)
                    accs.append(acc)
                    f1s.append(max_f1)
                    ts.append(max_t)
                conv_res_acc[i, j, :] = accs
                conv_res_f1[i, j, :] = f1s
                conv_res_t[i, j, :] = ts
                print("\nFFT Size: {:4d}, NNs: {:2d}: acc={:5.3f}±{:5.3f}, max_f1={:5.3f}±{:5.3f} @t={:4.2f}±{:4.2f}"
                      .format(fft_size, nb_mlps, np.mean(accs), np.std(accs),
                              np.mean(f1s), np.std(f1s), np.mean(ts), np.std(ts)))
        conv_res_acc.dump("conv_res_acc.pkl")
        conv_res_f1.dump("conv_res_f1.pkl")
        conv_res_t.dump("conv_res_t.pkl")
    make_plots(conv_res_acc, conv_res_f1, conv_res_t)


if __name__ == '__main__':
    main()

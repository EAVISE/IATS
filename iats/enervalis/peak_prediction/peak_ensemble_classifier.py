# An ensemble approach to peak prediction.
# The idea is:
#   - train multiple MLP classifiers, each trained using a different randomly sampled training set
#   - combine their results in an XGBoost random forest
import datetime
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
import torch
import torch.utils.data as data_utils
from matplotlib import gridspec
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from torch import Tensor, nn, optim
from xgboost.sklearn import XGBClassifier

from iats.enervalis.data_elia import DataElia
from iats.enervalis.peak_prediction.peak_model_conv1d import PeakModelConv1d
from iats.enervalis.peak_prediction.peak_model_mlp import PeakModelMLP
from iats.utils.tools import Tools

matplotlib.use('Qt5Agg')

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class PeakEnsembleClassifier:
    def __init__(self):
        home = os.path.expanduser("~")
        self.base_dir = os.path.join(home, "Work", "Code", "IATS-master")
        self.fft_size, self.fft_feats, self.total_feats = None, None, None
        self.back_steps = 0
        self.total_feats = None
        self.scalers, self.models = None, None
        self.xgb_model = None

    def run(self, peak_thres: float = 3.0, fft_size: int = 512, nb_mlps=10, back_steps=0,
            n_vs_p: int = 3, ens_n_vs_p: int = 3, b_use_conv=False, b_use_extra_feats=False, b_silent=False):
        """

        :param peak_thres: number of std a value should deviate from the mean in order to be considered a peak value
        :param fft_size: size of the fft window
        :param nb_mlps: number of MLP models to train that are then passed onto a Random Forest
        :param n_vs_p: number of negative (False) samples in training set for each positive (True) sample for NNs
        :param ens_n_vs_p: same as n_vs_p, but for the ensemble model
        :param b_use_conv: use Conv1D network instead of default MLP network
        :param b_use_extra_feats: add additional features to ensemble tree
        :param b_silent: if True, don't print output and don't create plots
        :return:
        """
        self.back_steps = back_steps
        self.fft_size = fft_size
        # Half of the FFT coefficients can be ignored; first coeff = bias
        self.fft_feats = (fft_size // 2) + 1
        self.total_feats = self.fft_feats * (1 + back_steps)
        # self.nb_feats = fft_size

        # Step 1: load data and determine peak values
        elia = DataElia.load_data(b_silent=b_silent)
        elia_dev = elia[elia['Year'].isin([2015, 2016, 2017])]
        elia_val = elia[elia['Year'] == 2018]
        ppos = elia_dev['PPOS'].values
        ppos_mean = np.mean(ppos)
        ppos_std = np.std(ppos)
        ppos_val = elia_val['PPOS'].values

        thres_lo = ppos_mean - peak_thres * ppos_std
        thres_hi = ppos_mean + peak_thres * ppos_std
        # Extract location of peak values
        peak_loc = [loc for loc, e in enumerate(ppos) if e > thres_hi or e < thres_lo]

        # Step 2: generate True and False targets
        # Only look at start of a (possible) peak series
        true_loc = []
        for i, pos in enumerate(peak_loc):
            if i > 0 and (pos - peak_loc[i - 1]) == 1:
                continue
            true_loc += list(range(pos - 4, pos))
        valid_true_loc = [x for x in true_loc if x >= fft_size + back_steps - 1]

        temp = set(true_loc + peak_loc)
        false_loc = [i for i in range(len(ppos)) if i not in temp]
        valid_false_loc = [x for x in false_loc if x >= fft_size + back_steps - 1]

        # plt.figure()
        # plt.plot(range(len(ppos)), ppos, '.b')
        # plt.plot(peak_loc, ppos[peak_loc], '.r')
        # plt.plot(valid_true_loc, ppos[valid_true_loc], '.g')
        # plt.show()

        # Step 3: For each MLP network to be trained...
        nb_true = len(valid_true_loc)
        scalers, models, test_accuracies = [], [], []
        model_preds = np.zeros((nb_mlps, len(ppos) - fft_size - back_steps + 1))
        for at_model in range(nb_mlps):
            if not b_silent:
                print("Training model {} of {}...".format(at_model + 1, nb_mlps))
            # Step 3.1: randomly sample False samples
            random_false = np.random.choice(valid_false_loc, n_vs_p * nb_true, replace=False)

            # Step 3.2: generate features and labels for samples
            input = np.zeros(((1 + n_vs_p) * nb_true, self.total_feats))
            target = np.zeros(((1 + n_vs_p) * nb_true, 1))
            values = np.zeros((1 + n_vs_p) * nb_true)
            for i, loc in enumerate(valid_true_loc):
                feats = scipy.fftpack.rfft(ppos[loc - fft_size + 1:loc + 1])[:self.fft_feats]
                for backstep in range(1, back_steps + 1):
                    backfeats = \
                        scipy.fftpack.rfft(ppos[loc - fft_size - backstep + 1:loc - backstep + 1])[:self.fft_feats]
                    feats = np.concatenate((feats, backfeats))
                input[(1 + n_vs_p) * i, :] = feats
                target[(1 + n_vs_p) * i, :] = 1.0
                values[(1 + n_vs_p) * i] = ppos[loc]
            for i, loc in enumerate(random_false):
                # Simple safety check
                if max(input[(1 + n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :]) != 0.0:
                    raise ValueError("Overwriting stuff that should not be overwritten")
                feats = scipy.fftpack.rfft(ppos[loc - fft_size + 1:loc + 1])[:self.fft_feats]
                for backstep in range(1, back_steps + 1):
                    backfeats = \
                        scipy.fftpack.rfft(ppos[loc - fft_size - backstep + 1:loc - backstep + 1])[:self.fft_feats]
                    feats = np.concatenate((feats, backfeats))
                input[(1 + n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :] = feats
                target[(1 + n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :] = 0.0
                values[(1 + n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p)] = ppos[loc]
            # Keep random 20% as test data
            # NOTE! Using this method, there is no guarantee different splits will have the same relative amount
            # of True and False examples! On average, however...
            input, test_input, target, test_target = train_test_split(input, target, test_size=0.2)
            # Scale input features
            scaler = StandardScaler()
            input = scaler.fit_transform(input)
            test_input = scaler.transform(test_input)
            # Convert to torch.Tensor
            input = torch.from_numpy(input).float()
            test_input = torch.from_numpy(test_input).float()
            target = torch.from_numpy(target).float()
            test_target = torch.from_numpy(test_target).float()

            # Step 3.3: train
            model, test_acc = self._train(input, target, test_input, test_target,
                                          b_use_conv=b_use_conv, b_silent=b_silent)

            scalers.append(scaler)
            models.append(model)
            test_accuracies.append(test_acc)

            # Step 3.4: predict probabilities on entire dataset using each model
            # For each model, a unique scaler has to be used, so might as well do this inside this loop
            input = np.zeros((len(ppos) - self.fft_size - self.back_steps + 1, self.total_feats))
            for idx, i in enumerate(range(self.fft_size + self.back_steps - 1, len(ppos))):
                feats = scipy.fftpack.rfft(ppos[i - fft_size + 1:i + 1])[:self.fft_feats]
                for backstep in range(1, back_steps + 1):
                    backfeats = \
                        scipy.fftpack.rfft(ppos[i - backstep - fft_size + 1:i - backstep + 1])[:self.fft_feats]
                    feats = np.concatenate((feats, backfeats))
                input[idx, :] = feats

            input = torch.from_numpy(scaler.transform(input)).float()
            if b_use_conv:
                input = input.reshape(input.size(0), 1, input.size(1))
            full_data = data_utils.TensorDataset(input)
            full_loader = data_utils.DataLoader(full_data, batch_size=256, shuffle=False,
                                                sampler=data_utils.SequentialSampler(full_data))
            full_pred = []
            for data in full_loader:
                data = data[0]
                data = data.to(device)
                full_pred.append(model(data).detach().cpu()[:, 0].numpy())
            full_pred = np.concatenate(full_pred)
            model_preds[at_model] = full_pred
        self.scalers, self.models = scalers, models

        # Step 4: train ensemble model
        if not b_silent:
            print("Training ensemble model...")
        true_loc = set(true_loc)
        set_peak_loc = set(peak_loc)
        # When making predictions over the full dataset, consider "True" to be the correct target for
        # a peak value.
        # TODO: should'nt this be range(fft_size+back_steps)?
        full_target = np.array([1.0 if i in true_loc or i in set_peak_loc else 0.0
                                for i in range(fft_size + back_steps - 1, len(ppos))])

        model_preds = model_preds.T

        # Add extra feats: one-hot encoded vectors for hour and day ot week
        if b_use_extra_feats:
            weekday_train = Tools.to_one_hot(elia_dev.apply(
                lambda x: datetime.date(x['Year'], x['Month'], x['Day']).isoweekday(), axis=1).values)[
                            fft_size + back_steps - 1:, :]
            hour_data_train = Tools.to_one_hot(
                elia_dev['Time'].map(lambda x: x.hour + 1).values)[fft_size + back_steps - 1:, :]
            model_preds = np.concatenate((model_preds, weekday_train, hour_data_train), axis=1)

        random_false = np.random.choice(valid_false_loc, ens_n_vs_p * nb_true, replace=False) - (
                    fft_size + back_steps - 1)
        test_set_idxs = np.concatenate(([x - fft_size - back_steps + 1 for x in valid_true_loc], random_false))
        np.random.shuffle(test_set_idxs)

        input = model_preds[test_set_idxs]
        target = full_target[test_set_idxs]
        input, test_input, target, test_target, idxs_train, idxs_test = \
            train_test_split(input, target, test_set_idxs, test_size=0.2)

        xgb = XGBClassifier(max_depth=2, n_estimators=200, scale_pos_weight=0.334)
        xgb.fit(input, target)
        test_pred = xgb.predict_proba(test_input)[:, 1]  # Extract probabilities for class=1
        acc = sum([1.0 if (test_target[i] == 0.0 and test_pred[i] < 0.5) or
                          (test_target[i] == 1.0 and test_pred[i] >= 0.5)
                   else 0 for i in range(len(test_target))])
        test_acc = acc / len(test_target)
        if not b_silent:
            print("Test accuracy      : {}".format(test_acc))
            print("Average over models: {}".format(np.average(test_accuracies)))
        max_f1, max_t = self._p_vs_r_analysis(test_target, test_pred, b_silent=b_silent)
        self.xgb_model = xgb

        # Predict all samples in validation set and compute performance
        if not b_silent:
            print("\nValidation analysis...")
        if b_use_extra_feats:
            weekday_val = Tools.to_one_hot(elia_val.apply(
                lambda x: datetime.date(x['Year'], x['Month'], x['Day']).isoweekday(), axis=1).values)[
                          fft_size + back_steps - 1:, :]
            hour_data_val = Tools.to_one_hot(
                elia_val['Time'].map(lambda x: x.hour + 1).values)[fft_size + back_steps - 1:, :]
            extra_feats_val = np.concatenate((weekday_val, hour_data_val), axis=1)
        else:
            extra_feats_val = None

        val_pred = self._predict(ppos_val, extra_feats_val, b_use_conv=b_use_conv)
        val_data_offset = len(ppos) - fft_size
        val_pred_offset = val_data_offset + fft_size
        val_peak_loc = np.array([i for i, e in enumerate(ppos_val)
                                 if e > thres_hi or e < thres_lo])
        val_true_loc = []
        for i, pos in enumerate(val_peak_loc):
            if i > 0 and (pos - val_peak_loc[i - 1]) == 1:
                continue
            val_true_loc += list(range(pos - 4, pos))
        val_true_loc = np.array([x for x in val_true_loc if x >= fft_size + back_steps - 1])
        set_val_true_loc = set(val_true_loc)
        set_val_peak_loc = set(val_peak_loc)
        val_target = np.array([1.0 if i in set_val_true_loc or i in set_val_peak_loc else 0.0
                               for i in range(fft_size + back_steps - 1, len(ppos_val))])

        val_acc = sum([1.0 if (val_target[i] == 0.0 and val_pred[i] < 0.5) or
                              (val_target[i] == 1.0 and val_pred[i] >= 0.5)
                       else 0 for i in range(len(val_target))])
        val_acc = val_acc / len(val_target)
        _, _ = self._p_vs_r_analysis(val_target, val_pred, b_silent=False)
        if not b_silent:
            print("Validation accuracy: {}".format(val_acc))

            nb_parts = 5
            val_part = len(val_pred) // nb_parts
            part_accs = []
            part_f1s = []
            till_part_accs = []
            till_part_f1s = []
            for i in range(1, nb_parts + 1):
                start = (i - 1) * val_part
                end = i * val_part if i < nb_parts else len(val_pred)
                till_part_acc = sum([1.0 if (val_target[:end][i] == 0.0 and val_pred[:end][i] < 0.5) or
                                            (val_target[:end][i] == 1.0 and val_pred[:end][i] >= 0.5)
                                     else 0 for i in range(end)])
                till_part_accs.append(till_part_acc / end)
                part_acc = sum([1.0 if (val_target[start:end][i] == 0.0 and val_pred[start:end][i] < 0.5) or
                                       (val_target[start:end][i] == 1.0 and val_pred[start:end][i] >= 0.5)
                                else 0 for i in range(end - start)])
                part_accs.append(part_acc / (end - start))

                till_part_f1, _ = self._p_vs_r_analysis(val_target[:end], val_pred[:end], b_silent=True)
                till_part_f1s.append(till_part_f1)
                part_f1, _ = self._p_vs_r_analysis(val_target[start:end], val_pred[start:end], b_silent=True)
                part_f1s.append(part_f1)

            plt.figure()
            plt.title("Evolution of F1 per validation part")
            plt.plot(list(range(1, nb_parts + 1)), part_accs, 'xb')
            plt.plot(list(range(1, nb_parts + 1)), till_part_accs, '.b')
            plt.plot(list(range(1, nb_parts + 1)), part_f1s, 'xr')
            plt.plot(list(range(1, nb_parts + 1)), till_part_f1s, '.r')
            plt.xticks([1, 2, 3, 4, 5])
            plt.xlabel("Part")
            plt.ylabel("Max(f1)")
            plt.legend(['Acc. of part', 'Acc. up to part', 'F1 of part', 'F1 up to part'])
            plt.show()

            # Predict all samples in development set
            full_pred = xgb.predict_proba(model_preds)[:, 1]  # Extract probabilities for class=1

            plt.figure()
            gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
            # Plot PPOS values; peak values are show in red
            ax1 = plt.subplot(gs[0])
            plt.plot(ppos[fft_size:], '.b')
            plt.plot([x - fft_size for x in peak_loc], ppos[peak_loc], '.r')
            plt.plot(idxs_train, ppos[[x + fft_size for x in idxs_train]], '.g')
            plt.plot(range(val_data_offset, val_data_offset + len(ppos_val)), ppos_val,
                     linestyle='', marker='.', color='black')
            plt.plot(val_data_offset + val_peak_loc, ppos_val[val_peak_loc], '.r')
            plt.ylabel("€/MWh")
            plt.legend(["PPOS values", "Peak values @{}σ".format(peak_thres), "Train set", "Validation set"])
            # Plot probabilities for class=1
            ax2 = plt.subplot(gs[1], sharex=ax1)
            plt.plot(full_pred, '.')
            plt.plot([x - fft_size for x in true_loc if x > fft_size],
                     full_pred[[x - fft_size for x in true_loc if x > fft_size]], '.r')
            plt.plot(idxs_train, full_pred[idxs_train], '.g')
            plt.plot(range(val_pred_offset, val_pred_offset + len(val_pred)), val_pred,
                     linestyle='', marker='.', color='black')
            plt.plot(val_data_offset + val_true_loc, val_pred[val_true_loc - fft_size], '.r')
            plt.legend(["Class=1 prob...", "...preceding PPOS peak_prediction".format(peak_thres), "Train set",
                        "Validation set"])
            # Plot targets
            # ax3 = plt.subplot(gs[2], sharex=ax1)
            # plt.plot(full_target, '.')
            # plt.plot(range(val_pred_offset, val_pred_offset+len(val_pred)), val_target, '.')
            plt.ylabel("p(class=1)")
            plt.xlabel("Timestep")
            plt.show()

        return test_acc, max_f1, max_t

    def _predict(self, data: np.ndarray, extra_feats: np.ndarray = None, b_use_conv=False):
        """
        Predict probability for all samples in data

        :param data:
        :return:
        """
        model_preds = np.zeros((len(self.models), len(data) - self.fft_size - self.back_steps + 1))

        input = np.zeros((len(data) - self.fft_size - self.back_steps + 1, self.total_feats))
        for idx, i in enumerate(range(self.fft_size + self.back_steps - 1, len(data))):
            feats = scipy.fftpack.rfft(data[i - self.fft_size + 1:i + 1])[:self.fft_feats]
            for backstep in range(1, self.back_steps + 1):
                backfeats = \
                    scipy.fftpack.rfft(data[i - backstep - self.fft_size + 1:i - backstep + 1])[:self.fft_feats]
                feats = np.concatenate((feats, backfeats))
            input[idx, :] = feats

        # input = np.zeros((len(data) - self.fft_size, self.total_feats))
        # for i in range(0, len(data) - self.fft_size):
        #     input[i, :] = scipy.fftpack.rfft(data[i:i + self.fft_size])[:self.total_feats]

        # Get predictions for the NN models
        for i, model in enumerate(self.models):
            scaled_input = torch.from_numpy(self.scalers[i].transform(input)).float()
            if b_use_conv:
                scaled_input = scaled_input.reshape(scaled_input.size(0), 1, scaled_input.size(1))
            full_data = data_utils.TensorDataset(scaled_input)
            full_loader = data_utils.DataLoader(full_data, batch_size=256, shuffle=False,
                                                sampler=data_utils.SequentialSampler(full_data))
            full_pred = []
            for batch in full_loader:
                batch = batch[0]
                batch = batch.to(device)
                full_pred.append(model(batch).detach().cpu()[:, 0].numpy())
            full_pred = np.concatenate(full_pred)
            model_preds[i] = full_pred

        model_preds = model_preds.T

        # Add extra features, if required
        if extra_feats is not None:
            model_preds = np.concatenate((model_preds, extra_feats), axis=1)

        # Feed NN predictions to XGB model
        pred = self.xgb_model.predict_proba(model_preds)[:, 1]  # Extract probabilities for class=1

        return pred

    def _train(self, input: torch.Tensor, target: torch.Tensor,
               test_input: torch.Tensor, test_target: torch.Tensor,
               b_use_conv=False, b_plot_results=False, b_silent=False):
        """

        :param input:
        :param target:
        :param test_input:
        :param test_target:
        :param b_use_conv:
        :param b_plot_results:
        :param b_silent:
        :return:
        """
        if b_use_conv:
            input = input.reshape(input.size(0), 1, input.size(1))
            test_input = test_input.reshape(test_input.size(0), 1, test_input.size(1))
            seq = PeakModelConv1d(nb_input=input.size(2)).float()
        else:
            seq = PeakModelMLP(nb_input=input.size(1), b_dropout=False).float()
        criterion = nn.BCELoss()
        # optimizer = optim.Adagrad(seq.parameters())
        optimizer = optim.Adam(seq.parameters())
        # optimizer = optim.LBFGS(seq.parameters())
        nb_epochs = 50

        # Create data Loader
        train_data = data_utils.TensorDataset(input, target)
        train_loader = data_utils.DataLoader(train_data, batch_size=256, shuffle=True)
        test_input = test_input.to(device)
        test_target = test_target.to(device)

        best_loss = np.PINF
        since_best = 0
        test_best_loss = np.PINF
        test_since_best = 0
        test_best_acc = 0
        test_since_best_acc = 0
        diff_loss = np.PINF
        pred_data = None
        test_acc = None
        # begin to train
        for at_epoch in range(nb_epochs):
            if not b_silent:
                print('Epoch: ', at_epoch)
            ep_loss = 0.0
            for at_batch, data in enumerate(train_loader):
                batch_input, batch_target = data
                batch_input = batch_input.to(device)
                batch_target = batch_target.to(device)

                optimizer.zero_grad()
                out = seq(batch_input)
                loss = criterion(out, batch_target)
                loss.backward()
                optimizer.step()
                ep_loss += loss.item()

            if ep_loss < best_loss:
                diff_loss = best_loss - ep_loss
                best_loss = ep_loss
                since_best = 0
            else:
                since_best += 1
            # begin to predict, no need to track gradient here
            with torch.no_grad():
                pred = seq(test_input)
                loss = criterion(pred, test_target)
                if not b_silent:
                    print('test loss:', loss.item())
                if loss.item() < test_best_loss:
                    test_best_loss = loss.item()
                    test_since_best = 0
                else:
                    test_since_best += 1
                pred_data = Tensor.cpu(pred).detach()[:, 0].numpy()

            correct = [1 if (pred_data[i] < 0.5 and test_target[i] == 0.0) or
                            (pred_data[i] >= 0.5 and test_target[i] == 1.0)
                       else 0 for i in range(len(test_target))]
            nb_correct = sum(correct)
            test_acc = nb_correct / len(test_target)
            if test_acc > test_best_acc:
                test_best_acc = test_acc
                test_since_best_acc = 0
            else:
                test_since_best_acc += 1

            if not b_silent:
                print("Accuracy: {}/{} [{:5.3f}%]".format(nb_correct, len(test_target), nb_correct / len(test_target)))

            if since_best >= 5:
                if not b_silent:
                    print("\nNo improvement on training since {} epochs; early stopping.".format(since_best))
                break
            if test_since_best >= 3:
                if not b_silent:
                    print("\nNo improvement on testing since {} epochs; early stopping.".format(test_since_best))
                break
            elif diff_loss < 0.00001:
                if not b_silent:
                    print("\nLoss improvement too small; early stopping.\n\tLoss: {}\n\tDiff: {}"
                          .format(best_loss, diff_loss))
                break

        if b_plot_results:
            self._print_model_performance(test_target, pred_data)

        return seq, test_acc

    @classmethod
    def _p_vs_r_analysis(cls, target, pred, b_silent=False):
        # Generate P vs R plot
        rec = []
        prec = []
        f1 = []

        xs = np.arange(1.0, -0.01, -0.01)
        for t in xs:
            pred_data_at_t = [1 if pred[i] >= t else 0 for i in range(len(pred))]
            tp = sum([pred_data_at_t[i] * target[i].item() for i in range(len(target))])
            # tn = sum([(1-pred_data_at_t[i]) * (1-test_target[i]) for i in range(len(test_target))])
            fp = sum([pred_data_at_t[i] * (1 - target[i].item()) for i in range(len(target))])
            fn = sum([(1 - pred_data_at_t[i]) * target[i].item() for i in range(len(target))])
            r = tp / (tp + fn) if (tp + fn) != 0 else 0
            p = tp / (tp + fp) if (tp + fp) != 0 else 0
            rec.append(r)
            prec.append(p)
            f1.append((2 * r * p) / (r + p) if (r + p) != 0 else 0)

        max_f1 = max(f1)
        max_t = xs[np.argmax(f1)]
        if not b_silent:
            best_p, best_r, best_s = 0, 0, 0
            for i in range(len(xs)):
                s = prec[i] * rec[i]
                if s > best_s:
                    best_p, best_r, best_s = prec[i], rec[i], s

            print("Max(f1) = {}, reached at t={}.".format(max_f1, max_t))

            plt.figure()

            ax_r = plt.subplot2grid((3, 5), (0, 0), colspan=3)
            ax_p = plt.subplot2grid((3, 5), (1, 0), colspan=3)
            ax_pvsr = plt.subplot2grid((3, 5), (0, 3), colspan=2, rowspan=2)
            ax_f1 = plt.subplot2grid((3, 5), (2, 0), colspan=5)

            plt.subplot(ax_r)
            plt.plot(np.arange(1.0, -0.01, -0.01), rec, '.')
            plt.title('Recall vs Threshold')
            plt.xlabel('Threshold')
            plt.ylabel('Recall')

            plt.subplot(ax_p, sharex=ax_r)
            plt.plot(np.arange(1.0, -0.01, -0.01), prec, '.')
            plt.title('Precision vs Threshold')
            plt.xlabel('Threshold')
            plt.ylabel('Precision')

            plt.subplot(ax_f1)
            plt.plot(np.arange(1.0, -0.01, -0.01), f1, '.')
            plt.plot([max_t], [max_f1], '.r')
            plt.title('F1 vs Threshold')
            plt.xlabel('Threshold')
            plt.ylabel('F1')

            p_vs_r = [(rec[i], prec[i]) for i in range(len(xs))]
            plt.subplot(ax_pvsr)
            plt.plot(*zip(*p_vs_r), '.')
            plt.plot(best_r, best_p, '.r')
            plt.title('Precision vs Recall')
            plt.xlabel('Recall')
            plt.ylabel('Precision')

            plt.tight_layout()
            plt.show()

        return max_f1, max_t


if __name__ == '__main__':
    pc = PeakEnsembleClassifier()
    pc.run(fft_size=256, nb_mlps=20, back_steps=3, n_vs_p=3, ens_n_vs_p=5, b_use_conv=False, b_use_extra_feats=True)

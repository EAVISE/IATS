import torch
import torch.nn as nn

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class PeakModelConv1d(nn.Module):
    def __init__(self, nb_input, nb_init_filters=8):
        super(PeakModelConv1d, self).__init__()
        self.flatten = Flatten().to(device)
        self.elu = nn.ELU().to(device)
        self.relu = nn.ReLU().to(device)
        self.sig = nn.Sigmoid().to(device)

        self.layer1 = nn.Sequential(
            nn.Conv1d(1, nb_init_filters, 3, padding=1).to(device),
            nn.Conv1d(nb_init_filters, nb_init_filters, 3, padding=1).to(device),
            nn.MaxPool1d(2).to(device))

        self.layer2 = nn.Sequential(
            nn.Conv1d(nb_init_filters, nb_init_filters*2, 3, padding=1).to(device),
            nn.Conv1d(nb_init_filters*2, nb_init_filters*2, 3, padding=1).to(device),
            nn.MaxPool1d(2).to(device))

        self.layer3 = nn.Sequential(
            nn.Conv1d(nb_init_filters*2, nb_init_filters*4, 3, padding=1).to(device),
            nn.Conv1d(nb_init_filters*4, nb_init_filters*4, 3, padding=1).to(device),
            nn.MaxPool1d(2).to(device))

        conv_out_size = (nb_input//8) * nb_init_filters * 4
        self.fc1 = nn.Linear(conv_out_size, conv_out_size).to(device)
        self.fc2 = nn.Linear(conv_out_size, conv_out_size//4).to(device)
        self.fc3 = nn.Linear(conv_out_size//4, 1).to(device)

    def forward(self, input):
        output = self.elu(self.layer1(input))
        output = self.elu(self.layer2(output))
        output = self.elu(self.layer3(output))
        output = self.flatten(output)
        output = self.sig(self.fc3(self.relu(self.fc2(self.relu(self.fc1(output))))))

        return output

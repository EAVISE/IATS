# First attempt at predicting the advent of PPOS peak_prediction.
# The idea is the following:
#   - define a peak value as a value that deviates from the mean with more than x*std.
#   - label as "True" those data points that are up to one hour 'in the past' of a peak value,
#     and all others as "False"; this means that a data point will be labeled "True" if at least one of the
#     following 4 data points is a peak, since the sampling rate is 15 minutes.
#   - since there are many more "False" than "True", randomly sample as many "False" as there are "True"
#   - as features, use FFT coefficients over window of length l, ending with the data point to be classified
import scipy.fftpack
from matplotlib import gridspec
from sklearn.preprocessing import StandardScaler

import matplotlib
import torch
from torch import nn, optim, Tensor

from iats.enervalis.peak_prediction.peak_model_mlp import PeakModelMLP

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import os

from iats.enervalis.data_elia import DataElia

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class PeakMLPClassifier:
    def __init__(self):
        home = os.path.expanduser("~")
        self.base_dir = os.path.join(home, "Work", "Code", "IATS-master")

    def run(self, peak_thres: float=3.0, fft_size: int=512):
        """

        :param peak_thres: number of std a value should deviate from the mean in order to be considered a peak value
        :param fft_size: size of the fft window
        :return:
        """
        # Step 1: load data and determine peak values
        elia = DataElia.load_data()
        ppos = elia['PPOS'].values
        ppos_mean = np.mean(ppos)
        ppos_std = np.std(ppos)

        thres_lo = ppos_mean - peak_thres*ppos_std
        thres_hi = ppos_mean + peak_thres*ppos_std
        # Extract location of peak values
        peak_loc = [loc for loc, e in enumerate(ppos) if e > thres_hi or e < thres_lo]

        # Step 2: generate True and False targets
        # Only look at start of a (possible) peak series
        true_loc = []
        for i, pos in enumerate(peak_loc):
            if i > 0 and (pos - peak_loc[i-1]) == 1:
                continue
            true_loc += list(range(pos-4, pos))

        temp = set(true_loc + peak_loc)
        false_loc = [i for i in range(len(ppos)) if i not in temp]

        # Visualle check the results
        # plt.figure()
        # plt.title('PPOS', fontsize=30)
        # plt.xlabel('Datetime', fontsize=20)
        # plt.ylabel('€/MWh', fontsize=20)
        #
        # plt.plot(peak_loc, ppos[peak_loc], '.r', linewidth=1.0)  # Peak values
        # plt.plot(true_loc, ppos[true_loc], '.g', linewidth=1.0)  # True values
        # plt.plot(false_loc, ppos[false_loc], '.b', linewidth=1.0)  # False values
        #
        # plt.tight_layout()
        # plt.show()
        # plt.close()

        # Step 3: randomly sample False samples
        n_vs_p = 3  # How many False for every True?
        nb_true = len([x for x in true_loc if x > fft_size])
        random_false = np.random.choice([x for x in false_loc if x > fft_size], n_vs_p*nb_true, replace=False)

        # Step 4: generate features and labels for samples
        input = np.zeros(((1+n_vs_p)*nb_true, fft_size))
        target = np.zeros(((1+n_vs_p)*nb_true, 1))
        values = np.zeros((1+n_vs_p)*nb_true)
        for i, loc in enumerate([x for x in true_loc if x > fft_size]):
            input[(1+n_vs_p)*i, :] = scipy.fftpack.rfft(ppos[loc-fft_size+1:loc+1])
            target[(1+n_vs_p)*i, :] = 1.0
            values[(1+n_vs_p)*i] = ppos[loc]
        for i, loc in enumerate(random_false):
            # Simple safety check
            if max(input[(1+n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :]) != 0.0:
                raise ValueError("Overwriting stuff that should not be overwritten")
            input[(1+n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :] = scipy.fftpack.rfft(ppos[loc-fft_size+1:loc+1])
            target[(1+n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p), :] = 0.0
            values[(1+n_vs_p) * (i // n_vs_p) + 1 + (i % n_vs_p)] = ppos[loc]
        # Keep 20% as test data
        split = int(input.shape[0]*0.8)
        input, test_input = input[:split], input[split:]
        target, test_target = target[:split], target[split:]
        # Scale input features
        scaler = StandardScaler()
        input = scaler.fit_transform(input)
        test_input = scaler.transform(test_input)
        # Convert to torch.Tensor
        input = torch.from_numpy(input).to(device).float()
        test_input = torch.from_numpy(test_input).to(device).float()
        target = torch.from_numpy(target).to(device).float()
        test_target = torch.from_numpy(test_target).to(device).float()

        # Step 5: train
        model = self._train(input, target, test_input, test_target)

        # Step 6: plot predicted probabilities vs PPOS
        input, test_input, target, test_target = None, None, None, None  # Free some memory
        input = np.zeros((len(ppos)-fft_size, fft_size))
        for i in range(0, len(ppos)-fft_size):
            input[i, :] = scipy.fftpack.rfft(ppos[i:i+fft_size])
        input = torch.from_numpy(scaler.transform(input)).to(device).float()
        full_pred = model(input).detach().cpu()[:, 0].numpy()

        plt.figure()
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        ax1 = plt.subplot(gs[0])
        plt.plot(ppos[fft_size:], '.b')
        plt.plot([x-fft_size for x in peak_loc], ppos[peak_loc], '.r')
        ax2 = plt.subplot(gs[1], sharex=ax1)
        plt.plot(full_pred, '.')
        plt.plot([x-fft_size for x in true_loc if x > fft_size], full_pred[[x-fft_size for x in true_loc if x > fft_size]], '.r')
        plt.show()

    def _train(self, input: torch.Tensor, target: torch.Tensor,
               test_input: torch.Tensor, test_target: torch.Tensor):
        """

        :param input:
        :param target:
        :param test_input:
        :param test_target:
        :return:
        """
        seq = PeakModelMLP(nb_input=input.size(1)).float()
        criterion = nn.BCELoss()
        optimizer = optim.Adagrad(seq.parameters())
        nb_epochs = 50

        best_loss = np.PINF
        since_best = 0
        test_best_loss = np.PINF
        test_since_best = 0
        test_best_acc = 0
        test_since_best_acc = 0
        diff_loss = np.PINF
        pred_data = None
        # begin to train
        for i in range(nb_epochs):
            print('STEP: ', i)

            def closure():
                optimizer.zero_grad()
                out = seq(input)
                loss = criterion(out, target)
                print('loss:', loss.item())
                loss.backward()
                return loss

            ep_loss = optimizer.step(closure).item()
            if ep_loss < best_loss:
                diff_loss = best_loss - ep_loss
                best_loss = ep_loss
                since_best = 0
            else:
                since_best += 1
            # begin to predict, no need to track gradient here
            with torch.no_grad():
                pred = seq(test_input)
                loss = criterion(pred, test_target)
                print('test loss:', loss.item())
                if loss.item() < test_best_loss:
                    test_best_loss = loss.item()
                    test_since_best = 0
                else:
                    test_since_best += 1
                pred_data = Tensor.cpu(pred).detach()[:, 0].numpy()

            correct = [1 if (pred_data[i] < 0.5 and test_target[i] == 0.0) or
                            (pred_data[i] >= 0.5 and test_target[i] == 1.0)
                       else 0 for i in range(len(test_target))]
            nb_correct = sum(correct)
            test_acc = nb_correct / len(test_target)
            if test_acc > test_best_acc:
                test_best_acc = test_acc
                test_since_best_acc = 0
            else:
                test_since_best_acc += 1

            print("Accuracy: {}/{} [{:5.3f}%]".format(nb_correct, len(test_target), nb_correct / len(test_target)))

            if since_best >= 5:
                print("\nNo improvement on training since {} epochs; early stopping.".format(since_best))
                break
            if test_since_best >= 3:
                print("\nNo improvement on testing since {} epochs; early stopping.".format(test_since_best))
                break
            elif diff_loss < 0.00001:
                print("\nLoss improvement too small; early stopping.\n\tLoss: {}\n\tDiff: {}"
                      .format(best_loss, diff_loss))
                break

        # Generate P vs R plot
        rec = []
        prec = []
        f1 = []
        xs = np.arange(1.0, -0.01, -0.01)
        for t in xs:
            pred_data_at_t = [1 if pred_data[i] >= t else 0 for i in range(len(pred_data))]
            tp = sum([pred_data_at_t[i] * test_target[i].item() for i in range(len(test_target))])
            # tn = sum([(1-pred_data_at_t[i]) * (1-test_target[i]) for i in range(len(test_target))])
            fp = sum([pred_data_at_t[i] * (1-test_target[i].item()) for i in range(len(test_target))])
            fn = sum([(1-pred_data_at_t[i]) * test_target[i].item() for i in range(len(test_target))])
            r = tp/(tp+fn) if (tp+fn) != 0 else 0
            p = tp/(tp+fp) if (tp+fp) != 0 else 0
            rec.append(r)
            prec.append(p)
            f1.append((2*r*p)/(r+p) if (r+p) != 0 else 0)

        max_f1 = max(f1)
        max_t = xs[np.argmax(f1)]
        print("Max(f1) = {}, reached at t={}.".format(max_f1, max_t))

        plt.figure()

        ax_r = plt.subplot2grid((3, 5), (0, 0), colspan=3)
        ax_p = plt.subplot2grid((3, 5), (1, 0), colspan=3)
        ax_pvsr = plt.subplot2grid((3, 5), (0, 3), colspan=2, rowspan=2)
        ax_f1 = plt.subplot2grid((3, 5), (2, 0), colspan=5)

        plt.subplot(ax_r)
        plt.plot(np.arange(1.0, -0.01, -0.01), rec, '.')
        plt.title('Recall vs Threshold')
        plt.xlabel('Threshold')
        plt.ylabel('Recall')

        plt.subplot(ax_p, sharex=ax_r)
        plt.plot(np.arange(1.0, -0.01, -0.01), prec, '.')
        plt.title('Precision vs Threshold')
        plt.xlabel('Threshold')
        plt.ylabel('Precision')

        plt.subplot(ax_f1)
        plt.plot(np.arange(1.0, -0.01, -0.01), f1, '.')
        plt.plot([max_t], [max_f1], '.r')
        plt.title('F1 vs Threshold')
        plt.xlabel('Threshold')
        plt.ylabel('F1')

        p_vs_r = [(rec[i], prec[i]) for i in range(len(prec))]
        plt.subplot(ax_pvsr)
        plt.plot(*zip(*p_vs_r), '.')
        plt.title('Precision vs Recall')
        plt.xlabel('Recall')
        plt.ylabel('Precision')

        plt.tight_layout()
        plt.show()

        return seq


if __name__ == '__main__':
    pc = PeakMLPClassifier()
    pc.run(fft_size=128)

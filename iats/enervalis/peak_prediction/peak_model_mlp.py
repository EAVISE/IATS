import torch
import torch.nn as nn

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class PeakModelMLP(nn.Module):
    def __init__(self, nb_input, b_dropout=True):
        super(PeakModelMLP, self).__init__()
        self.b_dropout = b_dropout

        self.relu = nn.ReLU().to(device)
        self.sig = nn.Sigmoid().to(device)

        self.l1 = nn.Linear(nb_input, 2*nb_input).to(device)
        self.d1 = nn.Dropout(p=0.5)
        self.l2 = nn.Linear(2*nb_input, nb_input).to(device)
        self.d2 = nn.Dropout(p=0.5)
        self.l3 = nn.Linear(nb_input, nb_input//4).to(device)
        self.d3 = nn.Dropout(p=0.25)
        self.l4 = nn.Linear(nb_input//4, 1).to(device)

    def forward(self, input):
        output = self.relu(self.l1(input))
        if self.b_dropout:
            output = self.d1(output)
        output = self.relu(self.l2(output))
        if self.b_dropout:
            output = self.d2(output)
        output = self.relu(self.l3(output))
        if self.b_dropout:
            output = self.d3(output)
        output = self.sig(self.l4(self.relu(output)))

        return output

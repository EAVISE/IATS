import datetime
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch import Tensor

from iats.enervalis.data_elia import DataElia
from iats.enervalis.entsoe.entsoe_dataloader import ENTSOEDataLoader
# from iats.enervalis.models.convgru import ConvGRU
# from iats.enervalis.models.convlstm import ConvLSTM
# from iats.enervalis.models.lstm_ppos import LSTMPPOS
# from iats.enervalis.models.lstm_updown import LSTMUpDn
from iats.enervalis.models.mlp import MLP
from iats.utils.tools import Tools

matplotlib.use('Qt5Agg')
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

if __name__ == '__main__':
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Code", "IATS-master")
    print("Using cuda? {}".format(use_cuda))

    b_add_time_data = True
    b_binary_targets = True
    # set random seed to 0
    np.random.seed(0)
    torch.manual_seed(0)
    # load data and make training set
    # data = torch.load('traindata.pt')
    elia = DataElia.load_data()
    entsoe = ENTSOEDataLoader.load_file("Total_Load_-_Day_Ahead_Actual.dill", convert_to_elia=False)

    months_train = [4, 5, 6, 7, 8, 9]
    months_test = [10]

    # Train data
    df_elia_2015_train = elia.loc[(elia['Year'] == 2015) & (elia['Month'].isin(months_train))]
    df_entsoe_2015_train = entsoe.loc[(entsoe['Year'] == 2015) & (entsoe['Month'].isin(months_train))]

    si_data_train = df_elia_2015_train['SI'].values
    si_data_train, si_m, si_std = Tools.normalize_data(si_data_train, b_return_params=True)
    si_data_train_alt = si_data_train.reshape(len(si_data_train), 1)
    alpha_data_train = df_elia_2015_train['Alpha'].values
    alpha_data_train, alpha_m, alpha_std = Tools.normalize_data(alpha_data_train, b_return_params=True)
    alpha_data_train_alt = alpha_data_train.reshape(len(alpha_data_train), 1)
    nrv_data_train = df_elia_2015_train['NRV'].values
    nrv_data_train, nrv_m, nrv_std = Tools.normalize_data(nrv_data_train, b_return_params=True)
    nrv_data_train_alt = nrv_data_train.reshape(len(nrv_data_train), 1)
    ppos_data_train = df_elia_2015_train['PPOS'].values
    ppos_data_train, ppos_m, ppos_std = Tools.normalize_data(ppos_data_train, b_return_params=True)
    ppos_data_train_alt = ppos_data_train.reshape(len(ppos_data_train), 1)
    curr_bal_train = df_entsoe_2015_train['Actual Total Load [MW] - BZN|BE'].fillna(method='ffill').values
    curr_bal_train, curr_bal_m, curr_bal_std = Tools.normalize_data(curr_bal_train, b_return_params=True)
    curr_bal_train_alt = curr_bal_train.reshape(len(curr_bal_train), 1)
    nday_bal_train = df_entsoe_2015_train['Day-ahead Total Load Forecast [MW] - BZN|BE'].fillna(method='ffill').values
    nday_bal_train, nday_bal_m, nday_bal_std = Tools.normalize_data(nday_bal_train, b_return_params=True)
    nday_bal_train_alt = nday_bal_train.reshape(len(nday_bal_train), 1)

    # Add one-hot encoded vectors for hour and day ot week
    if b_add_time_data:
        weekday_train = torch.from_numpy(Tools.to_one_hot(df_elia_2015_train.apply(
            lambda x: datetime.date(x['Year'], x['Month'], x['Day']).isoweekday(), axis=1).values)).to(device)
        hour_data_train = torch.from_numpy(Tools.to_one_hot(
            df_elia_2015_train['Time'].map(lambda x: x.hour).values)).to(device)

    # Test data
    df_elia_2015_test = elia.loc[(elia['Year'] == 2015) & (elia['Month'].isin(months_test))]
    df_entsoe_2015_test = entsoe.loc[(entsoe['Year'] == 2015) & (entsoe['Month'].isin(months_test))]

    si_data_test = df_elia_2015_test['SI'].values
    si_data_test = Tools.normalize_data(si_data_test, mean=si_m, std=si_std)
    si_data_test_alt = si_data_test.reshape(len(si_data_test), 1)
    alpha_data_test = df_elia_2015_test['Alpha'].values
    alpha_data_test = Tools.normalize_data(alpha_data_test, mean=alpha_m, std=alpha_std)
    alpha_data_test_alt = alpha_data_test.reshape(len(alpha_data_test), 1)
    nrv_data_test = df_elia_2015_test['NRV'].values
    nrv_data_test = Tools.normalize_data(nrv_data_test, mean=nrv_m, std=nrv_std)
    nrv_data_test_alt = nrv_data_test.reshape(len(nrv_data_test), 1)
    ppos_data_test = df_elia_2015_test['PPOS'].values
    ppos_data_test = Tools.normalize_data(ppos_data_test, mean=ppos_m, std=ppos_std)
    ppos_data_test_alt = ppos_data_test.reshape(len(ppos_data_test), 1)
    curr_bal_test = df_entsoe_2015_test['Actual Total Load [MW] - BZN|BE'].fillna(method='ffill').values
    curr_bal_test = Tools.normalize_data(curr_bal_test, mean=curr_bal_m, std=curr_bal_std)
    curr_bal_test_alt = curr_bal_test.reshape(len(curr_bal_test), 1)
    nday_bal_test = df_entsoe_2015_test['Day-ahead Total Load Forecast [MW] - BZN|BE'].fillna(method='ffill').values
    nday_bal_test = Tools.normalize_data(nday_bal_test, mean=nday_bal_m, std=nday_bal_std)
    nday_bal_test_alt = nday_bal_test.reshape(len(nday_bal_test), 1)

    if b_add_time_data:
        weekday_test = torch.from_numpy(Tools.to_one_hot(df_elia_2015_test.apply(
            lambda x: datetime.date(x['Year'], x['Month'], x['Day']).isoweekday(), axis=1).values)).to(device)
        hour_data_test = torch.from_numpy(Tools.to_one_hot(
            df_elia_2015_test['Time'].map(lambda x: x.hour).values)).to(device)

    feat_len = 1
    ws_si = torch.from_numpy(si_data_train_alt[:-1]).to(device)
    ws_alpha = torch.from_numpy(alpha_data_train_alt[:-1]).to(device)
    ws_nrv = torch.from_numpy(nrv_data_train_alt[:-1]).to(device)
    ws_ppos = torch.from_numpy(ppos_data_train_alt[:-1]).to(device)
    ws_curr_bal = torch.from_numpy(curr_bal_train_alt[:-1]).to(device)
    ws_nday_bal = torch.from_numpy(nday_bal_train_alt[:-1]).to(device)
    to_cat = (
        # ws_si,
        # ws_alpha,
        # ws_nrv,
        ws_ppos,
        ws_curr_bal,
        # ws_nday_bal,
        weekday_train[:-1],
        hour_data_train[:-1],
    )

    input = torch.cat(to_cat, dim=1).float()
    ws_si, ws_alpha, ws_nrv, ws_ppos = None, None, None, None
    if b_binary_targets:
        target = torch.empty(len(ppos_data_train) - feat_len, 1, dtype=torch.float).to(device)
        for i in range(target.shape[0]):
            target[i][0] = 1 if ppos_data_train[feat_len + i] > ppos_data_train[feat_len + i - 1] else 0
    else:
        target = torch.from_numpy(ppos_data_train_alt[1:]).float().to(device)

    ws_si = torch.from_numpy(si_data_test_alt[:-1]).to(device)
    ws_alpha = torch.from_numpy(alpha_data_test_alt[:-1]).to(device)
    ws_nrv = torch.from_numpy(nrv_data_test_alt[:-1]).to(device)
    ws_ppos = torch.from_numpy(ppos_data_test_alt[:-1]).to(device)
    ws_curr_bal = torch.from_numpy(curr_bal_test_alt[:-1]).to(device)
    ws_nday_bal = torch.from_numpy(nday_bal_test_alt[:-1]).to(device)
    to_cat = (
        # ws_si,
        # ws_alpha,
        # ws_nrv,
        ws_ppos,
        ws_curr_bal,
        # ws_nday_bal,
        weekday_test[:-1],
        hour_data_test[:-1],
    )

    test_input = torch.cat(to_cat, dim=1).float()
    if b_binary_targets:
        test_target = torch.empty(len(ppos_data_test) - feat_len, 1, dtype=torch.float).to(device)
        for i in range(test_target.shape[0]):
            test_target[i][0] = 1 if ppos_data_test[feat_len + i] > ppos_data_test[feat_len + i - 1] else 0
    else:
        test_target = torch.from_numpy(ppos_data_test_alt[1:]).float().to(device)

    # build the model
    # Layer conf: [(blocks, el. per block, hidden size for block),...]
    # seq = ConvGRU(dim_input=input.size(1), layer_conf=[(1, 1, 128)], b_binary=True).float()
    seq = MLP(nb_input=input.size(1), b_binary=True).float()
    nb_pos = sum(target).item()
    # criterion = nn.MSELoss()
    # criterion = nn.BCELoss()
    criterion = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([(target.shape[0] - nb_pos) / nb_pos]).to(device))
    # criterion = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([0.6]).to(device))

    # use LBFGS as optimizer since we can load the whole data to train
    # optimizer = optim.LBFGS(seq.parameters(), lr=0.8)
    optimizer = optim.Adagrad(seq.parameters())
    nb_epochs = 9999

    best_loss = np.PINF
    since_best = 0
    test_best_loss = np.PINF
    test_since_best = 0
    diff_loss = np.PINF
    # begin to train
    for i in range(nb_epochs):
        print('STEP: ', i)


        def closure():
            optimizer.zero_grad()
            out = seq(input)
            loss = criterion(out, target)
            print('loss:', loss.item())
            loss.backward()
            return loss


        ep_loss = optimizer.step(closure).item()
        if ep_loss < best_loss:
            diff_loss = best_loss - ep_loss
            best_loss = ep_loss
            since_best = 0
        else:
            since_best += 1
        # begin to predict, no need to track gradient here
        with torch.no_grad():
            future = 0
            pred = seq(test_input, future=future)
            if future:
                loss = criterion(pred[:, :-future], test_target)
            else:
                loss = criterion(pred, test_target)
            print('test loss:', loss.item())
            if loss.item() < test_best_loss:
                test_best_loss = loss.item()
                test_since_best = 0
            else:
                test_since_best += 1
            y = Tensor.cpu(pred).detach().numpy()

        if since_best > 20:
            print("No improvement on training since {} epochs; early stopping.".format(since_best))
            break
        if test_since_best > 100:
            print("No improvement on testing since {} epochs; early stopping.".format(test_since_best))
            break
        elif diff_loss < 0.000001:
            print("Loss improvement too small; early stopping.\n\tLoss: {}\n\tDiff: {}"
                  .format(best_loss, diff_loss))
            break
        plt_range = 500
        ref_data = ppos_data_test[-plt_range - 1:-1]
        tar_data = Tensor.cpu(test_target).detach().numpy()[-plt_range:, 0]
        pred_data = y[-plt_range:, 0]
        if b_binary_targets:
            updn_pred = [1 if pred_data[i] > 0.5 else 0 for i in range(len(pred_data) - 1)]
        else:
            updn_pred = [1 if pred_data[i] > ref_data[i] else 0 for i in range(len(pred_data) - 1)]
        correct = [1 if ref_data[i + 1] > ref_data[i] else 0 for i in range(len(ref_data) - 1)]

        nb_corr_pred = sum([1 if updn_pred[i] == correct[i] else 0 for i in range(len(correct))])
        print("Accuracy: {}/{} [{:5.3f}%]".format(nb_corr_pred, len(correct), nb_corr_pred / len(correct)))
        print("Baseline: {}/{} [{:5.3f}%]".format(sum(correct), len(correct), sum(correct) / len(correct)))
        print("Since best/ Test since best: {} - {}".format(since_best, test_since_best))

    # draw the result
    plt.figure(figsize=(30, 10))
    plt.title('Predict future values for time sequences\n(Dashlines are predicted values)', fontsize=30)
    plt.xlabel('x', fontsize=20)
    plt.ylabel('y', fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)

    plt.plot(np.arange(plt_range), ref_data, '*b', linewidth=4.0)
    plt.plot(np.arange(plt_range), tar_data, '+g', linewidth=2.0)
    plt.plot(np.arange(plt_range), pred_data, '.r', linewidth=4.0)
    # plt.plot(np.arange(plt_range-1), correct, '.g', linewidth=1.0)
    # plt.savefig(os.path.join(BASE_DIR, 'Plots', 'predict%d.pdf' % i))
    plt.show()
    plt.close()

    # pred = Tensor.cpu(seq(input, future=future)).detach().numpy()
    # target = Tensor.cpu(target).detach().numpy()
    # plt.plot(np.arange(plt_range), target[:plt_range, 0], '+b')
    # plt.plot(np.arange(plt_range), pred[:plt_range, 0], '.r')
    # plt.show()
    # plt.close()

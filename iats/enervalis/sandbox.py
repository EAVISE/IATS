# This script is used as a sandbox to experiment in.
import os

import dill
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from iats.enervalis.data_elia import DataElia
from iats.enervalis.data_kmi import DataKMI
from iats.utils.tools import Tools

matplotlib.use('Qt5Agg')


def main():
    # (A tiny bit) more info at: http://www.elia.be/en/grid-data/balancing/imbalance-prices
    # Available years: 2015, 2016, 2017
    elia = DataElia.load_data()
    kmi = DataKMI.load_data()
    tl = dill.load(open(os.path.join("D:\Work\Data\ENTSO-E", 'Total_Load_-_Day_Ahead_Actual.dill'), 'rb'))
    tl_2015 = tl.loc[tl['Year'] == 2015]
    df_2015 = elia.loc[elia['Year'] == 2015]
    df_2016 = elia.loc[elia['Year'] == 2016]
    # Columns are: Year Month Day Time NRV SI Alpha MIP MDP PPOS PNEG
    # plt.plot(df_2015['NRV'], 'bo', df_2016['NRV'], 'r+')
    params = ('SI', 'PPOS')
    si_data = df_2015['SI'].values
    ppos_data = df_2015['PPOS'].values
    plt_len = len(ppos_data)
    d_mean = np.mean(ppos_data)
    d_std = np.std(ppos_data)
    print("{} +- {}".format(d_mean, d_std))
    si_norm = Tools.normalize_data(si_data)
    ppos_norm = Tools.normalize_data(ppos_data)
    tl_norm = Tools.normalize_data(tl_2015.iloc[:, 5].fillna(method="ffill"))
    print(Tools.corr_norm(si_norm[15000:25000], tl_norm[15000:25000]))
    b_plot = True
    if b_plot:
        plt.plot(si_norm, '.')
        plt.plot(ppos_norm)
        plt.plot(tl_norm)
        # plt.plot([d_mean + 3 * d_std] * plt_len)
        # plt.plot([d_mean - 3 * d_std] * plt_len)
        plt.legend(['2015 {}'.format(params[0]), '2015 {}'.format(params[1]), '2015 TL'])
        plt.show()
    print("Wiih!")


if __name__ == '__main__':
    main()

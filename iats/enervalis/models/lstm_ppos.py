import torch
import torch.nn as nn

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class LSTMPPOS(nn.Module):
    HIDDEN_SIZE = 128

    def __init__(self, nb_input):
        super(LSTMPPOS, self).__init__()
        self.lstm1 = nn.LSTMCell(nb_input, self.HIDDEN_SIZE).to(device)
        self.lstm2 = nn.LSTMCell(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.lstm3 = nn.LSTMCell(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.lstm4 = nn.LSTMCell(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.lstm5 = nn.LSTMCell(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.linear1 = nn.Linear(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.act1 = nn.Tanh()
        self.linear2 = nn.Linear(self.HIDDEN_SIZE, self.HIDDEN_SIZE//4).to(device)
        self.act2 = nn.Tanh()
        self.linear3 = nn.Linear(self.HIDDEN_SIZE//4, 1).to(device)
        # self.sig = nn.Sigmoid().to(device)

    def forward(self, input, future=0):
        outputs = []
        h_t = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        c_t = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        h_t2 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        c_t2 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        h_t3 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        c_t3 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        h_t4 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        c_t4 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        h_t5 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)
        c_t5 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.float).to(device)

        for i, input_t in enumerate(input.chunk(1, dim=1)):
            h_t, c_t = self.lstm1(input_t, (h_t, c_t))
            h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
            h_t3, c_t3 = self.lstm3(h_t2, (h_t3, c_t3))
            h_t4, c_t4 = self.lstm4(h_t3, (h_t4, c_t4))
            h_t5, c_t5 = self.lstm5(h_t4, (h_t5, c_t5))
            output = self.linear3(self.act2(self.linear2(self.act1(self.linear1(h_t5)))))
            outputs += [output]
        if future:
            input_t = input
            output_new = output
            for i in range(future):  # if we should predict the future
                input_t = torch.cat([input_t, output_new], dim=1)[:, 1:]
                h_t, c_t = self.lstm1(input_t, (h_t, c_t))
                h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
                h_t3, c_t3 = self.lstm3(h_t2, (h_t3, c_t3))
                h_t4, c_t4 = self.lstm4(h_t3, (h_t4, c_t4))
                h_t5, c_t5 = self.lstm5(h_t4, (h_t5, c_t5))
                output_new = self.linear3(self.act2(self.linear2(self.act1(self.linear1(h_t5)))))
                outputs += [output_new]
        outputs = torch.stack(outputs, 1).squeeze(2)

        return outputs

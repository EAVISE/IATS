import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Module

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class HardBinary(Module):
    """
    f(x) =
        lo if x < flip_value
        hi if x >= flip_value

    """
    def __init__(self, flip_value=0.5, lo=0.0, hi=1.0, inplace=False):
        super(HardBinary, self).__init__()
        self.flip_value = flip_value
        self.lo = lo
        self.hi = hi
        self.inplace = inplace
        # TODO: check in THNN (if inplace == True, then assert value <= threshold)

    def forward(self, input):
        t = F.threshold(torch.add(input, -0.5), self.flip_value-0.5, self.lo-0.5, self.inplace)
        t = torch.add(torch.mul(torch.add(t, 0.5), -1.0), 0.5)
        t = F.threshold(t, self.flip_value-0.5, -(self.hi-0.5), self.inplace)
        t = torch.add(torch.mul(t, -1.0), 0.5)
        return t

    def extra_repr(self):
        inplace_str = ', inplace' if self.inplace else ''
        return 'threshold={}, value={}{}'.format(
            self.threshold, self.value, inplace_str
        )


class ConvGRU(nn.Module):
    def __init__(self, dim_input, layer_conf: [], b_binary=False):
        super(ConvGRU, self).__init__()
        self.b_binary = b_binary

        self.elu = nn.ELU().to(device)
        self.relu = nn.ReLU().to(device)
        self.tanh = nn.Tanh().to(device)
        self.sig = nn.Sigmoid().to(device)
        self.hardbinary = HardBinary().to(device)

        self.sizes = []
        self.layers = []
        self.linears = []
        nb_towers = 0
        prev_dim = -1
        for i, tpl in enumerate(layer_conf):
            nb_blocks, block_depth, block_size = tpl
            self.sizes.append(block_size)
            if i > 0:
                linears = []
                for j in range(nb_towers):
                    linears.append(nn.Linear(prev_dim, block_size).to(device))
                self.linears.append(linears)

            towers = []
            nb_towers = 1 if i == 0 else 2*i
            for tower in range(nb_towers):
                tower = []
                for at_block in range(nb_blocks):
                    block = []
                    for at_cell in range(block_depth):
                        block.append(nn.GRUCell(dim_input if i == 0 and at_block == 0 and at_cell == 0 else block_size,
                                                block_size).to(device))
                    tower.append(block)
                towers.append(tower)
            self.layers.append(towers)
            prev_dim = block_size
        # nb_towers is still at last value
        final_size = self.sizes[-1]*nb_towers
        self.final1 = nn.Linear(final_size, final_size//4).to(device)
        self.final2 = nn.Linear(final_size//4, 1).to(device)

    def forward(self, input, future=0):
        h_ts = []
        for at_layer in range(len(self.layers)):
            layer_h = []
            for at_tower in range(len(self.layers[at_layer])):
                layer_h.append(torch.zeros(input.size(0), self.sizes[at_layer], dtype=torch.float).to(device))
            h_ts.append(layer_h)

        inputs = [input]
        for at_layer, layer in enumerate(self.layers):
            tower_outputs = []
            for at_tower, tower in enumerate(layer):
                h_t = h_ts[at_layer][at_tower]
                local_input = inputs[at_layer//2]
                for at_block, block in enumerate(tower):
                    for at_cell, cell in enumerate(block):
                        h_t = cell(local_input if at_block + at_cell == 0 else h_t, h_t)
                    h_t = self.elu(h_t)
                if at_layer < len(self.layers)-1:
                    h_t = self.linears[at_layer][at_tower](h_t)
                tower_outputs.append(h_t)
            inputs = tower_outputs
        flat = torch.cat(inputs, dim=1)
        output = self.final2(self.tanh(self.final1(flat)))
        if self.b_binary:
            output = self.sig(output)

        # outputs = [output]

        # if future:
        #     input_t = input
        #     output_new = output
        #     for i in range(future):  # if we should predict the future
        #         input_t = torch.cat([input_t, output_new], dim=1)[:, 1:]
        #         h_t, c_t = self.lstm1(input_t, (h_t, c_t))
        #         h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
        #         h_t3, c_t3 = self.lstm3(h_t2, (h_t3, c_t3))
        #         h_t4, c_t4 = self.lstm4(h_t3, (h_t4, c_t4))
        #         h_t5, c_t5 = self.lstm5(h_t4, (h_t5, c_t5))
        #         output_new = self.linear3(self.act2(self.linear2(self.act1(self.linear1(h_t5)))))
        #         outputs += [output_new]
        # outputs = torch.stack(outputs, 1).squeeze(2)

        return output

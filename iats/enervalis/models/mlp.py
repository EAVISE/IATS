import torch
import torch.nn as nn

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class MLP(nn.Module):
    def __init__(self, nb_input, b_binary=False):
        super(MLP, self).__init__()
        self.b_binary = b_binary

        self.relu = nn.ReLU().to(device)
        self.elu = nn.ELU().to(device)
        self.sig = nn.Sigmoid().to(device)

        self.l1_1 = nn.Linear(nb_input, 128).to(device)
        self.l1_2 = nn.Linear(128, 128).to(device)
        self.l1_3 = nn.Linear(128, 128).to(device)
        self.gru1_1 = nn.GRUCell(128, 128).to(device)
        self.gru1_2 = nn.GRUCell(128, 128).to(device)
        self.l2 = nn.Linear(128, 64).to(device)
        self.gru2 = nn.GRUCell(64, 64).to(device)
        self.l3 = nn.Linear(64, 16).to(device)
        self.l4 = nn.Linear(16, 1).to(device)

    def forward(self, input, future=0):
        h_t1 = torch.zeros(input.size(0), 128, dtype=torch.float).to(device)
        h_t2 = torch.zeros(input.size(0), 64, dtype=torch.float).to(device)

        temp1 = self.l1_3(self.elu(self.l1_2(self.elu(self.l1_1(input)))))
        h_t1 = temp1
        # h_t1 = self.gru1_1(temp1, h_t1)
        # h_t1 = self.gru1_2(self.elu(h_t1), h_t1)
        temp2 = self.relu(self.l2(h_t1))
        h_t2 = temp2
        # h_t2 = self.gru2(temp2, h_t2)

        output = self.l4(self.relu(self.l3(h_t2)))
        if self.b_binary:
            output = self.sig(output)

        return output

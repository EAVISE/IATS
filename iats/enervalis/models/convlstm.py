# LSTM that predicts the next PPOS tick will be higher or lower than the current one.
import torch
import torch.nn as nn

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class ConvLSTM(nn.Module):
    def __init__(self, dim_input, layer_conf: []):
        super(ConvLSTM, self).__init__()
        self.elu = nn.ELU().to(device)
        self.relu = nn.ReLU().to(device)
        self.tanh = nn.Tanh().to(device)
        self.sig = nn.Sigmoid().to(device)

        self.sizes = []
        self.layers = []
        self.linears = []
        nb_towers = 0
        prev_dim = -1
        for i, tpl in enumerate(layer_conf):
            nb_blocks, block_depth, block_size = tpl
            self.sizes.append(block_size)
            if i > 0:
                linears = []
                for j in range(nb_towers):
                    linears.append(nn.Linear(prev_dim, block_size).to(device))
                self.linears.append(linears)

            towers = []
            nb_towers = 1 if i == 0 else 2*i
            for tower in range(nb_towers):
                tower = []
                for at_block in range(nb_blocks):
                    block = []
                    for at_cell in range(block_depth):
                        block.append(nn.LSTMCell(dim_input if i == 0 and at_block == 0 and at_cell == 0 else block_size,
                                                 block_size).to(device))
                    tower.append(block)
                towers.append(tower)
            self.layers.append(towers)
            prev_dim = block_size
        # nb_towers is still at last value
        final_size = self.sizes[-1]*nb_towers
        self.final1 = nn.Linear(final_size, final_size//4).to(device)
        self.final2 = nn.Linear(final_size//4, 1).to(device)

    def forward(self, input, future=0):
        h_ts, c_ts = [], []
        for at_layer in range(len(self.layers)):
            layer_h, layer_c = [], []
            for at_tower in range(len(self.layers[at_layer])):
                layer_h.append(torch.zeros(input.size(0), self.sizes[at_layer], dtype=torch.float).to(device))
                layer_c.append(torch.zeros(input.size(0), self.sizes[at_layer], dtype=torch.float).to(device))
            h_ts.append(layer_h)
            c_ts.append(layer_c)

        inputs = [input]
        for at_layer, layer in enumerate(self.layers):
            tower_outputs = []
            for at_tower, tower in enumerate(layer):
                local_input = inputs[at_layer//2]
                h_t = h_ts[at_layer][at_tower]
                c_t = c_ts[at_layer][at_tower]
                for at_block, block in enumerate(tower):
                    for at_cell, cell in enumerate(block):
                        h_t, c_t = cell(local_input if at_block + at_cell == 0 else h_t, (h_t, c_t))
                    h_t = self.elu(h_t)
                if at_layer < len(self.layers)-1:
                    h_t = self.linears[at_layer][at_tower](h_t)
                tower_outputs.append(h_t)
            inputs = tower_outputs
        flat = torch.cat(inputs, dim=1)
        output = self.final2(self.tanh(self.final1(flat)))

        return output

# LSTM that predicts the next PPOS tick will be higher or lower than the current one.
# An attempt by me, Laurent Mertens, to program an LSTM using PyTorch to predict the PPOS from the Elia data.
# Taken and adapted from: https://github.com/pytorch/examples/tree/master/time_sequence_prediction
import torch
import torch.nn as nn


use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")


class ConvLSTM2(nn.Module):
    def __init__(self, dim_input: int, hidden_size=128):
        super(ConvLSTM2, self).__init__()
        self.HIDDEN_SIZE = hidden_size

        self.elu = nn.ELU().to(device)
        self.relu = nn.ReLU().to(device)
        self.tanh = nn.Tanh().to(device)
        self.sig = nn.Sigmoid().to(device)

        self.lstm1_1 = nn.LSTMCell(dim_input, self.HIDDEN_SIZE).to(device)
        self.lstm1_2 = nn.LSTMCell(self.HIDDEN_SIZE, self.HIDDEN_SIZE).to(device)
        self.linear1 = nn.Linear(self.HIDDEN_SIZE, self.HIDDEN_SIZE//2)

        self.lstm2_1_1 = nn.LSTMCell(self.HIDDEN_SIZE//2, self.HIDDEN_SIZE//2).to(device)
        self.lstm2_1_2 = nn.LSTMCell(self.HIDDEN_SIZE//2, self.HIDDEN_SIZE//2).to(device)

        self.lstm2_2_1 = nn.LSTMCell(self.HIDDEN_SIZE//2, self.HIDDEN_SIZE//2).to(device)
        self.lstm2_2_2 = nn.LSTMCell(self.HIDDEN_SIZE//2, self.HIDDEN_SIZE//2).to(device)

        self.linear2 = nn.Linear(self.HIDDEN_SIZE, 1)

    def forward(self, input, future=0):
        outputs = []
        h_t1 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.double).to(device)
        c_t1 = torch.zeros(input.size(0), self.HIDDEN_SIZE, dtype=torch.double).to(device)
        h_t21 = torch.zeros(input.size(0), self.HIDDEN_SIZE//2, dtype=torch.double).to(device)
        c_t21 = torch.zeros(input.size(0), self.HIDDEN_SIZE//2, dtype=torch.double).to(device)
        h_t22 = torch.zeros(input.size(0), self.HIDDEN_SIZE//2, dtype=torch.double).to(device)
        c_t22 = torch.zeros(input.size(0), self.HIDDEN_SIZE//2, dtype=torch.double).to(device)

        for i, input_t in enumerate(input.chunk(1, dim=1)):
            h_t1, c_t1 = self.lstm1_1(input_t, (h_t1, c_t1))
            h_t1, c_t1 = self.lstm1_2(h_t1, (h_t1, c_t1))
            h_t1 = self.elu(h_t1)

            input2 = self.linear1(h_t1)

            h_t21, c_t21 = self.lstm2_1_1(input2, (h_t21, c_t21))
            h_t21, c_t21 = self.lstm2_1_2(h_t21, (h_t21, c_t21))
            h_t21 = self.elu(h_t21)

            h_t22, c_t22 = self.lstm2_2_1(input2, (h_t22, c_t22))
            h_t22, c_t22 = self.lstm2_2_2(h_t22, (h_t22, c_t22))
            h_t22 = self.elu(h_t22)

            input3 = torch.cat([h_t21, h_t22], dim=1)
            output = self.linear2(input3)
            output = self.sig(output)
            outputs += [output]

        # if future:
        #     input_t = input
        #     output_new = output
        #     for i in range(future):  # if we should predict the future
        #         input_t = torch.cat([input_t, output_new], dim=1)[:, 1:]
        #         h_t, c_t = self.lstm1(input_t, (h_t, c_t))
        #         h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
        #         h_t3, c_t3 = self.lstm3(h_t2, (h_t3, c_t3))
        #         h_t4, c_t4 = self.lstm4(h_t3, (h_t4, c_t4))
        #         h_t5, c_t5 = self.lstm5(h_t4, (h_t5, c_t5))
        #         output_new = self.linear3(self.act2(self.linear2(self.act1(self.linear1(h_t5)))))
        #         outputs += [output_new]
        outputs = torch.stack(outputs, 1).squeeze(2)

        return outputs

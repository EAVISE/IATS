import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

from iats.enervalis.data_elia import DataElia
from iats.utils.tools import Tools

matplotlib.use('Qt5Agg')


if __name__ == '__main__':
    HOME = os.path.expanduser("~")
    BASE_DIR = os.path.join(HOME, "Work", "Code", "IATS-master")

    # load data and make training set
    # data = torch.load('traindata.pt')
    elia = DataElia.load_data()

    # Train data
    df_elia_2015_train = elia.loc[(elia['Year'] == 2015) & (elia['Month'] == 1)]

    # si_data_train = df_2015_train['SI'].values
    # si_data_train, si_m, si_std = Tools.normalize_data(si_data_train, b_return_params=True)
    # si_data_train_alt = si_data_train.reshape(len(si_data_train), 1)
    # alpha_data_train = df_2015_train['Alpha'].values
    # alpha_data_train, alpha_m, alpha_std = Tools.normalize_data(alpha_data_train, b_return_params=True)
    # alpha_data_train_alt = alpha_data_train.reshape(len(alpha_data_train), 1)
    # nrv_data_train = df_2015_train['NRV'].values
    # nrv_data_train, nrv_m, nrv_std = Tools.normalize_data(nrv_data_train, b_return_params=True)
    # nrv_data_train_alt = nrv_data_train.reshape(len(nrv_data_train), 1)
    ppos_data_train = df_elia_2015_train['PPOS'].values
    ppos_data_train, ppos_m, ppos_std = Tools.normalize_data(ppos_data_train, b_return_params=True)
    ppos_data_train_alt = ppos_data_train.reshape(len(ppos_data_train), 1)

    # Jamming on https://stackoverflow.com/questions/25735153/plotting-a-fast-fourier-transform-in-python#25735274
    # Number of samplepoints
    N = 2048
    # sample spacing
    # T = 1.0 / 800.0
    # x = np.linspace(0.0, N*T, N)
    # y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)

    yf1 = scipy.fftpack.fft(ppos_data_train[:N])
    yf2 = scipy.fftpack.fft(ppos_data_train[1:N + 1])
    max_yf = max(yf1)
    max_data = max(ppos_data_train[:N])

    ffreq = 50
    iff1 = scipy.fftpack.ifft([yf1[x] if ffreq < x < 2048 - ffreq else 0 for x in range(len(yf1))])
    iff2 = scipy.fftpack.ifft([yf1[x] if x < ffreq or x > 2048 - ffreq else 0 for x in range(len(yf1))])
    # xf = list(range(N//2))

    fig, ax = plt.subplots()
    # ax.plot(2.0/N * np.abs(yf[:N//2]))
    ax.plot(ppos_data_train[:N])
    ax.plot(iff1)
    ax.plot(iff2)
    # ax.plot(yf1[:N//2] * (max_data/max_yf))
    # ax.plot(yf2[:N//2] * (max_data/max_yf))
    plt.show()

# Unit tests for utils/tools.py
import unittest

from iats.utils.tools import Tools


class TestTools(unittest.TestCase):
    def test_binary_search(self):
        a = [0, 1, 2, 3, 4, 6, 7, 8, 9]
        for i, e in enumerate(a):
            self.assertEqual(Tools.binary_search(a, e), i)
        self.assertEqual(Tools.binary_search(a, 5), -1)
        self.assertEqual(Tools.binary_search(a, 5, b_return_insert=True), -6)
        self.assertEqual(Tools.binary_search(a, 10, b_return_insert=True), -10)

    def test_sort_with_indices(self):
        # Numerical case
        a = [2, 4, 5, 1, 0]
        b, idxs = Tools.sort_with_indices(a)
        self.assertTrue(isinstance(b, tuple))
        self.assertTrue(isinstance(idxs, tuple))
        self.assertListEqual(list(b), [0, 1, 2, 4, 5])
        self.assertListEqual(list(idxs), [4, 3, 0, 1, 2])

        # String case
        a = ['haha', 'hihi', 'huhu', 'hoho', 'hehe']
        b, idxs = Tools.sort_with_indices(a)
        self.assertTrue(isinstance(b, tuple))
        self.assertTrue(isinstance(idxs, tuple))
        self.assertListEqual(list(b), ['haha', 'hehe', 'hihi', 'hoho', 'huhu'])
        self.assertListEqual(list(idxs), [0, 4, 1, 3, 2])

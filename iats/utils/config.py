import argparse
import configparser as cp
import os
import pathlib as pl
import typing as tp

from iats.settings import CONFIG_PATH


def preprocess_config(path: pl.Path) -> cp.ConfigParser:
    """
    Reads a config file at the specified path while expanding the environment variables.

    Args:
        path: Config file location.

    Returns:
        Parsed configuration object.
    """
    cfg_txt = os.path.expandvars(path.read_text())
    config = cp.ConfigParser()
    config.read_string(cfg_txt)
    return config


# https://stackoverflow.com/questions/18668227
class Nestedspace(argparse.Namespace):

    def __setattr__(self, name, value):
        if '.' in name:
            group, name = name.split('.', 1)
            ns = getattr(self, group, Nestedspace())
            setattr(ns, name, value)
            self.__dict__[group] = ns
        else:
            self.__dict__[name] = value

    def __getattr__(self, name):
        if '.' in name:
            group, name = name.split('.', 1)
            try:
                ns = self.__dict__[group]
            except KeyError:
                raise AttributeError
            return getattr(ns, name)
        else:
            raise AttributeError(f"'{name}' not found in config!")


def load_config(path: tp.Union[str, pl.Path] = None) -> argparse.Namespace:
    """
    Loads settings for running an analyzer. Falls back to default Oxya arguments if path is not specified.

    Args:
        path: Config file location.

    Returns:
        Namespace with settings.
    """
    if path is not None:
        path = pl.Path(path)
        print(f"Config path: {path.absolute()}")
        config = preprocess_config(path)
        args = Nestedspace()
        for section in config.values():
            section_args = Nestedspace()
            for setting, value in section.items():
                try:
                    value = float(value)
                except ValueError:
                    pass
                try:
                    val_to_list = [x.strip() for x in value.split(',')]
                    if len(val_to_list) > 1:
                        value = val_to_list
                except AttributeError:
                    pass
                section_args.__dict__[setting] = value
            args.__dict__[section.name] = section_args
    else:
        args = default_oxya_args()
    return args


def default_oxya_args() -> argparse.Namespace:
    """
    Creates argparser namespace with default settings for building an OxyaAnalyser object.

    Returns:
        Namespace object.
    """
    args = Nestedspace()
    args.dataset = Nestedspace()
    args.dataset.path = pl.Path.home() / 'Datasets' / 'oxya' / 'UG1'
    args.dataset.rebuild = False
    args.resample = Nestedspace()
    args.resample.dask = False
    args.resample.resample = 'S'
    args.resample.resample_cats = False
    args.resample.sample_factor = 50
    args.run = Nestedspace()
    args.run.rng = 0
    args.run.run = 'residuals'
    args.run.target = 'Response time (ms)'
    args.run.expand = False
    args.run.dropna = 1

    return args


def production_oxya_args():
    args = Nestedspace()
    args.dataset = Nestedspace()
    args.dataset.path = pl.Path.home() / 'Datasets' / 'oxya' / 'UG5'
    args.dataset.rebuild = True
    args.resample = Nestedspace()
    args.resample.dask = False
    args.resample.resample = 'S'
    args.resample.resample_cats = False
    args.resample.sample_factor = 50
    args.run = Nestedspace()
    args.run.rng = 0
    args.run.run = 'residuals'
    args.run.target = '_source.respti'
    args.run.expand = False
    args.run.dropna = 1
    return args


def main():
    print(load_config(CONFIG_PATH))
    print(load_config())


if __name__ == '__main__':
    main()

import typing as tp

import numpy as np
import pandas as pd


def evaluate_slopes(results: np.ndarray, ground_truth: np.ndarray, rolling: bool = False,
                    resample_rate: int = 1) -> tp.Tuple[float, float]:
    """
    Evaluates the "slope accuracy" of a time series. The signs of the difference quotients of real and predicted values
    are compared for each interval, and the total share of matches is returned.

    Args:
        results: Predicted values.
        ground_truth: Real values.
        rolling: True if rolling, False if binning/resampling.
        resample_rate: Rolling/resampling window size.

    Returns:
        - Total accuracy accounting for every window.
        - Total accuracy for windows where the predicted value has changed compared to the previous window.

    .. seealso::
       .. math::
          Acc(i) \Longleftrightarrow (Slope_{real}(i) \geq 0) \Leftrightarrow (Slope_{predicted}(i) \geq 0)
    """
    df = pd.DataFrame(np.array([results, ground_truth]).T, columns=['y_pred', 'y_true'])
    if not rolling:
        df = df.groupby(df.index // resample_rate).mean()
    else:
        df = df.rolling(resample_rate).mean()
    diffs = df.diff()
    diffs[diffs > 0] = 1
    diffs[diffs < 0] = -1
    slopes_acc = len(diffs[diffs['y_pred'] == diffs['y_true']]) / len(diffs)
    nonzero_slopes_acc = len(diffs[diffs['y_pred'] == diffs['y_true']]) / len(diffs[diffs['y_true'] != 0])
    return slopes_acc, nonzero_slopes_acc

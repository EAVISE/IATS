import pytest
import pathlib as pl
from iats.utils.connect import DropboxApp

LOC_TOKEN = pl.Path.home() / "Datasets" / "oxya" / "prod" / "token.txt"
TEST_UPL_NAME = "test_uploaded.txt"
LOC_TESTFILE = pl.Path.cwd() / "testfile.txt"


@pytest.fixture(scope="module")
def app():
    test_app = DropboxApp(str(LOC_TOKEN))
    yield test_app
    test_app.delete(TEST_UPL_NAME)


def test_upload(app):
    result = app.upload(str(LOC_TESTFILE), TEST_UPL_NAME, 80)
    print(result)
    assert True


def test_list(app):
    files = app.list_files()
    assert TEST_UPL_NAME in files['name'].tolist()


def test_download(app):
    dl = app.download(TEST_UPL_NAME)
    with open(LOC_TESTFILE, "r") as testfile:
        test_text = testfile.readline()
    assert dl.read().decode('utf-8') == test_text


def test_pop(app):
    assert len(list(app.pop(TEST_UPL_NAME))) != 0

import functools
import logging
import typing as tp
import warnings


def setup_log(tag: str = 'DA-RNN') -> logging.Logger:
    """
    Setup method for the DA-RNN network training and evaluation logger.

    Args:
        tag: Prefix string.

    Returns:
        Logger instance.
    """
    # create logger
    logger = logging.getLogger(tag)
    # logger.handlers = []
    logger.propagate = False
    logger.setLevel(logging.DEBUG)
    if not logger.hasHandlers():
        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%m/%d/%Y %I:%M:%S')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        # logger.handlers = []
        logger.addHandler(ch)
    return logger


def deprecated(func: tp.Callable) -> tp.Callable:
    """Function wrapper to emit a warning that the function is deprecated.
    Also adds deprecation info to docstring.

    Args:
        func: Deprecated function.

    Returns:
        Same function with deprecation warning.
    """

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', DeprecationWarning)  # turn off filter
        warnings.warn_explicit("Call to deprecated function {}.".format(func.__name__),
                               category=DeprecationWarning, filename=func.__code__.co_filename,
                               lineno=func.__code__.co_firstlineno + 1)
        warnings.simplefilter('default', DeprecationWarning)  # reset filter
        return func(*args, **kwargs)

    new_func.__name__ = func.__name__
    new_func.__doc__ = \
        """
        
        .. warning::
           This function is deprecated and will be removed in  a future version.
        """ \
        + func.__doc__
    new_func.__dict__.update(func.__dict__)
    return new_func

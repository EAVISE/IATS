import functools
import typing as tp
from time import time

import pandas as pd


class DataLoaderError(Exception):
    """Exception called when something is wrong with the data format. """


def timing(f: tp.Callable) -> tp.Callable:
    """Timing decorator.

    Args:
        f: Function to be timed.

    """

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        start = time()
        result = f(*args, **kwargs)
        end = time()
        print("Elapsed time for function '{}': {} seconds.".format(f.__name__, end - start))
        return result

    return wrapper


def df_types(df: pd.DataFrame) -> pd.Series:
    """Helper function to generate series of column dtypes.

    Args:
        df: Pandas dataframe.

    Returns:
        Series with numpy dtype for every column in the dataframe.

    """
    return pd.Series(df.dtypes, df.columns)


def convert_daterange_to_csvs(prefix: str, begin_date: pd.Timestamp, end_date: pd.Timestamp):
    names = []
    for date in pd.date_range(begin_date, end_date):
        for i in range(4):
            names.append("-".join((prefix, str(date.date()), f"{i}.csv")))
    return names

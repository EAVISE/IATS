"""
A collection of useful helper functions
"""
import math
from bisect import bisect_left

import numpy as np


class Tools:
    # #########
    # Searching
    # #########
    @staticmethod
    def binary_search(a: list, x, lo=0, hi=None, b_return_insert=False):
        """
        Search for x in a using binary search.
        Adapted from:
        https://stackoverflow.com/questions/212358/binary-search-bisection-in-python#2233940

        Args:
            a: array/list to search in
            x: element to search for
            b_return_insert: whether to return -1 (False) or the insert position if x is not in a

        Returns:
            The index of x in a if present, else -1 or, assuming 'p' would be the insert position of x in a, -(p+1)
        """
        hi = hi if hi is not None else len(a)
        pos = bisect_left(a, x, lo, hi)  # find insertion position
        if pos != hi and a[pos] == x:
            return pos
        else:
            return -(pos+1) if b_return_insert else -1

    # #######
    # Sorting
    # #######
    @ staticmethod
    def sort_with_indices(a, b_reverse=False):
        """
        Sort an array/list a, and also return the original indices of the elements.
        Args:
            a: array/list to sort
            b_reverse: sort in reverse order; default is ascending

        Returns:
            tpl(x,y) with x the sorted version of a, and y a list containing the original index in a of each element
            in x; i.e., x[i] == a(y[i]).
            x and y are also tuples.
        """
        # Create list of indices
        b = list(range(len(a)))
        # Zip data with indices
        l = list(zip(a, b))
        # Sort l
        l = sorted(l, key=lambda x: x[0], reverse=b_reverse)
        sorted_a, b = zip(*l)
        return sorted_a, b

    # #####
    # Other
    # #####
    @staticmethod
    def window_stack(a: np.ndarray, stepsize=1, width=3):
        """
        For a given input a, will create a new array containing "stacked subsets". This is most easily explained
        using an example.

        Say you provide a=np.array([[1],[2],[3],[4],[5]]), then the result of window_stack(a) with default parameters will be:
        res = np.array([[1,2,3],
                        [2,3,4],
                        [3,4,5]])

        Args:
            a: input data; should be at least 2D
            stepsize:
            width:

        Returns:

        """
        return np.hstack(a[i:1 + i - width or None:stepsize] for i in range(0, width))

    @staticmethod
    def corr_norm(a, b):
        """
        Compute normalized correlation between arrays a and b.

        Args:
            a: array or list
            b: array or list

        Returns:
            The normalized correlation
        """
        e1 = sum([x ** 2 for x in a])
        e2 = sum([x ** 2 for x in b])
        corr = np.correlate(a, b, mode='valid')
        return corr / (math.sqrt(e1 * e2))

    @staticmethod
    def normalize_data(x, mean=None, std=None, b_return_params=False):
        """
        Normalize an array or list. This means that, with 'm' the mean over x and 's' the standard deviation:

        x -> (x-m)/s

        Args:
            x: array or list
            mean: the mean to use, if different from the mean over the provided data
            std: the std to use, if different from the std over the provided data
            b_return_params: return used mean and std or not?

        Returns:
            The normalized data, as well as the used mean and std if so specified.
        """
        if mean is None:
            mean = np.mean(x)
        if std is None:
            std = np.std(x)
        if std == 0:
            raise ValueError("Standard deviation over data is 0, can not normalize data.")

        norm = (x - mean)/std

        if b_return_params:
            return norm, mean, std
        return norm

    @staticmethod
    def to_one_hot(x: np.ndarray, dim: int=None):
        """
        Given a 1D array containing integers, convert the array to a one-hot encoded representation of dimension dim.
        If not specified, will default to max(x).


        Args:
            x: the input data to convert
            dim: the dimensionality of the one-hot encodings; if None, will default to max(x)

        Returns:
            An np.array containing the one-hot encodings.
        """
        if not isinstance(x, np.ndarray):
            raise TypeError("Expected a numpy array as input. Got: {}.".format(type(x)))
        elif x.dtype != np.int:
            raise TypeError("Expected an array of integers. Got {}.".format(x.dtype))

        if dim is None:
            dim = max(x)

        res = np.zeros((len(x), dim))
        for i, k in enumerate(x):
            if k <= 0:
                raise ValueError("Value to be one-hot-encoded should be >0. Got {} instead.".format(k))
            res[i][k-1] = 1

        return res

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import contextlib as cl
import datetime
import io
import json
import os
import pathlib as pl
import shutil
import subprocess

import dropbox as dbx
import pandas as pd
from tqdm import tqdm

try:
    import magic
except ImportError:
    raise ImportWarning("Module 'magic' could not be imported. Do not attempt to call unpack function!")


class nullcontext(cl.AbstractContextManager):
    """BACKPORT FROM PYTHON 3.7.
    Context manager that does no additional processing.
    Used as a stand-in for a normal context manager, when a particular
    block of code is only sometimes used with a normal context manager:
    cm = optional_cm if condition else nullcontext()
    with cm:
        # Perform operation, using optional_cm if condition is True
    """

    def __init__(self, enter_result=None):
        self.enter_result = enter_result

    def __enter__(self):
        return self.enter_result

    def __exit__(self, *excinfo):
        pass


class DropboxApp:
    def __init__(self, token_loc: str) -> None:
        """
        A dropbox API wrapper to easily upload and download files.

        Args:
            token_loc: Path to the file containing the Dropbox API key.
        """
        self.token_loc = token_loc
        self.client = self.connect_dropbox()

    def connect_dropbox(self) -> dbx.Dropbox:
        with open(self.token_loc, 'r') as f:
            token = f.readline().rstrip()
        client = dbx.Dropbox(token)
        print(client.users_get_current_account())
        return client

    def upload(self, fname: str, server_path: str, chunk_size: int = 80) -> dbx.files.FileMetadata:
        """
        Upload a file to the dropbox app.

        Args:
            fname: Name of the file to upload.
            server_path: Name of the file on the Dropbox server (can be the same as fname).
            chunk_size: Size of file partitions (in MB). Cannot be larger than 150 MB.

        Returns:
            File info.
        """
        if chunk_size > 150:
            raise ValueError("Chunk_size cannot exceed 150 MB!")
        if server_path is None:
            server_path = fname

        server_path = "/" + server_path
        CHUNK_SIZE = chunk_size * 1024 ** 2
        with open(fname, 'rb') as f:
            f_size = os.fstat(f.fileno()).st_size
            if f_size < CHUNK_SIZE:
                return self.client.files_upload(f.read(), server_path)
            else:
                session_result = self.client.files_upload_session_start(f.read(CHUNK_SIZE))
                cursor = dbx.files.UploadSessionCursor(session_id=session_result.session_id,
                                                       offset=f.tell())
                commit = dbx.files.CommitInfo(path=server_path)

                while f.tell() < f_size:
                    if f_size - f.tell() <= CHUNK_SIZE:
                        return self.client.files_upload_session_finish(
                            f.read(CHUNK_SIZE),
                            cursor,
                            commit)
                    else:
                        self.client.files_upload_session_append_v2(
                            f.read(CHUNK_SIZE),
                            cursor)
                        cursor.offset = f.tell()

    def delete(self, path: str) -> None:
        """
        Deletes file from Dropbox folder.
        Warning: cannot be recovered afterwards!

        Args:
            path: Location of the file on the Dropbox server.
        """
        self.client.files_delete_v2("/" + path)

    def download(self, path: str) -> io.BytesIO:
        meta, response = self.client.files_download("/" + path)
        with cl.closing(response) as resp:
            return io.BytesIO(resp.content)

    def download_to_file(self, server_path: str, dest_path: str) -> dbx.files.FileMetadata:
        """
        Download a file from the dropbox app to the chosen location.

        Args:
            server_path: File location on the Dropbox server (starting from the application root).
            dest_path: Download location (local path).

        Returns:
            File info.
        """
        return self.client.files_download_to_file(server_path, dest_path)

    def download_and_extract(self, path: str):
        # TODO: implement
        pass

    def list_files(self) -> pd.DataFrame:
        """
        List the files in the dropbox app folder.

        Returns:
            DataFrame containing file ID, name, size and date of last modification.
        """
        results = self.client.files_list_folder(path="")
        data = [dict(id=x.id, name=x.name, size=x.size, modified=x.server_modified) for x in results.entries]
        return pd.DataFrame.from_records(data)

    def pop(self, path):
        dl_stream = self.download(path)
        return self.unpack(dl_stream, use_mime=True)
        # TODO: 1) read datastream  2) move archive from dropbox to safe local storage

    @staticmethod
    def unpack(stream: io.BytesIO, use_mime=True):
        ftype = magic.from_buffer(stream.read(1024), mime=use_mime)
        stream.seek(0)
        main_type, sub_type = ftype.split('/')
        if main_type == 'text':
            yield stream.read()
        else:
            import zipfile
            import tarfile
            if 'zip' in sub_type:
                ziplist = nullcontext(zipfile.ZipFile(stream).filelist)
            elif any(t in sub_type for t in ('xz', 'tar')):
                ziplist = tarfile.open(fileobj=stream, mode='r:*')
            else:
                ziplist = nullcontext([])
            with ziplist as zl:
                for z in zl:
                    yield z


def unpack_folders(location: str = '.', pattern: str = 'per-sapbeat') -> None:
    """ Unpack zipfiles containing ElasticSearch JSON data.

    Args:
        location: Zip location.
        pattern: Index pattern of the data.
    """
    pth = pl.Path(location).resolve()
    folders = pth.glob(pattern + '*')
    for fol in folders:
        try:
            f = fol.glob('var/lib/docker/elasticdump/*.json')
            for fi in f:
                new_pth = pth / fi.name
                if not new_pth.exists():
                    shutil.move(fi, new_pth)
        except FileNotFoundError:
            print("not found")


def upload_json(fname):
    with open(fname, "r") as json_file:
        head = json_file.readline()
    parsed = json.loads(head)

    url = "http://localhost:9200/"
    index = parsed['_index']

    subprocess.run([pl.Path.home() / "lib" / "elasticsearch-dump" / "bin" / "elasticdump",
                    '--output', url + index + '/',
                    '--input', fname,
                    '--type', "data"])


def upload_folder(path):
    for f in tqdm(path.glob("*.json")):
        tqdm.write(f"Uploading {f.name}...")
        upload_json(f)


def upload_single_zip():
    root = pl.Path.home() / "Datasets" / "oxya" / "prod"
    source_name = "sapbeat-prod-2019-02-28.zip"
    dest_name = "sapbeat-test"
    ext = "zip"
    now = datetime.datetime.now()
    zipname = ".".join([dest_name, now.strftime("%Y-%m-%d_%H:%M"), ext])

    app = DropboxApp(str(root / "token.txt"))
    app.upload(str(root / source_name), zipname, 80)


def main():
    # upload_single_zip()
    upload_folder(pl.Path.home() / 'Datasets' / 'oxya' / 'prod' / 'zipfiles' / 'unzip')


if __name__ == '__main__':
    main()

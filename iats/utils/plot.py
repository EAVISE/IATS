import pathlib as pl
import typing as tp

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.manifold import TSNE

from iats.oxya.attention import model as attn_model
from iats.oxya.attention.model import DARNNTrainer
from iats.utils.eval import evaluate_slopes


def plot_tsne(data: np.ndarray, n_components: int = 2, perplexity: int = 30, n_iter: int = 200) -> plt.Artist:
    """
    Plots the t-distributed Stochastic Neighbour Embedding of a dataset.

    Args:
        data: Input ndarray.
        n_components: Dimension of the reduced dataset.
        perplexity: Parameter describing attention between global and local aspects of the data. Set between 5 and 50.
        n_iter: Maximum amount of optimization iterations.

    Returns:
        Drawn artist.
    """
    fig, ax = plt.subplots()
    tsne = TSNE(n_components=n_components, perplexity=perplexity, init='pca', n_iter=n_iter)
    low_dim_embs = tsne.fit_transform(data)
    return ax.scatter(low_dim_embs[:, 0], low_dim_embs[:, 1])


def plot_test_vs_predicted(ax: plt.Axes, y_pred: np.ndarray, y_real: np.ndarray) -> tp.Tuple[plt.Artist, plt.Artist]:
    """
    Generates a plot with real and predicted values from a regressor. The Mean Square Error is given as figure title.

    Args:
        ax: Axis object to plot.
        y_pred: Predicted values.
        y_real: Real values.

    Returns:
        Drawn artists.
    """
    mse = np.mean((y_real - y_pred) ** 2)
    predicted = ax.plot(y_pred, label='predicted')
    real = ax.plot(y_real, label='real')
    ax.get_figure().legend(loc='best')
    ax.get_figure().suptitle("Accuracy: real vs. predicted")
    ax.set_title(f"Mean Square Error: {mse:.2f}", size='small')
    return predicted, real


def plot_training_vs_validation(ax: plt.Axes, y_train: np.ndarray, y_val: np.ndarray, y_real: np.ndarray, buffer: int):
    """
    Generates a plot containing sequential training and validation data with the model applied to them.

    Args:
        ax: Axis object to plot.
        y_train: Predicted values of the train set.
        y_val:  Predicted values of the validation set.
        y_real: Real values.
        buffer: Length of the databuffer (needed to fit the plot correctly).

    Returns:
        Drawn artists
    """
    real = ax.plot(range(1, 1 + len(y_real)), y_real, label="True")
    train = ax.plot(range(buffer, len(y_train) + buffer), y_train, label='Predicted - Train')
    val = ax.plot(range(buffer + len(y_train), len(y_real) + 1), y_val, label='Predicted - Test')
    ax.get_figure().legend(loc='upper left')
    return real, train, val


def plot_iteration_loss(ax: plt.Axes, model: DARNNTrainer) -> tp.List[plt.Artist]:
    """
    Plots the training loss of a neural network per iteration.

    Args:
        ax: Axis object to plot.
        model: PyTorch-based model.

    Returns:
        Drawn artists.
    """
    domain = len(model.iter_losses)
    iteration_loss = ax.semilogy(range(domain), model.iter_losses)
    iteration_loss_rolling = ax.semilogy(pd.Series(model.iter_losses).rolling(domain // 100).mean())
    ax.set_title("Loss (MSE) per training iteration")
    ax.set_xlabel("iteration")
    ax.set_ylabel("loss")
    return iteration_loss_rolling


def plot_epoch_loss(ax: plt.Axes, model: DARNNTrainer) -> tp.List[plt.Artist]:
    """
    Plots the training loss of a neural network per epoch (i.e. after having used all available training data).

    Args:
        ax: Axis object to plot.
        model: PyTorch-based model.

    Returns:
        Drawn artists.
    """
    domain = len(model.epoch_losses)
    epoch_loss = ax.semilogy(range(domain), model.epoch_losses)
    epoch_loss_rolling = ax.semilogy(pd.Series(model.epoch_losses).rolling(domain // 100).mean())
    ax.set_title("Loss (MSE) per training epoch")
    ax.set_xlabel("epoch")
    ax.set_ylabel("loss")
    return epoch_loss_rolling


def plot_slope_accuracy(y_pred: np.ndarray, y_true: np.ndarray, resample_rate_limit: int = 40) \
        -> tp.Tuple[plt.Figure, plt.Axes]:
    """
    Plots the ``slope accuracy`` of a model at different intervals. For every interval, calculates the difference
    quotients of both real and predicted values, and compares the signs of them. Experiment is repeated for several
    interval sizes until ``resample_rate_limit`` is reached. Plots are generated for normal resampled windows, as well
    as for rolling windows.

    Args:
        y_pred: Predicted values.
        y_true: Real values.
        resample_rate_limit: Interval size limit for the plot.

    Returns:
        Drawn artists.
    """
    slope_accuracies_resampled = pd.DataFrame(
        np.array([evaluate_slopes(y_pred, y_true, False, i) for i in range(0, resample_rate_limit)]),
        columns=['resampled', 'resampled (changes only)'])
    slope_accuracies_rolling = pd.DataFrame(
        np.array([evaluate_slopes(y_pred, y_true, True, i) for i in range(0, resample_rate_limit)]),
        columns=['rolling', 'rolling (changes only)'])
    fig, ax = plt.subplots()
    slope_accuracies_resampled.plot(ax=ax, lw=3, ls='--')
    slope_accuracies_rolling.plot(ax=ax, lw=3)
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    ax.grid(ls='--')
    ax.set_title('Slope accuracy')
    ax.set_xlabel('Resample rate')
    ax.set_ylabel('Accuracy')
    return fig, ax


def generate_training_figures(model: attn_model.DARNNTrainer, save: bool = False,
                              save_path: pl.Path = None) -> None:
    """
    Generates iteration loss, epoch loss, slope accuracy and regression accuracy plots.

    Args:
        model: PyTorch-based model.
        save: Save the generated plots.
        save_path: Plot save location.
    """
    # losses per iteration
    fig1, ax1 = plt.subplots()
    plot_iteration_loss(ax1, model)
    # losses per epoch
    fig2, ax2 = plt.subplots()
    plot_epoch_loss(ax2, model)

    y_true = model.dataset.y[model.dataset.train_size:]
    y_pred = model.predict()

    fig3, ax3 = plot_slope_accuracy(y_pred, y_true)

    # total accuracy
    fig4, ax4 = plt.subplots()
    plot_test_vs_predicted(ax4, y_pred, y_true)
    if save:
        fig1.savefig(save_path / "total_iter_loss.png")
        fig2.savefig(save_path / "total_epoch_loss.png")
        fig3.savefig(save_path / "resampled_accuracies.png")
        fig4.savefig(save_path / "total_accuracy.png")
    else:
        plt.show()

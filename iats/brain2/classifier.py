# coding: utf-8
import csv
import datetime
import functools
import pathlib as pl
import typing as tp
from collections import OrderedDict
from pprint import pprint

import catboost
import lightgbm
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pandas_ml as pdml
import seaborn as sns
import sklearn
import sklearn.discriminant_analysis
import sklearn.ensemble
import sklearn.naive_bayes
import sklearn.neural_network
import xgboost as xgb
from pandas.tseries.frequencies import to_offset
from scipy.stats import gamma, ks_2samp, kstest
from sklearn.base import BaseEstimator
from tqdm import tqdm

from iats.brain2 import dataloader as dl
from iats.settings import ROOT_DIR


def brain2_data(index: int = 2) -> dl.Brain2Reader:
    """
    Generates a Brain2Reader instance containing parsed Montova log data.

    Args:
        index: Index of csv file to read.

    Returns:
        Parsed brain2 data.
    """
    base_path = pl.Path.home() / "Datasets" / "brain_2"
    datasets = [base_path / "normal" / "153_343_EDIFACT_20171005_20171205.csv",
                base_path / "normal" / "1261_94_LPGPInspectionImage_20171005_20171205.csv",
                base_path / "abnormal" / "124_1555_Productie_20171005_20171205.csv",
                base_path / "abnormal" / "695_26_WHSDATA_20171005_20171205.csv"]
    data = dl.Brain2Reader(datasets[index])
    return data


def new_brain2_data(index: int = 0) -> dl.Brain2Reader:
    """
    Generates a Brain2Reader instance containing parsed Montova log data from the new dataset.

    Args:
        index: Index of csv file to read.

    Returns:
        Parsed brain2 data.
    """
    base_path = pl.Path.home() / "Datasets" / "brain_2" / "new"
    datasets = ['153_343_EDIFACT_full.csv',
                '153_343_EDIFACT_full_outtakes.csv',
                '124_1555_Productie_full.csv',
                '124_1555_Productie_full_outtakes.csv',
                '1261_94_LPGPInspectionImage_full.csv',
                '1261_94_LPGPInspectionImage_full_outtakes.csv']
    data = dl.Brain2Reader(base_path / datasets[index])
    return data


def august_brain2_data(index: int = 0) -> dl.Brain2Reader:
    """
    Generates a Brain2Reader instance containing parsed Montova log data from the august dataset.

    Args:
        index: Index of csv file to read.

    Returns:
        Parsed brain2 data.
    """
    base_path = pl.Path.home() / "Datasets" / "brain_2" / "august_data" / "2019-08-07" / "dump"
    datasets = sorted(list(base_path.glob("*.csv")))
    data = dl.Brain2Reader(base_path / datasets[index])
    return data


class GroupNode(object):
    """
    Class representing a subset of a dataset taken from a certain timeframe.
    Can be used as a node in a tree-like struct.

    Args:
        name: Node identifier.
        data: Data (view) contained in the node.
        groupers: Grouping windows to be applied on the data.
        aggregates: Aggregates to be calculated on applied groups.
    """

    def __init__(self, name: str, data: pd.DataFrame, groupers: tp.List[pd.Grouper] = None,
                 aggregates: tp.List[str] = None) -> None:
        self.name = name
        self.data = data
        self.agg_data = None
        self.children = []
        try:
            self.dist = {col: self.train(col) for col in self.data.columns}
        except AttributeError:
            self.dist = self.train(None)
        self.groupers = groupers.copy()
        self.grouper = self.groupers.pop()
        self.aggregates = aggregates or ['mean']

    def split(self) -> None:
        """
        Splits the node's data according to its groupers and creates new GroupNodes as children.

        Returns:
            None
        """
        if len(self.groupers) == 0:
            return
        for name, group in self.data.groupby(self.grouper):
            new_node = GroupNode(name, group, groupers=self.groupers)
            new_node.split()
            self.children.append(new_node)

    # untested
    def get_summary(self, aggregator: tp.Any) -> tp.Tuple[pd.DataFrame, tp.List[pd.DataFrame]]:
        """
        Calculates a given aggregate for the node's data and its children.

        Args:
            aggregator: Aggregator to be used.

        Returns:
            Aggregated information of the node and its children.
        """
        node_agg = self.data.agg(aggregator)
        child_agg = [child.data.agg(aggregator) for child in self.children]
        return node_agg, child_agg

    def has_children(self) -> bool:
        """
        Checks if the node has any child nodes attached.

        Returns:
            Parent status.
        """
        return len(self.children) > 0

    def train(self, column: str = None) -> tp.Tuple[float, float, float]:
        """
        Fits a gamma distribution to the data of the node.

        Args:
            column: Column to be fitted (optional if data only contains 1 column).

        Returns:
            Gamma parameters: alpha, location, beta.
        """
        if column is not None:
            fit_alpha, fit_loc, fit_beta = gamma.fit(self.data[column])
        else:
            fit_alpha, fit_loc, fit_beta = gamma.fit(self.data)
        return fit_alpha, fit_loc, fit_beta

    def evaluate(self, test_data: pd.DataFrame) -> tp.Tuple[float, float]:
        """
        Performs a Kolmogorov-Smirnov test on test data and the fitted gamma distribution of the node.
        A Low KS-statistic and high p-value indicate that the test data is drawn from the same distribution as the
        gamma distribution, and vice versa.

        Args:
            test_data: Incoming test data to be compared to the fitted distribution.

        Returns:
            KS-statistic and p-value.
        """
        ks_stat, p_val = kstest(test_data, "gamma", args=self.dist)
        return ks_stat, p_val

    # merge levels (i.e. all mondays of a month)
    def merge_levels(self):
        # [i for i in brain2_data.df.groupby(pd.Grouper(freq='M'))][0][1].groupby([j for j in
        # brain2_data.df.groupby(pd.Grouper(freq='M'))][0][1].index.weekday_name).mean()
        for name, group in self.data.groupby(self.grouper):
            print(group.groupby(group.index.weekday_name).mean())
        pass

    def calculate_aggregates(self):
        self.agg_data = self.data.groupby(self.groupers[0]).agg(self.aggregates)

    def __repr__(self):
        return f"{self.__class__.__name__}(length={len(self.data)}, grouper={self.grouper.freq}, " \
            f"#children={len(self.children)}, agg={' ,'.join(self.aggregates)})"


class HierarchicalClassifier(object):
    """
    Class that constructs a tree of timegrouped nodes for fast and memory-efficient evaluation of Kolmogorov-Smirnov
    tests at different time intervals.

    Args:
        data: Input DataFrame.
        time_offsets: Time offsets on which the tree is supposed to be split.
        classifier: SK-learn compatible classifier.
    """

    def __init__(self, data: pd.DataFrame, time_offsets: tp.List[str] = None, classifier: BaseEstimator = None) -> None:
        self.data = data
        self.offsets = time_offsets or ['T', 'H', 'D', 'W', 'SM', 'M', 'Q', 'A'][::-1]
        self.classifier = classifier
        self.tree = None
        self.groupers = self.construct_groupers()
        self.root = self.initialize_tree()

    def construct_groupers(self) -> tp.List[pd.Grouper]:
        """
        Generates a list of TimeGroupers based on given offsets.

        Returns:
            Appropriate TimeGroupers.
        """
        groupers = [pd.Grouper(freq=offset) for offset in self.offsets]
        return groupers

    def initialize_tree(self) -> GroupNode:
        """
        Recursively generates the GroupNode tree.

        Returns:
            The root node of the tree.
        """
        root = GroupNode("root", self.data, self.groupers)
        root.split()
        return root

    @staticmethod
    def get_scores(data: pd.DataFrame, node: GroupNode, stat_th: int = 0.1, p_th: int = 0.5) -> tp.Any:
        """
        Generates a list of evaluated KS-scores, filtered on a KS-statistic and p-value threshold, for test data
        compared to a node and its children.

        Args:
            data: Test data.
            node: GroupNode to be evaluated.
            stat_th: KS-statistic threshold.
            p_th: P-value threshold.

        Returns:
            Scores.
        """
        if not node.has_children():
            return
        ks_stat, pval = node.evaluate(data)
        if ks_stat < stat_th and pval > p_th:
            yield ks_stat, pval, node.grouper
        for child in node.children:
            yield HierarchicalClassifier.get_scores(data, child, stat_th, p_th)

    def evaluate_event(self, node):
        pass


class StructuralClassifier(object):
    def __init__(self, data: pd.DataFrame, scale_weights=False) -> None:
        """
        Class that allows evaluation of interval histogram comparisons by generating the intervals to be compared on the
        fly instead of creating a hierarchical tree.

        Args:
            data: Input DataFrame.
            time_offsets: Time offsets at which the input data gets sampled.
            classifier: SK-learn compatible classifier.
        """
        self.original_data = data
        self.data = self.original_data
        self.feats = None
        self._exp_name = None
        self._classifier = None
        self.scale_weights = scale_weights

    def __call__(self, *args, **kwargs):
        pass

    def generate_experiment_name(self):
        date_fmt = "%Y-%m-%d_%H%M"
        name = "_".join(["structural", datetime.datetime.today().strftime(date_fmt)])
        if self.scale_weights:
            name += "_weighted"
        return name

    @property
    def experiment_name(self):
        if self._exp_name is None:
            self.generate_experiment_name()
        return self._exp_name

    @experiment_name.setter
    def experiment_name(self, value):
        self._exp_name = value

    def experiment(self, kind=None) -> tp.Callable:
        @functools.wraps(self)
        def exp_wrapped(*args, **kwargs):
            obj = args[0]  # type: StructuralClassifier
            obj.experiment_name = obj.generate_experiment_name()
            return self(*args, **kwargs)

        return exp_wrapped

    @experiment
    def dummy_experiment(self):
        print("hi!")

    def balance_classes(self, df=None, imba_factor=1.5):
        if df is None:
            df = self.original_data
        if imba_factor is None:
            return df
        df = df.reset_index()
        ratios = df.groupby('anomaly')['latency'].count()
        biggest_cat = ratios.idxmax()
        drop_amt = min(max(int(ratios.max() - imba_factor * ratios.min()), 0), ratios.max() - ratios.min())
        drop_choice = np.random.choice(df[df['anomaly'] == biggest_cat].index, drop_amt, replace=False)
        df = df.drop(drop_choice)
        df = df.set_index('in_timestamp')
        return df

    @staticmethod
    def hourly_intervals_static(start: int = 0, stop: int = 24, step: float = 0.5) -> np.ndarray:
        """
        Generates a list of time intervals in which the data is to be divided.

        Args:
            start: Start value of the intervals.
            stop:  End value of the intervals.
            step: Size of the intervals.

        Returns:
            List of intervals.
        """
        times = [datetime.datetime.strptime("00:00", "%H:%M") + datetime.timedelta(hours=float(i)) for i in
                 np.arange(start, stop, step)]
        strtimes = [dt.strftime("%H:%M") for dt in times]
        return np.array([strtimes, np.roll(strtimes, -1)]).T

    @staticmethod
    def hourly_intervals_by_index(data: pd.DataFrame, freq: str = 'W') -> pd.IntervalIndex:
        """
        Generates a pandas IntervalIndex based on the start- and end dates of a DataFrame.

        Args:
            data: DataFrame to be used for determining start and end dates.
            freq: Interval frequency.

        Returns:
            IntervalIndex.
        """
        start_date = data.iloc[0:1].index.to_pydatetime()[0]
        end_date = data.iloc[-2:-1].index.to_pydatetime()[0]
        dates = pd.interval_range(start=start_date, end=end_date, freq=freq)
        return dates

    def generate_gamma_intervals(self, sort='rigid', data=None, freq=None):
        used_timestamps = []
        if sort == 'rigid':
            # bounds = ((str(i % 24) + ":00", str((i + 1) % 24) + ":00") for i in range(24))
            bounds = self.hourly_intervals_static(step=1)
        else:
            bounds = ((interval.left.to_pydatetime().time(), interval.right.to_pydatetime().time()) for interval in
                      self.hourly_intervals_by_index(data=data, freq=freq))
        for left, right in bounds:
            if left not in used_timestamps:
                used_timestamps.append(left)
                yield left, right
            else:
                break

    def get_distributions(self, data_to_check, freq, test_data, sort='rigid'):
        dists = {}
        used_data = {}
        for i, (left, right) in enumerate(self.generate_gamma_intervals(sort=sort, data=test_data, freq=freq)):
            interval_data = data_to_check.between_time(left, right)
            used_data[i] = interval_data
            try:
                dists[i] = gamma.fit(interval_data.dropna())
            except ValueError:
                dists[i] = 0
        return dists, used_data

    def plot_histograms(self, column: str = 'latency', intervals: tp.Any = None) -> None:
        """
        Plots the histograms of the data divided in the given intervals.

        Args:
            column: Selected column of the DataFrame.
            intervals: Intervals to divide the data by.

        Returns:
            None
        """
        if intervals is None:
            intervals = self.hourly_intervals_static(0, 24, 0.5)
        shape = len(intervals) // 2, len(intervals)
        # fig, ax = plt.subplots(*shape)  # type: plt.Figure, plt.axes
        for i, interval in enumerate(intervals):
            try:
                plt.figure()
                self.data[column].between_time(*interval).plot.hist(bins=20)  # , ax=ax[i // 2, i % 2])
                plt.title(interval)
            except TypeError:
                pass
            # ax[i // 2, i % 2].set_title(interval)
        return  # fig

    @staticmethod
    def evaluate(test_data: pd.DataFrame, dist: tp.Tuple[float, float, float]) -> tp.Tuple[float, float]:
        """
        Performs a Kolmogorov-Smirnov test on test data and a fitted gamma distribution.
        A Low KS-statistic and high p-value indicate that the test data is drawn from the same distribution as the
        gamma distribution, and vice versa.

        Args:
            test_data: Incoming test data to be compared to the fitted distribution.
            dist: Gamma distribution parameters.

        Returns:
            KS-statistic and p-value.
        """
        ks_stat, p_val = kstest(test_data, "gamma", args=dist)
        return ks_stat, p_val

    @staticmethod
    def evaluate_twosided(test_data: pd.DataFrame, train_data: pd.DataFrame) -> tp.Tuple[float, float]:
        """
        Performs a twosided Kolmogorov-Smirnov test on test data and train data.
        A Low KS-statistic and high p-value indicate that the test data is drawn from the same distribution as the
        train data, and vice versa.

        Args:
            test_data: Incoming test data to be compared.
            train_data: Original data to compare the test data with.

        Returns:
            KS-statistic and p-value.
        """
        ks_stat, p_val = ks_2samp(test_data, train_data)
        return ks_stat, p_val

    @staticmethod
    def eval_plot(eval_func: tp.Callable, data_dict: dict, test_data: pd.DataFrame, title: str) -> None:
        """
        Evaluates and plots the results of KS-tests. Currently limited to a maximum span of 24 hours.

        Args:
            eval_func: Function to evaluate with.
            data_dict: Dictionary of dataframes for each interval.
            test_data: Test data to be evaluated.
            title: Title of the resulting plot.

        Returns:
            None.
        """
        print("Now calculating:", title)
        results = []
        for key, data in data_dict.items():
            try:
                result = np.array(eval_func(test_data, data))
                result = np.where(np.isnan(result), [1, 0], result)
                results.append(tuple(result))
            except (TypeError, ValueError) as e:
                print("Couldn't be evaluated:", e.args)
                print("Setting to max. improbability...")
                results.append((1, 0))
        results = np.array(results)
        jump = 24 // results.shape[0]
        fig, ax = plt.subplots()
        try:
            ax.plot(np.arange(0, 24, jump), results)
        except ZeroDivisionError:
            print(jump, results)
            raise
        ax.set_title(title)
        plt.legend(['ks_stat', 'p_val'])
        print("New y limits:", np.nanmin(results[:, 0]), np.nanmax(results[:, 0]))
        try:
            x_min = np.nanargmin(results[:, 0])
            y_min = results[x_min, 0]
            ax.vlines(x_min * jump, 0, y_min, colors='red', linestyles='dotted')
            ax.hlines(y_min, 0, x_min * jump, colors='red', linestyles='dotted')
            ax.set_xticks([jump * x for x in range(results.shape[0])])
            ax.set_xticklabels([f"{x}-{x + jump}" for x in ax.get_xticks()])
            ax.tick_params(axis='x', rotation=70)
            ax.set_xlabel("hours")
        except ValueError:
            pass

    def generate_feature_set(self, df, choice, target='latency', freq_h=3):
        feats = df.loc[:, [target, 'anomaly', 'rel_size', 'in_size', 'out_size']]
        intervals = self.hourly_intervals_static(step=1)
        if choice is None:
            choice = tuple()
        # seasonal
        if "seasonal" in choice:
            feats.loc[:, 'weekday'] = df.index.dayofweek
            feats.loc[:, 'month'] = df.index.month
            feats.loc[:, 'quarter'] = df.index.quarter
            feats.loc[:, 'hour'] = feats.index.hour
        # derivative
        if "derivative" in choice:
            feats.loc[:, 'timediff'] = feats.index.to_series().diff().astype('timedelta64[ns]').dt.total_seconds()
            feats.loc[:, 'diff'] = feats[target].diff() / feats['timediff']
        # interval comparison
        if "interval" in choice:
            feats.loc[:, 'time_between_3'] = 0
            for i in range(len(intervals)):
                feats.iloc[df.index.indexer_between_time(intervals[i, 0], intervals[i, 1]), -1] = i
            pass
            for fr in (1, 2, 3, 4, 6, 12):
                freq_str = str(fr) + 'H'
                feats.loc[:, 'groupID' + str(fr)] = feats.groupby(pd.Grouper(freq=freq_str)).ngroup() % (24 / fr)
            feats.dropna(inplace=True)
            self.interval_features(feats, column=target)
            self.interval_features(feats, column=target, offset='3H')
            self.interval_features(feats, column=target, offset='6H')

        return feats

    def interval_features(self, data, column='latency', offset='1H'):
        for x, y in tqdm(data.resample(offset)):
            if not y.empty:
                comp_data = self.between_algebra(x, data, column, offset)
                res = self.evaluate_twosided(y[column].values, comp_data)
                data.loc[y.index, column + "_".join([column, offset, 'KS_score'])] = res[0]
                data.loc[y.index, column + "_".join([column, offset, 'KS_pval'])] = res[1]

    @staticmethod
    def map_offset(offset='H'):
        offset_dict = {
            'H': 'hour',
            'W': 'week',
            'M': 'month'
        }
        return offset_dict[offset[-1]]

    @staticmethod
    def between_algebra(x, data, column, offset):
        mapping = {
            'hour': range(24),
            'week': range(7),
            'month': range(12)
        }

        def _ga(obj, attr):  # get attribute
            return obj.__getattribute__(attr)

        _ta = StructuralClassifier.map_offset(offset)  # time attribute
        lb = _ga(x, _ta)  # left boundary
        rb = _ga(x + to_offset(offset), _ta)  # right boundary
        idx_vals = _ga(data.index, _ta)  # index values

        if (lb + _ga(pd.Timestamp("2000-01-01") + to_offset(offset), _ta)) not in mapping[_ta]:
            return data.loc[(lb <= idx_vals) | ((0 <= idx_vals) & (idx_vals < rb)), column]
        else:
            return data.loc[(lb <= idx_vals) & (idx_vals < rb), column]

    def set_classifier(self, clf_type='xgb', **kwargs):
        # TODO
        classifiers = {
            'xgb': xgb.XGBClassifier(scale_pos_weight=kwargs.get("scale_pos_weight", 1), verbosity=2, n_jobs=8),
            'lgbm': lightgbm.sklearn.LGBMClassifier(scale_pos_weight=kwargs.get("scale_pos_weight", 1), n_jobs=8,
                                                    n_estimators=1000, boosting='gbdt', tree_learner='feature'),
            'cat': catboost.CatBoostClassifier(scale_pos_weight=kwargs.get("scale_pos_weight", 1), verbose=2),
            'svc': sklearn.svm.SVC(),
            'rf': sklearn.ensemble.RandomForestClassifier(),
            'ada': sklearn.ensemble.AdaBoostClassifier(),
            'lda': sklearn.discriminant_analysis.LinearDiscriminantAnalysis(),
            'qda': sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis(),
            'log': sklearn.linear_model.LogisticRegression(),
            'nb': sklearn.naive_bayes.BernoulliNB(),
            'nn': sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(100, 200, 100))
        }
        self._classifier = classifiers[clf_type]
        return self._classifier

    def balancing_experiment(self, target='latency', imba_factor=40, write=False, plot=True, choice=None):
        # rebalance dataset
        if choice is None:
            choice = ("seasonal", "derivative", "interval")
        df = self.balance_classes(imba_factor=imba_factor)
        df_validation = df.loc[:"2018-07"]
        df_train = df.drop(df_validation.index)
        class_balance = df_train.groupby('anomaly')[target].count()
        max_ratio = class_balance.max() / class_balance.min() if self.scale_weights else 1
        feats_train = self.generate_feature_set(df=df_train, choice=choice, target=target)
        df_concat = pd.concat([df_validation, df_train])
        feats_validation = self.generate_feature_set(df=df_concat, choice=choice, target=target)
        dists, used_data = self.get_distributions(feats_train[target], freq='H', test_data=feats_train[target],
                                                  sort='rigid')
        mf_train = pdml.ModelFrame(feats_train, target='anomaly')
        mf_validation = pdml.ModelFrame(feats_validation[:"2018-07"], target='anomaly')
        # set index to integers for training
        mf_train_idx = mf_train.index  # index backup
        mf_val_idx = mf_validation.index  # index backup
        mf_train = mf_train.reset_index(drop=True)
        mf_validation = mf_validation.reset_index(drop=True)
        print(mf_train.head())
        print(mf_train.groupby('anomaly').count() / len(mf_train))
        clf = self.set_classifier('lgbm', scale_pos_weight=max_ratio * 1000)
        train_mf, test_mf = mf_train.model_selection.train_test_split()
        train_mf.fit(clf, verbose=4)
        Evaluator(clf, test_mf).summary()
        print(mf_validation.head())
        val_evaluator = Evaluator(clf, mf_validation)
        val_evaluator.summary()
        if write:
            with open(pl.Path(__file__).resolve().parent / "experiments" / (self.experiment_name + ".csv"), 'a') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([imba_factor] + list(val_evaluator.to_record()))
            with open(pl.Path(__file__).resolve().parent / "experiments" / (self.experiment_name + "_confmat.csv"),
                      'a') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([imba_factor] + list(val_evaluator.to_record(conf_mat=True)))
        if plot:
            # histogram of transactions
            fig, ax = plt.subplots()
            fig.suptitle("Histogram of transactions per hour")
            distribution = ax.bar(used_data.keys(), [len(x) for x in used_data.values()])
            # overview plot
            plot_anomaly_zones(self.original_data)
            # Test vs Predicted
            tvp_no_reindex = test_vs_predicted(mf_validation, clf)
            tvp_reindex = test_vs_predicted(mf_validation, clf, index=mf_val_idx)


def reconstruct_full_dataframe(df: pd.DataFrame, anomalies: pd.DataFrame) -> pd.DataFrame:
    df['anomaly'] = 0
    anomalies['anomaly'] = 1
    return pd.concat([df, anomalies]).sort_index()


def create_zeros_dataframe(df: pd.DataFrame, anomalies: pd.DataFrame) -> pd.DataFrame:
    df = df.reindex(index=df.index.union(anomalies.index)).sort_index()
    df['anomaly'] = 0
    df.loc[df.index.isin(anomalies.index), 'anomaly'] = 1
    return df


def test_kolmogorov_smirnov():
    for a in range(1, 100):
        data = gamma.rvs(a=a, size=10000, random_state=42)
        args = gamma.fit(data)
        # print(args)
        # fig, ax = plt.subplots()
        # x = np.linspace(gamma.ppf(0.001, a), gamma.ppf(0.999, a), 1000)
        # y = gamma.pdf(x, a)
        # ax.hist(data, bins=100, density=True)
        # ax.plot(x, y)
        # plt.show()
        stat, pvalue = kstest(data, "gamma", args)
        assert stat < 0.1 and pvalue > 0.5


def test_hierarchical(brain2_data):
    grouper = pd.Grouper(freq='T')
    brain2_data.df.groupby(grouper).transform(np.cumsum)['latency'].plot()
    test = HierarchicalClassifier(brain2_data.df[['in_size', 'out_size', 'latency', 'rel_size']],
                                  time_offsets=['A', 'Q', 'M', 'W'][::-1])
    print(test.root)


def test_structural(brain2_data):
    # np.random.seed(1)
    metric = "latency"
    # metric = "in/out"
    rand_size = 3000
    test = StructuralClassifier(brain2_data)
    test.data["in/out"] = test.data["in_size"] / test.data["out_size"]
    rand_idx_start = np.random.randint(0, len(test.data) - rand_size)
    print("starting at:", rand_idx_start)
    rand_day = test.data.iloc[[rand_idx_start]].index.weekday
    rand_hour = test.data.iloc[[rand_idx_start]].index.hour
    random_data = test.data.iloc[rand_idx_start:rand_idx_start + rand_size]
    left_bound = "2018-09-18 10:00"
    right_bound = "2018-09-18 11:00"
    freq = "H"

    # random_data = random_data[metric]
    random_data = test.data.loc[left_bound:right_bound, metric]
    # random_data = test.data.iloc[-rand_size:][metric]

    print(random_data.head())
    print(random_data.tail())
    if metric == "latency":
        data_to_check = test.data['latency']
    elif metric == "in/out":
        data_to_check = test.data['in_size'] / test.data['out_size']
    dists, used_data = test.get_distributions(data_to_check, freq, data_to_check, sort='rigid')
    pprint(dists)
    print("Evaluating data captured on a", rand_day, "at", rand_hour)
    fig, ax = plt.subplots()
    distribution = ax.bar(used_data.keys(), [len(x) for x in used_data.values()])
    test.eval_plot(test.evaluate, dists, random_data, "KS-test compared to fitted gamma distribution")
    test.eval_plot(test.evaluate_twosided, used_data, random_data, "KS-test comparing test and train histograms")
    print("started at:", rand_idx_start)
    plt.show()


class Evaluator:
    def __init__(self, classifier, test_data):
        self.clf = classifier
        self.test_mf = test_data
        self.results = None
        self.conf_mat = None

    def evaluate(self):
        self.results = self.test_mf.predict(self.clf)
        self.conf_mat = self.test_mf.metrics.confusion_matrix()

    def summary(self):
        if self.results is None:
            self.evaluate()
        print(self.confusion_matrix)
        print("accuracy on test:", self.accuracy)
        print("precision:", self.precision)
        print("recall:", self.recall)
        print("TNR:", self.tnr)

    def to_record(self, conf_mat=False):
        if self.results is None:
            self.evaluate()
        if not conf_mat:
            return self.accuracy, self.precision, self.recall, self.tnr
        else:
            return self.conf_mat.values.flatten().tolist()

    @property
    def confusion_matrix(self):
        return self.conf_mat

    @property
    def accuracy(self):
        return np.diag(self.conf_mat).sum() / self.conf_mat.values.sum()

    @property
    def tpr(self):
        return self.conf_mat.iloc[1, 1] / self.conf_mat.iloc[1].sum()

    @property
    def tnr(self):
        return self.conf_mat.iloc[0, 0] / self.conf_mat.iloc[0].sum()

    @property
    def precision(self):
        return self.conf_mat.iloc[1, 1] / self.conf_mat.iloc[:, 1].sum()

    @property
    def recall(self):
        return self.tpr


def generate_test_data(idx, zeros=False):
    # orig_data = new_brain2_data(index=2 * idx).df
    # anomaly_data = new_brain2_data(index=2 * idx + 1).df
    orig_data = august_brain2_data(index=0).df
    anomaly_data = august_brain2_data(index=1).df
    if not zeros:
        df = reconstruct_full_dataframe(orig_data, anomaly_data)
    else:
        df = create_zeros_dataframe(orig_data, anomaly_data)
    return df


def cluster_anomalous_data(data, labels, threshold):
    clf = sklearn.neighbors.KNeighborsClassifier(n_neighbors=10, weights='distance')
    clf.fit(data, labels)
    scores = clf.predict_proba(data)
    return scores[scores[:, 1] >= threshold]


def delayed_metric(data, column, labels, predictions):
    gt = list(map(lambda x: (mdates.num2date(x[0]), mdates.num2date(x[1]), x[2]), (intv for intv in get_solution_intervals(labels, column))))
    for left_label, right_label, c_l_id in get_solution_intervals(data, column):
        scores = []
        if c_l_id == 1:
            left_date = mdates.num2date(left_label)
            right_date = mdates.num2date(right_label)
            for left, right, c in gt:
                if left < left_date < right:
                    interval = right - left
                    delay = left_date - left
                    score = 1 - delay / interval
                    scores.append(score)
        return scores


def zero_experiment():
    data = generate_test_data(idx=2, zeros=True)
    #data['td'] = (pd.Series(data.index) - pd.Series(data.index).shift()).dt.total_seconds().values
    #data['td_no_anomaly'] = data.loc[data['anomaly'] == 0, 'td'].diff()
    transaction_counts = data.iloc[:, 1].resample('T').count().rename("count")
    transaction_counts = transaction_counts.to_frame()
    transaction_counts['filler'] = 1
    joined = data.join(transaction_counts, how='outer')
    joined['count'].fillna(method='bfill', inplace=True)
    joined.fillna(0, inplace=True)
    # joined.loc[joined['filler'] == 1, :] = joined.rolling(3).median().loc[joined['filler'] == 1, :]
    joined.loc[joined['filler'] == 1, joined.dtypes == np.number] = joined.select_dtypes(
        include=np.number).rolling(3).median().head()
    # joined.drop(columns=['filler'], inplace=True)
    # joined['latency'].fillna(value=0, inplace=True)
    joined.fillna(method='bfill', inplace=True)
    print(joined)
    # scores = cluster_anomalous_data(data.select_dtypes(include=np.number).dropna().drop(columns=['anomaly']),
    #                                 data.dropna()['anomaly'], 0)
    scl = StructuralClassifier(joined)
    scl.balancing_experiment()


def structural_experiment_v2():
    data = generate_test_data(idx=2)
    scl = StructuralClassifier(data)
    scl.balancing_experiment()


def structural_experiment_v3():
    data = generate_test_data(idx=2)
    X = data.select_dtypes(include=np.number).dropna().drop(columns=['anomaly'])
    y = data.dropna()['anomaly']
    scores = cluster_anomalous_data(X, y, 0)
    print(scores)
    fig, ax = plt.subplots()
    pd.DataFrame(scores).plot(style='.', ax=ax)
    # data.dropna()['anomaly'].plot(style='.', ax=ax)


def structural_experiment(idx=2, step_count=20, balance_ratios=None, scale_weights=False, write=False, plot=True):
    # test_structural(new_brain2_data(index=4))
    orig_df = generate_test_data(idx=idx)
    scl = StructuralClassifier(orig_df)
    print(orig_df[orig_df['anomaly'] == 1])
    # df ok!
    # parameters
    choice = ("seasonal", "derivative", "interval")
    orig_class_balance = orig_df.groupby('anomaly')['latency'].count()
    orig_max_ratio = orig_class_balance.max() / orig_class_balance.min()
    imba_factors = balance_ratios or np.geomspace(orig_max_ratio, 1, step_count)
    for imba_factor in imba_factors:
        # rebalance dataset
        df = scl.balance_classes(imba_factor=imba_factor)
        class_balance = df.groupby('anomaly')['latency'].count()
        max_ratio = class_balance.max() / class_balance.min() if scale_weights else 1
        feats = scl.generate_feature_set(df=df, choice=choice, target='latency')
        dists, used_data = scl.get_distributions(feats['latency'], freq='H', test_data=feats['latency'], sort='rigid')
        mf = pdml.ModelFrame(feats, target='anomaly')
        mf_validation, mf = mf.loc[:"2018-08"], mf.loc["2018-08":]
        # set index to integers for training
        mf_idx = mf.index  # index backup
        mf = mf.reset_index(drop=True)
        mf_val_idx = mf_validation.index  # index backup
        mf_validation = mf_validation.reset_index(drop=True)
        print(mf.head())
        print(mf.groupby('anomaly').count() / len(mf))
        clf = xgb.XGBClassifier(scale_pos_weight=max_ratio, verbosity=2, n_jobs=8)
        train_mf, test_mf = mf.model_selection.train_test_split()
        train_mf.fit(clf, verbose=True)
        Evaluator(clf, test_mf).summary()
        print(mf_validation.head())
        val_evaluator = Evaluator(clf, mf_validation)
        val_evaluator.summary()
        if write:
            with open(pl.Path(__file__).resolve().parent / "experiments" / (scl.experiment_name + ".csv"), 'a') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([imba_factor] + list(val_evaluator.to_record()))
            with open(pl.Path(__file__).resolve().parent / "experiments" / (scl.experiment_name + "_confmat.csv"),
                      'a') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([imba_factor] + list(val_evaluator.to_record(conf_mat=True)))
    if plot:
        # histogram of transactions
        fig, ax = plt.subplots()
        fig.suptitle("Histogram of transactions per hour")
        distribution = ax.bar(used_data.keys(), [len(x) for x in used_data.values()])
        # overview plot
        plot_anomaly_zones(orig_df)
        # Test vs Predicted
        tvp_no_reindex = test_vs_predicted(mf_validation, clf)
        tvp_reindex = test_vs_predicted(mf_validation, clf, index=mf_val_idx)


def get_solution_boundaries(df, group_col):
    prev_start, prev_end = -1, -1
    try:
        sr = df[group_col]
    except KeyError:
        sr = df
    grouper = (sr != sr.shift()).cumsum()
    for i, group in df.groupby(grouper):
        try:
            start = mdates.date2num(group.index[0].to_pydatetime())
            end = mdates.date2num(group.index[-1].to_pydatetime())
        except AttributeError:
            start = group.index[0]
            end = group.index[-1]
        if start != prev_start and end != prev_end:
            try:
                group_id = group[group_col].iloc[0]
            except KeyError:
                group_id = group.iloc[0]
            if not np.isnan(group_id):
                yield group_id, start, end
        prev_start = start
        prev_end = 0


def get_solution_intervals(df, group_col):
    bounds = list(get_solution_boundaries(df, group_col))
    for i in range(len(bounds)):
        b1, b2 = bounds[i % len(bounds)], bounds[(i + 1) % len(bounds)]
        cur_i, cur_start, cur_end = b1
        next_i, next_start, next_end = b2
        left = cur_start
        right = max(next_start, cur_end)
        yield left, right, cur_i


def generate_anomaly_rectangles(df, group_col, y0, height):
    cmap = plt.get_cmap("tab10")
    labels = {0: 'normal', 1: 'abnormal'}
    facecolors = {0: cmap(0), 1: cmap(1)}
    edgecolors = {0: None, 1: cmap(1)}
    for left, right, c_id in get_solution_intervals(df, group_col):
        rect = mpatches.Rectangle((left, y0), right - left, height, label=labels[c_id],
                                  facecolor=facecolors[c_id], edgecolor=edgecolors[c_id], alpha=0.75)
        yield rect


def plot_anomaly_zones(df, reindex=False):
    df = df.copy()
    if reindex:
        df = df.reset_index(drop=True)
    # df_feats.loc[df_feats['anomaly'] == 0, 'anomaly'] = 0
    fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [6, 1], 'hspace': 0.05})
    fig.suptitle("Anomalies in the dataset")
    df['latency'].plot(ax=ax[0])
    # df[['in_size', 'out_size']].plot(ax=ax[0])
    for rect in generate_anomaly_rectangles(df, 'anomaly', 0, int(ax[1].get_ylim()[1])):
        ax[1].add_patch(rect)
    ax[0].legend()
    handles, labels = ax[1].get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    ax[1].legend(by_label.values(), by_label.keys())
    ax[1].set_yticks([0.50])
    ax[1].set_yticklabels(['data labels'], fontweight='bold')
    ax[1].xaxis.set_tick_params(rotation=45)
    del df
    return ax


def anomaly_rectangle(group, ax, cmap):
    if max(group['anomaly'].mean(), 0) == 1:
        color = cmap(1)
    else:
        color = cmap(0)
    start = mdates.date2num(group.index[0].to_pydatetime())
    end = mdates.date2num(group.index[-1].to_pydatetime())
    rect = mpatches.Rectangle((start, 0), end - start, int(ax.get_ylim()[1]), color=color, alpha=0.75)
    ax.add_patch(rect)


def test_vs_predicted(mf, clf, index=None):
    mf = mf.copy()
    fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [6, 1], 'hspace': 0.05})
    # fig, ax = plt.subplots(2, 1, sharex=True)
    if index is not None:
        mf.set_index(index, inplace=True)
        fig.suptitle("Test vs Predicted - time index")
    else:
        fig.suptitle("Test vs Predicted - incremental index")
    mf['latency'].plot(ax=ax[0], label='latency')
    box_height = ax[1].get_ylim()[1] / 2
    for rect in generate_anomaly_rectangles(mf, 'anomaly', box_height, box_height):
        ax[1].add_patch(rect)
    for rect in generate_anomaly_rectangles(mf.predict(clf), 'anomaly', 0, box_height):
        ax[1].add_patch(rect)
    # (mf['anomaly'] * mf['latency'].max()).plot(ax=ax, label='true anomalies')
    # (mf.predict(clf) * mf['latency'].max()).plot(ax=ax, label='predicted anomalies', ls='--')
    ax[1].axhline(y=0.5, color='black')
    ax[0].legend()
    handles, labels = ax[1].get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    ax[1].legend(by_label.values(), by_label.keys())
    ax[1].set_yticks([0.25, 0.75])
    ax[1].set_yticklabels(['predicted', 'true'], fontweight='bold')
    if index is not None:
        ax[1].xaxis.set_tick_params(rotation=45)
    del mf
    return ax


def feature_selection_experiment():
    names = ['features', 'balanced', 'Recall', 'Precision', 'TNR']
    df = pd.read_csv(ROOT_DIR / "brain2" / "experiments" / "brain2_results_UG5.csv", sep=';', names=names)
    print(df)
    for metric in ('Precision', 'Recall'):
        plt.figure()
        sns.barplot(x='features', y=metric, hue='balanced', data=df, hue_order=['no', 'yes'])
        plt.yticks(np.arange(0, 1.1, 0.1))
        plt.legend(loc='upper left', title='balanced')
        plt.suptitle("Feature selection scores: " + metric.lower())


def plot_balancing_experiment(fname, drop=True, smooth=True):
    path = ROOT_DIR / "brain2" / "experiments"
    names = ['balance factor', 'accuracy', 'precision', 'recall', 'tnr']
    df = pd.read_csv(path / fname, sep=';', names=names)
    title = f"Balancing experiment - {len(df)} steps {'- weighted' if 'weighted' in fname else ''}"
    df = df.dropna() if drop else df.fillna(0)
    df['balanced accuracy'] = (df['tnr'] + df['recall']) / 2  # fix accuracy metric
    df = df.set_index('balance factor')
    fig, ax = plt.subplots()
    fig.suptitle(title)
    if smooth:
        plot_df = df.reindex(df.index.union(np.geomspace(df.index.max(), 1, 1000))).interpolate(method='spline',
                                                                                                order=3, ext=1)
    else:
        plot_df = df
    plot_df.plot(logx=True, ax=ax)
    ax.grid(linestyle='--')
    ax.invert_xaxis()


def main():
    # # balancing experiments
    # structural_experiment(idx=2, step_count=50, scale_weights=False, write=True, plot=False)
    # structural_experiment(idx=2, step_count=50, scale_weights=True, write=True, plot=False)
    # structural_experiment(idx=2, balance_ratios=[1.5], plot=True)
    # structural_experiment_v2()
    zero_experiment()
    # # feature selection experiment
    # test_structural(generate_test_data(idx=1))
    # plot results of balancing experiments
    # plot_balancing_experiment("structural_2019-03-19_1435.csv", smooth=False, drop=False)
    # plot_balancing_experiment("structural_2019-03-19_1700.csv", smooth=False, drop=False)
    # plot_balancing_experiment("structural_2019-03-20_1900.csv", smooth=False, drop=False)
    # plot_balancing_experiment("structural_2019-03-20_2112.csv", smooth=False, drop=False)
    # plot_balancing_experiment("structural_2019-03-24_1317.csv")
    # plot_balancing_experiment("structural_2019-03-24_1517_weighted.csv")
    # # plot results of feature selection experiment
    # feature_selection_experiment()
    plt.show()


if __name__ == '__main__':
    main()
    # bla = StructuralClassifier(None, None)
    # print(bla._exp_name)
    # bla.dummy_experiment()
    # print(bla._exp_name)

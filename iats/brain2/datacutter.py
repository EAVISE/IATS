import pathlib as pl
import typing as tp
from datetime import timedelta
from random import randrange

import pandas as pd

from iats.brain2.dataloader import Brain2Reader


def random_date(start: pd.Timestamp, end: pd.Timestamp) -> pd.Timestamp:
    """
    This function will return a random datetime between two datetime
    objects.

    Args:
        start: Start date.
        end: End date.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return pd.Timestamp(start + timedelta(seconds=random_second))


def create_cuts(df: pd.DataFrame, cut_amt: int = 4, max_cut: int = 3) -> tp.Generator[
                tp.Tuple[pd.Timestamp, pd.Timestamp]]:
    """ Returns a generator object containing begin and end dates of data that should be cut from the dataset
    for testing.

    Args:
        df: Input DataFrame.
        cut_amt: Amount of regions to be cut.
        max_cut: Maximum length of a cut (in days).

    Yields:
        Begin and end Timestamp of the cut.
    """
    cut_start = df.index[0]
    for i in range(cut_amt):
        cut1 = random_date(cut_start, df.index[-1])
        cut2 = random_date(cut1, pd.Timestamp(cut1 + timedelta(days=max_cut)))
        yield cut1, cut2
        cut_start = cut2


def cut_dataframe(df: pd.DataFrame, cut_amt: int = 4, max_cut: int = 3) -> tp.Tuple[
                pd.DataFrame, tp.List[pd.DataFrame]]:
    """ Cut regions out of a given DataFrame according to the given parameters.

    Args:
        df: Input DataFrame.
        cut_amt: Amount of regions to be cut.
        max_cut: Maximum length of a cut (in days).

    Returns: - DataFrame with several intervals filtered out.
    - List of DataFrames representing the regions that were cut out.

    """
    cuts = list((x for x in create_cuts(df, cut_amt, max_cut)))
    dropped = []
    for cut in cuts:
        left = df.index.get_loc(cut[0], method='nearest')
        right = df.index.get_loc(cut[1], method='nearest')
        selector = ((df.index > df.index[left]) & (df.index < df.index[right]))
        dropped.append(df[selector])
        df = df[~selector]
    return df, dropped


def cut_brain2_dataset(path: pl.Path, cut_amt=4, max_cut=3):
    """ Cut regions out of data stored in a CSV file at the given path according to the given parameters.
    Both the filtered DataFrame and the cutouts are stored as CSV in the same directory as the source file.

    Args:
        path: Path to CSV file containing the data.
        cut_amt: Amount of regions to be cut.
        max_cut: Maximum length of a cut (in days).
    """
    df = Brain2Reader(path).df
    df, dropped = cut_dataframe(df, cut_amt, max_cut)
    dropped_df = pd.concat(dropped)
    df.to_csv(path.parent / 'cropped_' + path.stem, sep=';')
    dropped_df.to_csv(path.parent / 'outtakes_' + path.stem, sep=';')


def main():
    data_path = pl.Path("/home/kve/Datasets/brain_2/august_data/2019-08-07")
    cut_brain2_dataset(data_path, cut_amt=4, max_cut=3)


if __name__ == '__main__':
    main()

import functools as ft
import pathlib as pl
import re
import typing as tp


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.signal
import statsmodels.tsa
import statsmodels.tsa.stattools

import iats.utils.load as load
from iats import utils


class Brain2Reader(object):
    """
    A data reading engine to parse extracted logs from the Montova platform.

    Args:
        path: Directory of files to be opened.
    """

    def __init__(self, path: pl.Path) -> None:
        # plt.style.use('dark_background')
        self.path = path
        self.df = self.open()
        type_cols = ['in_mime_type', 'out_mime_type']
        file_cols = ['in_filename', 'out_filename']
        self.clean_data(type_cols, file_cols)
        self.df['rel_size'] = self.calc_rel_size
        self.resampled = self.resample(sample_rate='T')
        # self.resampled['latency'].plot()
        # self.df.groupby(['in_mime_type', 'out_mime_type'])[['rel_size']].plot()

    @load.timing
    def open(self) -> pd.DataFrame:
        """
        Opens a csv file and parses it into a DataFrame.

        Returns:
            Parsed DataFrame.
        """
        print(f"Opening file at {self.path}")
        df = pd.read_csv(self.path, delimiter=';', index_col=0, parse_dates=[0])
        return df.sort_index()

    @load.timing
    def clean_data(self, type_cols: tp.List[str], file_cols: tp.List[str]) -> None:
        """
        DataFrame preprocessing in 3 steps:

        1. Converts latencies to timedeltas (in seconds).
        2. Filters away all text after the first semicolon.
        3. Filters away all parts of file names containing numbers.

        Args:
            type_cols: Names of columns containing file types.
            file_cols: Names of columns containing file names.
        """
        self.df['latency'] = pd.to_timedelta(self.df['latency']) / np.timedelta64(1, 's')
        self.df[type_cols] = self.df[type_cols].applymap(lambda x: str(x).split(';')[0])
        regexp = re.compile(r'\d')
        split_func = ft.partial(self.prune_numbers_in_str, regexp=regexp)
        self.df[file_cols] = self.df[file_cols].applymap(split_func)

    @property
    def calc_rel_size(self) -> pd.Series:
        """
        Gives the relative size difference between output and input for each transaction.

        Returns:
            Output Series.
        """
        return self.df['out_size'] / self.df['in_size']

    @load.timing
    def resample(self, sample_rate: str = 'T') -> pd.DataFrame:
        """
        Resamples the columns of the parser's DataFrame. Uses :func:`numpy.mean` for numerical columns, and takes the
        last entry of every block for object/string columns.

        Args:
            sample_rate: Resampling rate.

        Returns:
            Output DataFrame.
        """
        resampled = self.df.resample(sample_rate).mean().interpolate('linear')
        text_df = self.df.select_dtypes(include=[np.object])
        last_str = text_df.resample(sample_rate).last()
        return pd.concat([resampled, last_str], sort=True)

    @utils.utility.deprecated
    def resample_col(self, col=None, sample_rate='T') -> pd.Series:  # TODO: integrate into resample!
        """
        Alternative method for resampling columns of the parser's DataFrame.

        Args:
            col: Selected columns.
            sample_rate: Resampling rate.

        Returns:
            Output Series.
        """
        if col is None:
            col = self.df['latency']
        response = pd.Series(col.resample(sample_rate).mean().interpolate('linear').values).rolling(5).mean()
        return response

    def quantile_resample(self, col_name: str, sample_rate: str = 'T') -> pd.DataFrame:
        """
        Resamples the columns of the parser's DataFrame and calculates the 25%, 50% and 75% quantiles. Result is
        interpolated linearly.

        Args:
            col_name: Names of columns to resample.
            sample_rate: Resampling rate.

        Returns:
            Output DataFrame.
        """
        resampler = self.df.resample(sample_rate)[col_name]
        result = pd.DataFrame()
        result.index.rename(col_name, inplace=True)
        result['Q1'] = resampler.quantile(0.25)
        result['Q2'] = resampler.median()
        result['Q3'] = resampler.quantile(0.75)
        result.interpolate('linear', inplace=True)
        return result

    @load.timing
    def periodogram_analysis(self, data: pd.Series, kind: str = 'scipy') -> plt.Artist:
        """
        Calculates the periodogram of the signal in a Series and plots it. Uses either :func:`scipy.signal.periodogram`
        or :func:`statsmodels.tsa.stattools.periodogram`.

        Args:
            data: Input Series.
            kind: Backend function. Choose between "scipy" or "statsmodels".

        Returns:
            Drawn artist.
        """
        rounding_exp = int(np.log2(len(data)))
        fig, ax = plt.subplots()
        if kind == 'scipy':
            periodogram = scipy.signal.periodogram(data[:2 ** rounding_exp])
            return ax.plot(periodogram[0][1:1000] * 60, periodogram[1][1:1000])
        elif kind == 'statsmodels':
            periodogram = statsmodels.tsa.stattools.periodogram(data)
            return ax.plot(periodogram[1:1000])

    @staticmethod
    def fourier_extrapolation(x: tp.Union[np.ndarray, pd.Series], n_predict: int, n_harm: int = 100) -> np.ndarray:
        """
        Calculates the fourier decomposition of a time series and extrapolates it past its input range.

        Args:
            x: Input Series.
            n_predict: Amount of extra time points to predict.
            n_harm: Amount of harmonics to take into account.

        Returns:
            Extrapolated series.

        .. seealso::
           https://stackoverflow.com/questions/4479463/
        """
        n = x.size
        # n_harm = 100  # number of harmonics in model
        t = np.arange(0, n)
        p = np.polyfit(t, x, 1)  # find linear trend in x
        x_notrend = x - p[0] * t  # detrended x
        x_freqdom = np.fft.fft(x_notrend)  # detrended x in frequency domain
        f = np.fft.fftfreq(n)  # frequencies
        indexes = list(range(n))
        # sort indexes by frequency, lower -> higher
        indexes.sort(key=lambda z: np.absolute(f[z]))

        t = np.arange(0, n + n_predict)
        restored_sig = np.zeros(t.size)
        for j in indexes[:1 + n_harm * 2]:
            ampli = np.absolute(x_freqdom[j]) / n  # amplitude
            phase = np.angle(x_freqdom[j])  # phase
            restored_sig += ampli * np.cos(2 * np.pi * f[j] * t + phase)
        return restored_sig + p[0] * t

    @staticmethod
    def prune_numbers_in_str(string: str, regexp: tp.re.Pattern) -> str:
        """
        Splits and rejoins a string on contained underscores, leaving out the parts containing one or more numerical
        characters.

        Args:
            string: Input string.
            regexp: Compiled regex object to match a number.

        Returns:
            Original string without segments containing numerical characters.
        """
        try:
            splits = string.split('_')
        except AttributeError:
            splits = [str(string)]
        result = []
        for part in splits:
            if not regexp.search(part):
                result.append(part)
        return '_'.join(result)

    @load.timing
    def run(self, command: str = 'explore', mode: str = 'raw') -> None:
        """
        Run an experiment on a loaded Montova dataset. Current choices are:

        - explore
        - periodogram
        - quantiles
        - seasonal
        - pyramid
        - fourier
        - fourier2


        Args:
            command: Running argument. Currently, only a single argument is accepted.
            mode: Choose either 'raw' or 'resampled'.
        """
        df = self.df
        if mode == 'resampled':
            df = self.resampled

        if command == 'explore':
            plt.figure()
            df.loc[:, 'latency'].plot(legend=True)
        elif command == 'size_comp':
            try:
                plt.figure()
                df.loc[(df['in_mime_type'] == "text/plain") & (df['out_mime_type'] == 'text/xml'), "rel_size"].plot(
                    legend=True)
            except (TypeError, ValueError) as exc:
                print("Empty!")
                # print(traceback.format_exc())
                raise
                pass
        elif command == 'flow_comparison':
            print(df.head())
            delta = pd.Timedelta("0.25 days")
            resampled_df = df.resample(delta)
            resampled_df.mean()[['in_size', 'out_size']].plot(title=f'{"/".join(self.path.parts[-2:])}: mean')
            resampled_df.count()[['in_size', 'out_size']].plot(title=f'{"/".join(self.path.parts[-2:])}: count')
            pass
        elif command == 'periodogram':
            self.periodogram_analysis(df['latency'])
        elif command == 'quantiles':
            quantiles = self.quantile_resample('latency')
            quantiles.plot()
        elif command == 'seasonal':
            from seasonal import fit_seasons
            s = df.loc['2017-10-09':'2017-10-15', 'latency']
            seasons, trend = fit_seasons(s)
            # adjusted = adjust_seasons(s, seasons=seasons)
            # residual = adjusted - trend
            plt.figure()
            # plt.plot(s, label='data')
            plt.plot(trend, label='trend')
            # plt.plot(residual, label='residual')
        elif command == 'pyramid':
            from pyramid.arima import auto_arima
            s = df.loc['2017-10-09':'2017-10-15', 'latency']
            stepwise_fit = auto_arima(s, start_p=1, start_q=1, max_p=3, max_q=3, m=12, start_P=0, seasonal=True, d=1,
                                      D=1, trace=True, error_action='ignore', suppress_warnings=True, stepwise=True)
            stepwise_fit.summary()
        elif command == 'fourier':
            response = self.resample_col(self.df['latency'])
            s = response.loc[6250:36900]
            fig0, ax0 = plt.subplots()
            response.plot(ax=ax0, legend=True, label='latency')
            fig1, ax1 = plt.subplots(2, 1)
            four = self.fourier_extrapolation(s, len(response), n_harm=1000)
            print(len(four), len(response), len(four) - len(response))
            ax1[0].plot(four)
            ax1[1].plot(four, label='predicted')
            ax1[1].plot(response, label='response')
            ax1[1].plot(response - four[:len(response)], label='difference')
            fig1.legend()
        elif command == 'fourier2':  # TODO: REFACTOR!
            data = df.loc[(df['in_mime_type'] == "text/plain") & (df['out_mime_type'] == 'text/xml'), "rel_size"]
            resampled = data.resample('T').mean().interpolate('linear').dropna()
            s = resampled.loc['2017-11-13':'2017-11-19']
            try:
                # self.resample_col(col=data).plot()
                resampled.loc['2017-11-13':'2017-11-19'].plot()
                resampled.plot(legend=True)
            except (TypeError, KeyError):
                print("Not plotting resampled function!")
                raise
            try:
                fig1, ax1 = plt.subplots(2, 1)
                four = self.fourier_extrapolation(s.values, len(resampled), n_harm=1000)
                print(len(four), len(resampled), len(four) - len(resampled))
                ax1[0].plot(four)
                ax1[1].plot(four, label='predicted')
                ax1[1].plot(resampled.values, label='rel_size')
                ax1[1].plot(resampled.values - four[:len(resampled)], label='difference')
                fig1.legend()
            except TypeError:
                print("Not plotting all extrapolation funcs!")


if __name__ == '__main__':
    files = ["normal/153_343_EDIFACT_20171005_20171205.csv",
             "normal/1261_94_LPGPInspectionImage_20171005_20171205.csv",
             "abnormal/124_1555_Productie_20171005_20171205.csv",
             "abnormal/695_26_WHSDATA_20171005_20171205.csv"]
    for i in range(4):
        file_path = pl.Path.home() / 'Datasets' / 'brain_2' / files[i]
        print(f"Processing file {i}...")
        obj = Brain2Reader(file_path)
        obj.run(command='flow_comparison', mode='raw')
        # plt.title('/'.join(obj.path.parts[-2:]))

    plt.show()

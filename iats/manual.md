# Manual

## How to run

```bash
$ python analysis.py --config=*path_to_config_location*
# for more information, use:
$ python analysis.py --help  
```

## Config file settings

### dataset

* path: Data storage location. Can be a directory or a CSV filename.
* rebuild: The program will not rebuild the processed data if a processed CSV file is present.
Set this to 1 to rebuild it anyway. 
* begin (optional): Limits the data selection from processed ElasticSearch queries 
to events that happened after this date.
* end (optional): Limits the data selection from processed ElasticSearch queries 
to events that happened before this date.

### resample

* dask: If 1, uses dask DataFrames to speed up processing and decrease memory usage.
* resample (default: S): Resampling resolution for the calculation of new features.
* resample_cats: If 1, resamples categorical data as well (experimental).
* sample_factor: Minimum data fraction that is kept when dataset is very large. 
Models with a sample_factor other than 1 might not accurately represent the real nature of the data.

### run

* run (default: residuals): Analysis method to run. Use 'residuals' (without quotes) to find anomalies.
* target: Name of the target feature.
* rng (default: 0): Seed for the RNG.
* expand: Set to 1 if you want to calculate additional features.
* dropna: If 1, drops rows with missing data.

### features

* drop: Columns to be dropped before processing.
* time: Name of the timestamp column. If not set, index is used.
* feature_columns: Columns on which extra features are generated.
* ignore_columns: Feature columns that are to be ignored by the analysis pipeline.
* aggregates: Aggregate features to be calculated.
* type_col: If set, only rows where the value in the type column are equal to `type` are used. 
* type: Value of `type_col` to be considered as relevant data.
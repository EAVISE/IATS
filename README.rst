Intelligent Analysis of Time Series
###################################

All code and documentation for this VLAIO TeTra project can be found in this repository.
Data is not included, please contact the authors for more information.

Installing
==========

Clone the repository and execute the following commands:

.. code-block:: bash

    cd iats
    pip install -e .

Dependencies will automatically be installed.

Examples
========

Coming soon.

Authors
=======

* *Kristof Van Engeland* - Initial work - `GitLab <https://gitlab.com/kristofve>`_
* *Joost Vennekens* - Project Manager - `website <https://people.cs.kuleuven.be/~joost.vennekens/DN/>`_

License
=======

This project is licensed under the MIT License - see the LICENSE_ file for details.

.. _LICENSE: ./LICENSE.adoc

iats.tenforce.data package
==========================

.. automodule:: iats.tenforce.data
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

iats.tenforce.data.dataconverter module
---------------------------------------

.. automodule:: iats.tenforce.data.dataconverter
   :members:
   :undoc-members:
   :show-inheritance:

iats.tenforce.data.question\_mapper module
------------------------------------------

.. automodule:: iats.tenforce.data.question_mapper
   :members:
   :undoc-members:
   :show-inheritance:

iats.tenforce.data.tf\_data\_analysis module
--------------------------------------------

.. automodule:: iats.tenforce.data.tf_data_analysis
   :members:
   :undoc-members:
   :show-inheritance:

iats.tenforce.data.topic\_mapper module
---------------------------------------

.. automodule:: iats.tenforce.data.topic_mapper
   :members:
   :undoc-members:
   :show-inheritance:


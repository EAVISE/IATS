iats.tenforce.models package
============================

.. automodule:: iats.tenforce.models
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

iats.tenforce.models.bayesian\_model module
-------------------------------------------

.. automodule:: iats.tenforce.models.bayesian_model
   :members:
   :undoc-members:
   :show-inheritance:


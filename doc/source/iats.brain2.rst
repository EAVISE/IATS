iats.brain2 package
===================

.. automodule:: iats.brain2
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

iats.brain2.classifier module
-----------------------------

.. automodule:: iats.brain2.classifier
   :members:
   :undoc-members:
   :show-inheritance:

iats.brain2.dataloader module
-----------------------------

.. automodule:: iats.brain2.dataloader
   :members:
   :undoc-members:
   :show-inheritance:


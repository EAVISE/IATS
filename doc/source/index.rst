.. Intelligent Analysis of Time Series documentation master file, created by
   sphinx-quickstart on Tue Nov 14 12:41:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :maxdepth: 4
   :caption: Contents:

.. include:: ../../README.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
